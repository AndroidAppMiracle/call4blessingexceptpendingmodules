package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by Kshitiz Bali on 2/24/2017.
 */

public class PostDeleteModal {

    /**
     * status : 1
     * message : Post Deleted Successfully
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
