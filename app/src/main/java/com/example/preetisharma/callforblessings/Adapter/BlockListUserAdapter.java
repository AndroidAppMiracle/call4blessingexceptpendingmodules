package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.BlockResponse;
import com.example.preetisharma.callforblessings.Server.Modal.MutualFriendModal;
import com.example.preetisharma.callforblessings.Server.Modal.UnfriendAFriendModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.settings.SettingsBlock;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 10/27/2017.
 */

public class BlockListUserAdapter extends RecyclerView.Adapter<BlockListUserAdapter.DataViewHolder>{

    public List</*MyFriendsModal.ListBean*/MutualFriendModal.List> list;
    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;
    int location = 0;

    public BlockListUserAdapter(Activity mContext, List<MutualFriendModal.List> list) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_adapter_friends_list, parent, false);
        BlockListUserAdapter.DataViewHolder dataView = new BlockListUserAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
        location = position;

        holder.txtvw_ignore_friend_request.setVisibility(View.GONE);
        holder.txtvw_friends_request_name.setText(list.get(position)./**/getProfile_details().getFirstname() + " " + list.get(position).getProfile_details().getLastname());
        Glide.with(mContext).load(list.get(position).getProfile_details().getProfile_pic()).placeholder(R.drawable.placeholder_callforblessings).into(holder.iv_friend_request_image);
        holder.ll_my_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("user id", String.valueOf(list.get(holder.getAdapterPosition()).getId()));
                Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(holder.getAdapterPosition()).getId()));
                b.putBoolean(Constants.TIMELINE_ENABLED, false);
                b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_ACCEPT);
                b.putString("friendClicked", "yes");
                b.putString(Constants.USER_IMAGE, list.get(holder.getAdapterPosition()).getProfile_details().getProfile_pic());
                b.putString(Constants.COVER_PIC, list.get(holder.getAdapterPosition()).getProfile_details().getCover_pic());
                b.putString(Constants.USER_NAME, list.get(holder.getAdapterPosition()).getProfile_details().getFirstname() + " " + list.get(holder.getAdapterPosition()).getProfile_details().getLastname());
                b.putString(Constants.PRIVACY_WALL, list.get(holder.getAdapterPosition()).getWALL());
                b.putString(Constants.PRIVACY_ABOUT_INFO, list.get(holder.getAdapterPosition()).getABOUT_INFO());
                b.putString(Constants.PRIVACY_FRIEND_REQUEST, list.get(holder.getAdapterPosition()).getFRIEND_REQUEST());
                b.putString(Constants.PRIVACY_MESSAGE, list.get(holder.getAdapterPosition()).getMESSAGE());
                i.putExtras(b);
                mContext.startActivity(i);
            }
        });

        holder.txtvw_accept_friend_request.setText("UnBlock");
        holder.txtvw_accept_friend_request.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
        holder.txtvw_accept_friend_request.setTextColor(mContext.getResources().getColor(R.color.colorWhite));
        holder.txtvw_accept_friend_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ((BaseActivity) mContext).showLoading();
                        ServerAPI.getInstance().userBlockUnBlock(APIServerResponse.BLOCK_UNBLOCK, ((BaseActivity) mContext).getUserSessionId(), list.get(position).getId(),((BaseActivity) mContext).getUserID(), new APIServerResponse() {
                            @Override
                            public void onSuccess(int tag, Response response) {
                                BlockResponse blockObject = (BlockResponse) response.body();
                                if (blockObject.getStatus().equalsIgnoreCase("1")) {
                                    Toast.makeText(mContext, "" + blockObject.getMessage(), Toast.LENGTH_SHORT).show();
                                    refresh(position);
                                }
                                ((BaseActivity) mContext).hideLoading();
                            }

                            @Override
                            public void onError(int tag, Throwable throwable) {

                            }
                        });
                    } else {
                        ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_friends_request_name)
        AppCompatTextView txtvw_friends_request_name;
        @BindView(R.id.iv_friend_request_image)
        AppCompatImageView iv_friend_request_image;
        @BindView(R.id.layout_response_buttons)
        LinearLayout layout_response_buttons;
        @BindView(R.id.ll_my_friend)
        LinearLayout ll_my_friend;
        @BindView(R.id.txtvw_ignore_friend_request)
        AppCompatTextView txtvw_ignore_friend_request;

        public AppCompatTextView txtvw_accept_friend_request;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            txtvw_ignore_friend_request.setVisibility(View.GONE);
            txtvw_accept_friend_request = (AppCompatTextView) v.findViewById(R.id.txtvw_accept_friend_request);

            txtvw_accept_friend_request.setVisibility(View.VISIBLE);

            txtvw_accept_friend_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    new AlertDialog.Builder(mContext)
                            .setTitle("Do you want to Unfriend " + list.get(getAdapterPosition()).getProfile_details().getFirstname() + "?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                                        ServerAPI.getInstance().unfriend(APIServerResponse.UNFRIEND, ((BaseActivity) mContext).getUserSessionId(), ((BaseActivity) mContext).getUserID(),String.valueOf(list.get(getAdapterPosition()).getId()), BlockListUserAdapter.DataViewHolder.this);
                                    } else {
                                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_LONG);
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .setIcon(R.mipmap.ic_app_icon)
                            .show();


                }
            });

        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                if (response.isSuccessful()) {
                    UnfriendAFriendModal unfriendAFriendModal;

                    switch (tag) {
                        case APIServerResponse.UNFRIEND:
                            unfriendAFriendModal = (UnfriendAFriendModal) response.body();
                            if (unfriendAFriendModal.getStatus().equals("1")) {
                                refresh(getAdapterPosition());
                            }
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onError(int tag, Throwable throwable) {

        }
    }

    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        list.remove(position);
        notifyItemRemoved(position);
    }

}
