package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/11/2017.
 */

public class CreatePageModal {
    private String message;

    private Detail detail;

    private String status;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Detail getDetail ()
    {
        return detail;
    }

    public void setDetail (Detail detail)
    {
        this.detail = detail;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", detail = "+detail+", status = "+status+"]";
    }

    public static class Detail
    {
        private String id;

        private String description;

        private String name;

        private String banner_img;

        private String profile_img;

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getDescription ()
        {
            return description;
        }

        public void setDescription (String description)
        {
            this.description = description;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public String getBanner_img ()
        {
            return banner_img;
        }

        public void setBanner_img (String banner_img)
        {
            this.banner_img = banner_img;
        }

        public String getProfile_img ()
        {
            return profile_img;
        }

        public void setProfile_img (String profile_img)
        {
            this.profile_img = profile_img;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [id = "+id+", description = "+description+", name = "+name+", banner_img = "+banner_img+", profile_img = "+profile_img+"]";
        }
    }

}
