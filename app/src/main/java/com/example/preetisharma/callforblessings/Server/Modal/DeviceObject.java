package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/27/2017.
 */

public class DeviceObject {
    private String id;

    private String device_name;

    private String created_at;

    private String user_id;

    private String device_id;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getDevice_name ()
    {
        return device_name;
    }

    public void setDevice_name (String device_name)
    {
        this.device_name = device_name;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getDevice_id ()
    {
        return device_id;
    }

    public void setDevice_id (String device_id)
    {
        this.device_id = device_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", device_name = "+device_name+", created_at = "+created_at+", user_id = "+user_id+", device_id = "+device_id+"]";
    }
}
