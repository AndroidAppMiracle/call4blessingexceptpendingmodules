package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by satoti.garg on 10/28/2017.
 */

public class PlayListSongsListObject {
    private String amount;

    private String id;

    private List<PlaylistSongDetail> files;

    private String is_featured;

    private String is_downloaded;

    private String description;

    private String name;

    private String created_at;

    private String album_cover_image;

    private String language;

    private String payment_type;

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public List<PlaylistSongDetail> getFiles ()
    {
        return files;
    }

    public void setFiles (List<PlaylistSongDetail> files)
    {
        this.files = files;
    }

    public String getIs_featured ()
    {
        return is_featured;
    }

    public void setIs_featured (String is_featured)
    {
        this.is_featured = is_featured;
    }

    public String getIs_downloaded ()
    {
        return is_downloaded;
    }

    public void setIs_downloaded (String is_downloaded)
    {
        this.is_downloaded = is_downloaded;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getAlbum_cover_image ()
    {
        return album_cover_image;
    }

    public void setAlbum_cover_image (String album_cover_image)
    {
        this.album_cover_image = album_cover_image;
    }

    public String getLanguage ()
    {
        return language;
    }

    public void setLanguage (String language)
    {
        this.language = language;
    }

    public String getPayment_type ()
    {
        return payment_type;
    }

    public void setPayment_type (String payment_type)
    {
        this.payment_type = payment_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [amount = "+amount+", id = "+id+", files = "+files+", is_featured = "+is_featured+", is_downloaded = "+is_downloaded+", description = "+description+", name = "+name+", created_at = "+created_at+", album_cover_image = "+album_cover_image+", language = "+language+", payment_type = "+payment_type+"]";
    }
}
