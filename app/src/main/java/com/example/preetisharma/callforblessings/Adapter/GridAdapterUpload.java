package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusRemovedBookimagesModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusSendFriendRequestModal;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Kshitiz Bali on 5/2/2017.
 */

public class GridAdapterUpload extends RecyclerView.Adapter<GridAdapterUpload.ViewHolder> {
    private ArrayList<String> mItems;
    private Context mContext;
    private String TAG = "GridAdapter";
    //private ProgressBar mProgressBar;

    public GridAdapterUpload(ArrayList<String> items, Context mContext) {
        this.mItems = items;
        this.mContext = mContext;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_multiple_images_view, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        //viewHolder.mProgbar.setVisibility(View.VISIBLE);
        //viewHolder.mProgbar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(mContext,
        //       R.color.colorButton), android.graphics.PorterDuff.Mode.MULTIPLY);

        /*Picasso.with(mContext)
                .load(new File(mItems.get(position)))
                .into(viewHolder.imgThumbnail, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        viewHolder.mProgbar.setVisibility(View.INVISIBLE);
                    }
                    @Override
                    public void onError() {
                        viewHolder.mProgbar.setVisibility(View.INVISIBLE);
                    }
                });
        viewHolder.imgThumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);*/

        Glide.with(mContext).load(new File(mItems.get(position))).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).override(96, 96).fitCenter().crossFade().into(viewHolder.img_thumbnail);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView img_thumbnail;
        public AppCompatImageView iv_close_icon;
        public RelativeLayout layout_root;

        //public ProgressBar mProgbar;
        public ViewHolder(View itemView) {
            super(itemView);
            img_thumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
            iv_close_icon = (AppCompatImageView) itemView.findViewById(R.id.iv_close_icon);
            layout_root = (RelativeLayout) itemView.findViewById(R.id.layout_root);
            //mProgbar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            // itemView.setOnClickListener(this);

            iv_close_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    refresh(getAdapterPosition(), layout_root);
                }
            });
        }

    }

    public void refresh(int position, RelativeLayout relativeLayout) {
        Log.e("Refreshed Position", "Pos" + position);
        //userInfoBeanList.remove(position);
        //EventBus.getDefault().post(new EventBusRemovedBookimagesModal(mItems.size()));
        mItems.remove(position);
        //notifyItemRemoved(userInfoBeanList.size() - 1);
        notifyItemRemoved(position);
        EventBus.getDefault().post(new EventBusRemovedBookimagesModal(mItems.size()));
        if (mItems.isEmpty()) {
            relativeLayout.setVisibility(View.GONE);
        } else {
            relativeLayout.setVisibility(View.VISIBLE);
        }

    }


}
