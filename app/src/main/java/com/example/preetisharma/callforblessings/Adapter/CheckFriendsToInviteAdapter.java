package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/9/2017.
 */

public class CheckFriendsToInviteAdapter extends RecyclerView.Adapter<CheckFriendsToInviteAdapter.DataViewHolder> {

    private Context mContext;
    private List<MyFriendsModal.ListBean> myFriendsList = new ArrayList<>();


    public CheckFriendsToInviteAdapter(Context mContext, List<MyFriendsModal.ListBean> list) {
        this.myFriendsList = list;
        this.mContext = mContext;
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_checkbox_layout, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        try {
            Glide.with(mContext).load(myFriendsList.get(position).getUser_info().getProfile_details().getProfile_pic()).placeholder(R.drawable.placeholder_callforblessings).centerCrop().into(holder.iv_my_friend_profile_pic);
            holder.tv_my_friend_name.setText(myFriendsList.get(position).getUser_info().getUsername());

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (myFriendsList.size() != 0) {
            return myFriendsList.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.checkbox_invite)
        CheckBox checkbox_invite;

        @BindView(R.id.tv_my_friend_name)
        AppCompatTextView tv_my_friend_name;

        /*@BindView(R.id.txtvw_my_event_edit)
        AppCompatTextView txtvw_my_event_edit;*/

        @BindView(R.id.iv_my_friend_profile_pic)
        ImageView iv_my_friend_profile_pic;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/
        }


    }
}
