package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 7/10/2017.
 */

public class EventBusBookCreated {

    private final String isBookCreated;

    public EventBusBookCreated(String isBookCreated) {
        this.isBookCreated = isBookCreated;
    }

    public String getIsNewBookCreated() {
        return isBookCreated;
    }
}
