package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/25/2017.
 */

public class FollowResponse {

    private String message;

    private String status;

    private String is_followed;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getIs_followed ()
    {
        return is_followed;
    }

    public void setIs_followed (String is_followed)
    {
        this.is_followed = is_followed;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", status = "+status+", is_followed = "+is_followed+"]";
    }
}
