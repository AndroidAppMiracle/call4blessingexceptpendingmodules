package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by preeti.sharma on 4/5/2017.
 */

public class BookPaymentModal {


    /**
     * status : 1
     * hashes : {"payment_hash":"cd565d36706a3182414914fcfe8652e04443fea6ab45fda6fc8f3e755d554c103c5c4fea1f0487e46885f86759666f6e057235d9773363036de4a3853ddfdd04","get_merchant_ibibo_codes_hash":"3a99601bf820f0cf3cb268fe9bb6961dc896a6a46f396ec02e87ad01f113a7db5632f4703d1d424a712ae92125dfe4b9e036d2a33f415ef5a41a42ddc5f38c5e","vas_for_mobile_sdk_hash":"d336ec4be19f4f9eb2b3dd75b0a40534515afb53aad26d5b192995c37cd97f51e8d716422ae66bc799b2daae6ac5128fe65928b172eaa22830514cbb87b755e7","payment_related_details_for_mobile_sdk_hash":"699ee1b3214ae506b657f155b2d143ef1ffc929cb04ec63e7b579c1255de7a9efa07b51b2303de25a4d0c050ea648cf72a09e3512fb7e8cb44b040aca08c38da","verify_payment_hash":"b88c4ba1a43587d415dee23b73a956a0e1beca3c8dfc9abd9c42c2d7ba56c7a076bf6b68a9106c9fbe7f76f786d46574418ab73c22892830a8a02f9656e687b4","delete_user_card_hash":"e7898b1becbbee8d679625c67415e740aaefcb8b9c65eaf88bfb690bd57bfad1ad27f8d7ad42893df448a9d6e060f535538c92b3e5425e32ed9cc5f39cb06a07","get_user_cards_hash":"acaef613a93c60170a726127701be3b677e7f2fea8cf5201333d0c65ef5544f57ba6ce788f830c9f31004ad578cf3acdd6e88ffe4c7ddd6b03fd60d63c39b657","edit_user_card_hash":"ff15398d349dee407ca3c540472b135bd5cd90bd542c994a7b7f0ba26b636cb88df809db1ff5a45cb062d8e67beac566267fa82c82a8d60383f09b6b77f844cc","save_user_card_hash":"91e0ada16bd549b7e3c1edcb05d78a6458fe8cd56d690a8aa425a4ff2a212ca720fe3a165bc05ac143563c7560dac072f150bfc113c5d940f1599ee9250fd5f1"}
     * txnid : TNP1521491345256
     */

    private String status;
    private HashesBean hashes;
    private String txnid;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    private String error;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public HashesBean getHashes() {
        return hashes;
    }

    public void setHashes(HashesBean hashes) {
        this.hashes = hashes;
    }

    public String getTxnid() {
        return txnid;
    }

    public void setTxnid(String txnid) {
        this.txnid = txnid;
    }

    public static class HashesBean {
        /**
         * payment_hash : cd565d36706a3182414914fcfe8652e04443fea6ab45fda6fc8f3e755d554c103c5c4fea1f0487e46885f86759666f6e057235d9773363036de4a3853ddfdd04
         * get_merchant_ibibo_codes_hash : 3a99601bf820f0cf3cb268fe9bb6961dc896a6a46f396ec02e87ad01f113a7db5632f4703d1d424a712ae92125dfe4b9e036d2a33f415ef5a41a42ddc5f38c5e
         * vas_for_mobile_sdk_hash : d336ec4be19f4f9eb2b3dd75b0a40534515afb53aad26d5b192995c37cd97f51e8d716422ae66bc799b2daae6ac5128fe65928b172eaa22830514cbb87b755e7
         * payment_related_details_for_mobile_sdk_hash : 699ee1b3214ae506b657f155b2d143ef1ffc929cb04ec63e7b579c1255de7a9efa07b51b2303de25a4d0c050ea648cf72a09e3512fb7e8cb44b040aca08c38da
         * verify_payment_hash : b88c4ba1a43587d415dee23b73a956a0e1beca3c8dfc9abd9c42c2d7ba56c7a076bf6b68a9106c9fbe7f76f786d46574418ab73c22892830a8a02f9656e687b4
         * delete_user_card_hash : e7898b1becbbee8d679625c67415e740aaefcb8b9c65eaf88bfb690bd57bfad1ad27f8d7ad42893df448a9d6e060f535538c92b3e5425e32ed9cc5f39cb06a07
         * get_user_cards_hash : acaef613a93c60170a726127701be3b677e7f2fea8cf5201333d0c65ef5544f57ba6ce788f830c9f31004ad578cf3acdd6e88ffe4c7ddd6b03fd60d63c39b657
         * edit_user_card_hash : ff15398d349dee407ca3c540472b135bd5cd90bd542c994a7b7f0ba26b636cb88df809db1ff5a45cb062d8e67beac566267fa82c82a8d60383f09b6b77f844cc
         * save_user_card_hash : 91e0ada16bd549b7e3c1edcb05d78a6458fe8cd56d690a8aa425a4ff2a212ca720fe3a165bc05ac143563c7560dac072f150bfc113c5d940f1599ee9250fd5f1
         */

        private String payment_hash;
        private String get_merchant_ibibo_codes_hash;
        private String vas_for_mobile_sdk_hash;
        private String payment_related_details_for_mobile_sdk_hash;
        private String verify_payment_hash;
        private String delete_user_card_hash;
        private String get_user_cards_hash;
        private String edit_user_card_hash;
        private String save_user_card_hash;

        public String getPayment_hash() {
            return payment_hash;
        }

        public void setPayment_hash(String payment_hash) {
            this.payment_hash = payment_hash;
        }

        public String getGet_merchant_ibibo_codes_hash() {
            return get_merchant_ibibo_codes_hash;
        }

        public void setGet_merchant_ibibo_codes_hash(String get_merchant_ibibo_codes_hash) {
            this.get_merchant_ibibo_codes_hash = get_merchant_ibibo_codes_hash;
        }

        public String getVas_for_mobile_sdk_hash() {
            return vas_for_mobile_sdk_hash;
        }

        public void setVas_for_mobile_sdk_hash(String vas_for_mobile_sdk_hash) {
            this.vas_for_mobile_sdk_hash = vas_for_mobile_sdk_hash;
        }

        public String getPayment_related_details_for_mobile_sdk_hash() {
            return payment_related_details_for_mobile_sdk_hash;
        }

        public void setPayment_related_details_for_mobile_sdk_hash(String payment_related_details_for_mobile_sdk_hash) {
            this.payment_related_details_for_mobile_sdk_hash = payment_related_details_for_mobile_sdk_hash;
        }

        public String getVerify_payment_hash() {
            return verify_payment_hash;
        }

        public void setVerify_payment_hash(String verify_payment_hash) {
            this.verify_payment_hash = verify_payment_hash;
        }

        public String getDelete_user_card_hash() {
            return delete_user_card_hash;
        }

        public void setDelete_user_card_hash(String delete_user_card_hash) {
            this.delete_user_card_hash = delete_user_card_hash;
        }

        public String getGet_user_cards_hash() {
            return get_user_cards_hash;
        }

        public void setGet_user_cards_hash(String get_user_cards_hash) {
            this.get_user_cards_hash = get_user_cards_hash;
        }

        public String getEdit_user_card_hash() {
            return edit_user_card_hash;
        }

        public void setEdit_user_card_hash(String edit_user_card_hash) {
            this.edit_user_card_hash = edit_user_card_hash;
        }

        public String getSave_user_card_hash() {
            return save_user_card_hash;
        }

        public void setSave_user_card_hash(String save_user_card_hash) {
            this.save_user_card_hash = save_user_card_hash;
        }
    }
}
