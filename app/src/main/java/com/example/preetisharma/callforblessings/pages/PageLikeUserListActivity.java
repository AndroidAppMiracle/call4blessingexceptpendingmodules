package com.example.preetisharma.callforblessings.pages;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.LikeUserAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.HomeLikeUserListActivity;
import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class PageLikeUserListActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipe_refresh_layout;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @BindView(R.id.all_liked_user_rv)
    RecyclerView all_liked_user_rv;

    LikeUserAdapter likeUserAdapter;
    String postId = "", pageId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_like_user_list);
        ButterKnife.bind(this);
        txtvw_header_title.setText("All Likes");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PageLikeUserListActivity.this.finish();
            }
        });

        img_view_change_password.setVisibility(View.GONE);
        if (getIntent().hasExtra("postId")) {
            postId = getIntent().getStringExtra("postId");
        }

        //   getAllLikeUser();
        if (postId.equalsIgnoreCase("")) {

        } else {
            getAllLikeUserList();
        }
    }

    public void getAllLikeUserList() {
        showLoading();
        if (isConnectedToInternet()) {
            //http://call4blessing.com/api/page/post-like-user?id=214&post_id=1
            ServerAPI.getInstance().getLikeUserListOfPage(APIServerResponse.LIKES_USERLIST, getUserSessionId(), getUserID(), postId, "POST",PageLikeUserListActivity.this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.LIKES_USERLIST:
                        JsonObject jsonObject = (JsonObject) response.body();
                        Log.d("Like array response", response.body() + "");
                        if (jsonObject.get("status").getAsString().equalsIgnoreCase("1")) {
                            likeUserAdapter = new LikeUserAdapter(PageLikeUserListActivity.this, jsonObject.get("users").getAsJsonArray());
                            all_liked_user_rv.setLayoutManager(new WrapContentLinearLayoutManager(getApplicationContext(), this, LinearLayoutManager.HORIZONTAL, false));
                            all_liked_user_rv.setAdapter(likeUserAdapter);
                            likeUserAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(getApplicationContext(), "" + jsonObject.get("message"), Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }

    public class WrapContentLinearLayoutManager extends LinearLayoutManager {

        public WrapContentLinearLayoutManager(Context applicationContext, Context context, int horizontal, boolean b) {
            super(context);
        }

        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {
                Log.e("probe", "meet a IOOBE in RecyclerView");
            }
        }
    }
}
