package com.example.preetisharma.callforblessings.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.example.preetisharma.callforblessings.Fragment.BooksFragment;
import com.example.preetisharma.callforblessings.Fragment.EventInvitesFragment;
import com.example.preetisharma.callforblessings.Fragment.EventsCreatedFragment;
import com.example.preetisharma.callforblessings.Fragment.FriendsRequestFragment;
import com.example.preetisharma.callforblessings.Fragment.MyBooksFragment;
import com.example.preetisharma.callforblessings.Fragment.MyFriendsFragment;

/**
 * Created by Kshitiz Bali on 5/1/2017.
 */

public class BooksTabsPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public BooksTabsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;

    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            /*case 0:
                fragment = new EventInvitesFragment();
                return fragment;
            case 1:
                fragment = new EventsCreatedFragment();
                return fragment;
            default:
                fragment = new EventInvitesFragment();
                return fragment;*/


            case 0:
                fragment = new MyBooksFragment();
                return fragment;
            case 1:
                fragment = new BooksFragment();
                return fragment;
            default:
                fragment = new MyBooksFragment();
                return fragment;

        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    public Fragment getFragment(ViewPager container, int position, FragmentManager fm) {
        String name = makeFragmentName(container.getId(), position);
        return fm.findFragmentByTag(name);
    }

    private String makeFragmentName(int viewId, int index) {
        return "android:switcher:" + viewId + ":" + index;
    }
}