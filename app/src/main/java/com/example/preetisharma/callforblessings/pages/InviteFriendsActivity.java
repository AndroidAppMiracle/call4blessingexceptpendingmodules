package com.example.preetisharma.callforblessings.pages;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.InviteFriendAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.InivedUsers;
import com.example.preetisharma.callforblessings.Server.Modal.InvitedUsersId;
import com.example.preetisharma.callforblessings.Server.Modal.MutualFriendDetailUpper;
import com.example.preetisharma.callforblessings.Server.Modal.MutualFriendModal;
import com.example.preetisharma.callforblessings.Server.Modal.MutualFriendResponce;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.JoyMusicActivity;
import com.example.preetisharma.callforblessings.demo.JoyMusicMyDownloads;
import com.example.preetisharma.callforblessings.demo.JoyMusicSearch;
import com.example.preetisharma.callforblessings.joymusicplayer.playlist.AllPlaylistActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class InviteFriendsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, APIServerResponse {

    @BindView(R.id.rv_mutual_friends)
    RecyclerView rv_mutual_friends;
    InivedUsers inivedUsers;
    private List<MutualFriendDetailUpper> mList;

    List<InvitedUsersId> list;
    List<String> idInvitedUsers = new ArrayList<>();
    InviteFriendAdapter _adapter;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @BindView(R.id.txtvw_chat)
    AppCompatTextView txtvw_chat;
    String pageId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);
        ButterKnife.bind(this);
        img_view_change_password.setVisibility(View.GONE);
        txtvw_chat.setVisibility(View.VISIBLE);
        txtvw_chat.setText("Done");
        txtvw_header_title.setText("Friends");

        txtvw_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_adapter != null) {
                    String st = _adapter.listArr.toString().replace("[", "");
                    String st2 = st.replace("]", "");
                    inviteFriends(st2);
                }
            }
        });
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InviteFriendsActivity.this.finish();
            }
        });
        swipe_refresh_layout.setOnRefreshListener(this);
        if (getIntent().hasExtra("pageId")) {
            pageId = getIntent().getStringExtra("pageId");
        }
        getAllInvitedUsers();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        InviteFriendsActivity.this.finish();
    }

    public void getAllFriends() {
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().mutualFriendList(APIServerResponse.MYFRIENDLIST, getUserSessionId(), getUserID(),getUserID(), InviteFriendsActivity.this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    public void getAllInvitedUsers() {
        showLoading();
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().getAllInvitedUsers(APIServerResponse.INVITED_USERS, getUserSessionId(), pageId, InviteFriendsActivity.this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    public void inviteFriends(String friendsIds) {
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().inviteUserForPage(APIServerResponse.INVITE_FRIENDS, getUserSessionId(), friendsIds, pageId, InviteFriendsActivity.this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onRefresh() {

        getAllInvitedUsers();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        mList = new ArrayList<MutualFriendDetailUpper>();
        MutualFriendResponce userListingModal;
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.MYFRIENDLIST:
                        userListingModal = (MutualFriendResponce) response.body();
                        if (userListingModal.getStatus().equals("1")) {
                            Log.e("List data", "List data" + userListingModal.getList().size());
                            mList = userListingModal.getList();
                        } else {
                        }
                        _adapter = new InviteFriendAdapter(InviteFriendsActivity.this, mList, idInvitedUsers);
                        rv_mutual_friends.setAdapter(_adapter);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InviteFriendsActivity.this);
                        rv_mutual_friends.setLayoutManager(mLayoutManager);
                        if (swipe_refresh_layout.isRefreshing()) {
                            swipe_refresh_layout.setRefreshing(false);
                        }
                        hideLoading();
                        break;
                    case APIServerResponse.INVITED_USERS:
                        inivedUsers = (InivedUsers) response.body();
                        if (inivedUsers.getStatus().equals("1")) {
                            list = inivedUsers.getList();
                            for (InvitedUsersId invitedUsersId : list
                                    ) {
                                idInvitedUsers.add(invitedUsersId.getUser_id());
                            }
                            getAllFriends();
                        } else {
                            getAllFriends();
                        }
                        break;
                    case APIServerResponse.INVITE_FRIENDS:
                        GroupDeleteModal groupDeleteModal = (GroupDeleteModal) response.body();
                        if (groupDeleteModal.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(InviteFriendsActivity.this, "inviteed successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(InviteFriendsActivity.this, SinglePageActivity.class);
                            intent.putExtra("pageID", pageId);
                            startActivity(intent);
                            if (swipe_refresh_layout.isRefreshing()) {
                                swipe_refresh_layout.setRefreshing(false);
                            }
                            hideLoading();
                            finish();
                        } else {
                            Toast.makeText(InviteFriendsActivity.this, "Unsuccessfull", Toast.LENGTH_SHORT).show();
                            hideLoading();
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }
}
