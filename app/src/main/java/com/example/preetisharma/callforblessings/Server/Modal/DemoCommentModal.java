package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by preeti.sharma on 2/23/2017.
 */

public class DemoCommentModal {


    private final int comment_count;

    public DemoCommentModal(int count) {
        this.comment_count = count;
    }

    public int getMessage() {
        return comment_count;
    }

    /*private final CommentsListModal message;

    public DemoEventBusModel(CommentsListModal commentsListModal) {
        this.message = commentsListModal;
    }

    public CommentsListModal getMessage() {
        return message;
    }*/

}
