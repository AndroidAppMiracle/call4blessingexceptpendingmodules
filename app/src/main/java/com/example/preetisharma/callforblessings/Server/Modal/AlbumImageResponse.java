package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/24/2017.
 */

public class AlbumImageResponse {
    private String id;

    private String created_at;

    private String image;

    private String image_content;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getImage ()
    {
        return image;
    }

    public void setImage (String image)
    {
        this.image = image;
    }

    public String getImage_content ()
    {
        return image_content;
    }

    public void setImage_content (String image_content)
    {
        this.image_content = image_content;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", created_at = "+created_at+", image = "+image+", image_content = "+image_content+"]";
    }
}
