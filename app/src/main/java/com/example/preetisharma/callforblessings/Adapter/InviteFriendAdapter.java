package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.InvitedUsersId;
import com.example.preetisharma.callforblessings.Server.Modal.MutualFriendDetailUpper;
import com.example.preetisharma.callforblessings.Server.Modal.MutualFriendModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.pages.InviteFriendsActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 10/12/2017.
 */

public class InviteFriendAdapter extends RecyclerView.Adapter<InviteFriendAdapter.DataViewHolder> implements APIServerResponse {
    public List</*MyFriendsModal.ListBean*/MutualFriendDetailUpper> list;
    Activity mContext;
    RadioButton rd_btn;
    public static String inviteFriendids = "";
    List<String> invitedUsersIds;
    public static List<String> listArr;

    public InviteFriendAdapter(Activity mContext, List</*MyFriendsModal.ListBean*/MutualFriendDetailUpper> list, List<String> invitedUsersIds) {
        this.list = list;
        listArr = new ArrayList<>();
        this.mContext = mContext;
        this.invitedUsersIds = invitedUsersIds;
    }

    public void addAllItems(List</*MyFriendsModal.ListBean*/MutualFriendDetailUpper> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.invite_friend_item, parent, false);
        InviteFriendAdapter.DataViewHolder dataView = new InviteFriendAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
        final String friendId = list.get(position).getId();
        holder.txtvw_friends_request_name.setText(list.get(position)./**/getProfile_details().getFirstname() + " " + list.get(position).getProfile_details().getLastname());
        Glide.with(mContext).load(list.get(position).getProfile_details().getProfile_pic()).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                holder.mProgressbar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                // image ready, hide progress now
                holder.mProgressbar.setVisibility(View.GONE);
                return false;   // return false if you want Glide to handle everything else.
            }
        }).placeholder(R.drawable.placeholder_callforblessings).into(holder.iv_friend_request_image);

        holder.invite_friend_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.invite_friend_checkbox.isChecked()) {
                    if (listArr.size() == 0) {
                        listArr.add(friendId);
                    } else {
                        if (!listArr.contains(friendId)) {
                            listArr.add(friendId);
                        }
                    }
                } else {
                    if (listArr.contains(friendId)) {
                        listArr.remove(friendId);
                    }
                }
              /*  if (((BaseActivity) mContext).isConnectedToInternet()) {
                    ServerAPI.getInstance().unfriend(APIServerResponse.UNFRIEND, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), MutualFriendsAdapter.DataViewHolder.this);
                } else {
                    ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_LONG);
                }*/
            }
        });

        if (invitedUsersIds.contains(list.get(position).getId())) {
            holder.invite_friend_checkbox.setChecked(true);
            holder.invite_friend_checkbox.setVisibility(View.GONE);
            holder.already_invited_txt.setVisibility(View.VISIBLE);
        }else
        {
            holder.already_invited_txt.setVisibility(View.GONE);
            holder.invite_friend_checkbox.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        list.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onSuccess(int tag, Response response) {

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtvw_friends_request_name)
        AppCompatTextView txtvw_friends_request_name;
        @BindView(R.id.iv_friend_request_image)
        AppCompatImageView iv_friend_request_image;

        @BindView(R.id.ll_my_friend)
        LinearLayout ll_my_friend;
        @BindView(R.id.invite_friend_txt)
        AppCompatTextView invite_friend_txt;

        @BindView(R.id.invite_friend_checkbox)
        CheckBox invite_friend_checkbox;

        @BindView(R.id.movie_progress)
        ProgressBar mProgressbar;
        @BindView(R.id.already_invited_txt)
        TextView already_invited_txt;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            invite_friend_txt.setText("invited");


        }


    }
}
