package com.example.preetisharma.callforblessings.demo;

import android.Manifest;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.CustomBookLanguagesAdapter;
import com.example.preetisharma.callforblessings.Adapter.GridAdapterUpload;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.BooksLanguagesModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusBookCreated;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusRemovedBookimagesModal;
import com.example.preetisharma.callforblessings.Server.Modal.UPloadBookModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.FileUtils;
import com.example.preetisharma.callforblessings.Utils.Utilities;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/21/2017.
 */

public class AddBookActivity extends BaseActivity implements APIServerResponse, EasyPermissions.PermissionCallbacks, ImagePickerCallback {
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.edt_txt_book_title)
    AppCompatEditText edt_txt_book_title;
    @BindView(R.id.edt_txt_book_description)
    AppCompatEditText edt_txt_book_description;
    @BindView(R.id.edt_txt_writer_name)
    AppCompatEditText edt_txt_writer_name;
    @BindView(R.id.edt_txt_any_other_book)
    AppCompatEditText edt_txt_any_other_book;
    @BindView(R.id.imgvw_upload_book_cover)
    AppCompatImageView imgvw_upload_book_cover;
    @BindView(R.id.txtvw_image_name)
    AppCompatTextView txtvw_image_name;
    @BindView(R.id.txtvw_upload_book)
    AppCompatTextView txtvw_upload_book;
    @BindView(R.id.edt_txt_book_language)
    AppCompatEditText edt_txt_book_language;
    @BindView(R.id.edt_txt_book_year)
    AppCompatEditText edt_txt_book_year;
    /*    @BindView(R.id.txtvw_pdf_name)
        AppCompatTextView txtvw_pdf_name;
        @BindView(R.id.imgvw_upload_book_pdf)
        AppCompatImageView imgvw_upload_book_pdf;*/

    private ImagePicker imagePicker;

    Spinner spinner, spinnerLanguage;

    String mCurrentPhotoPath = null;


    //////////

    RecyclerView recyclerView_myc;
    static int PICK_IMAGE_MULTIPLE = 1;
    static final int REQUEST_IMAGE_CAPTURE = 2;
    static final int RC_CAMERA_PERM_AND_STORAGE = 3;
    static final int RC_GALLERY_PERM_AND_STORAGE = 4;
    static final int REQUEST_BOOK_COVER_IMAGE_CAPTURE = 5;
    static final int REQUEST_BOOK_COVER_IMAGE_SELECTION = 6;
    String[] perms = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    String[] galleryPerms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    ArrayList<String> filePaths = new ArrayList<>();
    GridAdapterUpload mAdapter;
    private static final String JPEG_FILE_PREFIX = "IMG_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    //////////

    private CameraImagePicker cameraPicker;
    private String pickerPath = "";
    //private Uri bookCoverUri;
    private static final int PLACE_PICKER_REQUEST = 1;
    //String[] perms = {Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET};
    private int RC_LOCATION_INTERNET = 102;
    private static final int RC_GALLERY_PERM = 545;
    private static final String TAG = "CallForBlessings";
    private static final int RC_CAMERA_PERM = 342;
    String selectedYearOfPublication, selectedLanguageOfBook;
    List<BooksLanguagesModal.ListBean> list;
    @BindView(R.id.rd_grp_book_payment)
    RadioGroup rd_grp_book_payment;
    @BindView(R.id.rdbtn_free)
    RadioButton rdbtn_free;
    @BindView(R.id.rdbtn_paid)
    RadioButton rdbtn_paid;
    String bookPaymentType = "";
    @BindView(R.id.edt_txt_amount)
    AppCompatEditText edt_txt_amount;
    AppCompatImageView imgvw_upload_book_cover_image;
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

    /*Uri picUri;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_book_activity);
        updateStatusBar();
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1); // 1 can be any number, doesn't matter
                return;
            }
        }
        edt_txt_book_description.setMaxHeight(edt_txt_book_description.getHeight());
        txtvw_header_title.setText(getString(R.string.upload_books));
        img_view_change_password.setVisibility(View.GONE);
        img_view_change_password.setClickable(false);
        txtvw_upload_book = (AppCompatTextView) findViewById(R.id.txtvw_upload_book);
        spinner = (Spinner) findViewById(R.id.spinner);
        spinnerLanguage = (Spinner) findViewById(R.id.spinnerLanguage);
        recyclerView_myc = (RecyclerView) findViewById(R.id.recyclerView_myc);
        imgvw_upload_book_cover_image = (AppCompatImageView) findViewById(R.id.imgvw_upload_book_cover_image);

        if (isConnectedToInternet()) {
            ServerAPI.getInstance().getBooksLanguage(APIServerResponse.BOOK_LANGUAGES, getUserSessionId(), AddBookActivity.this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
        rd_grp_book_payment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {


                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton) radioGroup.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked) {
                    // Changes the textview's text to "Checked: example radiobutton text"
                    bookPaymentType = checkedRadioButton.getText().toString();
                    Toast.makeText(AddBookActivity.this, "Book Type : " + bookPaymentType, Toast.LENGTH_SHORT).show();
                    if (bookPaymentType.equalsIgnoreCase("Paid")) {
                        edt_txt_amount.setVisibility(View.VISIBLE);
                    } else {
                        edt_txt_amount.setVisibility(View.GONE);
                    }
                }
            }

        });

        List<String> yearList = new ArrayList<String>();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1950; i <= year; i++) {
            yearList.add(String.valueOf(i));
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, yearList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedYearOfPublication = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedLanguageOfBook = list.get(position).getLanguage();
                //showToast(selectedLanguageOfBook, Toast.LENGTH_SHORT);

                //showToast(, Toast.LENGTH_SHORT);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        txtvw_upload_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String bookPrice = "";
                if (validation()) {
                    if (isConnectedToInternet()) {
                        if (mCurrentPhotoPath == null || mCurrentPhotoPath.equals("")) {
                            showToast("Please select a book cover image.", Toast.LENGTH_LONG);
                            imgvw_upload_book_cover_image.requestFocus();
                            imgvw_upload_book_cover_image.requestFocusFromTouch();
                        } else if (filePaths.isEmpty()) {
                            showToast("Please select atleast one additional book image.", Toast.LENGTH_LONG);
                        } else {
                            showLoading();
                            if (bookPaymentType.equalsIgnoreCase("Paid")) {
                                bookPrice = edt_txt_amount.getText().toString();
                            } else {
                                bookPrice = "0";
                            }
                            //ServerAPI.getInstance().addBookwithcover(APIServerResponse.ADD_BOOK, getUserSessionId(), edt_txt_book_title.getText().toString().trim(), bookPaymentType, edt_txt_writer_name.getText().toString().trim(), bookPrice, pickerPath, edt_txt_book_description.getText().toString().trim(), selectedLanguageOfBook.trim(), selectedYearOfPublication.trim(), edt_txt_amount.getText().toString(), edt_txt_any_other_book.getText().toString().trim(), AddBookActivity.this);

                            for (int i = filePaths.size(); i < 6; i++) {
                                filePaths.add(i, "");
                            }
                            ServerAPI.getInstance().addBookwithcover(APIServerResponse.ADD_BOOK, getUserSessionId(), edt_txt_book_title.getText().toString().trim(), bookPaymentType, edt_txt_writer_name.getText().toString().trim(), mCurrentPhotoPath, edt_txt_book_description.getText().toString().trim(), selectedLanguageOfBook.trim(), selectedYearOfPublication.trim(), bookPrice, edt_txt_any_other_book.getText().toString().trim(), filePaths.get(0), filePaths.get(1), filePaths.get(2), filePaths.get(3), filePaths.get(4), filePaths.get(5), AddBookActivity.this);

                        }
                    } else {
                        showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            }
        });

      /*  edt_txt_book_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yearSelectCalendar();
            }
        });*/

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                AddBookActivity.this.finish();
            }
        });

        imgvw_upload_book_cover_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialogSelectBookCover();
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }


    }

    /*@OnClick(R.id.edt_txt_book_year)
    public void yearOfBook() {
        yearSelectCalendar();
    }*/


    /*@OnClick(R.id.txtvw_upload_book)
    public void uploadBook() {
        try {
            if (validation()) {
                if (isConnectedToInternet()) {
                    if (pickerPath.equals("")) {
                        ServerAPI.getInstance().addBookwithoutcover(APIServerResponse.ADD_BOOK, edt_txt_book_title.getText().toString(), "FREE", edt_txt_book_description.getText().toString(), edt_txt_writer_name.getText().toString(), edt_txt_any_other_book.getText().toString(), this);
                    } else {
                        ServerAPI.getInstance().addBookwithcover(APIServerResponse.ADD_BOOK, edt_txt_book_title.getText().toString(), "FREE", edt_txt_book_description.getText().toString(), edt_txt_writer_name.getText().toString(), pickerPath, edt_txt_any_other_book.getText().toString(), this);

                    }
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public boolean validation() {

        if (edt_txt_book_title.getText().toString().isEmpty()) {
            showToast(Constants.BOOK_TITLE_NOT_BLANK, Toast.LENGTH_SHORT);
            edt_txt_book_title.setError(Constants.BOOK_TITLE_NOT_BLANK);
            return false;
        } else if (edt_txt_book_description.getText().toString().isEmpty()) {
            showToast("Book description should not be blank.", Toast.LENGTH_SHORT);
            edt_txt_book_description.setError("Book description should not be blank.");
            return false;
        } else if (edt_txt_writer_name.getText().toString().isEmpty()) {
            showToast(Constants.BOOK_AUTHOR_NOT_BLANK, Toast.LENGTH_SHORT);
            edt_txt_writer_name.setError(Constants.BOOK_AUTHOR_NOT_BLANK);
            return false;
        } else if (Character.isDigit(edt_txt_writer_name.getText().charAt(0))) {
            showToast(Constants.FIRST_NAME_START, Toast.LENGTH_SHORT);
            return false;
        } else if (mCurrentPhotoPath.isEmpty()) {
            showToast("Choose Book cover image.", Toast.LENGTH_SHORT);
            return false;
        } else if (bookPaymentType == "") {
            showToast("Select Payment method", Toast.LENGTH_SHORT);
            return false;
        } else {
            return true;
        }

    }


    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    public void onSuccess(int tag, Response response) {

        hideLoading();
        UPloadBookModal uPloadBookModal;
        BooksLanguagesModal booksLanguagesModal;

        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.ADD_BOOK:


                        uPloadBookModal = (UPloadBookModal) response.body();


                        if (uPloadBookModal.getStatus().equals("1")) {
                            EventBus.getDefault().post(new EventBusBookCreated("1"));
                            showToast(uPloadBookModal.getMessage(), Toast.LENGTH_SHORT);
                            AddBookActivity.this.finish();
                        }


                        break;
                    case APIServerResponse.BOOK_LANGUAGES:

                        booksLanguagesModal = (BooksLanguagesModal) response.body();

                        if (booksLanguagesModal.getStatus().equals("1")) {
                            list = booksLanguagesModal.getList();

                            CustomBookLanguagesAdapter customBookLanguagesAdapter = new CustomBookLanguagesAdapter(AddBookActivity.this, R.layout.adapter_layout_custom_spinner_item, R.id.tv_language, list);
                            spinnerLanguage.setAdapter(customBookLanguagesAdapter);

                        }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        showToast(throwable.getMessage(), Toast.LENGTH_SHORT);

    }

/*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
           */
/* if (requestCode == PLACE_PICKER_REQUEST) {

                final Place place = PlacePicker.getPlace(this, data);
                final CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                String attributions = (String) place.getAttributions();


                edt_txt_location.setText(name);
                txtvw_state.setText(place.getLocale().getCountry());
                txtvw_country.setText(place.getAddress());


            }*//*

            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
*/

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }


    @OnClick(R.id.imgvw_upload_book_cover)
    public void imagePickerDialog() {

        if (filePaths.size() < 7) {
            showAlertDialogSelectBookImages();
        } else {
            showToast(getString(R.string.cant_share_more_than_6_media_items), Toast.LENGTH_SHORT);
        }

        /*new AlertDialog.Builder(this)

                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();*/
    }


    @AfterPermissionGranted(RC_CAMERA_PERM_AND_STORAGE)
    private void dispatchTakePictureIntent() {
        if (EasyPermissions.hasPermissions(AddBookActivity.this, perms)) {


            dispatchTakePictureIntent(REQUEST_IMAGE_CAPTURE);
            /*Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }*/
        } else {
            EasyPermissions.requestPermissions(AddBookActivity.this, getString(R.string.access_camera_storage),
                    RC_CAMERA_PERM_AND_STORAGE, perms);
        }

    }

    @AfterPermissionGranted(RC_GALLERY_PERM_AND_STORAGE)
    private void dispatchSelectGalleryPicIntent() {

        if (EasyPermissions.hasPermissions(AddBookActivity.this, galleryPerms)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Pictures"), PICK_IMAGE_MULTIPLE);
        } else {
            EasyPermissions.requestPermissions(AddBookActivity.this, getString(R.string.access_camera_storage),
                    RC_CAMERA_PERM_AND_STORAGE, perms);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_MULTIPLE) {
            if (data != null) {

                // If data.getData() == null means multiple images selected, else single image selected.
                if (data.getData() == null) {
                    ClipData clipData = data.getClipData();
                    if (clipData != null) {
                        for (int i = 0; i < clipData.getItemCount(); i++) {
                            ClipData.Item item = clipData.getItemAt(i);
                            Uri uri = item.getUri();
                            filePaths.add(Utilities.compressImage(FileUtils.getPath(AddBookActivity.this, uri)));
                        }
                    }
                } else {
                    Log.i("HelloURIGAll", "" + data.getData());
                    filePaths.add(Utilities.compressImage(FileUtils.getPath(AddBookActivity.this, data.getData())));
                }
                //sendToServer();
                //previewMediaInGrid(filePaths);
                previewMediaInGrid(filePaths);
            }
        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            if (mCurrentPhotoPath != null && !mCurrentPhotoPath.equalsIgnoreCase("")) {
                handleBigCameraPhoto();

                Log.i("Photo Path", mCurrentPhotoPath);
                filePaths.add(mCurrentPhotoPath);
                previewMediaInGrid(filePaths);
            }
            /*Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            //imgvw_upload_book_cover_image.setImageBitmap(imageBitmap);

            Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);
            String imagePath = getRealPathFromURI(tempUri);
            filePaths.add(imagePath);
            previewMediaInGrid(filePaths);*/

        } else if (requestCode == REQUEST_BOOK_COVER_IMAGE_CAPTURE && resultCode == RESULT_OK) {


            if (mCurrentPhotoPath != null && !mCurrentPhotoPath.equalsIgnoreCase("")) {
                handleBigCameraPhoto();

                Log.i("Photo Path", mCurrentPhotoPath);
                Glide.with(AddBookActivity.this).load(mCurrentPhotoPath).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).override(96, 96).fitCenter().crossFade().into(imgvw_upload_book_cover_image);

            }

            /*Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            //imgvw_upload_book_cover_image.setImageBitmap(imageBitmap);

            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
            Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);
            pickerPath = getRealPathFromURI(tempUri);

            Glide.with(AddBookActivity.this).load(pickerPath).placeholder(R.drawable.placeholder).thumbnail(0.1f).override(96, 96).fitCenter().crossFade().into(imgvw_upload_book_cover_image);*/


            //Bundle extras = data.getExtras();
            //Log.i("HelloURIGAll", "" + data.getExtras());
            //bookCoverUri = data.getData();

            /*Uri filePath = picUri;
            Glide.with(AddBookActivity.this).load(filePath).placeholder(R.drawable.placeholder).thumbnail(0.1f).override(96, 96).fitCenter().crossFade().into(imgvw_upload_book_cover_image);
            String anotherPath = FileUtils.getPath(AddBookActivity.this, filePath);
            String path = filePath.getPath();*/



            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                imgvw_upload_book_cover_image.setImageBitmap(imageBitmap);
                *//*final Uri image = data.getData();
                final File file = new File(image.getPath());
                // now you can upload your image file*//*
            }*/
            //Uri image = data.getData();

            //pickerPath = FileUtils.getPath(AddBookActivity.this, data.getData());

            /*//Log.i("HelloURIGAll", "" + bookCoverUri);
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            imgvw_upload_book_cover_image.setImageBitmap(imageBitmap);*/
            //Glide.with(AddBookActivity.this).load(new File(pickerPath)).placeholder(R.drawable.placeholder).thumbnail(0.1f).override(96, 96).fitCenter().crossFade().into(imgvw_upload_book_cover_image);
        } else if (requestCode == REQUEST_BOOK_COVER_IMAGE_SELECTION && resultCode == RESULT_OK) {
            if (data != null) {
                mCurrentPhotoPath = FileUtils.getPath(AddBookActivity.this, data.getData());
                Glide.with(AddBookActivity.this).load(new File(mCurrentPhotoPath)).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).override(96, 96).fitCenter().crossFade().into(imgvw_upload_book_cover_image);
            }

        }

    }


    private void handleBigCameraPhoto() {

        if (mCurrentPhotoPath != null && mCurrentPhotoPath.equalsIgnoreCase("")) {
            galleryAddPic();
            mCurrentPhotoPath = Utilities.compressImage(mCurrentPhotoPath);
            //mCurrentPhotoPath = null;
        }


    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        return File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private String getAlbumName() {

        return getString(R.string.call4blessing_pictures);
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Image" + String.valueOf(System.currentTimeMillis()), null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        int idx = 0;
        if (cursor != null) {
            idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        }
        if (cursor != null) {
            String path = cursor.getString(idx);
            cursor.close();
            return path;
        } else {
            return "";
        }
    }


    private void previewMediaInGrid(ArrayList<String> mArrayUri) {
        //recyclerView_myc = (AutofitRecyclerView) findViewById(R.id.recyclerView_myc); //recyclerView_myc will be autofitrecyelrview
        if (!mArrayUri.isEmpty()) {
            if (mArrayUri.size() == 6) {
                imgvw_upload_book_cover.setVisibility(View.GONE);
            }

            recyclerView_myc.setVisibility(View.VISIBLE);
            //vidPreview.setVisibility(View.GONE);
            //imgPreview.setVisibility(View.GONE);
            mAdapter = new GridAdapterUpload(mArrayUri, getApplicationContext());
            recyclerView_myc.setAdapter(mAdapter);
        }


    }


    @Subscribe
    public void onBookImageRemoved(EventBusRemovedBookimagesModal eventBusRemovedBookimagesModal) {
        //showToast("hello " + eventBusRemovedBookimagesModal.getMessage(), Toast.LENGTH_LONG);
        if (eventBusRemovedBookimagesModal.getMessage() < 6) {
            imgvw_upload_book_cover.setVisibility(View.VISIBLE);

      /*  if (imgvw_upload_book_cover.getVisibility() == View.GONE) {
            imgvw_upload_book_cover.setVisibility(View.VISIBLE);
        }*/

        }
    }


    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        if (EasyPermissions.hasPermissions(AddBookActivity.this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(AddBookActivity.this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(AddBookActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(AddBookActivity.this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        ChosenImage image = images.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else
                pickerPath = image.getOriginalPath();
            imgvw_upload_book_cover.setImageURI(Uri.fromFile(new File(pickerPath)));
        } else
            showSnack("Invalid Image");
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // Handle negative button on click listener
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Let's show a toast
                //Toast.makeText(getContext(), R.string.settings_dialog_canceled, Toast.LENGTH_SHORT).show();
            }
        };

        // (Optional) Check whether the user denied permissions and checked NEVER ASK AGAIN.
        // This will display a dialog directing them to enable the permission in app settings.
        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
                "Need Permission to access your Gallery and Camera",
                R.string.setting, R.string.cancel, onClickListener, perms);
    }


    private void showAlertDialogSelectBookImages() {

        try {
            //final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);

            android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
            dialogBuilder.setTitle("Select image");
            /*dialogBuilder.setMessage("Select ");*/
            dialogBuilder.setCancelable(true);
            dialogBuilder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    dispatchTakePictureIntent();
                    //dispatchTakePictureIntent(REQUEST_IMAGE_CAPTURE);


                }
            });
            dialogBuilder.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    dispatchSelectGalleryPicIntent();


                }
            });

           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            android.support.v7.app.AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterPermissionGranted(RC_CAMERA_PERM_AND_STORAGE)
    private void showAlertDialogSelectBookCover() {

        try {
            //final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);

            android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
            dialogBuilder.setTitle("Select image");
            /*dialogBuilder.setMessage("Select ");*/
            dialogBuilder.setCancelable(true);
            dialogBuilder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (EasyPermissions.hasPermissions(AddBookActivity.this, perms)) {


                        //use standard intent to capture an image
                      /*  Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        String imageFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/picture.jpg";
                        File imageFile = new File(imageFilePath);
                        picUri = Uri.fromFile(imageFile); // convert path to Uri
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
                        startActivityForResult(takePictureIntent, REQUEST_BOOK_COVER_IMAGE_CAPTURE);*/


                        dispatchTakePictureIntent(REQUEST_BOOK_COVER_IMAGE_CAPTURE);


                      /*  if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                            startActivityForResult(takePictureIntent, REQUEST_BOOK_COVER_IMAGE_CAPTURE);
                        }*/
                    } else {
                        EasyPermissions.requestPermissions(AddBookActivity.this, getString(R.string.access_camera_storage),
                                RC_CAMERA_PERM_AND_STORAGE, perms);
                    }


                }
            });
            dialogBuilder.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (EasyPermissions.hasPermissions(AddBookActivity.this, perms)) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Pictures"), REQUEST_BOOK_COVER_IMAGE_SELECTION);
                    } else {
                        EasyPermissions.requestPermissions(AddBookActivity.this, getString(R.string.access_camera_storage),
                                RC_CAMERA_PERM_AND_STORAGE, perms);
                    }


                }
            });

           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            android.support.v7.app.AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void dispatchTakePictureIntent(int actionCode) {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        switch (actionCode) {
            case REQUEST_BOOK_COVER_IMAGE_CAPTURE:
                File f = null;
                mCurrentPhotoPath = null;
                try {
                    f = setUpPhotoFile();
                    mCurrentPhotoPath = f.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                } catch (IOException e) {
                    e.printStackTrace();
                    f = null;
                    mCurrentPhotoPath = null;
                }
                break;
            case REQUEST_IMAGE_CAPTURE:
                File f_images = null;
                mCurrentPhotoPath = null;
                try {
                    f_images = setUpPhotoFile();
                    mCurrentPhotoPath = f_images.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f_images));
                } catch (IOException e) {
                    e.printStackTrace();
                    f_images = null;
                    mCurrentPhotoPath = null;
                }


                break;

            default:
                break;
        } // switch

        startActivityForResult(takePictureIntent, actionCode);
    }

    //Year Dialogue


/*    public void yearSelectCalendar() {

        try {
// inflate DatePicker, or e.g. get it from a DatePickerDialog:


            DatePicker datePicker = new DatePicker(this);

 *//*           DatePickerDialog datePicker = new DatePickerDialog(this, R.style.DialogTheme,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

//                        calendar = Calendar.getInstance();
                            calendar.set(Calendar.YEAR, year);
                            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            calendar.set(Calendar.MONTH, (monthOfYear));
                            Log.e("Selected date", "Date" + dayOfMonth + "Month" + monthOfYear + "Year" + year);
                            String format = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                            Log.e("Date is", "Date is" + format);

//                        }else{
//                            Utils.showToast(BookAppointmentActivity.this, "Please select future date");
//                        }

//                        dateTime = year + "-" + (int)(monthOfYear+1) + "-" + dayOfMonth;

                        }
                    },
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH));*//*


            //DatePicker datepicker = new DatePicker(AddBookActivity.this);
            //DatePickerDialog datepicker = new DatePickerDialog(this, R.style.DialogTheme

// pre-Honeycomb fields:
            findAndHideField(datePicker, "mDayPicker");
            findAndHideField(datePicker, "mMonthPicker");

// Honeycomb(+) fields:
            findAndHideField(datePicker, "mDaySpinner");
            findAndHideField(datePicker, "mMonthSpinner");

// Lollipop(+) fields (wrapped in a delegate):
            final Object mDatePickerDelegate = findFieldInstance(datePicker, "mDelegate");
            findAndHideField(mDatePickerDelegate, "mDaySpinner");
            findAndHideField(mDatePickerDelegate, "mMonthSpinner");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    /**
     * find a member field by given name and hide it
     */
  /*  private static void findAndHideField(Object object, String name) {
        try {
            final Field field = object.getClass().getDeclaredField(name);
            field.setAccessible(true);
            final View fieldInstance = (View) field.get(object);
            fieldInstance.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /* find a member field by given name and return its instance value */
    /*private static Object findFieldInstance(DatePicker datepicker, String name) {
        try {
            final Field field = DatePicker.class.getDeclaredField(name);
            field.setAccessible(true);
            return field.get(datepicker);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
*/

   /* public void showFileChooser() {
        try {
            Intent intentPDF = new Intent(Intent.ACTION_GET_CONTENT);
            intentPDF.setType("application/pdf");
            intentPDF.addCategory(Intent.CATEGORY_OPENABLE);

            PackageManager packageManager = getPackageManager();

            List activitiesPDF = packageManager.queryIntentActivities(intentPDF,
                    PackageManager.MATCH_DEFAULT_ONLY);
            boolean isIntentSafePDF = activitiesPDF.size() > 0;


            if (!isIntentSafePDF) {

                // Potentially direct the user to the Market with a Dialog
                showToast("hello !", Toast.LENGTH_SHORT);

            } else {
                showToast("hello", Toast.LENGTH_SHORT);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

}
