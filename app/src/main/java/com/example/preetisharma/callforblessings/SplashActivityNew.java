package com.example.preetisharma.callforblessings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.lang.ref.WeakReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.fabric.sdk.android.Fabric;

/**
 * Created by satoti.garg on 6/9/2017.
 */

public class SplashActivityNew extends BaseActivity {

    boolean backpress = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getApplicationContext(), new Crashlytics(), new CrashlyticsNdk());
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        System.out.println("SplashActivityNew onCreate");
        mHandler.postDelayed(mRunnable, 3000);

    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("SplashActivityNew onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("SplashActivityNew onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("SplashActivityNew onDestroy");
    }

    private Handler mHandler = new Handler(Looper.getMainLooper());

    private Runnable mRunnable = new Runnable() {
        private WeakReference<Activity> weak = new WeakReference<Activity>(SplashActivityNew.this);

        @Override
        public void run() {
            Activity a = weak.get();
            if (a != null) {
                if (isConnectedToInternet()) {

                    if (!backpress) {
                        if (getUserLoggedIn()) {
                            Intent gotoMain = new Intent(SplashActivityNew.this, HomeActivity.class);
                            startActivity(gotoMain);
                        } else {
                            Intent gotoMain = new Intent(SplashActivityNew.this, LandingScreenActivity.class);
                            startActivity(gotoMain);
                        }
                    }

                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
                a.finish();
            }
        }
    };


    @Override
    public void onBackPressed() {
// TODO Auto-generated method stub
        super.onBackPressed();
        System.out.println("SplashActivityNew onBackPressed");
        backpress = true;
    }


    /*private void findHtmlTags() {

        String stringToSearch = "<p>Yada yada yada <code>StringBuffer</code> yada yada ...</p>";


     *//*   if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(stringToSearch, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {


            return Html.fromHtml(stringToSearch).toString();
        }
*//*
        // the pattern we want to search for
        Pattern p = Pattern.compile("<code>(\\S+)</code>");
        Matcher m = p.matcher(stringToSearch);

        // if we find a match, get the group
        if (m.find()) {

            // get the matching group
            String codeGroup = m.group(1);

            // print the group
            System.out.format("'%s'\n", codeGroup);

            Log.i("Output", codeGroup);


        }


    }*/

    /*public void updateStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorStatusBar));
        }
    }*/
}

