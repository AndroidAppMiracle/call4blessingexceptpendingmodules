/*
package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.HelpRequestAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AmenPrayerModal;
import com.example.preetisharma.callforblessings.Server.Modal.HelpRequestListModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

*/
/**
 * Created by Kshitiz Bali on 1/23/2017.
 *//*


public class HelpListActivity extends BaseActivity implements APIServerResponse {


    @BindView(R.id.help_list)
    RecyclerView help_list;
    HelpRequestAdapter _adapter;

    //Prayer of the Day
    @BindView(R.id.txtvw_help_of_the_title)
    AppCompatTextView txtvw_help_of_the_title;
    @BindView(R.id.txtvw_help_of_the_day_description)
    AppCompatTextView txtvw_help_of_the_day_description;
    @BindView(R.id.txtvw_help_of_the_day_likes_count)
    AppCompatTextView txtvw_help_of_the_day_likes_count;
    @BindView(R.id.txtvw_help_of_the_day_comments_count)
    AppCompatTextView txtvw_help_of_the_day_comments_count;
    @BindView(R.id.txtvw_help_of_the_day_like)
    AppCompatTextView txtvw_help_of_the_day_like;
    @BindView(R.id.txtvw_help_of_the_day_comment)
    AppCompatTextView txtvw_help_of_the_day_comment;
    @BindView(R.id.txtvw_help_of_the_day_amen)
    AppCompatTextView txtvw_help_of_the_day_amen;
    List<HelpRequestListModal.ListBean> listAllPrayers;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    private String prayerId;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    HelpRequestListModal helpRequestListModal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_request_list);
        ButterKnife.bind(this);

        help_list = (RecyclerView) findViewById(R.id.help_list);
        try {
            setSupportActionBar(toolbar);
            txtvw_header_title.setText("Help");
            img_view_change_password.setVisibility(View.GONE);
            showLoading();
            if (isConnectedToInternet()) {
                ServerAPI.getInstance().getHelpRequest(APIServerResponse.GET_HELP_REQUEST,getUserSessionId() ,this);
            } else {
                showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.img_view_back)
    public void onBackPressed() {
        super.onBackPressed();

    }


    @Override
    public void onSuccess(int tag, Response response) {
        try {

            listAllPrayers = new ArrayList<>();
            LikeModal likeModal;
            AmenPrayerModal amenPrayerModal;
            hideLoading();
            if (response.isSuccessful()) {


                switch (tag) {


                    case APIServerResponse.LIKE:

                        likeModal = (LikeModal) response.body();
                        if (likeModal.getStatus().equals("1")) {
                            showToast("Liked", Toast.LENGTH_SHORT);
                        }

                        break;
                    case APIServerResponse.AMEN_PRAYER:
                        amenPrayerModal = (AmenPrayerModal) response.body();
                        if (amenPrayerModal.getStatus().equals("1")) {
                            showToast("Amen", Toast.LENGTH_SHORT);
                        }
                        break;
                    case APIServerResponse.GET_HELP_REQUEST:
                        helpRequestListModal = (HelpRequestListModal) response.body();
                        if (helpRequestListModal.getStatus().equals("1")) {

                            //txtvw_prayer_of_the_title.setText(prayerIndexModal.getPrayer_of_day().getTitle());
                            //txtvw_prayer_of_the_day_description.setText(prayerIndexModal.getPrayer_of_day().getDesc());
                            // prayerId = String.valueOf(prayerIndexModal.getPrayer_of_day().getId());
                            listAllPrayers = helpRequestListModal.getList();
                            _adapter = new HelpRequestAdapter(HelpListActivity.this, listAllPrayers);

                        } else {
                            showToast("Error", Toast.LENGTH_LONG);
                        }
                        help_list.setAdapter(_adapter);
                        help_list.setItemAnimator(new DefaultItemAnimator());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(HelpListActivity.this);
                        help_list.setLayoutManager(mLayoutManager);

                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.GET_HELP_REQUEST:
                    System.out.println("Error");
                    break;
                case APIServerResponse.LIKE:
                    System.out.println("Error");
                    break;
                case APIServerResponse.AMEN_PRAYER:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.txtvw_help_of_the_day_like)
    public void likePrayerOfTheDay() {
        showLoading();
        ServerAPI.getInstance().like(APIServerResponse.LIKE, getUserSessionId(), prayerId, "PRAYER", HelpListActivity.this);
    }

    @OnClick(R.id.txtvw_help_of_the_day_amen)
    public void amenPrayerOfTheDayPost() {
        showLoading();
        ServerAPI.getInstance().amenPrayerPost(APIServerResponse.AMEN_PRAYER, getUserSessionId(), prayerId, this);
    }

    @OnClick(R.id.txtvw_help_of_the_day_comment)
    public void helpCommentOfTheDay() {
        Intent i = new Intent(this, CommentsListingActivity.class);
        Bundle b = new Bundle();
        //b.putString(Constants.COMMENTS, String.valueOf(helpRequestListModal.getList().get));
        b.putString(Constants.POSTTYPE, Constants.PRAYER);
        i.putExtras(b);
        startActivity(i);
    }
}
*/
