package com.example.preetisharma.callforblessings.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.MyFriendsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusSendFriendRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.DemoHomeFragment;
import com.example.preetisharma.callforblessings.listener.PaginationScrollListener;
import com.kbeanie.multipicker.utils.BitmapUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 2/1/2017.
 */

public class MyFriendsFragment extends Fragment implements APIServerResponse {
    @BindView(R.id.friends_recycler_view)
    RecyclerView friends_recycler_view;
    private List<MyFriendsModal.ListBean> mList = new ArrayList<>();
    MyFriendsAdapter _adapter;

    private static final int PAGE_NUMBER = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private int currentPage = PAGE_NUMBER;
    private int TOTAL_PAGES = PAGE_NUMBER;

    @BindView(R.id.load_more_btn)
    Button load_more_btn;
    LinearLayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_friends_list_search_request, container, false);
        ButterKnife.bind(this, rootView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }


        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), currentPage, this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }


        _adapter = new MyFriendsAdapter(getActivity(), mList);
        mLayoutManager = new WrapContentLinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        //    friends_recycler_view.setNestedScrollingEnabled(false);
        friends_recycler_view.setLayoutManager(mLayoutManager);
        friends_recycler_view.setNestedScrollingEnabled(false);
        friends_recycler_view.setAdapter(_adapter);

   /*     friends_recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy!=0) {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                        Log.e("Last", "last " + totalItemCount);
                        //End of list
                        currentPage++;
                        isLastPage = false;
                        loadItems();
                        friends_recycler_view.smoothScrollToPosition(0);
                    }
                } else {
                    Log.e("Last", "last " +  mLayoutManager.getItemCount());
                }
            }
        });


        friends_recycler_view.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                Toast.makeText(getActivity(), "Down", Toast.LENGTH_SHORT).show();
                isLoading = true;
                currentPage += 1;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadItems();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });*/

        load_more_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseActivity) getActivity()).showLoading();
                loadItems();
                friends_recycler_view.smoothScrollToPosition(0);
            }
        });
        return rootView;
    }


    public void loadItems() {
        currentPage++;
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), currentPage, MyFriendsFragment.this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        mList = new ArrayList<MyFriendsModal.ListBean>();
        ((BaseActivity) getActivity()).hideLoading();

        MyFriendsModal userListingModal;

        if (response.isSuccessful()) {
            try {
                switch (tag) {

                    case APIServerResponse.MYFRIENDLIST:
                        userListingModal = (MyFriendsModal) response.body();
                        if (userListingModal.getStatus().equals("1")) {
                            Log.e("List data", "List data" + userListingModal.getList().size());
                            mList = userListingModal.getList();
                        } else {

                        }
                        if (mList.size() != 0) {
                            load_more_btn.setVisibility(View.VISIBLE);
                            _adapter = new MyFriendsAdapter(getActivity(), mList);
                            mLayoutManager = new LinearLayoutManager(getActivity());
                            friends_recycler_view.setLayoutManager(mLayoutManager);
                            friends_recycler_view.setAdapter(_adapter);
                        } else {
                            load_more_btn.setVisibility(View.GONE);
                            //  Toast.makeText(getActivity(),"No More Friend found",Toast.LENGTH_SHORT).show();
                        }


                       /* friends_recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                super.onScrollStateChanged(recyclerView, newState);

                            }
                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);

                                int visibleItemCount = mLayoutManager.getChildCount();
                                int totalItemCount = mLayoutManager.getItemCount();
                                int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                                if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                                    Log.e("Last","last "+totalItemCount);
                                    //End of list
                                    currentPage++;
                                    isLastPage=false;
                                    loadItems();
                                   *//* loadNextPages();
                                    home_listing.smoothScrollToPosition(0);*//*
                                } else {
                                    Log.e("Last","last "+totalItemCount);
                                }
                            }
                        });*/

                      /*  //---------------NEW CODE---------------------

                        home_listing.addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                super.onScrollStateChanged(recyclerView, newState);

                            }

                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);

                                int visibleItemCount = .getChildCount();
                                int totalItemCount = linearLayoutManager.getItemCount();
                                int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                                if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                                    Log.e("Last","last "+totalItemCount);
                                    //End of list
                                    currentPage++;
                                    isLastPage=false;
                                    loadNextPages();
                                    home_listing.smoothScrollToPosition(0);
                                } else {
                                    Log.e("Last","last "+totalItemCount);
                                }
                            }
                        });

                        //---------------------------------------------*/

                        break;


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @Subscribe
    public void friend_request_accepted(DemoFriendsModal friendsModal) {
        if (friendsModal.getMessage().equals("1")) {
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), currentPage, this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }

    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) getActivity()).showLoading();
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), currentPage, this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Subscribe
    public void onFriendRequestSent(EventBusSendFriendRequestModal eventBusSendFriendRequestModal) {
        if (eventBusSendFriendRequestModal.getMessage().equals("1")) {
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().myfriendsList(APIServerResponse.MYFRIENDLIST, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), currentPage, this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }
    }

    public class WrapContentLinearLayoutManager extends LinearLayoutManager {

        public WrapContentLinearLayoutManager(Context context, int horizontal, boolean b) {
            super(context);
        }

        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {
                Log.e("probe", "meet a IOOBE in RecyclerView");
            }
        }
    }
}