package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by satoti.garg on 10/25/2017.
 */

public class AllAlbumImageResponse {
    private String status;

    private List<AlbumImageListObject> list;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<AlbumImageListObject> getList ()
    {
        return list;
    }

    public void setList (List<AlbumImageListObject> list)
    {
        this.list = list;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", list = "+list+"]";
    }
}
