package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.FriendRequestAcceptedModal;
import com.example.preetisharma.callforblessings.Server.Modal.FriendRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.InvitationRequestsList;
import com.example.preetisharma.callforblessings.Server.Modal.InvitedRequest;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 12/11/2017.
 */

public class InvitationRequestAdapter extends RecyclerView.Adapter<InvitationRequestAdapter.DataViewHolder> {

    Activity mContext;
    List<InvitedRequest> list;
    FriendRequestAcceptedModal acceptedModal;


    public InvitationRequestAdapter(Activity mContext, List<InvitedRequest> list) {
        this.mContext = mContext;
        this.list = list;
    }


    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<InvitedRequest> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_adapter_friends_list, parent, false);
        InvitationRequestAdapter.DataViewHolder dataView = new InvitationRequestAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        holder.txtvw_friends_request_name.setText(list.get(position).getName() + " ");
        Glide.with(mContext).load(list.get(position).getProfile_img()).placeholder(R.drawable.placeholder_callforblessings).centerCrop().into(holder.iv_friend_request_image);

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {
        @BindView(R.id.txtvw_accept_friend_request)
        AppCompatTextView txtvw_accept_friend_request;
        @BindView(R.id.txtvw_ignore_friend_request)
        AppCompatTextView txtvw_ignore_friend_request;
        @BindView(R.id.txtvw_friends_request_name)
        AppCompatTextView txtvw_friends_request_name;
        @BindView(R.id.iv_friend_request_image)
        AppCompatImageView iv_friend_request_image;
        @BindView(R.id.layout_response_buttons)
        LinearLayout layout_response_buttons;
        @BindView(R.id.ll_my_friend)
        LinearLayout ll_my_friend;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            txtvw_accept_friend_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ServerAPI.getInstance().acceptRejectInvitation(APIServerResponse.INVITATION_ACCEPT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), ((BaseActivity) mContext).getUserID(), "1", new APIServerResponse() {
                        @Override
                        public void onSuccess(int tag, Response response) {
                            acceptedModal = (FriendRequestAcceptedModal) response.body();
                            if (acceptedModal.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                                // EventBus.getDefault().post(new DemoFriendsModal(acceptedModal.getStatus()));
                            } else {
                                ((BaseActivity) mContext).showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);

                            }
                        }

                        @Override
                        public void onError(int tag, Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    });
                }
            });
            txtvw_ignore_friend_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ServerAPI.getInstance().acceptRejectInvitation(APIServerResponse.INVITATION_REJECT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), ((BaseActivity) mContext).getUserID(), "2", new APIServerResponse() {
                        @Override
                        public void onSuccess(int tag, Response response) {
                            acceptedModal = (FriendRequestAcceptedModal) response.body();
                            if (acceptedModal.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            } else {
                                ((BaseActivity) mContext).showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);

                            }
                        }

                        @Override
                        public void onError(int tag, Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    });
                }
            });

        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {

                    switch (tag) {
                        case APIServerResponse.INVITATION_ACCEPT:
                            acceptedModal = (FriendRequestAcceptedModal) response.body();
                            if (acceptedModal.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                                // EventBus.getDefault().post(new DemoFriendsModal(acceptedModal.getStatus()));
                            } else {
                                ((BaseActivity) mContext).showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);

                            }
                            break;
                        case APIServerResponse.INVITATION_REJECT:
                            acceptedModal = (FriendRequestAcceptedModal) response.body();
                            if (acceptedModal.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            } else {
                                ((BaseActivity) mContext).showToast(acceptedModal.getMessage(), Toast.LENGTH_SHORT);

                            }
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            throwable.printStackTrace();
        }
    }


    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        if(list.size()!=0) {
            list.remove(position);
            notifyItemRemoved(position);
        }
    }
}
