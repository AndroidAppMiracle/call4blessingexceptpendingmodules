package com.example.preetisharma.callforblessings.pages;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.DemoDataMyPhotosAdapter;
import com.example.preetisharma.callforblessings.Adapter.PagePhotosAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.PageImagesArray;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class AllPageImagesActivity extends BaseActivity implements APIServerResponse, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rv_photos)
    RecyclerView rv_photos;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    String pageId = "";
    private PageImagesArray list;
    private List<String> listOfImages;
    PagePhotosAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_page_images);
        ButterKnife.bind(this);
        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("Page Photos");

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllPageImagesActivity.this.finish();
            }
        });

        if (getIntent().hasExtra("pageId")) {
            pageId = getIntent().getStringExtra("pageId");
            getAllImages();
        }
        swipe_refresh_layout.setOnRefreshListener(this);
    }


    public void getAllImages() {
        try {
            showLoading();
            ServerAPI.getInstance().getAllPageImages(APIServerResponse.ALL_PAGE_IMAGE, getUserSessionId(),getUserID(), pageId, this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.ALL_PAGE_IMAGE:
                        list = (PageImagesArray) response.body();
                        if (list.getStatus().equalsIgnoreCase("1")) {
                            if (list.getList().size() != 0) {

                                listOfImages=list.getList();

                                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(AllPageImagesActivity.this, DividerItemDecoration.VERTICAL);
                                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
                                rv_photos.setLayoutManager(mLayoutManager);
                                rv_photos.addItemDecoration(dividerItemDecoration);
                                adapter = new PagePhotosAdapter(AllPageImagesActivity.this, listOfImages);
                                rv_photos.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            } else {
                                Toast.makeText(getApplicationContext(), "no Images posted yet", Toast.LENGTH_SHORT);
                            }
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @Override
    public void onRefresh() {
        getAllImages();
    }
}
