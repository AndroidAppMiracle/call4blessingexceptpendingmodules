package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by satoti.garg on 10/28/2017.
 */

public class AllPlaylistSongObject {
    private String status;

    private List<PlayListSongsListObject> list;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<PlayListSongsListObject> getList ()
    {
        return list;
    }

    public void setList (List<PlayListSongsListObject> list)
    {
        this.list = list;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", list = "+list+"]";
    }
}
