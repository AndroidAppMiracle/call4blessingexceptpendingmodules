package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.DeviceListResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DeviceObject;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 10/27/2017.
 */

public class SettingSecurityAdapter extends RecyclerView.Adapter<SettingSecurityAdapter.DataViewHolder> {

    public List</*MyFriendsModal.ListBean*/DeviceObject> list;
    Activity mContext;


    public SettingSecurityAdapter(Activity mContext, List</*MyFriendsModal.ListBean*/DeviceObject> list) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_item_layout, parent, false);
        SettingSecurityAdapter.DataViewHolder dataView = new SettingSecurityAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        holder.logined_date.setText("you have logined on"+list.get(position).getCreated_at());
        holder.devicename.setText(" Device name is "+list.get(position).getDevice_name());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.logined_date)
        AppCompatTextView logined_date;
        @BindView(R.id.devicename)
        AppCompatTextView devicename;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }


    }
}
