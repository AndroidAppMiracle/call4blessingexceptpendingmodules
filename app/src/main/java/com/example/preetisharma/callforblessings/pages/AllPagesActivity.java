package com.example.preetisharma.callforblessings.pages;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.PagesListAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AllPagesModal;
import com.example.preetisharma.callforblessings.Server.Modal.PagesList;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.joymusicplayer.playlist.AllPlaylistActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class AllPagesActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.all_pages_rv)
    RecyclerView all_pages_rv;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    //
    List<PagesList> pagesList;
    PagesListAdapter _adapter;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_pages);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
        getAllPages();

        txtvw_header_title.setText("Your Pages ");
        img_view_change_password.setVisibility(View.GONE);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllPagesActivity.this.finish();
            }
        });
        linearLayoutManager = new LinearLayoutManager(AllPagesActivity.this);
        all_pages_rv.setLayoutManager(linearLayoutManager);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllPages();
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AllPagesActivity.this.finish();
    }

    @OnClick(R.id.txtvw_create_page)
    public void createPage() {
        Intent intent = new Intent(AllPagesActivity.this, CreatePageFirstActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.pageFirst)
    public void pageDesc() {
        Intent intent = new Intent(AllPagesActivity.this, SinglePageActivity.class);
        intent.putExtra("pageId", "10");
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.delete_invited_request, menu);
        return super.onCreateOptionsMenu(menu);

    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case R.id.menu_page_invitation:

                Intent intentSubscribe = new Intent(AllPagesActivity.this, InvitationRequestActivity.class);
                startActivity(intentSubscribe);

                break;
           /* case R.id.menu_delete:

                AlertDialog.Builder builder = new AlertDialog.Builder(AllPagesActivity.this);
                builder.setTitle("Are you sure want to delete this page ?");
                builder.setCancelable(true);
                builder.setMessage(null);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (isConnectedToInternet()) {
                            ServerAPI.getInstance().delete(APIServerResponse.DELETE_PAGE, getUserSessionId(), pageId, SinglePageActivity.this);
                        } else {
                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();

                break;

  */      }

        return super.onOptionsItemSelected(item);
    }

    public void getAllPages() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getAllPages(APIServerResponse.ALL_PAGES, getUserSessionId(), getUserID(), AllPagesActivity.this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllPages();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.ALL_PAGES:
                        AllPagesModal allPagesModal = (AllPagesModal) response.body();
                        if (allPagesModal.getStatus().equals("1")) {
                            if (allPagesModal.getList().size() > 0) {
                                pagesList = allPagesModal.getList();
                                _adapter = new PagesListAdapter(AllPagesActivity.this, pagesList);
                                all_pages_rv.setAdapter(_adapter);
                            }
                            if (swipeRefreshLayout.isRefreshing()) {
                                swipeRefreshLayout.setRefreshing(false);
                            }


                        }
                        break;
                }
            } catch (Exception ex) {

            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
