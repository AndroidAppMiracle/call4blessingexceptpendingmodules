package com.example.preetisharma.callforblessings.Server.Modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by satoti.garg on 10/23/2017.
 */

public class ProfileImageModal implements Parcelable {
    private String status;

    private List<ProfileObjectListModal> list;

    protected ProfileImageModal(Parcel in) {
        status = in.readString();
        list = in.createTypedArrayList(ProfileObjectListModal.CREATOR);
    }

    public static final Creator<ProfileImageModal> CREATOR = new Creator<ProfileImageModal>() {
        @Override
        public ProfileImageModal createFromParcel(Parcel in) {
            return new ProfileImageModal(in);
        }

        @Override
        public ProfileImageModal[] newArray(int size) {
            return new ProfileImageModal[size];
        }
    };

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<ProfileObjectListModal> getList ()
    {
        return list;
    }

    public void setList (List<ProfileObjectListModal> list)
    {
        this.list = list;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", list = "+list+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeTypedList(list);
    }
}
