package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/24/2017.
 */

public class PlaylistCreateResponse {
    private String message;

    private PlayListCreateDetail detail;

    private String status;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public PlayListCreateDetail getPlayListCreateDetail ()
    {
        return detail;
    }

    public void setPlayListCreateDetail (PlayListCreateDetail detail)
    {
        this.detail = detail;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", detail = "+detail+", status = "+status+"]";
    }
}
