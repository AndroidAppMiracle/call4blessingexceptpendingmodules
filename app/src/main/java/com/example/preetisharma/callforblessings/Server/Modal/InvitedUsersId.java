package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/31/2017.
 */

public class InvitedUsersId {
    private String user_id;

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [user_id = "+user_id+"]";
    }
}
