package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 12/7/2017.
 */

public class MutualFriendDetailUpper {

    private String is_friend;

    private String id;

    private String MESSAGE;

    private String WALL;

    private String username;

    private String ABOUT_INFO;

    private Profile_details profile_details;

    private String updated;

    private String email;

    private String FRIEND_REQUEST;

    private String request_respond;

    public String getIs_friend ()
    {
        return is_friend;
    }

    public void setIs_friend (String is_friend)
    {
        this.is_friend = is_friend;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getMESSAGE ()
    {
        return MESSAGE;
    }

    public void setMESSAGE (String MESSAGE)
    {
        this.MESSAGE = MESSAGE;
    }

    public String getWALL ()
    {
        return WALL;
    }

    public void setWALL (String WALL)
    {
        this.WALL = WALL;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getABOUT_INFO ()
    {
        return ABOUT_INFO;
    }

    public void setABOUT_INFO (String ABOUT_INFO)
    {
        this.ABOUT_INFO= ABOUT_INFO;
    }

    public Profile_details getProfile_details ()
    {
        return profile_details;
    }

    public void setProfile_details (Profile_details profile_details)
    {
        this.profile_details = profile_details;
    }

    public String getUpdated ()
    {
        return updated;
    }

    public void setUpdated (String updated)
    {
        this.updated = updated;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getFRIEND_REQUEST ()
    {
        return FRIEND_REQUEST;
    }

    public void setFRIEND_REQUEST (String FRIEND_REQUEST)
    {
        this.FRIEND_REQUEST = FRIEND_REQUEST;
    }

    public String getRequest_respond ()
    {
        return request_respond;
    }

    public void setRequest_respond (String request_respond)
    {
        this.request_respond = request_respond;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [is_friend = "+is_friend+", id = "+id+", MESSAGE = "+MESSAGE+", WALL = "+WALL+", username = "+username+", ABOUT INFO = "+ABOUT_INFO+", profile_details = "+profile_details+", updated = "+updated+", email = "+email+", FRIEND REQUEST = "+FRIEND_REQUEST+", request_respond = "+request_respond+"]";
    }
}
