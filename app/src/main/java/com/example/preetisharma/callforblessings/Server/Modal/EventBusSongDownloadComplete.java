package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 5/22/2017.
 */

public class EventBusSongDownloadComplete {
    private final int songPositions;

    public EventBusSongDownloadComplete(int count) {
        this.songPositions = count;
    }

    public int getMessage() {
        return songPositions;
    }
}

