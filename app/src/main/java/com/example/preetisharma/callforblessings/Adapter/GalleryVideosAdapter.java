package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.GalleryModal;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.VideoEnabledWebChromeClient;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by preeti.sharma on 2/9/2017.
 */

public class GalleryVideosAdapter extends RecyclerView.Adapter<GalleryVideosAdapter.DataViewHolder> {
    public ArrayList<GalleryModal.ListBean> list;
    Activity mContext;
    private VideoEnabledWebChromeClient webChromeClient;

    public GalleryVideosAdapter(Activity mContext, ArrayList<GalleryModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(ArrayList<GalleryModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public GalleryVideosAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_video_item, parent, false);
        GalleryVideosAdapter.DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final GalleryVideosAdapter.DataViewHolder holder, final int position) {

        String videoId;
        try {
            videoId = extractYoutubeId(list.get(position).getVideo_link());
            Log.e("VideoId is->", "" + videoId);
            if (videoId == null) {
                Glide.with(mContext).load(R.drawable.placeholder_callforblessings).placeholder(R.drawable.placeholder_callforblessings).centerCrop().into(holder.img_view_thumbnail);
            } else {
                String img_url = "http://img.youtube.com/vi/" + videoId + "/0.jpg";
                Glide.with(mContext).load(img_url).centerCrop().placeholder(R.drawable.placeholder_callforblessings).into(holder.img_view_thumbnail);
                holder.img_view_thumbnail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(list.get(position).getVideo_link()));
                        mContext.startActivity(i);
                    }
                });
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }

    public int getcount() {
        return list.size();

    }

    private static final int RECOVERY_DIALOG_REQUEST = 1;

    @Override
    public int getItemCount() {
        return list.size();
    }

    private class InsideWebViewClient extends WebViewClient {
        @Override
        // Force links to be opened inside WebView and not in Default Browser
        // Thanks http://stackoverflow.com/a/33681975/1815624
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            return true;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_thumbnail)
        AppCompatImageView img_view_thumbnail;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }


    }

    public String extractYoutubeId(String url) throws MalformedURLException {
        String id = null;
        try {
            String query = new URL(url).getQuery();
            String[] param = query.split("&");

            for (String row : param) {
                String[] param1 = row.split("=");
                if (param1[0].equals("v")) {
                    id = param1[1];
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }
/*// Initializing video player with developer key
        webChromeClient = new VideoEnabledWebChromeClient(holder.nonVideoLayout, holder.videoLayout, loadingView, holder.webView) // See all available constructors...
        {
            // Subscribe to standard events, such as onProgressChanged()...
            @Override
            public void onProgressChanged(WebView view, int progress) {

            }
        };

        holder.webView.setWebChromeClient(webChromeClient);
        webChromeClient.setOnToggledFullscreen(new VideoEnabledWebChromeClient.ToggledFullscreenCallback() {
            @Override
            public void toggledFullscreen(boolean fullscreen) {
                if (fullscreen) {
                    holder.nonVideoLayout.setVisibility(View.GONE);
                    mContext.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                }
            }
        });
        // Call private class InsideWebViewClient
        holder.webView.setWebViewClient(new InsideWebViewClient());

        // Navigate anywhere you want, but consider that this classes have only been tested on YouTube's mobile site
        holder.webView.loadUrl(list.get(position).getVideo_link());
 */

}
