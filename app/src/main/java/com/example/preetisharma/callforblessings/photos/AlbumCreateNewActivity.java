package com.example.preetisharma.callforblessings.photos;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by satoti.garg on 7/14/2017.
 */

public class AlbumCreateNewActivity extends BaseActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    //AppCompatTextView txtvw_create_group;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;

    @BindView(R.id.tet_album_name)
    TextInputEditText tet_album_name;

    @BindView(R.id.tet_album_desc)
    TextInputEditText tet_album_desc;
    @BindView(R.id.attv_album_privacy)
    AppCompatTextView attv_album_privacy;

    private static final int INTENT_REQUEST_CODE = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_create);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText(R.string.new_album);


    }


    @OnClick(R.id.img_view_back)
    public void toolbarBack() {

        AlbumCreateNewActivity.this.finish();

    }

    @OnClick(R.id.attv_album_privacy)
    public void setAlbumPrivacy() {

        try {
            Intent intentAlbumPrivacy = new Intent(AlbumCreateNewActivity.this, AlbumSelectPrivacy.class);
            startActivityForResult(intentAlbumPrivacy, INTENT_REQUEST_CODE);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
