package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by satoti.garg on 12/11/2017.
 */

public class InvitationRequestsList {

    private String status;

    private List<InvitedRequest> list;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<InvitedRequest> getList ()
    {
        return list;
    }

    public void setList (List<InvitedRequest> list)
    {
        this.list = list;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", list = "+list+"]";
    }
}
