package com.example.preetisharma.callforblessings.mypth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.MyPrayerAdapter;
import com.example.preetisharma.callforblessings.Adapter.PrayerAdapter;
import com.example.preetisharma.callforblessings.LandingScreenActivity;
import com.example.preetisharma.callforblessings.RequestActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AmenPrayerModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyPrayerModal;
import com.example.preetisharma.callforblessings.Server.Modal.PrayerCreatedByModal;
import com.example.preetisharma.callforblessings.Server.Modal.PrayerListModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CommentsListingActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 8/1/2017.
 */

public class MyPrayersFragment extends Fragment implements APIServerResponse {

    @BindView(R.id.prayers_list)
    RecyclerView prayers_list;
    MyPrayerAdapter _adapter;
    @BindView(R.id.txtvw_prayer_of_the_title)
    AppCompatTextView txtvw_prayer_of_the_title;
    @BindView(R.id.txtvw_prayer_of_the_day_description)
    AppCompatTextView txtvw_prayer_of_the_day_description;
    @BindView(R.id.txtvw_prayer_of_the_day_likes_count)
    AppCompatTextView txtvw_prayer_of_the_day_likes_count;
    @BindView(R.id.txtvw_prayer_of_the_day_amen_count)
    AppCompatTextView txtvw_prayer_of_the_day_amen_count;
    @BindView(R.id.txtvw_prayer_of_the_day_comments_count)
    AppCompatTextView txtvw_prayer_of_the_day_comments_count;
    @BindView(R.id.txtvw_prayer_of_the_day_like)
    AppCompatTextView txtvw_prayer_of_the_day_like;
    @BindView(R.id.txtvw_prayer_of_the_day_comment)
    AppCompatTextView txtvw_prayer_of_the_day_comment;
    @BindView(R.id.txtvw_prayer_of_the_day_amen)
    AppCompatTextView txtvw_prayer_of_the_day_amen;
    List<PrayerListModal> listAllPrayers;
    @BindView(R.id.cardview_title)
    CardView cardview_title;
    /* @BindView(R.id.nestedscroll)
     NestedScrollView nestedscroll;*/
    private String prayerId;
    MyPrayerModal prayerIndexModal;
    private static String likescount, commentsCount, amencount, amen = "Not amen";
    private static String liked = "not liked";

    SwipeRefreshLayout swipe_refresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.prayer_tab, container, false);

        ButterKnife.bind(this, rootView);
        swipe_refresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh);
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllMyPrayer();
            }
        });

        prayers_list = (RecyclerView) rootView.findViewById(R.id.prayers_list);
        getAllMyPrayer();
        return rootView;
    }

    public void getAllMyPrayer() {
        try {
            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().getMyPrayers(APIServerResponse.MYPRAYERS, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), this);
            } else {
                ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            listAllPrayers = new ArrayList<>();
            LikeModal likeModal;
            AmenPrayerModal amenPrayerModal;
            ((BaseActivity) getActivity()).hideLoading();
            if (response.isSuccessful()) {
                switch (tag) {
                    case APIServerResponse.LIKE: {
                        likeModal = (LikeModal) response.body();
                        if (likeModal.getStatus().equals("1")) {
                            if (!txtvw_prayer_of_the_day_like.isEnabled()) {
                                txtvw_prayer_of_the_day_like.setEnabled(true);
                            }
                            int imgResource = R.drawable.ic_liked;
                            txtvw_prayer_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                            int likescountValue = Integer.parseInt(likescount);
                            likescountValue++;
                            liked = "liked";
                            prayerIndexModal.getList().get(0).setLike_flag("liked");
                            prayerIndexModal.getList().get(0).setLike_count(String.valueOf(likescountValue));
                            if (likescountValue == 0 || likescountValue == 1) {
                                txtvw_prayer_of_the_day_likes_count.setText(String.valueOf(likescountValue) + " " );
                            } else {
                                txtvw_prayer_of_the_day_likes_count.setText(String.valueOf(likescountValue) + " " );
                            }
                            if (swipe_refresh.isRefreshing()) {
                                swipe_refresh.setRefreshing(false);
                            }
                        } else {
                            swipe_refresh.setRefreshing(false);
                            Toast.makeText(getContext(), "No Prayers found", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                    case APIServerResponse.UNLIKE: {
                        likeModal = (LikeModal) response.body();
                        if (likeModal.getStatus().equals("1")) {
                            if (!txtvw_prayer_of_the_day_like.isEnabled()) {
                                txtvw_prayer_of_the_day_like.setEnabled(true);
                            }
                            int imgResource = R.drawable.prayer_like;
                            txtvw_prayer_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                            int likescountValue = Integer.parseInt(likescount);
                            likescountValue--;
                            liked = "not liked";
                            prayerIndexModal.getList().get(0).setLike_flag("not liked");
                            prayerIndexModal.getList().get(0).setLike_count(String.valueOf(likescountValue));

                            if (likescountValue == 0 || likescountValue == 1) {
                                txtvw_prayer_of_the_day_likes_count.setText(String.valueOf(likescountValue) + " ");
                            } else {
                                txtvw_prayer_of_the_day_likes_count.setText(String.valueOf(likescountValue) + " " );
                            }
                        }

                    }
                    break;
                  /*  case APIServerResponse.UNAMEN: {
                        amenPrayerModal = (AmenPrayerModal) response.body();
                        if (amenPrayerModal.getStatus().equals("1")) {
                        }
                        int imgResource = R.drawable.prayer_amen;
                        txtvw_prayer_of_the_day_amen_count.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        int likescountValue = Integer.parseInt(amencount);
                        likescountValue--;
                        liked = "not amen";
                        prayerIndexModal.getList().setAmen_flag("not amen");
                        prayerIndexModal.getList().setAmen_count(String.valueOf(likescountValue));

                        if (likescountValue == 0 || likescountValue == 1) {
                            txtvw_prayer_of_the_day_amen_count.setText(String.valueOf(likescountValue) + "Amen");
                        } else {
                            txtvw_prayer_of_the_day_amen_count.setText(String.valueOf(likescountValue) + "Amens");
                        }
                    }
                    break;*/
                    case APIServerResponse.MYAMENPRAYER:
                        amenPrayerModal = (AmenPrayerModal) response.body();
                        if (amenPrayerModal.getStatus().equals("1")) {
                            if (!txtvw_prayer_of_the_day_amen.isEnabled()) {
                                txtvw_prayer_of_the_day_amen.setEnabled(true);
                            }
                            if (amenPrayerModal.getAmen().equalsIgnoreCase("YES")) {
                                int imgResource = R.drawable.ic_selected_amen;
                                txtvw_prayer_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(amencount);
                                likescountValue++;
                                prayerIndexModal.getList().get(0).setAmen_count(String.valueOf(likescountValue));

                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_prayer_of_the_day_amen_count.setText(String.valueOf(likescountValue) + " ");
                                } else {
                                    txtvw_prayer_of_the_day_amen_count.setText(String.valueOf(likescountValue) + " " );
                                }
                            } else {
                                int imgResource = R.drawable.prayer_amen;
                                txtvw_prayer_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(amencount);
                                if (likescountValue > 0)
                                    likescountValue--;
                                prayerIndexModal.getList().get(0).setAmen_count(String.valueOf(likescountValue));
                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_prayer_of_the_day_amen_count.setText(String.valueOf(likescountValue) + " ");
                                } else {
                                    txtvw_prayer_of_the_day_amen_count.setText(String.valueOf(likescountValue) + " ");
                                }
                            }
                        }
                        if (swipe_refresh.isRefreshing()) {
                            swipe_refresh.setRefreshing(false);
                        }
                        break;

                    case APIServerResponse.MYPRAYERS:
                        try {
                            prayerIndexModal = (MyPrayerModal) response.body();
                            if (prayerIndexModal.getStatus().equals("1")) {
                                if (prayerIndexModal.getList().size() > 0) {
                                    cardview_title.setVisibility(View.VISIBLE);
                                    if (prayerIndexModal.getList().get(0).getLike_flag().equalsIgnoreCase("Not Liked")) {
                                        int imgResource = R.drawable.prayer_like_icon;
                                        txtvw_prayer_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                    } else {
                                        int imgResource = R.drawable.ic_liked;
                                        txtvw_prayer_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                    }
                                    txtvw_prayer_of_the_title.setText(prayerIndexModal.getList().get(0).getTitle());
                                    txtvw_prayer_of_the_day_description.setText(prayerIndexModal.getList().get(0).getDesc());
                                    prayerId = String.valueOf(prayerIndexModal.getList().get(0).getId());
                                    amencount = prayerIndexModal.getList().get(0).getAmen_count();
                                } else {

                                }
                                listAllPrayers = prayerIndexModal.getList();
                                _adapter = new MyPrayerAdapter(getActivity(), listAllPrayers);
                                if (Integer.parseInt(prayerIndexModal.getList().get(0).getLike_count()) == 0 || Integer.parseInt(prayerIndexModal.getList().get(0).getLike_count()) == 1) {
                                    txtvw_prayer_of_the_day_likes_count.setText(prayerIndexModal.getList().get(0).getLike_count() + " ");
                                } else {
                                    txtvw_prayer_of_the_day_likes_count.setText(prayerIndexModal.getList().get(0).getLike_count() + " " );
                                }
                                if (Integer.parseInt(prayerIndexModal.getList().get(0).getComment_count()) == 0 || Integer.parseInt(prayerIndexModal.getList().get(0).getComment_count()) == 1) {
                                    txtvw_prayer_of_the_day_comments_count.setText(prayerIndexModal.getList().get(0).getComment_count() + " ");
                                } else {
                                    txtvw_prayer_of_the_day_comments_count.setText(prayerIndexModal.getList().get(0).getComment_count() + " " );
                                }
                                if (Integer.parseInt(prayerIndexModal.getList().get(0).getAmen_count()) == 0 || Integer.parseInt(prayerIndexModal.getList().get(0).getAmen_count()) == 1) {
                                    txtvw_prayer_of_the_day_amen_count.setText(prayerIndexModal.getList().get(0).getAmen_count() + " ");
                                } else {
                                    txtvw_prayer_of_the_day_amen_count.setText(prayerIndexModal.getList().get(0).getAmen_count() + " ");
                                }
                                if (prayerIndexModal.getList().get(0).getAmen_flag().equalsIgnoreCase("Not Amen")) {
                                    int imgResource = R.drawable.prayer_amen;
                                    txtvw_prayer_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                } else {
                                    int imgResource = R.drawable.ic_selected_amen;
                                    txtvw_prayer_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                }
                            }
                            if (swipe_refresh.isRefreshing()) {
                                swipe_refresh.setRefreshing(false);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        prayers_list.setAdapter(_adapter);
                       /* prayers_list.smoothScrollToPosition(0);
                        prayers_list.scrollToPosition(0);*/
                        prayers_list.setItemAnimator(new DefaultItemAnimator());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        prayers_list.setLayoutManager(mLayoutManager);

                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            ((BaseActivity) getActivity()).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.PRAYER_INDEX:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.txtvw_prayer_of_the_day_like)
    public void likePrayerOfTheDay() {
        if (prayerIndexModal != null)
            if (prayerIndexModal.getList() != null) {


                if (txtvw_prayer_of_the_day_like.isEnabled()) {
                    txtvw_prayer_of_the_day_like.setEnabled(false);
                }
                liked = prayerIndexModal.getList().get(0).getLike_flag();
                likescount = prayerIndexModal.getList().get(0).getLike_count();
                if (prayerIndexModal.getList().get(0).getLike_flag().equalsIgnoreCase("Not liked")) {
                    int imgResource = R.drawable.ic_liked;
                    txtvw_prayer_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                    if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                        ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), String.valueOf(prayerIndexModal.getList().get(0).getId()), "PRAYER", this);
                    } else {
                        ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }

                } else {
                    int imgResource = R.drawable.prayer_like;
                    txtvw_prayer_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                    if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                        ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), String.valueOf(prayerIndexModal.getList().get(0).getId()), "PRAYER", this);
                    } else {
                        ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            }
    }


    @OnClick(R.id.txtvw_prayer_of_the_day_amen)
    public void amenPrayerOfTheDayPost() {
        if (prayerIndexModal != null)
            if (prayerIndexModal.getList() != null) {

                if (txtvw_prayer_of_the_day_amen.isEnabled()) {
                    txtvw_prayer_of_the_day_amen.setEnabled(false);
                }
                amen = prayerIndexModal.getList().get(0).getAmen_flag();
                amencount = prayerIndexModal.getList().get(0).getAmen_count();
                if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                    ServerAPI.getInstance().amenPrayerPost(APIServerResponse.MYAMENPRAYER, ((BaseActivity) getActivity()).getUserSessionId(),((BaseActivity) getActivity()).getUserID(), prayerId, this);
                } else {
                    ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }



/*        if (amen.equalsIgnoreCase("Not amen")) {
            int imgResource = R.drawable.ic_selected_amen;
            txtvw_prayer_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ((BaseActivity) getActivity()).showLoading();
                ServerAPI.getInstance().amenPrayerPost(APIServerResponse.AMEN_PRAYER, ((BaseActivity) getActivity()).getUserSessionId(), prayerId, this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        } else {
            int imgResource = R.drawable.prayer_amen;
            txtvw_prayer_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ((BaseActivity) getActivity()).showLoading();
                ServerAPI.getInstance().amenPrayerPost(APIServerResponse.UNAMEN, ((BaseActivity) getActivity()).getUserSessionId(), prayerId, this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }*/

            }
    }

    @Override
    public void onResume() {
        super.onResume();
        cardview_title.requestFocus();
        cardview_title.setFocusable(true);
        prayers_list.smoothScrollToPosition(0);
        /*try {

            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().getMyPrayers(APIServerResponse.MYPRAYERS, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), this);
            } else {
                ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
            cardview_title.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        getAllMyPrayer();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_add, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_add_new_item) {
            Intent signUpIntent = new Intent(getActivity(), RequestActivity.class);
            startActivity(signUpIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            try {
                ((BaseActivity) getActivity()).showLoading();
                if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                    ServerAPI.getInstance().getMyPrayers(APIServerResponse.MYPRAYERS, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), this);
                } else {
                    ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
                }
                cardview_title.requestFocus();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //Load Data
        } else {
            //No Loading of Data
        }
    }



}
