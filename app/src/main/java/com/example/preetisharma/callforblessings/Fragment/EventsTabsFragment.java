package com.example.preetisharma.callforblessings.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/7/2017.
 */

public class EventsTabsFragment extends Fragment implements TabLayout.OnTabSelectedListener {


    ViewPager viewPager;

    public static EventsTabsFragment newInstance() {

        EventsTabsFragment fragment = new EventsTabsFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.events_tab_layout, container, false);

        ButterKnife.bind(this, rootView);

        ((BaseActivity) getActivity()).getmPrefs();
        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tabs_friends);
        tabLayout.addTab(tabLayout.newTab().setText("Invites"));
        tabLayout.addTab(tabLayout.newTab().setText("Created"));
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager_friendsTab);

        viewPager.setAdapter(new EventsTabsFragment.EventsAdapter(

                getChildFragmentManager(), tabLayout.getTabCount()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(0);
        tabLayout.setOnTabSelectedListener(this);
       /* if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().getMyCreatedEvents(APIServerResponse.MY_CREATED_EVENTS, EventsTabsFragment.this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }*/
/*        iv_close_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_text.setText("");
            }
        });*/


        return rootView;
    }


    /*@Override
    public void onResume() {
        super.onResume();
        try {
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().getMyCreatedEvents(APIServerResponse.MY_CREATED_EVENTS, EventsTabsFragment.this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }


    public class EventsAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public EventsAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {

                case 0:
                    fragment = new EventInvitesFragment();
                    return fragment;
                case 1:
                    fragment = new EventsCreatedFragment();
                    return fragment;
                default:
                    fragment = new EventInvitesFragment();
                    return fragment;

            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
