package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSongsModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSubscriptionCheckModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.JoyMusicSubscriptionActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.AudioPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 5/9/2017.
 */

public class SongsListAdapter extends RecyclerView.Adapter<SongsListAdapter.DataViewHolder> {
    List<JoyMusicSongsModal.ListBean.SongsBean> mList = new ArrayList<>();
    //public static List<PlayMusicModal> mPlayListList = new ArrayList<>();
    ArrayList<PlayMusicModal> arrayListMusic = new ArrayList<>();

    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;
    private int songsListId = 0;

    public SongsListAdapter(Activity mContext, List<JoyMusicSongsModal.ListBean.SongsBean> list, int songsListId) {
        this.mList = list;
        this.songsListId = songsListId;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        mList.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(ArrayList<JoyMusicSongsModal.ListBean.SongsBean> list) {
        this.mList.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public SongsListAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.joy_music_scroll_view_item, parent, false);
        SongsListAdapter.DataViewHolder dataView = new SongsListAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }


    @Override
    public void onBindViewHolder(final SongsListAdapter.DataViewHolder holder, final int position) {


        holder.album_name.setText(mList.get(holder.getAdapterPosition()).getSong_name());

        Glide.with(mContext).load(mList.get(holder.getAdapterPosition()).getAlbum_cover_image()).placeholder(R.drawable.placeholder_callforblessings).override(100, 100).into(holder.albumImage);




        /*holder.albumImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Intent to player activity
                Intent listAlbum = new Intent(mContext, AlbumPlayerActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.SONG_ID, mList.get(holder.getAdapterPosition()).getMusic_file());
                bundle.putString(Constants.SONG_NAME, mList.get(holder.getAdapterPosition()).getSong_name());
                bundle.putString(Constants.ALBUM_COVER, mList.get(holder.getAdapterPosition()).getAlbum_cover_image());
                bundle.putString(Constants.ALBUM_NAME, mList.get(holder.getAdapterPosition()).getAlbum_name());
                listAlbum.putExtras(bundle);
                mContext.startActivity(listAlbum);
            }
        });*/


    }

    public int getcount() {
        if (mList.size() != 0) {
            return mList.size();
        } else {
            return 0;
        }


    }

    @Override
    public int getItemCount() {
        if (mList.size() != 0) {
            return mList.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.album_name)
        AppCompatTextView album_name;
        @BindView(R.id.albumImage)
        AppCompatImageView albumImage;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            albumImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    playAudio(getAdapterPosition());

                    /*if (mList.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase(Constants.PAID)) {
                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().checkjoyMusicSubscription(APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY, ((BaseActivity) mContext).getUserSessionId(), SongsListAdapter.DataViewHolder.this);
                        }

                    } else {
                        playAudio(getAdapterPosition());
                    }*/

                }
            });

        }

        @Override
        public void onSuccess(int tag, Response response) {
            try {
                if (response.isSuccessful()) {
                    JoyMusicSubscriptionCheckModal joyMusicSubscriptionCheckModal;


                    switch (tag) {

                        case APIServerResponse.JOY_MUSIC_CHECK_SUBSCRIPTION_PLAY:

                            joyMusicSubscriptionCheckModal = (JoyMusicSubscriptionCheckModal) response.body();

                            if (joyMusicSubscriptionCheckModal.getStatus().equalsIgnoreCase("1")) {

                                playAudio(getAdapterPosition());
                            } else {
                                Intent player = new Intent(mContext, JoyMusicSubscriptionActivity.class);
                                mContext.startActivity(player);
                            }
                            ((BaseActivity) mContext).hideLoading();
                            break;
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            ((BaseActivity) mContext).hideLoading();
        }
    }


    private void playAudio(int position) {

        if (!arrayListMusic.isEmpty()) {
            arrayListMusic.clear();
        }
        for (int i = 0; i < mList.size(); i++) {
            PlayMusicModal playMusicModal = new PlayMusicModal();
            playMusicModal.setMediaUrl(mList.get(i).getMusic_file());
            playMusicModal.setThumbnailUrl(mList.get(i).getAlbum_cover_image());
            playMusicModal.setArtworkUrl(mList.get(i).getAlbum_cover_image());
            playMusicModal.setTitle(mList.get(i).getSong_name());
            playMusicModal.setAlbum(mList.get(i).getAlbum_name());
            playMusicModal.setArtist(mList.get(i).getAlbum_name());
            playMusicModal.setPaymentType(mList.get(i).getPayment_type());
            playMusicModal.setDownloaded(mList.get(i).getIs_downloaded());
            arrayListMusic.add(playMusicModal);
        }

        //mPlayListList = mList;

        Intent intent = new Intent(mContext, AudioPlayerActivity.class);
        intent.putExtra(AudioPlayerActivity.EXTRA_INDEX, position);
        intent.putExtra(AudioPlayerActivity.PLAYLIST_ID_TAG, songsListId);
        intent.putExtra(Constants.SONG_NAME, mList.get(position).getSong_name());
        intent.putExtra(Constants.ALBUM_COVER, mList.get(position).getAlbum_cover_image());
        intent.putExtra(Constants.MUSIC, Constants.SONG);




        intent.putExtra(Constants.ALBUM_NAME, mList.get(position).getAlbum_name());
        intent.putExtra(Constants.TYPE, Constants.SONG_CAPS);
        intent.putExtra(Constants.SONG_ID, mList.get(position).getId());
        intent.putExtra(Constants.SONG_DOWNLOAD_URL, mList.get(position).getMusic_file());
        intent.putExtra(Constants.POSITION, position);
        intent.putExtra(Constants.IS_DOWNLOADED, mList.get(position).getIs_downloaded());

        Bundle b = new Bundle();
        b.putParcelableArrayList(Constants.SONGS_LIST, arrayListMusic);
        intent.putExtra("songs_bundle", b);
        //intent.putParcelableArrayListExtra(Constants.SONGS_LIST, songsBeanArrayList);
        mContext.startActivity(intent);


    }
}
