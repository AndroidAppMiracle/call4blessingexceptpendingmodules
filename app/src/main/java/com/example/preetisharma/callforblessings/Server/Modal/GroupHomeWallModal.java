package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by satoti.garg on 6/6/2017.
 */

public class GroupHomeWallModal {


    private String status;
    private DetailBean detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailBean getDetail() {
        return detail;
    }

    public void setDetail(DetailBean detail) {
        this.detail = detail;
    }

    public static class DetailBean {


        private int id;
        private String group_name;
        private String group_description;
        private int is_active;
        private String group_icon;
        private UserInfoBean user_info;
        private boolean is_created_by_me;
        private String created_on;
        private String modified_on;
        private String members_count;
        private int totalPages;
        private int currentPage;
        private List<MembersBean> members;
        private List<PostBean> post;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public String getGroup_description() {
            return group_description;
        }

        public void setGroup_description(String group_description) {
            this.group_description = group_description;
        }

        public int getIs_active() {
            return is_active;
        }

        public void setIs_active(int is_active) {
            this.is_active = is_active;
        }

        public String getGroup_icon() {
            return group_icon;
        }

        public void setGroup_icon(String group_icon) {
            this.group_icon = group_icon;
        }

        public UserInfoBean getUser_info() {
            return user_info;
        }

        public void setUser_info(UserInfoBean user_info) {
            this.user_info = user_info;
        }

        public boolean isIs_created_by_me() {
            return is_created_by_me;
        }

        public void setIs_created_by_me(boolean is_created_by_me) {
            this.is_created_by_me = is_created_by_me;
        }

        public String getCreated_on() {
            return created_on;
        }

        public void setCreated_on(String created_on) {
            this.created_on = created_on;
        }

        public String getModified_on() {
            return modified_on;
        }

        public void setModified_on(String modified_on) {
            this.modified_on = modified_on;
        }

        public String getMembers_count() {
            return members_count;
        }

        public void setMembers_count(String members_count) {
            this.members_count = members_count;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public List<MembersBean> getMembers() {
            return members;
        }

        public void setMembers(List<MembersBean> members) {
            this.members = members;
        }

        public List<PostBean> getPost() {
            return post;
        }

        public void setPost(List<PostBean> post) {
            this.post = post;
        }

        public static class UserInfoBean {


            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBean profile_details;
            private int is_friend;
            private String request_respond;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBean getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBean profile_details) {
                this.profile_details = profile_details;
            }

            public int getIs_friend() {
                return is_friend;
            }

            public void setIs_friend(int is_friend) {
                this.is_friend = is_friend;
            }

            public String getRequest_respond() {
                return request_respond;
            }

            public void setRequest_respond(String request_respond) {
                this.request_respond = request_respond;
            }

            public static class ProfileDetailsBean {


                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }

        public static class MembersBean {

            private int group_id;
            private UserInfoBeanX user_info;

            public int getGroup_id() {
                return group_id;
            }

            public void setGroup_id(int group_id) {
                this.group_id = group_id;
            }

            public UserInfoBeanX getUser_info() {
                return user_info;
            }

            public void setUser_info(UserInfoBeanX user_info) {
                this.user_info = user_info;
            }

            public static class UserInfoBeanX {


                private int id;
                private String username;
                private String email;
                private int updated;
                private ProfileDetailsBeanX profile_details;
                private int is_friend;
                private String request_respond;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public int getUpdated() {
                    return updated;
                }

                public void setUpdated(int updated) {
                    this.updated = updated;
                }

                public ProfileDetailsBeanX getProfile_details() {
                    return profile_details;
                }

                public void setProfile_details(ProfileDetailsBeanX profile_details) {
                    this.profile_details = profile_details;
                }

                public int getIs_friend() {
                    return is_friend;
                }

                public void setIs_friend(int is_friend) {
                    this.is_friend = is_friend;
                }

                public String getRequest_respond() {
                    return request_respond;
                }

                public void setRequest_respond(String request_respond) {
                    this.request_respond = request_respond;
                }

                public static class ProfileDetailsBeanX {


                    private String firstname;
                    private String lastname;
                    private String d_o_b;
                    private String phone;
                    private String gender;
                    private String location;
                    private String country;
                    private String state;
                    private String cover_pic;
                    private String profile_pic;

                    public String getFirstname() {
                        return firstname;
                    }

                    public void setFirstname(String firstname) {
                        this.firstname = firstname;
                    }

                    public String getLastname() {
                        return lastname;
                    }

                    public void setLastname(String lastname) {
                        this.lastname = lastname;
                    }

                    public String getD_o_b() {
                        return d_o_b;
                    }

                    public void setD_o_b(String d_o_b) {
                        this.d_o_b = d_o_b;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public String getGender() {
                        return gender;
                    }

                    public void setGender(String gender) {
                        this.gender = gender;
                    }

                    public String getLocation() {
                        return location;
                    }

                    public void setLocation(String location) {
                        this.location = location;
                    }

                    public String getCountry() {
                        return country;
                    }

                    public void setCountry(String country) {
                        this.country = country;
                    }

                    public String getState() {
                        return state;
                    }

                    public void setState(String state) {
                        this.state = state;
                    }

                    public String getCover_pic() {
                        return cover_pic;
                    }

                    public void setCover_pic(String cover_pic) {
                        this.cover_pic = cover_pic;
                    }

                    public String getProfile_pic() {
                        return profile_pic;
                    }

                    public void setProfile_pic(String profile_pic) {
                        this.profile_pic = profile_pic;
                    }
                }
            }
        }

        public static class PostBean {


            private int id;
            private PostByBean post_by;
            private String post_content;
            private String posted_at;
            private String like_count;
            private String comment_count;
            private String like_flag;
            private List<MediaBean> media;
            private List<CommentsBean> comments;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public PostByBean getPost_by() {
                return post_by;
            }

            public void setPost_by(PostByBean post_by) {
                this.post_by = post_by;
            }

            public String getPost_content() {
                return post_content;
            }

            public void setPost_content(String post_content) {
                this.post_content = post_content;
            }

            public String getPosted_at() {
                return posted_at;
            }

            public void setPosted_at(String posted_at) {
                this.posted_at = posted_at;
            }

            public String getLike_count() {
                return like_count;
            }

            public void setLike_count(String like_count) {
                this.like_count = like_count;
            }

            public String getComment_count() {
                return comment_count;
            }

            public void setComment_count(String comment_count) {
                this.comment_count = comment_count;
            }

            public String getLike_flag() {
                return like_flag;
            }

            public void setLike_flag(String like_flag) {
                this.like_flag = like_flag;
            }

            public List<MediaBean> getMedia() {
                return media;
            }

            public void setMedia(List<MediaBean> media) {
                this.media = media;
            }

            public List<CommentsBean> getComments() {
                return comments;
            }

            public void setComments(List<CommentsBean> comments) {
                this.comments = comments;
            }

            public static class PostByBean {


                private int id;
                private String username;
                private String email;
                private int updated;
                private ProfileDetailsBeanXX profile_details;
                private int is_friend;
                private String request_respond;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public int getUpdated() {
                    return updated;
                }

                public void setUpdated(int updated) {
                    this.updated = updated;
                }

                public ProfileDetailsBeanXX getProfile_details() {
                    return profile_details;
                }

                public void setProfile_details(ProfileDetailsBeanXX profile_details) {
                    this.profile_details = profile_details;
                }

                public int getIs_friend() {
                    return is_friend;
                }

                public void setIs_friend(int is_friend) {
                    this.is_friend = is_friend;
                }

                public String getRequest_respond() {
                    return request_respond;
                }

                public void setRequest_respond(String request_respond) {
                    this.request_respond = request_respond;
                }

                public static class ProfileDetailsBeanXX {


                    private String firstname;
                    private String lastname;
                    private String d_o_b;
                    private String phone;
                    private String gender;
                    private String location;
                    private String country;
                    private String state;
                    private String cover_pic;
                    private String profile_pic;

                    public String getFirstname() {
                        return firstname;
                    }

                    public void setFirstname(String firstname) {
                        this.firstname = firstname;
                    }

                    public String getLastname() {
                        return lastname;
                    }

                    public void setLastname(String lastname) {
                        this.lastname = lastname;
                    }

                    public String getD_o_b() {
                        return d_o_b;
                    }

                    public void setD_o_b(String d_o_b) {
                        this.d_o_b = d_o_b;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public String getGender() {
                        return gender;
                    }

                    public void setGender(String gender) {
                        this.gender = gender;
                    }

                    public String getLocation() {
                        return location;
                    }

                    public void setLocation(String location) {
                        this.location = location;
                    }

                    public String getCountry() {
                        return country;
                    }

                    public void setCountry(String country) {
                        this.country = country;
                    }

                    public String getState() {
                        return state;
                    }

                    public void setState(String state) {
                        this.state = state;
                    }

                    public String getCover_pic() {
                        return cover_pic;
                    }

                    public void setCover_pic(String cover_pic) {
                        this.cover_pic = cover_pic;
                    }

                    public String getProfile_pic() {
                        return profile_pic;
                    }

                    public void setProfile_pic(String profile_pic) {
                        this.profile_pic = profile_pic;
                    }
                }
            }

            public static class MediaBean {


                private String file;
                private String type;
                private String thumbnail_file;

                public String getFile() {
                    return file;
                }

                public void setFile(String file) {
                    this.file = file;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getThumbnail_file() {
                    return thumbnail_file;
                }

                public void setThumbnail_file(String thumbnail_file) {
                    this.thumbnail_file = thumbnail_file;
                }
            }

            public static class CommentsBean {


                private int comment_id;
                private int group_post_id;
                private String comment;
                private String created_at;
                private UserInfoBeanXX user_info;

                public int getComment_id() {
                    return comment_id;
                }

                public void setComment_id(int comment_id) {
                    this.comment_id = comment_id;
                }

                public int getGroup_post_id() {
                    return group_post_id;
                }

                public void setGroup_post_id(int group_post_id) {
                    this.group_post_id = group_post_id;
                }

                public String getComment() {
                    return comment;
                }

                public void setComment(String comment) {
                    this.comment = comment;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public UserInfoBeanXX getUser_info() {
                    return user_info;
                }

                public void setUser_info(UserInfoBeanXX user_info) {
                    this.user_info = user_info;
                }

                public static class UserInfoBeanXX {


                    private int id;
                    private String username;
                    private String email;
                    private int updated;
                    private ProfileDetailsBeanXXX profile_details;
                    private int is_friend;
                    private String request_respond;

                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }

                    public String getUsername() {
                        return username;
                    }

                    public void setUsername(String username) {
                        this.username = username;
                    }

                    public String getEmail() {
                        return email;
                    }

                    public void setEmail(String email) {
                        this.email = email;
                    }

                    public int getUpdated() {
                        return updated;
                    }

                    public void setUpdated(int updated) {
                        this.updated = updated;
                    }

                    public ProfileDetailsBeanXXX getProfile_details() {
                        return profile_details;
                    }

                    public void setProfile_details(ProfileDetailsBeanXXX profile_details) {
                        this.profile_details = profile_details;
                    }

                    public int getIs_friend() {
                        return is_friend;
                    }

                    public void setIs_friend(int is_friend) {
                        this.is_friend = is_friend;
                    }

                    public String getRequest_respond() {
                        return request_respond;
                    }

                    public void setRequest_respond(String request_respond) {
                        this.request_respond = request_respond;
                    }

                    public static class ProfileDetailsBeanXXX {


                        private String firstname;
                        private String lastname;
                        private String d_o_b;
                        private String phone;
                        private String gender;
                        private String location;
                        private String country;
                        private String state;
                        private String cover_pic;
                        private String profile_pic;

                        public String getFirstname() {
                            return firstname;
                        }

                        public void setFirstname(String firstname) {
                            this.firstname = firstname;
                        }

                        public String getLastname() {
                            return lastname;
                        }

                        public void setLastname(String lastname) {
                            this.lastname = lastname;
                        }

                        public String getD_o_b() {
                            return d_o_b;
                        }

                        public void setD_o_b(String d_o_b) {
                            this.d_o_b = d_o_b;
                        }

                        public String getPhone() {
                            return phone;
                        }

                        public void setPhone(String phone) {
                            this.phone = phone;
                        }

                        public String getGender() {
                            return gender;
                        }

                        public void setGender(String gender) {
                            this.gender = gender;
                        }

                        public String getLocation() {
                            return location;
                        }

                        public void setLocation(String location) {
                            this.location = location;
                        }

                        public String getCountry() {
                            return country;
                        }

                        public void setCountry(String country) {
                            this.country = country;
                        }

                        public String getState() {
                            return state;
                        }

                        public void setState(String state) {
                            this.state = state;
                        }

                        public String getCover_pic() {
                            return cover_pic;
                        }

                        public void setCover_pic(String cover_pic) {
                            this.cover_pic = cover_pic;
                        }

                        public String getProfile_pic() {
                            return profile_pic;
                        }

                        public void setProfile_pic(String profile_pic) {
                            this.profile_pic = profile_pic;
                        }
                    }
                }
            }
        }
    }
}
