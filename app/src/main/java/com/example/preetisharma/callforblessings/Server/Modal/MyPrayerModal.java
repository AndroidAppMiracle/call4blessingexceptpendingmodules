package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by satoti.garg on 10/16/2017.
 */

public class MyPrayerModal {
    private String status;

    private List<PrayerListModal> list;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<PrayerListModal> getList ()
    {
        return list;
    }

    public void setList (List<PrayerListModal> list)
    {
        this.list = list;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", list = "+list+"]";
    }
}
