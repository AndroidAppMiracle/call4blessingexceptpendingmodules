package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.PaymentGatewayActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

/**
 * Created by satoti.garg on 6/5/2017.
 */

public class DonationActivity extends BaseActivity {

    //Toolbar items
    // @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    // @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    //@BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;


    //Layout
    AppCompatEditText aet_donation_amount;
    AppCompatTextView atv_donate, atv_donation_title, atv_donation_message;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation);


        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        aet_donation_amount = (AppCompatEditText) findViewById(R.id.aet_donation_amount);

        atv_donate = (AppCompatTextView) findViewById(R.id.atv_donate);
        atv_donation_title = (AppCompatTextView) findViewById(R.id.atv_donation_title);
        atv_donation_message = (AppCompatTextView) findViewById(R.id.atv_donation_message);


        txtvw_header_title.setText(R.string.donation);
        img_view_change_password.setVisibility(View.GONE);

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                DonationActivity.this.finish();
            }
        });


        atv_donate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                atv_donate.setEnabled(false);
                if (isConnectedToInternet()) {
                    Intent intentPaymentGateway = new Intent(DonationActivity.this, PaymentGatewayActivity.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.AMOUNT, aet_donation_amount.getText().toString().trim());
                    b.putString(Constants.TYPE, Constants.DONATION);
                    intentPaymentGateway.putExtras(b);
                    startActivityForResult(intentPaymentGateway, APIServerResponse.DONATION_PAYMENT_HASH);
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }


            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == APIServerResponse.DONATION_PAYMENT_HASH) {
            if (data != null) {
                atv_donate.setEnabled(true);
                if (data.getData().toString().equalsIgnoreCase(Constants.SUCCESS)) {
                    atv_donation_title.setVisibility(View.VISIBLE);
                    atv_donation_message.setText(getString(R.string.thanks_for_donating_in_this_noble_cause));
                    aet_donation_amount.setText("");

                } else if (data.getData().toString().equalsIgnoreCase(Constants.FAILURE)) {
                    atv_donation_title.setVisibility(View.VISIBLE);
                    atv_donation_title.setText("Transaction Failed.");
                    atv_donation_message.setVisibility(View.GONE);
                }

            }
        }
    }
}
