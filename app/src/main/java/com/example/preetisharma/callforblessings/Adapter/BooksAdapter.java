package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.PaymentGatewayActivity;
import com.example.preetisharma.callforblessings.Server.Modal.BooksListingModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.BookDetailsActivity;
import com.example.preetisharma.callforblessings.demo.BooksBuyFree;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by preeti.sharma on 1/21/2017.
 */

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.DataViewHolder> {
    public List<BooksListingModal.ListBean> list;
    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;

    public BooksAdapter(Activity mContext, List<BooksListingModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<BooksListingModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_book, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, int position) {

        holder.txtvw_book_upload_date.setText(list.get(position).getCreated_at());

        if (String.valueOf(list.get(position).getUser_info().getId()).equalsIgnoreCase(((BaseActivity) mContext).getUserID())) {
            holder.txtvw_download_book.setVisibility(View.GONE);
        } else {

            if (list.get(position).getPurchased_by_current_user().equalsIgnoreCase("true")) {
                holder.txtvw_download_book.setText("Purchased");
            } else if (list.get(position).getPayment_type().equalsIgnoreCase("FREE")) {
                holder.txtvw_download_book.setText("Free");
            } else {
                holder.txtvw_download_book.setText("Buy");
            }
        }

        holder.txtvw_book_name.setText(list.get(position).getName());
        holder.txtvw_book_description.setText(list.get(position).getDescription());
        Glide.with(mContext).load(list.get(position).getBook_cover()).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).into(holder.book_cover_preview);


        holder.ll_book_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, BookDetailsActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(holder.getAdapterPosition()).getId()));
                //b.putString(Constants.EVENT_INVITE_STATUS, myEventInvitesList.get(position).getInvitation_status());
                i.putExtras(b);
                mContext.startActivity(i);
            }
        });

    }

    public int getcount() {
        return list.size();

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtvw_book_description)
        AppCompatTextView txtvw_book_description;
        @BindView(R.id.book_cover_preview)
        AppCompatImageView book_cover_preview;
        @BindView(R.id.txtvw_book_name)
        AppCompatTextView txtvw_book_name;
        @BindView(R.id.txtvw_download_book)
        AppCompatTextView txtvw_download_book;
        @BindView(R.id.ll_book_item)
        LinearLayout ll_book_item;
        @BindView(R.id.txtvw_book_upload_date)
        AppCompatTextView txtvw_book_upload_date;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPurchased_by_current_user().equalsIgnoreCase("true")) {

            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {

                Intent purchasebookIntent = new Intent(mContext, BooksBuyFree.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                //b.putString(Constants.AMOUNT, list.get(getAdapterPosition()).getAmount());
                //b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getName());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);


                /*new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();*/
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, list.get(getAdapterPosition()).getAmount());
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getName());
                b.putString(Constants.TYPE, Constants.BOOK);
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }

    }
}
