package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.demo.EventsDetailsActivityNew;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DeleteEventModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyEventsCreatedModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.EventEditActivity;
import com.example.preetisharma.callforblessings.demo.EventInviteFriendsActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/8/2017.
 */

public class MyEventsCreatedAdapter extends RecyclerView.Adapter<MyEventsCreatedAdapter.DataViewHolder> {


    private Context mContext;
    private List<MyEventsCreatedModal.ListBean> myEventsCreatedModalList = new ArrayList<>();

    public MyEventsCreatedAdapter(Context mContext, List<MyEventsCreatedModal.ListBean> list) {
        this.myEventsCreatedModalList = list;
        this.mContext = mContext;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_events_created_adapter_layout, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {

        try {
            holder.txtvw_my_event_name.setText(myEventsCreatedModalList.get(position).getEvent_name());
            holder.txtvw_my_event_location.setText(myEventsCreatedModalList.get(position).getEvent_location());
            holder.txtvw_my_event_date.setText(myEventsCreatedModalList.get(position).getEvent_date());
            Glide.with(mContext).load(myEventsCreatedModalList.get(position).getEvent_photo()).thumbnail(0.1f).placeholder(R.drawable.placeholder_callforblessings).into(holder.iv_event_invite_image);


            holder.txtvw_my_event_invite_friends.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        Intent i = new Intent(mContext, EventInviteFriendsActivity.class);
                        Bundle b = new Bundle();
                        b.putString(Constants.EVENT_ID, String.valueOf(myEventsCreatedModalList.get(position).getId()));
                        b.putInt(Constants.POSITION,holder.getAdapterPosition());
                        i.putExtras(b);
                        mContext.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            holder.txtvw_my_event_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        Intent i = new Intent(mContext, EventEditActivity.class);
                        Bundle b = new Bundle();
                        b.putString(Constants.EVENT_ID, String.valueOf(myEventsCreatedModalList.get(position).getId()));
                        i.putExtras(b);
                        mContext.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            holder.ll_created_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, EventsDetailsActivityNew.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.EVENT_ID, String.valueOf(myEventsCreatedModalList.get(holder.getAdapterPosition()).getId()));
                    b.putString(Constants.EVENT_TYPE_FLAG, "My Events");
                    i.putExtras(b);
                    mContext.startActivity(i);
                }
            });

           /* holder.txtvw_my_event_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(myEventsCreatedModalList.get(position).getId()), MyEventsCreatedAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (myEventsCreatedModalList.size() != 0) {
            return myEventsCreatedModalList.size();
        } else {
            return 0;
        }

    }


    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_my_event_name)
        AppCompatTextView txtvw_my_event_name;
        @BindView(R.id.txtvw_my_event_location)
        AppCompatTextView txtvw_my_event_location;
        @BindView(R.id.txtvw_my_event_date)
        AppCompatTextView txtvw_my_event_date;

        @BindView(R.id.txtvw_my_event_invite_friends)
        AppCompatTextView txtvw_my_event_invite_friends;

        @BindView(R.id.txtvw_my_event_delete)
        AppCompatTextView txtvw_my_event_delete;

        @BindView(R.id.txtvw_my_event_edit)
        AppCompatTextView txtvw_my_event_edit;

        @BindView(R.id.iv_event_invite_image)
        AppCompatImageView iv_event_invite_image;
        @BindView(R.id.ll_created_event)
        LinearLayout ll_created_event;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/

            txtvw_my_event_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(myEventsCreatedModalList.get(getAdapterPosition()).getId()), DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });

            /*txtvw_my_event_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });*/
        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {
                    DeleteEventModal deleteEvent;

                    switch (tag) {
                        case APIServerResponse.DELETEEVENT:
                            deleteEvent = (DeleteEventModal) response.body();
                            if (deleteEvent.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast("Event Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
           /* switch (tag) {
                case APIServerResponse.DELETEEVENT:
                    ((BaseActivity) mContext).showToast(throwable.getMessage().toString(), Toast.LENGTH_SHORT);

            }*/
        }
    }

    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        myEventsCreatedModalList.remove(position);
        notifyItemRemoved(position);
    }
}
