package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/8/2017.
 */

public class EventBusGroupCreatePostModal {

    private final String isNewPostCreated;

    public EventBusGroupCreatePostModal(String isNewPostCreated) {
        this.isNewPostCreated = isNewPostCreated;
    }

    public String getIsNewPostCreated() {
        return isNewPostCreated;
    }
}
