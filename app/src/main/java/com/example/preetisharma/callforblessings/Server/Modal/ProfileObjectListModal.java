package com.example.preetisharma.callforblessings.Server.Modal;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 10/23/2017.
 */

public class ProfileObjectListModal implements Parcelable{
    private Created_by created_by;

    private String id;

    private String image;

    private String created_at;

    private String model_type;

    @TargetApi(Build.VERSION_CODES.M)
    protected ProfileObjectListModal(Parcel in) {
        id = in.readString();
        created_by=in.readParcelable(getCreated_by());
        model_type = in.readString();
        created_at = in.readString();
        image = in.readString();
    }

    public static final Creator<ProfileObjectListModal> CREATOR = new Creator<ProfileObjectListModal>() {
        @Override
        public ProfileObjectListModal createFromParcel(Parcel in) {
            return new ProfileObjectListModal(in);
        }

        @Override
        public ProfileObjectListModal[] newArray(int size) {
            return new ProfileObjectListModal[size];
        }
    };

    public Created_by getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (Created_by created_by)
    {
        this.created_by = created_by;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getImage ()
    {
        return image;
    }

    public void setImage (String image)
    {
        this.image = image;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getModel_type ()
    {
        return model_type;
    }

    public void setModel_type (String model_type)
    {
        this.model_type = model_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [created_by = "+created_by+", id = "+id+", image = "+image+", created_at = "+created_at+", model_type = "+model_type+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(null);
        dest.writeString(model_type);
        dest.writeString(created_at);


        /*  created_by=in.readTypedObject(getCreated_by());
        model_type = in.readString();
        created_at = in.readString();*/
        dest.writeString(image);
        /*dest.writeString(paymentType);
        dest.writeByte((byte) (isDownloaded ? 1 : 0));*/
    }


    static   public class Created_by extends ClassLoader implements Parcelable, Creator<Created_by> {
        private String is_friend;

        private String id;

        private String MESSAGE;

        private String WALL;

        private String username;

        private String ABOUT_INFO;

        private Profile_details profile_details;

        private String updated;

        private String email;

        private String FRIEND_REQUEST;

        private String request_respond;

        protected Created_by(Parcel in) {
            is_friend = in.readString();
            id = in.readString();
            MESSAGE = in.readString();
            WALL = in.readString();
            username = in.readString();
            ABOUT_INFO = in.readString();
            updated = in.readString();
            email = in.readString();
            FRIEND_REQUEST = in.readString();
            request_respond = in.readString();
        }

        public static final Creator<Created_by> CREATOR = new Creator<Created_by>() {
            @Override
            public Created_by createFromParcel(Parcel in) {
                return new Created_by(in);
            }

            @Override
            public Created_by[] newArray(int size) {
                return new Created_by[size];
            }
        };

        public String getIs_friend ()
        {
            return is_friend;
        }

        public void setIs_friend (String is_friend)
        {
            this.is_friend = is_friend;
        }

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getMESSAGE ()
        {
            return MESSAGE;
        }

        public void setMESSAGE (String MESSAGE)
        {
            this.MESSAGE = MESSAGE;
        }

        public String getWALL ()
        {
            return WALL;
        }

        public void setWALL (String WALL)
        {
            this.WALL = WALL;
        }

        public String getUsername ()
        {
            return username;
        }

        public void setUsername (String username)
        {
            this.username = username;
        }

        public String getABOUT_INFO ()
    {
        return ABOUT_INFO;
    }

        public void setABOUT_INFO (String ABOUT_INFO)
    {
        this.ABOUT_INFO = ABOUT_INFO;
    }

        public Profile_details getProfile_details ()
        {
            return profile_details;
        }

        public void setProfile_details (Profile_details profile_details)
        {
            this.profile_details = profile_details;
        }

        public String getUpdated ()
        {
            return updated;
        }

        public void setUpdated (String updated)
        {
            this.updated = updated;
        }

        public String getEmail ()
        {
            return email;
        }

        public void setEmail (String email)
        {
            this.email = email;
        }

        public String getFRIEND_REQUEST ()
    {
        return FRIEND_REQUEST;
    }

        public void setFRIEND_REQUEST (String FRIEND_REQUEST)
    {
        this.FRIEND_REQUEST = FRIEND_REQUEST;
    }

        public String getRequest_respond ()
        {
            return request_respond;
        }

        public void setRequest_respond (String request_respond)
        {
            this.request_respond = request_respond;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [is_friend = "+is_friend+", id = "+id+", MESSAGE = "+MESSAGE+", WALL = "+WALL+", username = "+username+", ABOUT INFO = "+ABOUT_INFO+", profile_details = "+profile_details+", updated = "+updated+", email = "+email+", FRIEND REQUEST = "+FRIEND_REQUEST+", request_respond = "+request_respond+"]";
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(is_friend);
            dest.writeString(id);
            dest.writeString(MESSAGE);
            dest.writeString(WALL);
            dest.writeString(username);
            dest.writeString(ABOUT_INFO);
            dest.writeString(updated);
            dest.writeString(email);
            dest.writeString(FRIEND_REQUEST);
            dest.writeString(request_respond);
        }

        @Override
        public Created_by createFromParcel(Parcel source) {
            return null;
        }

        @Override
        public Created_by[] newArray(int size) {
            return new Created_by[0];
        }
    }
    static public class Profile_details
    {
        private String phone;

        private String d_o_b;

        private String cover_pic;

        private String location;

        private String state;

        private String gender;

        private String lastname;

        private String profile_pic;

        private String firstname;

        private String country;

        public String getPhone ()
        {
            return phone;
        }

        public void setPhone (String phone)
        {
            this.phone = phone;
        }

        public String getD_o_b ()
        {
            return d_o_b;
        }

        public void setD_o_b (String d_o_b)
        {
            this.d_o_b = d_o_b;
        }

        public String getCover_pic ()
        {
            return cover_pic;
        }

        public void setCover_pic (String cover_pic)
        {
            this.cover_pic = cover_pic;
        }

        public String getLocation ()
        {
            return location;
        }

        public void setLocation (String location)
        {
            this.location = location;
        }

        public String getState ()
        {
            return state;
        }

        public void setState (String state)
        {
            this.state = state;
        }

        public String getGender ()
        {
            return gender;
        }

        public void setGender (String gender)
        {
            this.gender = gender;
        }

        public String getLastname ()
        {
            return lastname;
        }

        public void setLastname (String lastname)
        {
            this.lastname = lastname;
        }

        public String getProfile_pic ()
        {
            return profile_pic;
        }

        public void setProfile_pic (String profile_pic)
        {
            this.profile_pic = profile_pic;
        }

        public String getFirstname ()
        {
            return firstname;
        }

        public void setFirstname (String firstname)
        {
            this.firstname = firstname;
        }

        public String getCountry ()
        {
            return country;
        }

        public void setCountry (String country)
        {
            this.country = country;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [phone = "+phone+", d_o_b = "+d_o_b+", cover_pic = "+cover_pic+", location = "+location+", state = "+state+", gender = "+gender+", lastname = "+lastname+", profile_pic = "+profile_pic+", firstname = "+firstname+", country = "+country+"]";
        }
    }

}
