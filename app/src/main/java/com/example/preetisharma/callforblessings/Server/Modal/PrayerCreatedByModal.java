package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/16/2017.
 */

public class PrayerCreatedByModal {
    private String flags;

    private String password_hash;

    private String q_answer;

    private String auth_key;

    private String registration_ip;

    private String confirmed_at;

    private String id;

    private String login_status;

    private String username;

    private String updated_at;

    private String blocked_at;

    private String email;

    private String role;

    private String created_at;

    private String unconfirmed_email;

    public String getFlags ()
    {
        return flags;
    }

    public void setFlags (String flags)
    {
        this.flags = flags;
    }

    public String getPassword_hash ()
    {
        return password_hash;
    }

    public void setPassword_hash (String password_hash)
    {
        this.password_hash = password_hash;
    }

    public String getQ_answer ()
    {
        return q_answer;
    }

    public void setQ_answer (String q_answer)
    {
        this.q_answer = q_answer;
    }

    public String getAuth_key ()
    {
        return auth_key;
    }

    public void setAuth_key (String auth_key)
    {
        this.auth_key = auth_key;
    }

    public String getRegistration_ip ()
    {
        return registration_ip;
    }

    public void setRegistration_ip (String registration_ip)
    {
        this.registration_ip = registration_ip;
    }

    public String getConfirmed_at ()
    {
        return confirmed_at;
    }

    public void setConfirmed_at (String confirmed_at)
    {
        this.confirmed_at = confirmed_at;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getLogin_status ()
    {
        return login_status;
    }

    public void setLogin_status (String login_status)
    {
        this.login_status = login_status;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getBlocked_at ()
    {
        return blocked_at;
    }

    public void setBlocked_at (String blocked_at)
    {
        this.blocked_at = blocked_at;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getRole ()
    {
        return role;
    }

    public void setRole (String role)
    {
        this.role = role;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getUnconfirmed_email ()
    {
        return unconfirmed_email;
    }

    public void setUnconfirmed_email (String unconfirmed_email)
    {
        this.unconfirmed_email = unconfirmed_email;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [flags = "+flags+", password_hash = "+password_hash+", q_answer = "+q_answer+", auth_key = "+auth_key+", registration_ip = "+registration_ip+", confirmed_at = "+confirmed_at+", id = "+id+", login_status = "+login_status+", username = "+username+", updated_at = "+updated_at+", blocked_at = "+blocked_at+", email = "+email+", role = "+role+", created_at = "+created_at+", unconfirmed_email = "+unconfirmed_email+"]";
    }
}
