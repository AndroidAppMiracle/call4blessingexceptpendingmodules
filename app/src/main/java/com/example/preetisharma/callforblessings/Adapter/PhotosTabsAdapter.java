package com.example.preetisharma.callforblessings.Adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.example.preetisharma.callforblessings.photos.AlbumsFragment;
import com.example.preetisharma.callforblessings.photos.PhotosOfYouFragment;
import com.example.preetisharma.callforblessings.photos.UploadedByYouFragment;

/**
 * Created by satoti.garg on 7/17/2017.
 */

public class PhotosTabsAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;
    String userId="";

    public PhotosTabsAdapter(FragmentManager fm, int mNumOfTabs,String userId) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
        this.userId=userId;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        Bundle bundle;
        switch (position) {
            case 0:
                fragment = new UploadedByYouFragment();
                bundle = new Bundle();
                bundle.putString("userId", userId);
                fragment.setArguments(bundle);
                return fragment;
            case 1:
                fragment = new AlbumsFragment();
                bundle = new Bundle();
                bundle.putString("userId", userId);
                fragment.setArguments(bundle);
                return fragment;
            default:
                fragment = new UploadedByYouFragment();
                bundle = new Bundle();
                bundle.putString("userId", userId);
                fragment.setArguments(bundle);
                return fragment;

        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    public Fragment getFragment(ViewPager container, int position, FragmentManager fm) {
        String name = makeFragmentName(container.getId(), position);
        return fm.findFragmentByTag(name);
    }

    private String makeFragmentName(int viewId, int index) {
        return "android:switcher:" + viewId + ":" + index;
    }
}
