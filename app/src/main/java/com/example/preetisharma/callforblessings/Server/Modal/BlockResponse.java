package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/25/2017.
 */

public class BlockResponse {

    private String message;

    private String is_blocked;

    private String status;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getIs_blocked ()
    {
        return is_blocked;
    }

    public void setIs_blocked (String is_blocked)
    {
        this.is_blocked = is_blocked;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", is_blocked = "+is_blocked+", status = "+status+"]";
    }
}
