package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.ArrayList;

/**
 * Created by satoti.garg on 11/2/2017.
 */

public class PlaylistSongsResponse {
    private String status;

    private ArrayList<PlayListSongDescrption> list;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ArrayList<PlayListSongDescrption> getList ()
    {
        return list;
    }

    public void setList (ArrayList<PlayListSongDescrption> list)
    {
        this.list = list;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", list = "+list+"]";
    }
}
