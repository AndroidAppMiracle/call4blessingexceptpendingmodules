package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.FilterModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.UpdateGroupParcebleModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CreateGroupActivity;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/12/2017.
 */

public class UpdateGroupAdapter extends RecyclerView.Adapter<UpdateGroupAdapter.DataViewHolder> {


    private Activity mContext;
    private List<UpdateGroupParcebleModal> myFriends = new ArrayList<>();
    private FilterModal modal;
    //private ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> taggedFriendsDemo = new ArrayList<>();
    private ArrayList<UpdateGroupParcebleModal> taggedFriendsIds = new ArrayList<>();
    private ArrayList<UpdateGroupParcebleModal> groupMembers = new ArrayList<>();
    //private ArrayList<UpdateGroupParcebleModal> allFriends = new ArrayList<>();
    private ArrayList<UpdateGroupParcebleModal> myFriendsAll = new ArrayList<>();

    public UpdateGroupAdapter(Activity mContext, ArrayList<UpdateGroupParcebleModal> list) {
        this.mContext = mContext;
        this.groupMembers = list;
        this.myFriendsAll.addAll(groupMembers);
    }


    @Override
    public UpdateGroupAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_create_group_add_friends_layout, parent, false);
        UpdateGroupAdapter.DataViewHolder dataView = new UpdateGroupAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }


    @Override
    public void onBindViewHolder(final UpdateGroupAdapter.DataViewHolder holder, int position) {

        try {

            holder.txtvw_friends_request_name.setText(groupMembers.get(position).getName());

            if (groupMembers.get(position).isGroupMember()) {
                holder.checkbox_select_friend.setChecked(true);
                taggedFriendsIds.add(groupMembers.get(holder.getAdapterPosition()));
            } else {
                holder.checkbox_select_friend.setChecked(false);
            }
            /*
             * */

            Picasso.with(mContext).load(groupMembers.get(position).getImageUrl()).into(holder.iv_friend_request_image);
            holder.movie_progress.setVisibility(View.GONE);
    /*        Glide.with(mContext)
                    .load(groupMembers.get(position).getImageUrl())
                    .into(holder.iv_friend_request_image);*/
    /*        Glide.with(mContext)
                    .load(groupMembers.get(position).getImageUrl())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.movie_progress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            // image ready, hide progress now
                            holder.movie_progress.setVisibility(View.GONE);
                            return false;   // return false if you want Glide to handle everything else.
                        }
                    })
                    *//*.diskCacheStrategy(DiskCacheStrategy.RESULT)   // cache both original & resized image*//*
                    .centerCrop()
                    .dontAnimate()
                    .thumbnail(0.1f)
                    .crossFade()
                    .into(holder.iv_friend_request_image);*/



          /*  URL url = new URL(groupMembers.get(position).getImageUrl());
            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            holder.iv_friend_request_image.setImageBitmap(bmp);*/
            holder.checkbox_select_friend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        Log.i("isChecked", "Checked");
                        taggedFriendsIds.add(groupMembers.get(holder.getAdapterPosition()));
                        Log.i("isChecked size", "" + taggedFriendsIds.size());

                    } else {
                        Log.i("isChecked", "notChecked");
                        taggedFriendsIds.remove(groupMembers.get(holder.getAdapterPosition()));
                        Log.i("isChecked size", "" + taggedFriendsIds.size());
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

   /* public ArrayList<FilterModal> unselectList() {
        FilterModal filterModal = new FilterModal();
        filterModal.setFriendsArray(myTaggedFriends);
        return myTaggedFriends;

    }*/

    /*public ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> selectedList() {

        ArrayList<MyFriendsModal.ListBean.UserInfoBean.ProfileDetailsBean> list = new ArrayList<>();
        list = taggedFriendsDemo;
        return list;
    }*/

    public ArrayList<UpdateGroupParcebleModal> selectedListIds() {
        ArrayList<UpdateGroupParcebleModal> list = new ArrayList<>();
        list.addAll(taggedFriendsIds);
        return list;
    }

    @Override
    public int getItemCount() {
        if (groupMembers.size() != 0) {
            return groupMembers.size();
        } else {
            return 0;
        }

    }


    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtvw_friends_request_name)
        AppCompatTextView txtvw_friends_request_name;
        @BindView(R.id.iv_friend_request_image)
        ImageView iv_friend_request_image;
        @BindView(R.id.checkbox_select_friend)
        AppCompatCheckBox checkbox_select_friend;
        @BindView(R.id.movie_progress)
        ProgressBar movie_progress;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }


    public void notifyDataList(List<UpdateGroupParcebleModal> list) {
        Log.d("notifyData ", list.size() + "");
        this.myFriends = list;
        notifyDataSetChanged();
    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        if(myFriends!=null) {
            myFriends.clear();
        if (charText.length() == 0) {
            myFriends.addAll(myFriendsAll);
        } else {
            for (UpdateGroupParcebleModal wp : myFriendsAll) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    myFriends.add(wp);
                }
            }
        }
        notifyDataSetChanged();}
    }


}

