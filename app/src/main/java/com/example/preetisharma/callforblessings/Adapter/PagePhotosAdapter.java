package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.ProfileObjectListModal;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.pages.AllPageImagesActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 12/8/2017.
 */

public class PagePhotosAdapter extends RecyclerView.Adapter<PagePhotosAdapter.DataViewHolder> {

    Context mContext;
    List<String> list;
    private int x, y;

    public PagePhotosAdapter(Activity mContext, List<String> listOfImages) {
        this.mContext = mContext;
        this.list=listOfImages;

        x = (int) mContext.getResources().getDimension(R.dimen.height_video_item);
        y = (int) mContext.getResources().getDimension(R.dimen.height_video_item);
    }



    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_photos_mine, parent, false);
        PagePhotosAdapter.DataViewHolder dataView = new PagePhotosAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, int position) {
        try {
            Glide.with(mContext)
                    .load(list.get(position))
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.mProgress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            // image ready, hide progress now
                            holder.mProgress.setVisibility(View.GONE);
                            return false;   // return false if you want Glide to handle everything else.
                        }
                    })
                    /*.diskCacheStrategy(DiskCacheStrategy.RESULT)   // cache both original & resized image*/
                    .centerCrop()
                    .override(x, y)
                    .thumbnail(0.1f)
                    .crossFade()
                    .into(holder.atv_my_photo_Cover);
            // Glide.with(mContext).load(myGroupsList.get(position).getImage()).into(holder.itemView);
            // holder.itemView.setActivated(selectedItems.get(position, false));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.atv_my_photo_Cover)
        AppCompatImageView atv_my_photo_Cover;

        @BindView(R.id.my_photo_progress)
        ProgressBar mProgress;

        @BindView(R.id.cv_my_photo_item)
        CardView cv_my_photo_item;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }
}
