package com.example.preetisharma.callforblessings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by preeti.sharma on 12/30/2016.
 */

public class LandingScreenActivity extends BaseActivity {

    @BindView(R.id.txtvw_signin)
    AppCompatTextView txtvw_signin;
    @BindView(R.id.txtvw_signup)
    AppCompatTextView txtvw_signup;
    @BindView(R.id.txtvw_prayer_request)
    AppCompatTextView txtvw_prayer_request;
    @BindView(R.id.txtvw_testimony_request)
    AppCompatTextView txtvw_testimony_request;
    @BindView(R.id.txtvw_know_jesus)
    AppCompatTextView txtvw_know_jesus;
    @BindView(R.id.txtvw_help_request)
    AppCompatTextView txtvw_help_request;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_screen_activity);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        updateStatusBar();

        ButterKnife.bind(this);


    }

    @OnClick(R.id.txtvw_signup)
    void submit() {
        Intent signUpIntent = new Intent(LandingScreenActivity.this, SignUpActivity.class);
        startActivity(signUpIntent);
    }

    @OnClick(R.id.txtvw_signin)
    void login() {
        Intent loginIntent = new Intent(this, SignInActivity.class);

        startActivity(loginIntent);

    }

    @OnClick(R.id.txtvw_prayer_request)
    void request() {
        Intent signUpIntent = new Intent(LandingScreenActivity.this, RequestActivity.class);
        startActivity(signUpIntent);


    }

    @OnClick(R.id.txtvw_testimony_request)
    void requestTestimony() {
        Intent signUpIntent = new Intent(LandingScreenActivity.this, TestimonyRequestActivity.class);
        startActivity(signUpIntent);

    }

    @OnClick(R.id.txtvw_help_request)
    void helpRequest() {
        Intent signUpIntent = new Intent(LandingScreenActivity.this, HelpRequest.class);
        startActivity(signUpIntent);
    }

    @OnClick(R.id.txtvw_know_jesus)
    void knowJesus() {
        Intent signUpIntent = new Intent(LandingScreenActivity.this, KnowAboutJesusActivity.class);
        startActivity(signUpIntent);
    }

    @Override
    public void onBackPressed() {
        if (getUserLoggedIn()) {
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
