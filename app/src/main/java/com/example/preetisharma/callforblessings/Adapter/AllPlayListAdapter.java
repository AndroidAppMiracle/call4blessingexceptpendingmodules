package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.PlaylistCreateResponse;
import com.example.preetisharma.callforblessings.Server.Modal.PlaylistObject;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.joymusicplayer.AudioPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.playlist.PlaylistListing;
import com.example.preetisharma.callforblessings.joymusicplayer.playlist.PlaylistSongsList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 10/30/2017.
 */

public class AllPlayListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements APIServerResponse {
    public static final int ADD_ALBUM = 0;
    public final int ALBUM = 1;

    Activity mContext;
    Dialog dialogAlert;
    Dialog dialog;
    List<PlaylistObject> list;

    int location;

    public AllPlayListAdapter(Activity mContext, List<PlaylistObject> list) {
        this.mContext = mContext;

        this.list = list;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            //Return "Create New Album" Item
            return ADD_ALBUM;
        } else {
            //Return Albums Item
            return ALBUM;
        }
        //return super.getItemViewType(position);
    }

    public PlaylistObject getItem(int position) {
        return list.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View v;

        switch (viewType) {
            case ADD_ALBUM:
                v = inflater.inflate(R.layout.playlist_pop_items, parent, false);
                viewHolder = new AllPlayListAdapter.DataViewHolderAddAlbum(v);
                //Constants.setAppFont((ViewGroup) v, parent.getContext());
                break;

            case ALBUM:
                v = inflater.inflate(R.layout.playlist_pop_items, parent, false);
                viewHolder = new AllPlayListAdapter.DataViewHolderAlbum(v);
                //Constants.setAppFont((ViewGroup) v, parent.getContext());
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        try {
            switch (getItemViewType(position)) {
                case ADD_ALBUM:
                    //Do Nothing
                    break;

                case ALBUM:
                    AllPlayListAdapter.DataViewHolderAlbum dataViewHolderAlbum = (AllPlayListAdapter.DataViewHolderAlbum) holder;
                    dataViewHolderAlbum.img_logo.setVisibility(View.GONE);
                    dataViewHolderAlbum.playListName.setText(list.get(position).getName());

                    dataViewHolderAlbum.cv_playList_item.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(mContext, PlaylistSongsList.class);
                            Bundle b = new Bundle();
                            b.putString(Constants.PLAYLIST_ID, String.valueOf(list.get(position).getId()));
                            i.putExtras(b);
                            mContext.startActivity(i);
                        }
                    });
                    location = position;
                    dataViewHolderAlbum.delete_playlist.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                if (((BaseActivity) mContext).isConnectedToInternet()) {
                                    ((BaseActivity) mContext).showLoading();
                                    ServerAPI.getInstance().deletePlaylist(APIServerResponse.DELETE_PLAYLIST, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(position).getId()), new APIServerResponse() {
                                        @Override
                                        public void onSuccess(int tag, Response response) {
                                            GroupDeleteModal deleteModal = (GroupDeleteModal) response.body();
                                            if (deleteModal.getStatus().equalsIgnoreCase("1")) {
                                                ((BaseActivity) mContext).hideLoading();
                                                ((BaseActivity) mContext).showToast(deleteModal.getMessage(), Toast.LENGTH_SHORT);
                                                removeAt(location);
                                            }
                                        }

                                        @Override
                                        public void onError(int tag, Throwable throwable) {
                                            ((BaseActivity) mContext).hideLoading();
                                            throwable.printStackTrace();
                                        }
                                    });
                                } else {
                                    ((BaseActivity) mContext).hideLoading();
                                    ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    });
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }

    }

    public class DataViewHolderAlbum extends RecyclerView.ViewHolder {

        @BindView(R.id.img_logo)
        ImageView img_logo;
        @BindView(R.id.cv_playList_item)
        CardView cv_playList_item;
        @BindView(R.id.playListName)
        AppCompatTextView playListName;


        @BindView(R.id.delete_playlist)
        ImageView delete_playlist;

        public DataViewHolderAlbum(View v) {
            super(v);
            ButterKnife.bind(this, v);
            delete_playlist.setVisibility(View.VISIBLE);
          /*  cv_playList_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    *//*Toast.makeText(mContext, "Item Click", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mContext, PlaylistSongsList.class);
                    mContext.startActivity(intent);*//*

                }
            });*/
        }

    }


    public class DataViewHolderAddAlbum extends RecyclerView.ViewHolder {

        /*@BindView(R.id.atv_Name)
        AppCompatTextView atv_Name;

        @BindView(R.id.atv_AlbumCover)
        AppCompatImageView atv_AlbumCover;

        @BindView(R.id.ll_songItem)
        LinearLayout ll_songItem;

        @BindView(R.id.movie_progress)
        ProgressBar mProgress;*/

     /*   @BindView(R.id.cv_playList_item)
        CardView cv_playList_item;
*/

        @BindView(R.id.delete_playlist)
        ImageView delete_playlist;
        @BindView(R.id.cv_playList_item)
        CardView cv_playList_item;

        public DataViewHolderAddAlbum(View v) {
            super(v);
            ButterKnife.bind(this, v);
          /*  cv_playList_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Item Click", Toast.LENGTH_SHORT).show();
                }
            });*/
            delete_playlist.setVisibility(View.GONE);

            cv_playList_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog = new Dialog(mContext);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(true);
                    dialog.setContentView(R.layout.custom_create_playlist_layout);
                    final EditText playlist_name_et = (EditText) dialog.findViewById(R.id.playlist_name_et);
                    Button cancleBtn = (Button) dialog.findViewById(R.id.cancle_playlist_btn);
                    Button createPlayList = (Button) dialog.findViewById(R.id.create_playlist_btn);

                    cancleBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    createPlayList.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            createPlaylist(playlist_name_et.getText().toString());
                        }
                    });
                    //  dialogAlert.dismiss();
                    dialog.show();
                }
            });
        }

    }


    public void createPlaylist(String playListName) {
        try {
            ((BaseActivity) mContext).showLoading();
            ServerAPI.getInstance().createPlayList(APIServerResponse.CREATE_NEW_PLAYLIST, ((BaseActivity) mContext).getUserSessionId(),((BaseActivity) mContext).getUserID(), playListName,"", this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void addSongList(String playListId, String songId) {
        try {
            ((BaseActivity) mContext).showLoading();
            ServerAPI.getInstance().addSongToPlaylist(APIServerResponse.ADD_SONG_PLAYLIST, ((BaseActivity) mContext).getUserSessionId(), ((BaseActivity) mContext).getUserID(),playListId, songId, this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {
                ((BaseActivity) mContext).hideLoading();
                try {
                    switch (tag) {
                        case APIServerResponse.CREATE_NEW_PLAYLIST:
                            PlaylistCreateResponse profileImageModal = (PlaylistCreateResponse) response.body();
                            if (profileImageModal.getStatus().equalsIgnoreCase("1")) {
                                Toast.makeText(mContext, "New playlist is created..", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(mContext, PlaylistSongsList.class);
                                intent.putExtra(Constants.PLAYLIST_ID, profileImageModal.getPlayListCreateDetail().getId());
                                intent.putExtra(Constants.PLAYLIST_NAME, profileImageModal.getPlayListCreateDetail().getName());
                                mContext.startActivity(intent);
                                dialog.dismiss();
                            } else {
                                Toast.makeText(mContext, "Unsuccessfull", Toast.LENGTH_SHORT).show();
                            }
                            break;

                        case APIServerResponse.ADD_SONG_PLAYLIST:
                            GroupDeleteModal groupDeleteModal = (GroupDeleteModal) response.body();
                            if (groupDeleteModal.getStatus().equalsIgnoreCase("1")) {
                                Toast.makeText(mContext, groupDeleteModal.getMessage(), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(mContext, AudioPlayerActivity.class);
                                mContext.startActivity(intent);
                                mContext.finish();
                                ((BaseActivity) mContext).hideLoading();
                                Toast.makeText(mContext, "Successfully Added to the list", Toast.LENGTH_SHORT).show();
                            } else {
                                ((BaseActivity) mContext).hideLoading();
                                Toast.makeText(mContext, "Unsuccessfull", Toast.LENGTH_SHORT).show();
                            }
                            break;

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    ((BaseActivity) mContext).hideLoading();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            ((BaseActivity) mContext).hideLoading();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }

    public void removeAt(int position) {
        list.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, list.size());
    }

}
