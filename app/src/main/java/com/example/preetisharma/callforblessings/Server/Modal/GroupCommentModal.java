package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/9/2017.
 */

public class GroupCommentModal {


    /**
     * status : 1
     * detail : {"comment_id":13,"group_post_id":"17","comment":"xcxvcxvcxvcxvcxvc","created_at":"0  seconds   from now","user_info":{"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1495520168user_profile.jpg"},"is_friend":0,"request_respond":"NO"}}
     */

    private String status;
    private DetailBean detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailBean getDetail() {
        return detail;
    }

    public void setDetail(DetailBean detail) {
        this.detail = detail;
    }

    public static class DetailBean {
        /**
         * comment_id : 13
         * group_post_id : 17
         * comment : xcxvcxvcxvcxvcxvc
         * created_at : 0  seconds   from now
         * user_info : {"id":214,"username":"plash.jindal","email":"plash.jindal@emptask.com","updated":1,"profile_details":{"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1495520168user_profile.jpg"},"is_friend":0,"request_respond":"NO"}
         */

        private int comment_id;
        private String group_post_id;
        private String comment;
        private String created_at;
        private UserInfoBean user_info;

        public int getComment_id() {
            return comment_id;
        }

        public void setComment_id(int comment_id) {
            this.comment_id = comment_id;
        }

        public String getGroup_post_id() {
            return group_post_id;
        }

        public void setGroup_post_id(String group_post_id) {
            this.group_post_id = group_post_id;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public UserInfoBean getUser_info() {
            return user_info;
        }

        public void setUser_info(UserInfoBean user_info) {
            this.user_info = user_info;
        }

        public static class UserInfoBean {
            /**
             * id : 214
             * username : plash.jindal
             * email : plash.jindal@emptask.com
             * updated : 1
             * profile_details : {"firstname":"Palash","lastname":"Jindal","d_o_b":"29-5-1996","phone":"1234567890","gender":"MALE","location":"","country":"Spain","state":"Madrid","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1495520168user_profile.jpg"}
             * is_friend : 0
             * request_respond : NO
             */

            private int id;
            private String username;
            private String email;
            private int updated;
            private ProfileDetailsBean profile_details;
            private int is_friend;
            private String request_respond;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getUpdated() {
                return updated;
            }

            public void setUpdated(int updated) {
                this.updated = updated;
            }

            public ProfileDetailsBean getProfile_details() {
                return profile_details;
            }

            public void setProfile_details(ProfileDetailsBean profile_details) {
                this.profile_details = profile_details;
            }

            public int getIs_friend() {
                return is_friend;
            }

            public void setIs_friend(int is_friend) {
                this.is_friend = is_friend;
            }

            public String getRequest_respond() {
                return request_respond;
            }

            public void setRequest_respond(String request_respond) {
                this.request_respond = request_respond;
            }

            public static class ProfileDetailsBean {
                /**
                 * firstname : Palash
                 * lastname : Jindal
                 * d_o_b : 29-5-1996
                 * phone : 1234567890
                 * gender : MALE
                 * location :
                 * country : Spain
                 * state : Madrid
                 * cover_pic : http://call4blessing.com/uploads/coverphoto/1492259192user_cover.jpg
                 * profile_pic : http://call4blessing.com/uploads/profilepic/1495520168user_profile.jpg
                 */

                private String firstname;
                private String lastname;
                private String d_o_b;
                private String phone;
                private String gender;
                private String location;
                private String country;
                private String state;
                private String cover_pic;
                private String profile_pic;

                public String getFirstname() {
                    return firstname;
                }

                public void setFirstname(String firstname) {
                    this.firstname = firstname;
                }

                public String getLastname() {
                    return lastname;
                }

                public void setLastname(String lastname) {
                    this.lastname = lastname;
                }

                public String getD_o_b() {
                    return d_o_b;
                }

                public void setD_o_b(String d_o_b) {
                    this.d_o_b = d_o_b;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getLocation() {
                    return location;
                }

                public void setLocation(String location) {
                    this.location = location;
                }

                public String getCountry() {
                    return country;
                }

                public void setCountry(String country) {
                    this.country = country;
                }

                public String getState() {
                    return state;
                }

                public void setState(String state) {
                    this.state = state;
                }

                public String getCover_pic() {
                    return cover_pic;
                }

                public void setCover_pic(String cover_pic) {
                    this.cover_pic = cover_pic;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }
            }
        }
    }
}
