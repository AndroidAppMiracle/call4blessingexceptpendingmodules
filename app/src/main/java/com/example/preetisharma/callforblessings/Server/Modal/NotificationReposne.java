package com.example.preetisharma.callforblessings.Server.Modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by satoti.garg on 10/25/2017.
 */

public class NotificationReposne implements Parcelable{
    private List<NotificationObjectResponse> detail;

    private String status;

    protected NotificationReposne(Parcel in) {
        status = in.readString();
    }

    public static final Creator<NotificationReposne> CREATOR = new Creator<NotificationReposne>() {
        @Override
        public NotificationReposne createFromParcel(Parcel in) {
            return new NotificationReposne(in);
        }

        @Override
        public NotificationReposne[] newArray(int size) {
            return new NotificationReposne[size];
        }
    };

    public List<NotificationObjectResponse> getDetail ()
    {
        return detail;
    }

    public void setDetail (List<NotificationObjectResponse> detail)
    {
        this.detail = detail;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [detail = "+detail+", status = "+status+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
    }
}
