package com.example.preetisharma.callforblessings.pages;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CreatePageModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.PageStatusMessage;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

public class UpdatePagesDescription extends BaseActivity implements ImagePickerCallback, EasyPermissions.PermissionCallbacks, APIServerResponse {

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.txtvw_page_delete)
    AppCompatTextView txtvw_page_delete;


    @BindView(R.id.page_name)
    AppCompatEditText pageName;
    @BindView(R.id.page_desc)
    AppCompatEditText pageDesc;
    @BindView(R.id.back_cover_img)
    AppCompatImageView back_cover_img_rl;
    @BindView(R.id.img_view_profile_pic)
    AppCompatImageView img_view_profile_pic;

    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private String profileImagePath = "";
    private String coverImagePath = "";
    CreatePageModal createPageModal;
    //txtvw_page_delete

    private int coverImg = 0;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int RC_GALLERY_PERM = 545;
    private static final String TAG = "CallForBlessings";
    private static final int RC_CAMERA_PERM = 342;


    String pageId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_pages_description);
        ButterKnife.bind(this);
        // img_view_change_password.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit));
        img_view_change_password.setVisibility(View.GONE);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if (getIntent().hasExtra("pageId")) {
            pageId = getIntent().getStringExtra("pageId");
        }
        getDescription();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        UpdatePagesDescription.this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        getDescription();
    }

    public void getDescription() {
        showLoading();
        if (isConnectedToInternet()) {
            try {
                ServerAPI.getInstance().getPageDesc(APIServerResponse.PAGE_DESC, getUserSessionId(), getUserID(), pageId, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @OnClick(R.id.txtvw_page_update)
    public void updatePageDescription() {
        showLoading();
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().updatePage(APIServerResponse.UPDATE_PAGE, getUserSessionId(), getUserID(), pageId, pageName.getText().toString(), pageDesc.getText().toString(), profileImagePath, coverImagePath, UpdatePagesDescription.this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }


    @OnClick(R.id.txtvw_page_delete)
    public void deletePage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(UpdatePagesDescription.this);
        builder.setTitle("Are you sure want to delete this page ?");
        builder.setCancelable(true);
        builder.setMessage(null);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isConnectedToInternet()) {
                    Toast.makeText(getApplicationContext(), "Page Id needs to be change", Toast.LENGTH_SHORT).show();
                    ServerAPI.getInstance().delete(APIServerResponse.DELETE_PAGE, getUserSessionId(), getUserID(), pageId, UpdatePagesDescription.this);
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
        // Toast.makeText(getApplicationContext(), "Deletion api pending ", Toast.LENGTH_LONG).show();
    }


    @OnClick(R.id.img_view_profile_pic)
    public void imagePickerDialog() {

        coverImg = 1;

        new AlertDialog.Builder(this)

                .setTitle("Select Image for page ..")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        takePicture();

                    }

                })

                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        pickImageSingle();

                    }

                })

                .setIcon(R.mipmap.ic_app_icon)

                .show();

    }


    @AfterPermissionGranted(RC_CAMERA_PERM)

    public void takePicture() {

        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            cameraPicker = new CameraImagePicker(this);

            cameraPicker.shouldGenerateMetadata(true);

            cameraPicker.shouldGenerateThumbnails(true);

            cameraPicker.setImagePickerCallback(this);

            profileImagePath = cameraPicker.pickImage();

        } else {

            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",

                    RC_CAMERA_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        }

    }


    @AfterPermissionGranted(RC_GALLERY_PERM)

    public void pickImageSingle() {

        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            imagePicker = new ImagePicker(this);

            imagePicker.shouldGenerateMetadata(true);

            imagePicker.shouldGenerateThumbnails(true);

            imagePicker.setImagePickerCallback(this);

            imagePicker.pickImage();

        } else {

            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",

                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        }

    }


    @Override

    public void onImagesChosen(List<ChosenImage> images) {
        ChosenImage image = images.get(0);

        if (image != null) {

            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0) {

                if (coverImg == 0) {

                    //Toast.makeText(getApplicationContext(), "coverImage is selected", Toast.LENGTH_LONG).show();

                    coverImagePath = image.getThumbnailPath();

                } else {

                    profileImagePath = image.getThumbnailPath();

                }

            } else {

                if (coverImg == 0) {

                    //    Toast.makeText(getApplicationContext(), "coverImage is selected", Toast.LENGTH_LONG).show();

                    coverImagePath = image.getOriginalPath();

                } else {

                    profileImagePath = image.getOriginalPath();

                }

                profileImagePath = image.getOriginalPath();

            }

            if (coverImg == 0) {
                hideLoading();
                Drawable d = Drawable.createFromPath(coverImagePath);
                back_cover_img_rl.setBackground(d);
                if (isConnectedToInternet()) {
                    ServerAPI.getInstance().updatePageBanner(APIServerResponse.UPDATE_PAGE_BANNER, getUserSessionId(), getUserID(), pageId, coverImagePath, UpdatePagesDescription.this);
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            } else {
                hideLoading();
                img_view_profile_pic.setImageURI(Uri.fromFile(new File(profileImagePath)));

                if (isConnectedToInternet()) {
                    ServerAPI.getInstance().updatePageDp(APIServerResponse.UPDATE_PAGE_DP, getUserSessionId(), getUserID(), pageId, profileImagePath, UpdatePagesDescription.this);
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }

        } else

            showSnack("Invalid Image");

    }


    @Override

    public void onError(String s) {


    }


    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {

                if (requestCode == Picker.PICK_IMAGE_DEVICE) {

                    if (imagePicker == null) {
                        imagePicker = new ImagePicker(this);
                    }
                    imagePicker.submit(data);

                } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {

                    if (cameraPicker == null) {

                        cameraPicker = new CameraImagePicker(this);

                        cameraPicker.reinitialize(profileImagePath);
                    }
                    cameraPicker.submit(data);
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

    }


    @Override

    public void onPermissionsGranted(int requestCode, List<String> perms) {

        try {

            PlacePicker.IntentBuilder intentBuilder =

                    new PlacePicker.IntentBuilder();

            intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);

            Intent intent = intentBuilder.build(this);

            startActivityForResult(intent, PLACE_PICKER_REQUEST);


        } catch (GooglePlayServicesRepairableException

                | GooglePlayServicesNotAvailableException e) {

            e.printStackTrace();

        }

    }


    @Override

    public void onPermissionsDenied(int requestCode, List<String> perms) {

        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());


        // Handle negative button on click listener

        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int which) {

                // Let's show a toast

                //Toast.makeText(getContext(), R.string.settings_dialog_canceled, Toast.LENGTH_SHORT).show();

            }

        };


        // (Optional) Check whether the user denied permissions and checked NEVER ASK AGAIN.

        // This will display a dialog directing them to enable the permission in app settings.

        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,

                "Need Permission to access your Gallery and Camera",

                R.string.setting, R.string.cancel, onClickListener, perms);

    }


    @OnClick(R.id.choose_cover_img)

    public void chooseImageActivity() {

        coverImg = 0;

        new AlertDialog.Builder(this)

                .setTitle("Select Image for page ..")

                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        takePicture();

                    }

                })

                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        pickImageSingle();

                    }

                })

                .setIcon(R.mipmap.ic_app_icon)

                .show();

       /* Intent changePic = new Intent(this, CoverPictureForPageActivity.class);

        startActivity(changePic);*/

    }


    @Override

    public void onSuccess(int tag, Response response) {


        createPageModal = new CreatePageModal();

        hideLoading();

        if (response.isSuccessful()) {
            try {
                switch (tag) {


                    case APIServerResponse.UPDATE_PAGE:
                        createPageModal = (CreatePageModal) response.body();

                        if (createPageModal.getStatus().equals("1")) {
                            Toast.makeText(UpdatePagesDescription.this, "" + createPageModal.getMessage(), Toast.LENGTH_SHORT).show();
                            pageName.setText(createPageModal.getDetail().getName() + "");
                            pageDesc.setText(createPageModal.getDetail().getDescription() + "");
                            Glide.with(this).load(createPageModal.getDetail().getProfile_img()).thumbnail(0.1f).fitCenter().placeholder(R.mipmap.profile_holder).into(img_view_profile_pic);
                            Glide.with(this).load(createPageModal.getDetail().getBanner_img()).thumbnail(0.1f).fitCenter().placeholder(R.mipmap.profile_holder).into(back_cover_img_rl);
                            UpdatePagesDescription.this.finish();
                        } else {
                            Toast.makeText(UpdatePagesDescription.this, "" + createPageModal.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                        break;
                    case APIServerResponse.DELETE_PAGE:
                        GroupDeleteModal groupDeleteModal = (GroupDeleteModal) response.body();
                        if (groupDeleteModal.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(UpdatePagesDescription.this, "page deleted successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(UpdatePagesDescription.this, AllPagesActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(UpdatePagesDescription.this, "Unsuccessfull", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case APIServerResponse.UPDATE_PAGE_DP:
                        GroupDeleteModal groupDeleteModal1 = (GroupDeleteModal) response.body();
                        if (groupDeleteModal1.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(UpdatePagesDescription.this, "Page profile pic change successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(UpdatePagesDescription.this, AllPagesActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(UpdatePagesDescription.this, "Unsuccessfull", Toast.LENGTH_SHORT).show();
                        }

                        break;
                    case APIServerResponse.UPDATE_PAGE_BANNER:
                        GroupDeleteModal groupDeleteModal2 = (GroupDeleteModal) response.body();
                        if (groupDeleteModal2.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(UpdatePagesDescription.this, "Page cover image is changed successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(UpdatePagesDescription.this, AllPagesActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(UpdatePagesDescription.this, "Unsuccessfull", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case APIServerResponse.PAGE_DESC:
                        PageStatusMessage pageStatusMessage = (PageStatusMessage) response.body();
                        if (pageStatusMessage.getStatus().equalsIgnoreCase("1")) {
                            pageName.setText(pageStatusMessage.getList().getName() + "");
                            pageDesc.setText(pageStatusMessage.getList().getDescription() + "");
                            Glide.with(this).load(pageStatusMessage.getList().getProfile_img()).thumbnail(0.1f).fitCenter().placeholder(R.mipmap.profile_holder).into(img_view_profile_pic);
                            Glide.with(this).load(pageStatusMessage.getList().getBanner_img()).thumbnail(0.1f).fitCenter().placeholder(R.mipmap.profile_holder).into(back_cover_img_rl);
                        } else {
                            Toast.makeText(UpdatePagesDescription.this, "Unsuccessfull", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }

            } catch (Exception ex) {


            }

        }

    }


    @Override
    public void onError(int tag, Throwable throwable) {

        throwable.printStackTrace();

    }

}
