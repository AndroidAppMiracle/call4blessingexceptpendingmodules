package com.example.preetisharma.callforblessings.demo;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialcamera.MaterialCamera;
import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.HomeActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EditUserPostModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventBusEditPostModal;
import com.example.preetisharma.callforblessings.Server.Modal.FilterModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.ImageUtils;
import com.example.preetisharma.callforblessings.Utils.Utilities;
import com.example.preetisharma.callforblessings.file.FileUtils;
import com.example.preetisharma.callforblessings.video.MediaController;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 3/1/2017.
 */

public class EditUserPostActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks, ImagePickerCallback, APIServerResponse {

    AppCompatImageView img_vw_user_profile, viewholder_image, img_view_back, img_view_change_password;
    AppCompatTextView txtvw_user_name;
    AppCompatEditText edt_txt_post_details;
    AppCompatTextView txtvw_add_to_your_post, txtvw_header_title;
    @BindView(R.id.img_vw_tag_friends)
    AppCompatImageView img_vw_tag_friends;
    @BindView(R.id.img_vw_play_video)
    AppCompatImageView img_vw_play_video;

    private static final int RC_CAMERA_PERM = 342;
    private static final int RC_GALLERY_PERM = 545;
    public int responseCode = 101;
    String username, userImage, postText, postImage, postID;
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private String pickerPath = "";
    Toolbar toolbar;
    private String picChange;
    @BindView(R.id.lnr_add_to_your_post)
    LinearLayout lnr_add_to_your_post;
    ArrayList<FilterModal> arrayListTagFriends = new ArrayList<>();
    @BindView(R.id.txtvw_tagged_friends)
    AppCompatTextView txtvw_tagged_friends;
    String userId = "", videoIntentData, CompressedFileName, text;
    private static final int RESULT_CODE_COMPRESS_VIDEO = 3;
    private static final int DEMO_RECORD_VIDEO_COMPRESSED = 4;
    private Uri fileUri;
    private File tempFile;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static String[] thumbColumns = {MediaStore.Video.Thumbnails.DATA};
    public static String[] mediaColumns = {MediaStore.Video.Media._ID};
    public String pagePost = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_post_edit);
        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        img_vw_user_profile = (AppCompatImageView) findViewById(R.id.img_vw_user_profile);

        txtvw_user_name = (AppCompatTextView) findViewById(R.id.txtvw_user_name);
        edt_txt_post_details = (AppCompatEditText) findViewById(R.id.edt_txt_post_details);
        txtvw_add_to_your_post = (AppCompatTextView) findViewById(R.id.txtvw_add_to_your_post);
        viewholder_image = (AppCompatImageView) findViewById(R.id.viewholder_image);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        img_view_change_password.setVisibility(View.GONE);
        lnr_add_to_your_post = (LinearLayout) findViewById(R.id.lnr_add_to_your_post);
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        txtvw_header_title.setText("Edit Post");
        showLoading();

        if (getIntent().getExtras() != null) {
            username = getIntent().getExtras().getString(Constants.USER_NAME);
            userImage = getIntent().getExtras().getString(Constants.USER_IMAGE);
            postText = getIntent().getExtras().getString(Constants.POST_TEXT);
            postID = getIntent().getExtras().getString(Constants.POSTID);
            // postImage = getIntent().getExtras().getString(Constants.POSTIMAGE);
            picChange = getIntent().getExtras().getString(Constants.PIC_CHANGE);
            if (getIntent().getExtras().containsKey("PagePost")) {
                pagePost = getIntent().getExtras().getString("PagePost");
            }

        }

        if (getIntent().getExtras().getString(Constants.POST_IMAGE) != null) {

            if (getIntent().getExtras().getString(Constants.POST_IMAGE).equalsIgnoreCase("http://call4blessing.com/theme/images/big-no-image-found.jpg")) {
                viewholder_image.setVisibility(View.GONE);
            } else {
                postImage = getIntent().getExtras().getString(Constants.POST_IMAGE);
            }
        } else {
            viewholder_image.setVisibility(View.GONE);
        }


        //Glide.with(EditUserPostActivity.this).load(R.drawable.ic_send_post).thumbnail(0.1f).into(img_view_change_password);

        try {
            setSupportActionBar(toolbar);
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            img_view_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditUserPostActivity.this.finish();
                }
            });


            img_view_change_password.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (isConnectedToInternet()) {
                            if (!pickerPath.equals("") && !edt_txt_post_details.getText().toString().equals("")) {

                                if (pagePost.equalsIgnoreCase("1")) {
                                    if (!pickerPath.equals("")) {
                                        ServerAPI.getInstance().editUserPost(APIServerResponse.EDIT_POST, getUserSessionId(), postID, getUserID(), edt_txt_post_details.getText().toString(), pickerPath, "IMAGE", "WALL", userId, EditUserPostActivity.this);
                                    } else {
                                        ServerAPI.getInstance().editUserPost(APIServerResponse.EDIT_POST, getUserSessionId(), postID, getUserID(), edt_txt_post_details.getText().toString(), pickerPath, "IMAGE", "WALL", userId, EditUserPostActivity.this);

                                    }
                                } else {
                                    if (!pickerPath.equals("")) {
                                        ServerAPI.getInstance().editUserPost(APIServerResponse.EDIT_POST, getUserSessionId(), postID, getUserID(), edt_txt_post_details.getText().toString(), pickerPath, "IMAGE", "WALL", userId, EditUserPostActivity.this);
                                    } else {
                                        ServerAPI.getInstance().editUserPost(APIServerResponse.EDIT_POST, getUserSessionId(), postID, getUserID(), edt_txt_post_details.getText().toString(), pickerPath, "IMAGE", "WALL", userId, EditUserPostActivity.this);

                                    }
                                }
                            } else {
                                showToast("Hello", Toast.LENGTH_SHORT);
                            }


                        } else {
                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_LONG);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        txtvw_user_name.setText(username);
        if (postImage != null && postImage != "") {
            viewholder_image.setVisibility(View.VISIBLE);
            Glide.with(EditUserPostActivity.this).load(postImage).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).into(viewholder_image);
            if (picChange.equalsIgnoreCase(Constants.COVER_PIC) || picChange.equalsIgnoreCase(Constants.PROFILE_PIC)) {
                //viewholder_image.setVisibility(View.VISIBLE);
                lnr_add_to_your_post.setVisibility(View.GONE);
            } else {
                lnr_add_to_your_post.setVisibility(View.VISIBLE);
            }
        } else if (pickerPath != null && pickerPath != "") {
            viewholder_image.setVisibility(View.VISIBLE);
            Glide.with(EditUserPostActivity.this).load(pickerPath).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).into(viewholder_image);

        } else {
            viewholder_image.setVisibility(View.GONE);
        }

        Glide.with(EditUserPostActivity.this).load(userImage).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).into(img_vw_user_profile);

        edt_txt_post_details.setText(postText);


        txtvw_add_to_your_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(EditUserPostActivity.this)

                        .setTitle("Select Image")
                        .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                takePicture();
                            }
                        })
                        .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                pickImageSingle();
                            }
                        })
                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            }
        });

        hideLoading();
    }

    @OnClick(R.id.img_vw_upload_image)
    public void uploadImage() {
        new AlertDialog.Builder(this)

                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }

    @OnClick(R.id.img_vw_tag_friends)
    public void TagFriend() {
        Intent tagIntent = new Intent(this, TagFriendActivity.class);
        startActivityForResult(tagIntent, responseCode, new Bundle());
    }

    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        if (EasyPermissions.hasPermissions(EditUserPostActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(EditUserPostActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    public void taggedFriendsArrayResult() {
        arrayListTagFriends = FilterModal.getFriendsArray();
        if (arrayListTagFriends != null && arrayListTagFriends.size() > 0) {

            txtvw_tagged_friends.setVisibility(View.VISIBLE);
            if (arrayListTagFriends.size() == 1) {
                txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getLastname());
            } else if (arrayListTagFriends.size() == 2) {
                txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getLastname() + " and " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getLastname());

            } else {
                txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + (arrayListTagFriends.size() - 1) + " others");

            }
        }
    }

    @OnClick(R.id.img_vw_upload_video)
    public void uploadVideo() {
        hasPermissionInManifest(EditUserPostActivity.this, Manifest.permission.CAMERA);
        new AlertDialog.Builder(this)

                .setTitle("Select Video")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        createVideo();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickVideoSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }

    public boolean hasPermissionInManifest(Context context, String permissionName) {
        final String packageName = context.getPackageName();
        try {
            final PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equals(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {

        }
        return false;
    }

    public void createVideo() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            /*Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT,  getOutputMediaFileUri(MEDIA_TYPE_VIDEO));
            //takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(temp_file_with_mp4_extension))
            startActivityForResult(takeVideoIntent, DEMO_RECORD_VIDEO_COMPRESSED);
*/

            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            // create a file to save the video
            fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);


            // set the video image quality to high
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

            // start the Video Capture Intent
            startActivityForResult(intent, RESULT_CODE_COMPRESS_VIDEO);
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickVideoSingle() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("video/*");
            startActivityForResult(intent, RESULT_CODE_COMPRESS_VIDEO);
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    class VideoCompressor extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
            Log.d("", "Start video compression");
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (MediaController.getInstance().convertVideo(tempFile.getPath())) {
                return MediaController.getInstance().compressedFilePath();
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String compressedPath) {
            super.onPostExecute(compressedPath);
            hideLoading();
            if (compressedPath != null) {
                CompressedFileName = compressedPath;
                Log.e("File path", compressedPath);
            }
            img_vw_play_video.setVisibility(View.VISIBLE);

        }
    }

    private class DemoVideoCompressor extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
            Log.d("", "Start video compression");
        }

        @Override
        protected String doInBackground(Void... voids) {
            if (MediaController.getInstance().convertVideo(videoIntentData)) {
                return MediaController.getInstance().compressedFilePath();
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String compressedPath) {
            super.onPostExecute(compressedPath);
            hideLoading();
            if (compressedPath != null) {
                CompressedFileName = compressedPath;
                Log.e("File path", compressedPath);

            }


            //img_vw_play_video.setVisibility(View.VISIBLE);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            arrayListTagFriends = FilterModal.getFriendsArray();
            if (arrayListTagFriends != null && arrayListTagFriends.size() > 0) {

                txtvw_tagged_friends.setVisibility(View.VISIBLE);
                if (arrayListTagFriends.size() == 1) {
                    txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getLastname());
                } else if (arrayListTagFriends.size() == 2) {
                    txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getLastname() + " and " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getFirstname() + " " + arrayListTagFriends.get(1).getList().getUser_info().getProfile_details().getLastname());

                } else {
                    txtvw_tagged_friends.setText(" with " + arrayListTagFriends.get(0).getList().getUser_info().getProfile_details().getFirstname() + " " + (arrayListTagFriends.size() - 1) + " others");
                }
            }
            if (resultCode == Activity.RESULT_OK) {
                taggedFriendsArrayResult();

                if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                    if (imagePicker == null) {
                        imagePicker = new ImagePicker(this);
                    }
                    imagePicker.submit(data);
                } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                    if (cameraPicker == null) {
                        cameraPicker = new CameraImagePicker(this);
                        cameraPicker.reinitialize(pickerPath);
                    }
                    cameraPicker.submit(data);
                }
            }
            if (requestCode == DEMO_RECORD_VIDEO_COMPRESSED) {
                if (data.getData() != null) {
                    Log.i("data", "" + data.getData());
                    Log.i("dataGetPath", "" + data.getData().getPath());
                    Uri uri = Uri.fromFile(new File(data.getData().getPath()));
                    Bitmap mThumbnailVideo = ThumbnailUtils.createVideoThumbnail(data.getData().getPath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                    Bitmap bitmap = ThumbnailUtils.extractThumbnail(mThumbnailVideo, 100, 100, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                    Bitmap bt = getPreview(uri);
                    img_vw_play_video.setVisibility(View.VISIBLE);
                    viewholder_image.setVisibility(View.VISIBLE);
                    viewholder_image.setImageBitmap(bitmap);
                    videoIntentData = data.getData().getPath();
                    text = ImageUtils.saveImageOnSDCard(this, ImageUtils.getBytesFromBitmap(mThumbnailVideo));
                    text = Utilities.compressImage(text);
                    new DemoVideoCompressor().execute();
                }

                //mVideoUri = data.getData();
                //videoView.setVideoURI(mVideoUri);
            } else {
                super.onActivityResult(requestCode, resultCode, data);
                Uri uri = data.getData();

                if (requestCode == RESULT_CODE_COMPRESS_VIDEO) {
                    if (uri != null) {
                        Cursor cursor = getContentResolver().query(uri, null, null, null, null, null);

                        try {
                            if (cursor != null && cursor.moveToFirst()) {

                                String displayName = cursor.getString(
                                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                CompressedFileName = displayName;
                                int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                                String size = null;
                                if (!cursor.isNull(sizeIndex)) {
                                    size = cursor.getString(sizeIndex);
                                } else {
                                    size = "Unknown";
                                }
                                tempFile = FileUtils.saveTempFile(displayName, this, uri);
                                viewholder_image.setVisibility(View.VISIBLE);
                                ContentResolver crThumb = getContentResolver();
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inSampleSize = 1;

                                Bitmap mThumbnailVideo = ThumbnailUtils.createVideoThumbnail(tempFile.getPath(), MediaStore.Images.Thumbnails.MINI_KIND);
                                //   img_vw_play_video.setVisibility(View.VISIBLE);
                                viewholder_image.setImageBitmap(mThumbnailVideo);
                                img_vw_play_video.setImageBitmap(mThumbnailVideo);
                                img_vw_play_video.setVisibility(View.GONE);

                                text = ImageUtils.saveImageOnSDCard(this, ImageUtils.getBytesFromBitmap(mThumbnailVideo));
                                text = Utilities.compressImage(text);

                                //viewholder_image.setImageBitmap(ThumbnailUtils.createVideoThumbnail(tempFile.getPath(), MediaStore.Images.Thumbnails.MINI_KIND));
                                new VideoCompressor().execute();
                                //text= getThumbnailPathForLocalFile(this, uri);
                                Log.e("Path of file", "thumbnail" + getThumbnailPathForLocalFile(this, uri));

                                /*text = getThumbnailPathForLocalFile(this, uri);
                                 *//* thumbnailString = BitMapToString(ThumbnailUtils.createVideoThumbnail(tempFile.getPath(), MediaStore.Images.Thumbnails.MINI_KIND));
                                decodeBase64(thumbnailString);*//*
                                //create uri for thumbnail bitmap
                               *//* Uri tempUri = ImageUtils.getImageUri(CreatePostActivity.this, mThumbnailVideo);

                                // CALL THIS METHOD TO GET THE ACTUAL PATH
                                //File mUriThumbnail = new File(ImageUtils.getRealPathFromURI(CreatePostActivity.this, tempUri));
                                String thumbnailpath = ImageUtils.getFilePath(CreatePostActivity.this, tempUri);

                                thumbnailString = thumbnailpath;*//*
                               // BitMapToString(ThumbnailUtils.createVideoThumbnail(tempFile.getPath(), MediaStore.Images.Thumbnails.MINI_KIND));


                               *//* viewholder_image.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent videoIntent = new Intent(CreatePostActivity.this, VideoViewActivity.class);
                                        Bundle b = new Bundle();
                                        b.putString(Constants.FRAGMENT_NAME, "CREATEPOST");
                                        b.putString(Constants.VIDEO, tempFile.getPath());

                                        videoIntent.putExtras(b);
                                        startActivity(videoIntent);
                                    }
                                });*/
                            }
                        } finally {
                            if (cursor != null) {
                                cursor.close();
                            }
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public String getThumbnailPathForLocalFile(Activity context,
                                               Uri fileUri) {

        long fileId = getFileId(context, fileUri);

        MediaStore.Video.Thumbnails.getThumbnail(context.getContentResolver(),
                fileId, MediaStore.Video.Thumbnails.MICRO_KIND, null);

        Cursor thumbCursor = null;
        try {

            thumbCursor = context.managedQuery(
                    MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI,
                    thumbColumns, MediaStore.Video.Thumbnails.VIDEO_ID + " = "
                            + fileId, null, null);

            if (thumbCursor.moveToFirst()) {
                String thumbPath = thumbCursor.getString(thumbCursor
                        .getColumnIndex(MediaStore.Video.Thumbnails.DATA));

                return thumbPath;
            }

        } finally {
        }

        return null;
    }

    public long getFileId(Activity context, Uri fileUri) {
        Cursor cursor = context.managedQuery(fileUri, mediaColumns, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
            return cursor.getInt(columnIndex);
        }
        return 0;
    }

    Bitmap getPreview(Uri uri) {
        File image = new File(uri.getPath());

        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(image.getPath(), bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1))
            return null;

        int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
                : bounds.outWidth;

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / 2;
        return BitmapFactory.decodeFile(image.getPath(), opts);
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        ChosenImage image = list.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else
                pickerPath = image.getOriginalPath();
            viewholder_image.setVisibility(View.VISIBLE);
            viewholder_image.setImageURI(Uri.parse(pickerPath));
            viewholder_image.setImageURI(Uri.fromFile(new File(pickerPath)));
        } else
            showSnack("Invalid Image");
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
            Intent intent = intentBuilder.build(EditUserPostActivity.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            if (response.isSuccessful()) {
                EditUserPostModal editUserPostModal;
                hideLoading();
                switch (tag) {
                    case APIServerResponse.EDIT_POST:
                        editUserPostModal = (EditUserPostModal) response.body();
                        EventBus.getDefault().post(new EventBusEditPostModal(editUserPostModal.getStatus()));
                        if (editUserPostModal.getStatus().equals("1")) {
                            EventBus.getDefault().post(new EventBusEditPostModal(editUserPostModal.getStatus()));
                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                            intent.putExtra("editPost", "yes");
                            startActivity(intent);
                            finish();
                            showToast(editUserPostModal.getMessage(), Toast.LENGTH_SHORT);
                        }

                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_send, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.menu_send_item) {
            try {
                if (isConnectedToInternet()) {
                    hideKeyboard();
                    if (arrayListTagFriends != null && arrayListTagFriends.size() >= 0) {
                        for (int i = 0; i < arrayListTagFriends.size(); i++) {
                            if (i == 0) {
                                userId = String.valueOf(arrayListTagFriends.get(i).getList().getUser_info().getId());
                            } else {
                                userId += "," + String.valueOf(arrayListTagFriends.get(i).getList().getUser_info().getId());
                            }
                        }
                    }
                    Log.e("User id for tagging", "Tagged users" + userId);
                    if (!pickerPath.equals("") || !edt_txt_post_details.getText().toString().equals("")) {
                        if (!pickerPath.equalsIgnoreCase("")) {
                            showLoading();
                            ServerAPI.getInstance().editUserPost(APIServerResponse.EDIT_POST, getUserSessionId(), postID, getUserID(), edt_txt_post_details.getText().toString(), pickerPath, "IMAGE", "WALL", userId, EditUserPostActivity.this);
                        } else {
                            showLoading();
                            ServerAPI.getInstance().editUserPostWithoutImage(APIServerResponse.EDIT_POST, getUserSessionId(), postID, getUserID(), edt_txt_post_details.getText().toString(), "WALL", userId, EditUserPostActivity.this);
                        }
                    }

                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_LONG);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return super.onOptionsItemSelected(item);
    }


    private static Uri getOutputMediaFileUri(int type) {

        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // Check that the SDCard is mounted
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraVideo");


        // Create the storage directory(MyCameraVideo) if it does not exist
        if (!mediaStorageDir.exists()) {

            if (!mediaStorageDir.mkdirs()) {

                Log.e("", "Failed to create directory MyCameraVideo.");


                Log.d("MyCameraVideo", "Failed to create directory MyCameraVideo.");
                return null;
            }
        }
        java.util.Date date = new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(date.getTime());

        File mediaFile;

        if (type == MEDIA_TYPE_VIDEO) {

            // For unique video file name appending current timeStamp with file name
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");

        } else {
            return null;
        }

        return mediaFile;
    }
}
