package com.example.preetisharma.callforblessings.pages;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.HomeWallPaginationAdapterNew;
import com.example.preetisharma.callforblessings.Adapter.PagePostListAdapter;
import com.example.preetisharma.callforblessings.Adapter.PostItemsAdapter;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoHomeModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.PageStatusMessage;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CreatePostActivity;
import com.example.preetisharma.callforblessings.demo.JoyMusicMyDownloads;
import com.example.preetisharma.callforblessings.joymusicplayer.playlist.AllPlaylistActivity;
import com.example.preetisharma.callforblessings.listener.PaginationScrollListener;
import com.example.preetisharma.callforblessings.photos.PhotosTabs;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

public class SinglePageActivity extends BaseActivity implements ImagePickerCallback, EasyPermissions.PermissionCallbacks, APIServerResponse {

    @BindView(R.id.home_listing)
    RecyclerView home_listing;
    private LinearLayoutManager linearLayoutManager;
    PagePostListAdapter postItemsAdapter;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.page_name)
    AppCompatTextView page_name;
    @BindView(R.id.page_desc)
    AppCompatTextView page_desc;
    @BindView(R.id.img_vw_post)
    AppCompatImageView img_vw_post;
    @BindView(R.id.img_vw_photo)
    AppCompatImageView img_vw_photo;
    @BindView(R.id.edit_post_setting_img)
    AppCompatImageView edit_post_setting_img;
    @BindView(R.id.back_cover_img)
    ImageView back_cover_img_rl;

    @BindView(R.id.img_view_profile_pic)
    AppCompatImageView img_view_profile_pic;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;
    @BindView(R.id.img_invite_friend)
    AppCompatImageView img_invite_friend;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    static String pageId = "";
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;
    private String profileImagePath = "";
    private String coverImagePath = "";

    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int RC_GALLERY_PERM = 545;
    private static final String TAG = "CallForBlessings";
    private static final int RC_CAMERA_PERM = 342;
    private int coverImg = 0;
    private static final int PAGE_NUMBER = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int currentPage = PAGE_NUMBER;
    private int TOTAL_PAGES = 1;
    @BindView(R.id.invite_friend_layout)
    LinearLayout invite_friend_layout;

    @BindView(R.id.delete_ll)
    LinearLayout delete_ll;

    //delete_ll
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_page);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getIntent().hasExtra("pageID")) {
            pageId = getIntent().getStringExtra("pageID");
        }
        getAllDescrption();
        callApi(1);
        txtvw_header_title.setText("Your Page ");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        img_view_change_password.setVisibility(View.GONE);

        postItemsAdapter = new PagePostListAdapter(this);
        linearLayoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }

        };

        home_listing.setLayoutManager(linearLayoutManager);
        home_listing.setNestedScrollingEnabled(false);
        home_listing.setItemAnimator(new DefaultItemAnimator());
        home_listing.setAdapter(postItemsAdapter);

        if (isConnectedToInternet()) {
            try {
                currentPage = 1;
                TOTAL_PAGES = 1;
                isLastPage = false;
                postItemsAdapter.removeAll();
                ServerAPI.getInstance().getAllPagesPosts(APIServerResponse.ALLPOSTSOFPAGE, getUserSessionId(), pageId, getUserID(), String.valueOf(currentPage), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            hideLoading();
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }


        home_listing.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPages();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }


    private void loadNextPages() {

        Log.d("loadNextPage: ", "" + currentPage);
        if (isConnectedToInternet()) {
            callApi(currentPage);
        }

    }

    public void getAllDescrption() {
        if (isConnectedToInternet()) {
            try {
                ServerAPI.getInstance().getPageDesc(APIServerResponse.PAGE_DESC, getUserSessionId(), getUserID(), pageId, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            hideLoading();
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @OnClick(R.id.img_view_profile_pic)
    public void changeProfile() {
        coverImg = 1;
        new android.app.AlertDialog.Builder(this)
                .setTitle("Select Image for page ..")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }

    @OnClick(R.id.choose_cover_img)
    public void chooseImageActivity() {
        coverImg = 0;
        new android.app.AlertDialog.Builder(this)
                .setTitle("Select Cover Image for page ..")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
       /* Intent changePic = new Intent(this, CoverPictureForPageActivity.class);
        startActivity(changePic);*/
    }


    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            profileImagePath = cameraPicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        ChosenImage image = images.get(0);
        showLoading();
        if (coverImg == 0) {
            Toast.makeText(getApplicationContext(), "coverImage is selected", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "profile is selected", Toast.LENGTH_LONG).show();
        }

        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0) {
                if (coverImg == 0) {
                    //   Toast.makeText(getApplicationContext(), "coverImage is selected", Toast.LENGTH_LONG).show();
                    coverImagePath = image.getThumbnailPath();
                } else {
                    profileImagePath = image.getThumbnailPath();
                }
            } else {
                if (coverImg == 0) {
                    //Toast.makeText(getApplicationContext(), "coverImage is selected", Toast.LENGTH_LONG).show();
                    coverImagePath = image.getOriginalPath();
                } else {
                    profileImagePath = image.getOriginalPath();
                }
                profileImagePath = image.getOriginalPath();
            }

            if (coverImg == 0) {
                hideLoading();
                Drawable d = Drawable.createFromPath(coverImagePath);
                back_cover_img_rl.setBackground(d);
                if (isConnectedToInternet()) {
                    ServerAPI.getInstance().updatePageBanner(APIServerResponse.UPDATE_PAGE_BANNER, getUserSessionId(), getUserID(), pageId, coverImagePath, SinglePageActivity.this);
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            } else {
                hideLoading();
                img_view_profile_pic.setImageURI(Uri.fromFile(new File(profileImagePath)));

                if (isConnectedToInternet()) {
                    ServerAPI.getInstance().updatePageDp(APIServerResponse.UPDATE_PAGE_DP, getUserSessionId(), getUserID(), pageId, profileImagePath, SinglePageActivity.this);
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        } else
            showSnack("Invalid Image");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
           /* if (requestCode == PLACE_PICKER_REQUEST) {

                final Place place = PlacePicker.getPlace(this, data);
                final CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                String attributions = (String) place.getAttributions();


                edt_txt_location.setText(name);
                txtvw_state.setText(place.getLocale().getCountry());
                txtvw_country.setText(place.getAddress());


            }*/
                if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                    if (imagePicker == null) {
                        imagePicker = new ImagePicker(this);
                    }
                    imagePicker.submit(data);
                } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                    if (cameraPicker == null) {
                        cameraPicker = new CameraImagePicker(this);
                        cameraPicker.reinitialize(profileImagePath);
                    }
                    cameraPicker.submit(data);
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        callApi(1);
        getAllDescrption();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
            Intent intent = intentBuilder.build(this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // Handle negative button on click listener
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Let's show a toast
                //Toast.makeText(getContext(), R.string.settings_dialog_canceled, Toast.LENGTH_SHORT).show();
            }
        };

        // (Optional) Check whether the user denied permissions and checked NEVER ASK AGAIN.
        // This will display a dialog directing them to enable the permission in app settings.
        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
                "Need Permission to access your Gallery and Camera",
                R.string.setting, R.string.cancel, onClickListener, perms);
    }

    public void callApi(int pageNumber) {
        if (isConnectedToInternet()) {
            try {
                //     ServerAPI.getInstance().Home(APIServerResponse.HOME, getUserSessionId(), pageNumber, this);
                ServerAPI.getInstance().getAllPagesPosts(APIServerResponse.ALLPOSTSOFPAGE, getUserSessionId(), pageId, getUserID(), String.valueOf(currentPage), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            hideLoading();
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }


    @OnClick(R.id.open_gallery_layout)
    public void openGalleryLayout() {
        img_vw_post.setImageResource(R.drawable.post);
        img_invite_friend.setImageResource(R.drawable.invite_black);
        img_vw_photo.setImageResource(R.drawable.icon_red_camera);
        edit_post_setting_img.setImageResource(R.drawable.edit);
        Intent intentPhotos = new Intent(SinglePageActivity.this, AllPageImagesActivity.class);
        intentPhotos.putExtra("userId", getUserID());
        intentPhotos.putExtra("pageId", pageId);
        startActivity(intentPhotos);
    }

    @OnClick(R.id.invite_friend_layout)
    public void inviteFriends() {
        img_vw_post.setImageResource(R.drawable.post);
        img_invite_friend.setImageResource(R.drawable.invite_red);
        img_vw_photo.setImageResource(R.drawable.icon_camera);
        edit_post_setting_img.setImageResource(R.drawable.edit);
        Intent intent = new Intent(SinglePageActivity.this, InviteFriendsActivity.class);
        intent.putExtra("pageId", pageId);
        startActivity(intent);
    }

    @OnClick(R.id.post_new_layout)
    public void openPostActivity() {
        img_vw_post.setImageResource(R.drawable.post_red);
        img_invite_friend.setImageResource(R.drawable.invite_black);
        img_vw_photo.setImageResource(R.drawable.icon_camera);
        edit_post_setting_img.setImageResource(R.drawable.edit);

        Intent intentPhotos = new Intent(SinglePageActivity.this, CreatePagePostActivity.class);
        intentPhotos.putExtra("pageId", pageId);
        startActivity(intentPhotos);
    }

    @OnClick(R.id.edit_post_setting_layout)
    public void editPage() {

        img_vw_post.setImageResource(R.drawable.post);
        img_invite_friend.setImageResource(R.drawable.invite_black);
        img_vw_photo.setImageResource(R.drawable.icon_camera);
        edit_post_setting_img.setImageResource(R.drawable.edit_red);
        Intent changePic = new Intent(getApplicationContext(), UpdatePagesDescription.class);
        changePic.putExtra("pageId", pageId);
        startActivity(changePic);
    }

    @OnClick(R.id.delete_ll)
    public void deletePage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SinglePageActivity.this);
        builder.setTitle("Are you sure want to delete this page ?");
        builder.setCancelable(true);
        builder.setMessage(null);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isConnectedToInternet()) {
                    ServerAPI.getInstance().delete(APIServerResponse.DELETE_PAGE, getUserSessionId(), getUserID(), pageId, SinglePageActivity.this);
                } else {
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }


    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        if (response.isSuccessful()) {
            DemoHomeModal demoHomeModal;
            hideLoading();
            try {
                switch (tag) {
                    case APIServerResponse.DELETE_PAGE:
                        GroupDeleteModal groupDeleteModal = (GroupDeleteModal) response.body();
                        if (groupDeleteModal.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(SinglePageActivity.this, "page deleted successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SinglePageActivity.this, AllPagesActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(SinglePageActivity.this, "Unsuccessfull", Toast.LENGTH_SHORT).show();
                        }

                        break;
                    case APIServerResponse.UPDATE_PAGE_DP:
                        GroupDeleteModal groupDeleteModal1 = (GroupDeleteModal) response.body();
                        if (groupDeleteModal1.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(SinglePageActivity.this, "Page profile pic change successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SinglePageActivity.this, AllPagesActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(SinglePageActivity.this, "Unsuccessfull", Toast.LENGTH_SHORT).show();
                        }

                        break;
                    case APIServerResponse.UPDATE_PAGE_BANNER:
                        GroupDeleteModal groupDeleteModal2 = (GroupDeleteModal) response.body();
                        if (groupDeleteModal2.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(SinglePageActivity.this, "Page cover image is changed successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(SinglePageActivity.this, AllPagesActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(SinglePageActivity.this, "Unsuccessfull", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case APIServerResponse.PAGE_DESC:
                        PageStatusMessage pageStatusMessage = (PageStatusMessage) response.body();
                        if (pageStatusMessage.getStatus().equalsIgnoreCase("1")) {
                            page_name.setText(pageStatusMessage.getList().getName() + "");
                            page_desc.setText(pageStatusMessage.getList().getDescription() + "");
                            if (pageStatusMessage.getList().getIs_owner().equalsIgnoreCase("0")) {
                                invite_friend_layout.setVisibility(View.GONE);
                                delete_ll.setVisibility(View.GONE);
                            }
                            Glide.with(this).load(pageStatusMessage.getList().getProfile_img()).override(800, 800).thumbnail(0.1f).fitCenter().listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    progress_bar.setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    progress_bar.setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(img_view_profile_pic);
                            Glide.with(this).load(pageStatusMessage.getList().getBanner_img()).thumbnail(0.1f).fitCenter().into(back_cover_img_rl);
                        } else {
                            Toast.makeText(SinglePageActivity.this, "Unsuccessfull", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case APIServerResponse.ALLPOSTSOFPAGE:
                        demoHomeModal = (DemoHomeModal) response.body();
                        postItemsAdapter.removeAll();
                        Log.d("All Post Data", response.body().toString());
                        if (demoHomeModal.getStatus().equalsIgnoreCase("1")) {
                            if (demoHomeModal.getPosts().size() != 0) {
                                TOTAL_PAGES = Integer.parseInt(demoHomeModal.getTotalPages());
                                currentPage = Integer.parseInt(demoHomeModal.getCurrentPage());
                                List<DemoHomeModal.PostsBean> listBeen = new ArrayList<>();
                                listBeen = demoHomeModal.getPosts();

                                postItemsAdapter.addAll(listBeen);
                                postItemsAdapter.pagePostComment = "pagePostComment";
                                home_listing.setAdapter(postItemsAdapter);
                                postItemsAdapter.notifyDataSetChanged();
                                if (currentPage < TOTAL_PAGES) postItemsAdapter.addLoadingFooter();
                                else isLastPage = true;


                            } else {
                                //TOTAL_PAGES = currentPage;
                                // txtvw_no_posts.setVisibility(View.VISIBLE);
                            }
                            /*  }*//*
                            home_listing.setAdapter(postItemsAdapter);
                            postItemsAdapter.notifyDataSetChanged();*/
                        }

                        break;

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }


    @Override
    public void onError(String s) {
        Log.d("Error not found", "" + s);
    }

}
