package com.example.preetisharma.callforblessings.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Fragment.MoreFragment;
import com.example.preetisharma.callforblessings.ProfileActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.FetchProfileModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.LogoutModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.SignInActivity;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.settings.general_setting.UpdateGeneralSettingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 9/20/2017.
 */

public class SettingsGeneral extends BaseActivity implements APIServerResponse {


    @BindView(R.id.atv_general_setting_name)
    AppCompatTextView atv_general_setting_name;

    @BindView(R.id.cv_setting_item)
    CardView cv_setting_item;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @BindView(R.id.atv_setting_value)
    AppCompatTextView atv_setting_value;
    @BindView(R.id.atv_setting_email_value)
    AppCompatTextView atv_setting_email_value;
    @BindView(R.id.atv_setting_phone_value)
    AppCompatTextView atv_setting_phone_value;

    Switch locationSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_general);
        ButterKnife.bind(this);
        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("General Settings");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        locationSwitch = (Switch) findViewById(R.id.switch1);

        getUserDetail();


        locationSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (isConnectedToInternet()) {
                        try {
                            ServerAPI.getInstance().accountDeactivate(APIServerResponse.DEACTIVATE, getUserSessionId(), SettingsGeneral.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        hideLoading();
                        showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

    }


    public void getUserDetail() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().fetchPeofile(APIServerResponse.FETCHPROFILE, getUserSessionId(),getUserID(), this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }

    }

    public void logout() {
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().logOut(APIServerResponse.LOGOUT, getUserSessionId(), getUserID()/*,getFirebaseToken()*/,SettingsGeneral.this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @OnClick(R.id.txtvw_update)
    public void updateSetting() {
        Intent intent = new Intent(SettingsGeneral.this, ProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void onSuccess(int tag, Response response) {
        hideLoading();
        try {
            if (response.isSuccessful()) {
                hideLoading();
                switch (tag) {
                    case APIServerResponse.DEACTIVATE:
                        GroupDeleteModal addEventModal = (GroupDeleteModal) response.body();
                        if (addEventModal.getStatus().equals("1")) {
                            showToast(addEventModal.getMessage(), Toast.LENGTH_LONG);
                            logout();
                        } else {
                            showToast(addEventModal.getMessage(), Toast.LENGTH_LONG);
                        }
                        break;
                    case APIServerResponse.LOGOUT:
                        LogoutModal logout = (LogoutModal) response.body();
                        if (logout.getStatus().equals("1")) {
                            showToast("successfully logged out!", Toast.LENGTH_SHORT);
                            clearPreferences();
                            Intent i = new Intent(SettingsGeneral.this, SignInActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            //getActivity().finish();

                        } else {
                            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                        break;
                    case APIServerResponse.FETCHPROFILE:
                        FetchProfileModal fetchProfile = (FetchProfileModal) response.body();
                        if (fetchProfile.getStatus().equals("1")) {
                            atv_setting_value.setText(fetchProfile.getDetail().getProfile_details().getFirstname()+""+fetchProfile.getDetail().getProfile_details().getLastname());

                            atv_setting_phone_value.setText(fetchProfile.getDetail().getProfile_details().getPhone());
                            atv_setting_email_value.setText(fetchProfile.getDetail().getEmail());
                        }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
