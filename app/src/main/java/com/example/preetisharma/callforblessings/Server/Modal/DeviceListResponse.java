package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by satoti.garg on 10/27/2017.
 */

public class DeviceListResponse {
    private String status;

    private List<DeviceObject> list;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<DeviceObject> getList ()
    {
        return list;
    }

    public void setList (List<DeviceObject> list)
    {
        this.list = list;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", list = "+list+"]";
    }
}
