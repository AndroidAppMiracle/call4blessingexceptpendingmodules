package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/12/2017.
 */

public class PagesList {

    private String id;

    private String description;

    private String name;

    private String created_by_me;

    private String banner_img;

    private String invited;

    private String profile_img;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCreated_by_me ()
    {
        return created_by_me;
    }

    public void setCreated_by_me (String created_by_me)
    {
        this.created_by_me = created_by_me;
    }

    public String getBanner_img ()
    {
        return banner_img;
    }

    public void setBanner_img (String banner_img)
    {
        this.banner_img = banner_img;
    }

    public String getInvited ()
    {
        return invited;
    }

    public void setInvited (String invited)
    {
        this.invited = invited;
    }

    public String getProfile_img ()
    {
        return profile_img;
    }

    public void setProfile_img (String profile_img)
    {
        this.profile_img = profile_img;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", description = "+description+", name = "+name+", created_by_me = "+created_by_me+", banner_img = "+banner_img+", invited = "+invited+", profile_img = "+profile_img+"]";
    }
}
