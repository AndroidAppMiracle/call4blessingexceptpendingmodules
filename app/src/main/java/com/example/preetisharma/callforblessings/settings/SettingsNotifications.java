package com.example.preetisharma.callforblessings.settings;

import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.SettingNotificationAdapter;
import com.example.preetisharma.callforblessings.Fragment.MoreFragment;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.NotificationObjectResponse;
import com.example.preetisharma.callforblessings.Server.Modal.NotificationReposne;
import com.example.preetisharma.callforblessings.Server.Modal.NotificationsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 7/24/2017.
 */

public class SettingsNotifications extends BaseActivity implements APIServerResponse {

    List<String> settingsNamesList = new ArrayList<>();
    List<String> settingsList = new ArrayList<>();

    List<NotificationObjectResponse> notificationObjectResponse;
//rv_security_settings


    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @BindView(R.id.rv_security_settings)
    RecyclerView rv_security_settings;

    SettingNotificationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_notifications);
        ButterKnife.bind(this);

        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("Notifications");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getNotificationSettingDetail();
        rv_security_settings.setItemAnimator(new DefaultItemAnimator());
        rv_security_settings.setLayoutManager(new LinearLayoutManager(SettingsNotifications.this));
    }

    public void getNotificationSettingDetail() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getMyNotificationSetting(APIServerResponse.GET_ALL_NOTIFICATION, getUserSessionId(),getUserID(), SettingsNotifications.this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            settingsNamesList.clear();
            settingsList.clear();
            switch (tag) {
                case APIServerResponse.GET_ALL_NOTIFICATION:
                   // JsonObject jsonObject=(JsonObject)response.body();
                   NotificationReposne notificationsModal = (NotificationReposne) response.body();
                    if (notificationsModal.getStatus().equalsIgnoreCase("1")) {
                        notificationObjectResponse = notificationsModal.getDetail();
                        adapter = new SettingNotificationAdapter(SettingsNotifications.this,getUserID(), notificationObjectResponse);
                        rv_security_settings.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        hideLoading();
                    } else {
                        Toast.makeText(getApplicationContext(), "No Notification found", Toast.LENGTH_SHORT).show();
                        hideLoading();
                    }

                    break;
              /*  case APIServerResponse.GET_ALL_NOTIFICATION:
                    NotificationReposne notificationsModal = (NotificationReposne) response.body();
                    if (notificationsModal.getStatus().equalsIgnoreCase("1")) {
                        notificationObjectResponse = notificationsModal.getDetail();
                        adapter = new SettingNotificationAdapter(SettingsNotifications.this, notificationObjectResponse);
                        rv_security_settings.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getApplicationContext(), "No Nottification found", Toast.LENGTH_SHORT).show();
                    }
                    hideLoading();
                    break;*/
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            hideLoading();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
