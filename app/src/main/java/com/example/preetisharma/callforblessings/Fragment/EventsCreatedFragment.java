package com.example.preetisharma.callforblessings.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.MyEventsCreatedAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.MyEventsCreatedModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/7/2017.
 */

public class EventsCreatedFragment extends Fragment implements APIServerResponse {


    public List<MyEventsCreatedModal.ListBean> list = new ArrayList<MyEventsCreatedModal.ListBean>();

    public MyEventsCreatedAdapter myEventsCreatedAdapter;

    public RecyclerView rv_my_events_recycler_view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_events_created_layout, container, false);
        ButterKnife.bind(this, rootView);

        rv_my_events_recycler_view = (RecyclerView) rootView.findViewById(R.id.rv_my_events_recycler_view);

        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().getMyCreatedEvents(APIServerResponse.MY_CREATED_EVENTS, ((BaseActivity) getActivity()).getUserSessionId(), EventsCreatedFragment.this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ((BaseActivity) getActivity()).showLoading();
            ServerAPI.getInstance().getMyCreatedEvents(APIServerResponse.MY_CREATED_EVENTS, ((BaseActivity) getActivity()).getUserSessionId(), EventsCreatedFragment.this);
        } else {
            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            MyEventsCreatedModal myEventsCreatedModal;
            if (response.isSuccessful()) {

                switch (tag) {

                    case APIServerResponse.MY_CREATED_EVENTS:
                        myEventsCreatedModal = (MyEventsCreatedModal) response.body();
                        if (myEventsCreatedModal.getStatus().equals("1")) {

                            list = myEventsCreatedModal.getList();
                        } else {
                            ((BaseActivity) getActivity()).showToast("No Events Created", Toast.LENGTH_LONG);
                        }

                        myEventsCreatedAdapter = new MyEventsCreatedAdapter(getActivity(), list);

                        rv_my_events_recycler_view.setAdapter(myEventsCreatedAdapter);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        rv_my_events_recycler_view.setLayoutManager(mLayoutManager);

                        break;
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onError(int tag, Throwable throwable) {

        try {

            switch (tag) {
                case APIServerResponse.MY_CREATED_EVENTS:

                    throwable.printStackTrace();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
