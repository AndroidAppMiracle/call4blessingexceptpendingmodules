package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.PlayListSongDescrption;
import com.example.preetisharma.callforblessings.Server.Modal.PlayListSongsListObject;
import com.example.preetisharma.callforblessings.Server.Modal.PlaylistSongDetail;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.joymusicplayer.AudioPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 8/1/2017.
 */

public class DemoDataPlayListSongsAdaper extends RecyclerView.Adapter<DemoDataPlayListSongsAdaper.DataViewHolder> {

    ArrayList<PlayMusicModal> arrayListMusic = new ArrayList<>();
    private Context mContext;
    private List<PlayListSongDescrption> myGroupsList = new ArrayList<>();
    private SparseBooleanArray selectedItems;

    String playlistId;

    public DemoDataPlayListSongsAdaper(Context mContext, List<PlayListSongDescrption> list, String playlistId) {
        this.myGroupsList = list;
        this.mContext = mContext;
        this.playlistId = playlistId;
        selectedItems = new SparseBooleanArray();
    }

    @Override
    public DemoDataPlayListSongsAdaper.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_notifications_item, parent, false);
        DemoDataPlayListSongsAdaper.DataViewHolder dataView = new DemoDataPlayListSongsAdaper.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DemoDataPlayListSongsAdaper.DataViewHolder holder, final int position) {

        try {
            holder.tv_notification_text.setText(myGroupsList.get(position).getSong_name());
            holder.tv_notification_date.setVisibility(View.INVISIBLE);

            holder.cv_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, AudioPlayerActivity.class);
                    intent.putExtra(AudioPlayerActivity.EXTRA_INDEX, position);
                    intent.putExtra(AudioPlayerActivity.PLAYLIST_ID_TAG, playlistId);
                    intent.putExtra(Constants.SONG_NAME, myGroupsList.get(position).getSong_name());
                    intent.putExtra(Constants.ALBUM_COVER, myGroupsList.get(position).getAlbum_cover_image());
                    intent.putExtra(Constants.MUSIC, Constants.SONG);
                    intent.putExtra(Constants.ALBUM_NAME, myGroupsList.get(position).getAlbum_name());
                    intent.putExtra(Constants.TYPE, Constants.SONG_CAPS);
                    intent.putExtra(Constants.SONG_ID, myGroupsList.get(position).getSong_id());
                    intent.putExtra(Constants.SONG_DOWNLOAD_URL, myGroupsList.get(position).getMusic_file());
                    intent.putExtra(Constants.POSITION, position);
                    intent.putExtra(Constants.IS_DOWNLOADED, myGroupsList.get(position).getIs_downloaded());
                    mContext.startActivity(intent);
                }
            });

           /* Glide.with(mContext)
                    .load(myGroupsList.get(position).get())
                    *//*.listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            holder.mProgress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            // image ready, hide progress now
                            holder.mProgress.setVisibility(View.GONE);
                            return false;   // return false if you want Glide to handle everything else.
                        }
                    })*//*
                    *//*.diskCacheStrategy(DiskCacheStrategy.RESULT)   // cache both original & resized image*//*
                    .centerCrop()
                    .override(64, 64)
                    .thumbnail(0.1f)
                    .crossFade()
                    .into(holder.aiv_notification_image);
*/
            holder.aiv_notification_image.setImageResource(R.drawable.black_lock);
            holder.itemView.setActivated(selectedItems.get(position, false));
            holder.del_playlist.setVisibility(View.VISIBLE);
            holder.del_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().deletSongFromPlaylist(APIServerResponse.DELETE_SONG_PLAYLIST, ((BaseActivity) mContext).getUserSessionId(), playlistId, String.valueOf(myGroupsList.get(position).getSong_id()), new APIServerResponse() {
                                @Override
                                public void onSuccess(int tag, Response response) {
                                    GroupDeleteModal deleteModal = (GroupDeleteModal) response.body();
                                    if (deleteModal.getStatus().equalsIgnoreCase("1")) {
                                        ((BaseActivity) mContext).showToast(deleteModal.getMessage(), Toast.LENGTH_SHORT);
                                        ((BaseActivity) mContext).hideLoading();
                                        removeAt(position);
                                    }
                                }

                                @Override
                                public void onError(int tag, Throwable throwable) {
                                    throwable.printStackTrace();
                                    ((BaseActivity) mContext).hideLoading();
                                }
                            });
                        } else {
                            ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                            ((BaseActivity) mContext).hideLoading();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        ((BaseActivity) mContext).hideLoading();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ((BaseActivity) mContext).hideLoading();
        }

    }


    public void removeAt(int position) {
        myGroupsList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, myGroupsList.size());
    }

    @Override
    public int getItemCount() {
        if (myGroupsList.size() != 0) {
            return myGroupsList.size();
        } else {
            return 0;
        }

    }

    public PlayListSongDescrption getItem(int position) {
        return myGroupsList.get(position);
    }

    public void toggleSelection(int pos) {
        /*currentSelectedIndex = pos;*/
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            /*animationItemsIndex.delete(pos);*/
        } else {
            selectedItems.put(pos, true);
            /*animationItemsIndex.put(pos, true);*/
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        /*reverseAllAnimations = true;*/
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        myGroupsList.remove(position);
        //resetCurrentIndex();
    }


    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_notification_text)
        AppCompatTextView tv_notification_text;

        @BindView(R.id.aiv_notification_image)
        AppCompatImageView aiv_notification_image;

        @BindView(R.id.tv_notification_date)
        AppCompatTextView tv_notification_date;

        @BindView(R.id.del_playlist)
        ImageView del_playlist;
        @BindView(R.id.cv_layout)
        CardView cv_layout;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            del_playlist.setVisibility(View.VISIBLE);

            /*txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });*/


            /*ll_songItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(mContext, PlaylistSongsList.class);
                    *//*Bundle b = new Bundle();
                    b.putString(Constants.GROUP_ID, String.valueOf(myGroupsList.get(getAdapterPosition()).getId()));
                    i.putExtras(b);*//*
                    mContext.startActivity(i);

                   *//* if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ((BaseActivity) mContext).showLoading();
                        ServerAPI.getInstance().deleteGroup(APIServerResponse.GROUP_DELETE, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(myGroupsList.get(getAdapterPosition()).getId()), DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }*//*
                }
            });*/

            /*txtvw_my_event_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });*/
        }

    }


}