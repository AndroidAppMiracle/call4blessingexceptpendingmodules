package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.QuestionsModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by preeti.sharma on 1/6/2017.
 */

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.DataViewHolder> {
    public List<QuestionsModal.ListBean> list;
    Activity mContext;
    private static int countNext = 0;
    LinkedHashMap<String, String> hashmap = new LinkedHashMap<>();
    LinkedHashMap<String, String> hashmapAns = new LinkedHashMap<>();
    RadioButton rd_btn;

    public QuestionsAdapter(Activity mContext, List<QuestionsModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<QuestionsModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public QuestionsAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.questions_item_layout, parent, false);
        QuestionsAdapter.DataViewHolder dataView = new QuestionsAdapter.DataViewHolder(v);

        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final QuestionsAdapter.DataViewHolder holder, final int position) {
        holder.txtvw_question.setText(list.get(position).getQuestion_title());
        holder.first_ans.setText(list.get(position).getOption_a());
        holder.second_ans.setText(list.get(position).getOption_b());
        holder.third_ans.setText(list.get(position).getOption_c());
        holder.fourth_ans.setText(list.get(position).getOption_d());


    }

    public int answercheck(HashMap<String, String> ques, HashMap<String, String> answer) {
        int count = 0;




            for (String key : ques.keySet())
                if (ques.get(key).equals(answer.get(key))) {

                    count++;
                }

        return count;

    }

    public int getcount() {
        return answercheck(hashmap, hashmapAns);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtvw_question)
        AppCompatTextView txtvw_question;
        @BindView(R.id.rd_grp_ans)
        RadioGroup rd_grp_ans;
        @BindView(R.id.first_ans)
        RadioButton first_ans;
        @BindView(R.id.second_ans)
        RadioButton second_ans;
        @BindView(R.id.third_ans)
        RadioButton third_ans;
        @BindView(R.id.fourth_ans)
        RadioButton fourth_ans;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            rd_grp_ans.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    hashmapAns.put(String.valueOf(list.get(getAdapterPosition()).getId()), list.get(getAdapterPosition()).getRight_answer());
                    if (first_ans.isChecked()) {
                        hashmap.put(String.valueOf(list.get(getAdapterPosition()).getId()), "option_a");

                    } else if (second_ans.isChecked()) {
                        hashmap.put(String.valueOf(list.get(getAdapterPosition()).getId()), "option_b");
                    } else if (third_ans.isChecked()) {
                        hashmap.put(String.valueOf(list.get(getAdapterPosition()).getId()), "option_c");
                    } else if (fourth_ans.isChecked()) {
                        hashmap.put(String.valueOf(list.get(getAdapterPosition()).getId()), "option_d");
                    } else {
                        ((BaseActivity) mContext).showToast("Please select an option", Toast.LENGTH_SHORT);
                    }
                    Log.e("Clicked pos tag", "position" + hashmap.size());


                }
            });

        }
    }
}
