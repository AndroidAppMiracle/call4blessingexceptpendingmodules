package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.BooksAdapter;
import com.example.preetisharma.callforblessings.Adapter.EventsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EventsListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.QuestionsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/24/2017.
 */

public class EventsListing extends BaseActivity implements APIServerResponse {
    @BindView(R.id.events_recycler_view)
    RecyclerView events_recycler_view;
    BooksAdapter booksAdapter;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    //@BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    public List<QuestionsModal.ListBean> list;
    private List<EventsListingModal.ListBean> mList;
    EventsAdapter _adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_event_listing);
        updateStatusBar();
        ButterKnife.bind(this);
        txtvw_header_title.setText("Events");
        img_view_change_password.setImageResource(R.drawable.ic_add);

        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        if (isConnectedToInternet()) {

            ServerAPI.getInstance().eventsList(APIServerResponse.EVENTS_LIST, this);
            showLoading();
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
        img_view_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addEvent = new Intent(EventsListing.this, AddEventActivity.class);
                startActivity(addEvent);
            }
        });


        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventsListing.this.finish();
            }
        });

    }


    @Override
    public void onSuccess(int tag, Response response) {


        hideLoading();

        EventsListingModal eventsListingModal;

        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.EVENTS_LIST:

                    {
                        eventsListingModal = (EventsListingModal) response.body();


                        if (eventsListingModal.getStatus().equals("1")) {
                            Log.e("List data", "List data" + eventsListingModal.getList());
                            mList = eventsListingModal.getList();

                        }

                    }
                    _adapter = new EventsAdapter(this, mList);
                    events_recycler_view.setAdapter(_adapter);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                    events_recycler_view.setLayoutManager(mLayoutManager);

                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {


    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.e("Psot resumed", "resume");
        if (isConnectedToInternet()) {

            ServerAPI.getInstance().eventsList(APIServerResponse.EVENTS_LIST, this);
            showLoading();
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
