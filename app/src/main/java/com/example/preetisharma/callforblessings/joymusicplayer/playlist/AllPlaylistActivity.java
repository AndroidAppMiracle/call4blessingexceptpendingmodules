package com.example.preetisharma.callforblessings.joymusicplayer.playlist;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.AllPlayListAdapter;
import com.example.preetisharma.callforblessings.Adapter.CustomePlayListPopAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AllPlayList;
import com.example.preetisharma.callforblessings.Server.Modal.PlaylistObject;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class AllPlaylistActivity extends BaseActivity implements APIServerResponse,SwipeRefreshLayout.OnRefreshListener {

    RecyclerView play_list_pop_rv;
    List<PlaylistObject> playlistObjects;
    AllPlayListAdapter allPlayListAdapter;
    String songId;
    SwipeRefreshLayout swipeRefreshLayout;
    AppCompatTextView txtvw_header_title;
    AppCompatImageView img_view_back, img_view_change_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_playlist);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        swipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllPlaylistActivity.this.finish();
            }
        });
        txtvw_header_title.setText("All Playlists ");
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);
        img_view_change_password.setImageResource(R.drawable.ic_add);
        img_view_change_password.setVisibility(View.GONE);
        play_list_pop_rv = (RecyclerView) findViewById(R.id.play_list_pop_rv);
        getAllPlayListName();
        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        getAllPlayListName();
                    }
                }
        );
    }


    @Override
    protected void onResume() {
        super.onResume();
        getAllPlayListName();
    }

    public void getAllPlayListName() {
        try {
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().getAllPlayList(APIServerResponse.ALL_PLAY_LIST, getUserSessionId(), getUserID(), this);

            } else {
                showSnack("Not connected to Internet ");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {
                AllPlayList allPlayList;
                switch (tag) {
                    case APIServerResponse.ALL_PLAY_LIST:
                        allPlayList = (AllPlayList) response.body();
                        if (allPlayList.getStatus().equalsIgnoreCase("1")) {
                            playlistObjects = new ArrayList<>();
                            PlaylistObject playlistObject = new PlaylistObject();
                            playlistObject.setName("Create play list");
                            playlistObject.setCreated_at("");
                            playlistObject.setCreated_by("");
                            playlistObject.setId("0");
                            playlistObjects.add(playlistObject);

                            List<PlaylistObject> playlistObjectList = allPlayList.getList();
                            playlistObjects.addAll(playlistObjectList);
                        } else {
                            playlistObjects = new ArrayList<>();
                            PlaylistObject playlistObject = new PlaylistObject();
                            playlistObject.setName("Create play list");
                            playlistObject.setCreated_at("");
                            playlistObject.setCreated_by("");
                            playlistObject.setId("0");
                            playlistObjects.add(playlistObject);
                        }
                        allPlayListAdapter = new AllPlayListAdapter(AllPlaylistActivity.this, playlistObjects);
                        play_list_pop_rv.setAdapter(allPlayListAdapter);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AllPlaylistActivity.this);
                        play_list_pop_rv.setLayoutManager(mLayoutManager);
                        allPlayListAdapter.notifyDataSetChanged();
                        hideLoading();
                        swipeRefreshLayout.setEnabled(false);
                        break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    @Override
    public void onRefresh() {
        getAllPlayListName();
    }
}
