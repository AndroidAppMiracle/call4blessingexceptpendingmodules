package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by Kshitiz Bali on 1/18/2017.
 */

public class HaleTestimonyModal {

    /**
     * status : 1
     */

    private String status;

    private String hallelujau;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHallelujau() {
        return hallelujau;
    }

    public void setHallelujau(String hallelujau) {
        this.hallelujau = hallelujau;
    }
}
