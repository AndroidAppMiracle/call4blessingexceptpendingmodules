package com.example.preetisharma.callforblessings.joymusicplayer.playlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.DemoDataPlayListAdapter;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 7/26/2017.
 */

public class PlaylistListing extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.main_collapsing_bar_tab_layout)
    CollapsingToolbarLayout main_collapsing_bar_tab_layout;
    @BindView(R.id.iv_playlist_list_banner)
    ImageView iv_playlist_list_banner;
    @BindView(R.id.main_toolbar)
    Toolbar main_toolbar;
    @BindView(R.id.nestedscroll)
    NestedScrollView nestedscroll;
    @BindView(R.id.rv_playlist)
    RecyclerView rv_playlist;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode;


    List<DemoDataModal> list = new ArrayList<>();

    DemoDataPlayListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist_listing);
        ButterKnife.bind(this);

        setSupportActionBar(main_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        main_collapsing_bar_tab_layout.setTitle("My Playlists");
        swipeRefreshLayout.setOnRefreshListener(this);


        rv_playlist.setLayoutManager(new GridLayoutManager(this, 2));
        rv_playlist.setItemAnimator(new DefaultItemAnimator());
        rv_playlist.addItemDecoration(new DividerItemDecoration(this, GridLayoutManager.VERTICAL));
        rv_playlist.setNestedScrollingEnabled(false);


        main_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaylistListing.this.finish();
            }
        });

        actionModeCallback = new ActionModeCallback();

        // show loader and fetch messages
        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        addItemsToList();
                    }
                }
        );


        ItemClickSupport.addTo(rv_playlist).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                if (adapter.getSelectedItemCount() > 0) {
                    enableActionMode(position);
                } else {
                    Intent i = new Intent(PlaylistListing.this, PlaylistSongsList.class);
                    Bundle b = new Bundle();
                    b.putString(Constants.PLAYLIST_ID, String.valueOf(list.get(position).getId()));
                    i.putExtras(b);
                    startActivity(i);
                }

            }
        }).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                enableActionMode(position);
                return true;
            }
        });

    }

    @Override
    public void onRefresh() {
        addItemsToList();
    }


    public void addItemsToList() {

        if (!list.isEmpty()) {
            list.clear();
        }
        for (int i = 0; i < 10; i++) {
            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setName("Playlist " + (i + 1));
            demoDataModal.setId(i);
            demoDataModal.setImage("http://www.happybirthdayvinyl.co.uk/images/album-artwork/big/grace-jones-nightclubbing-deluxe-edition-disc1.jpg");
            list.add(demoDataModal);
        }
        swipeRefreshLayout.setRefreshing(false);

        adapter = new DemoDataPlayListAdapter(PlaylistListing.this, list);
        rv_playlist.setAdapter(adapter);
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_delete, menu);

            // disable swipe refresh if action mode is enabled
            swipeRefreshLayout.setEnabled(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_delete_item:
                    // delete all the selected messages
                    deleteMessages();
                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            adapter.clearSelections();
            swipeRefreshLayout.setEnabled(true);
            actionMode = null;
            /*recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.resetAnimationIndex();
                    // mAdapter.notifyDataSetChanged();
                }
            });*/
        }
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {

            actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        adapter.toggleSelection(position);
        int count = adapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    private void deleteMessages() {
        List<Integer> selectedItemPositions =
                adapter.getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            adapter.removeData(selectedItemPositions.get(i));
        }
        adapter.notifyDataSetChanged();
    }

}
