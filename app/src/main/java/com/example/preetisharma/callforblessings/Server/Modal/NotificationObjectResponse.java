package com.example.preetisharma.callforblessings.Server.Modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by satoti.garg on 10/25/2017.
 */

public class NotificationObjectResponse implements Parcelable{

    private String setting ;

    private String value;

    protected NotificationObjectResponse(Parcel in) {
        setting = in.readString();
        value = in.readString();
    }

    public static final Creator<NotificationObjectResponse> CREATOR = new Creator<NotificationObjectResponse>() {
        @Override
        public NotificationObjectResponse createFromParcel(Parcel in) {
            return new NotificationObjectResponse(in);
        }

        @Override
        public NotificationObjectResponse[] newArray(int size) {
            return new NotificationObjectResponse[size];
        }
    };

    public String getSetting ()
    {
        return setting;
    }

    public void setSetting (String setting)
    {
        this.setting = setting;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [setting = "+setting+", value = "+value+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(setting);
        dest.writeString(value);
    }
}
