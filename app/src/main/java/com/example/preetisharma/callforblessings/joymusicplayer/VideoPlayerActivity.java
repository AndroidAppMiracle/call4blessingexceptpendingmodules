package com.example.preetisharma.callforblessings.joymusicplayer;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.callforblessings.R;
import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.devbrackets.android.playlistcore.listener.PlaylistListener;
import com.devbrackets.android.playlistcore.manager.BasePlaylistManager;
import com.devbrackets.android.playlistcore.service.PlaylistServiceCore;
import com.example.preetisharma.callforblessings.Utils.CallForBlessingsApplication;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class VideoPlayerActivity extends Activity implements PlaylistListener<MediaItem> {
    public static final String EXTRA_INDEX = "EXTRA_INDEX";
    public static int PLAYLIST_ID = 6; //Arbitrary, for the example (different from audio)
    public static final String PLAYLIST_FLAG = "playlist_id";

    protected VideoView videoView;
    protected PlaylistManager playlistManager;

    protected int selectedIndex;
    protected boolean pausedInOnStop = false;
    private ArrayList<PlayMusicModal> list;
    private boolean isFirstPlayed = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_player_activity);

        retrieveExtras();
        init();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (videoView.isPlaying()) {
            pausedInOnStop = true;
            videoView.pause();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (pausedInOnStop) {
            videoView.start();
            pausedInOnStop = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        playlistManager.unRegisterPlaylistListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        playlistManager = CallForBlessingsApplication.getPlaylistManager();
        playlistManager.registerPlaylistListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        playlistManager.invokeStop();
    }

    @Override
    public boolean onPlaylistItemChanged(MediaItem currentItem, boolean hasNext, boolean hasPrevious) {
        return false;
    }

    @Override
    public boolean onPlaybackStateChanged(@NonNull PlaylistServiceCore.PlaybackState playbackState) {
        if (playbackState == PlaylistServiceCore.PlaybackState.STOPPED) {
            finish();

            /*playlistManager.setVideoPlayer(new VideoApi(videoView));
            playlistManager.play(, false);*/

            return true;
        } else if (playbackState == PlaylistServiceCore.PlaybackState.ERROR) {
            showErrorMessage();
        }
        /* else if (playbackState == PlaylistServiceCore.PlaybackState.PLAYING) {
            //playlistManager.invokeRepeat();
            playlistManager.play(0, false);
        }*/

        return false;
    }

    /**
     * Retrieves the extra associated with the selected playlist index
     * so that we can start playing the correct item.
     */
    protected void retrieveExtras() {
        Bundle extras = getIntent().getExtras();
        selectedIndex = extras.getInt(EXTRA_INDEX, 0);
        PLAYLIST_ID = extras.getInt(PLAYLIST_FLAG, 0);
        Bundle b = getIntent().getBundleExtra("video_bundle");
        list = b.getParcelableArrayList(Constants.ALBUM_SONGS_LIST);

    }

    protected void init() {
        setupPlaylistManager();

        videoView = (VideoView) findViewById(R.id.video_play_activity_video_view);

        playlistManager.setVideoPlayer(new VideoApi(videoView));
        playlistManager.play(0, false);
    }

    protected void showErrorMessage() {
        new AlertDialog.Builder(this)
                .setTitle("Playback Error")
                .setMessage(String.format("There was an error playing \"%s\"", playlistManager.getCurrentItem().getTitle()))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .show();
    }

    /**
     * Retrieves the playlist instance and performs any generation
     * of content if it hasn't already been performed.
     */
    private void setupPlaylistManager() {
        playlistManager = CallForBlessingsApplication.getPlaylistManager();

        List<MediaItem> mediaItems = new LinkedList<>();
        for (PlayMusicModal sample : list) {
            MediaItem mediaItem = new MediaItem(sample, false);
            mediaItems.add(mediaItem);
        }

        playlistManager.setAllowedMediaType(BasePlaylistManager.AUDIO | BasePlaylistManager.VIDEO);
        playlistManager.setParameters(mediaItems, selectedIndex);
        playlistManager.setId(PLAYLIST_ID);
    }
}
