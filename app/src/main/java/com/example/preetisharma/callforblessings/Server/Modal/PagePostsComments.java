package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/13/2017.
 */

public class PagePostsComments {
    private String content;

    private String page_post_id;

    private String id;

    private String user_id;

    public String getContent ()
    {
        return content;
    }

    public void setContent (String content)
    {
        this.content = content;
    }

    public String getPage_post_id ()
    {
        return page_post_id;
    }

    public void setPage_post_id (String page_post_id)
    {
        this.page_post_id = page_post_id;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [content = "+content+", page_post_id = "+page_post_id+", id = "+id+", user_id = "+user_id+"]";
    }
}
