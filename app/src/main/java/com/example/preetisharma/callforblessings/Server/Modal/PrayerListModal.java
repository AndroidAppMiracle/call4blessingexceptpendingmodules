package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/16/2017.
 */

public class PrayerListModal {
    private PrayerCreatedByModal created_by;

    private String comment_count;

    private String desc;

    private String video_link;

    private String image;

    private String amen_flag;

    private String id;

    private String title;

    private String like_count;

    private String name;

    private String amen_count;

    private String like_flag;

    private String[] comments;

    public PrayerCreatedByModal getCreated_by() {
        return created_by;
    }

    public void setCreated_by(PrayerCreatedByModal created_by) {
        this.created_by = created_by;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getVideo_link() {
        return video_link;
    }

    public void setVideo_link(String video_link) {
        this.video_link = video_link;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAmen_flag() {
        return amen_flag;
    }

    public void setAmen_flag(String amen_flag) {
        this.amen_flag = amen_flag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmen_count() {
        return amen_count;
    }

    public void setAmen_count(String amen_count) {
        this.amen_count = amen_count;
    }

    public String getLike_flag() {
        return like_flag;
    }

    public void setLike_flag(String like_flag) {
        this.like_flag = like_flag;
    }

    public String[] getComments() {
        return comments;
    }

    public void setComments(String[] comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "ClassPojo [created_by = " + created_by + ", comment_count = " + comment_count + ", desc = " + desc + ", video_link = " + video_link + ", image = " + image + ", amen_flag = " + amen_flag + ", id = " + id + ", title = " + title + ", like_count = " + like_count + ", name = " + name + ", amen_count = " + amen_count + ", like_flag = " + like_flag + ", comments = " + comments + "]";
    }
}
