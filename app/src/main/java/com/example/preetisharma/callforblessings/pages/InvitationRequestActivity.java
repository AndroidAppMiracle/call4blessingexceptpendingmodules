package com.example.preetisharma.callforblessings.pages;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.InvitationRequestAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.InvitationRequestsList;
import com.example.preetisharma.callforblessings.Server.Modal.InvitedRequest;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class InvitationRequestActivity extends BaseActivity implements APIServerResponse {


    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @BindView(R.id.rv_mutual_friends)
    RecyclerView rv_mutual_friends;

    InvitationRequestsList invitationRequestsList;
    InvitationRequestAdapter invitationRequestAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation_request);
        ButterKnife.bind(this);

        img_view_change_password.setVisibility(View.GONE);

        txtvw_header_title.setText("Invitation");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InvitationRequestActivity.this.finish();
            }
        });
        getAllInvitationRequest();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        InvitationRequestActivity.this.finish();
    }

    public void getAllInvitationRequest() {
        showLoading();
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().getAllInvitationRequest(APIServerResponse.INVITATION_REQUEST, getUserSessionId(), getUserID(), InvitationRequestActivity.this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {

        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.INVITATION_REQUEST:
                        invitationRequestsList = (InvitationRequestsList) response.body();
                        hideLoading();

                        if(swipe_refresh_layout.isRefreshing())
                        {
                            swipe_refresh_layout.setRefreshing(false);
                        }
                        if (invitationRequestsList.getStatus().equalsIgnoreCase("1")) {
                            invitationRequestAdapter = new InvitationRequestAdapter(InvitationRequestActivity.this, invitationRequestsList.getList());
                            rv_mutual_friends.setAdapter(invitationRequestAdapter);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(InvitationRequestActivity.this);
                            rv_mutual_friends.setLayoutManager(mLayoutManager);
                        } else
                        {
                            Toast.makeText(getApplicationContext(),"No Invitation found",Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(getApplicationContext(), "Please check your Internet Server", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
