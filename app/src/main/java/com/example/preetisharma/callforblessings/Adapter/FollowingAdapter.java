package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.MaterialUpConceptActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.DemoFriendsModal;
import com.example.preetisharma.callforblessings.Server.Modal.FriendRequestAcceptedModal;
import com.example.preetisharma.callforblessings.Server.Modal.FriendRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.MutualFriendModal;
import com.example.preetisharma.callforblessings.Server.Modal.PostDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.ShareModal;
import com.example.preetisharma.callforblessings.Server.Modal.TimeLineModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CommentsListingActivity;
import com.example.preetisharma.callforblessings.demo.EditUserPostActivity;
import com.example.preetisharma.callforblessings.demo.SharePostActivity;
import com.example.preetisharma.callforblessings.demo.ViewFullScreenImage;
import com.example.preetisharma.callforblessings.joymusicplayer.FullScreenVideoPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;
import com.example.preetisharma.callforblessings.joymusicplayer.VideoPlayerActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 9/21/2017.
 */

public class FollowingAdapter extends RecyclerView.Adapter<FollowingAdapter.DataViewHolder> {

    public List<MutualFriendModal.List> list;
    Context mContext;
    private static int countNext = 0;
    RadioButton rd_btn;

    public FollowingAdapter(Context context, List<MutualFriendModal.List> list) {
        this.list = list;
        this.mContext = context;
    }

    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List</*MyFriendsModal.ListBean*/MutualFriendModal.List> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public FollowingAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_adapter_friends_list, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    /*@NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.homescreen_item_adapter_layout, parent, false);
        viewHolder = new FollowingAdapter.MovieVH(v1);
        return viewHolder;
    }*/

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
        holder.layout_response_buttons.setVisibility(View.GONE);
        holder.txtvw_friends_request_name.setText(list.get(position)./**/getProfile_details().getFirstname() + " " + list.get(position).getProfile_details().getLastname());
        Glide.with(mContext).load(list.get(position).getProfile_details().getProfile_pic()).placeholder(R.drawable.placeholder_callforblessings).into(holder.iv_friend_request_image);
        holder.ll_my_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("user id", String.valueOf(list.get(holder.getAdapterPosition()).getId()));
                Intent i = new Intent(mContext, MaterialUpConceptActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.SHARED_PREF_USER_ID, String.valueOf(list.get(holder.getAdapterPosition()).getId()));
                b.putBoolean(Constants.TIMELINE_ENABLED, false);
                b.putInt(Constants.IS_FRIEND, Constants.USER_REQUEST_ACCEPT);
                b.putString("friendClicked", "yes");
                b.putString(Constants.USER_IMAGE, list.get(holder.getAdapterPosition()).getProfile_details().getProfile_pic());
                b.putString(Constants.COVER_PIC, list.get(holder.getAdapterPosition()).getProfile_details().getCover_pic());
                b.putString(Constants.USER_NAME, list.get(holder.getAdapterPosition()).getProfile_details().getFirstname() + " " + list.get(holder.getAdapterPosition()).getProfile_details().getLastname());
                b.putString(Constants.PRIVACY_WALL, list.get(holder.getAdapterPosition()).getWALL());
                b.putString(Constants.PRIVACY_ABOUT_INFO, list.get(holder.getAdapterPosition()).getABOUT_INFO());
                b.putString(Constants.PRIVACY_FRIEND_REQUEST, list.get(holder.getAdapterPosition()).getFRIEND_REQUEST());
                b.putString(Constants.PRIVACY_MESSAGE, list.get(holder.getAdapterPosition()).getMESSAGE());
                i.putExtras(b);
                mContext.startActivity(i);
            }
        });

    }

    public int getcount() {
        return list.size();

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtvw_friends_request_name)
        AppCompatTextView txtvw_friends_request_name;
        @BindView(R.id.iv_friend_request_image)
        AppCompatImageView iv_friend_request_image;
        @BindView(R.id.layout_response_buttons)
        LinearLayout layout_response_buttons;
        @BindView(R.id.ll_my_friend)
        LinearLayout ll_my_friend;
        @BindView(R.id.txtvw_ignore_friend_request)
        AppCompatTextView txtvw_ignore_friend_request;
        @BindView(R.id.txtvw_accept_friend_request)
        AppCompatTextView txtvw_accept_friend_request;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            txtvw_ignore_friend_request.setVisibility(View.GONE);
            txtvw_accept_friend_request.setText("Unfriend");
            txtvw_accept_friend_request.setVisibility(View.GONE);
        }

    }

    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        list.remove(position);
        notifyItemRemoved(position);
    }

}

