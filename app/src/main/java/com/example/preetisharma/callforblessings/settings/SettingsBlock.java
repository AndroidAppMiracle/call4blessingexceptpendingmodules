package com.example.preetisharma.callforblessings.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.BlockListUserAdapter;
import com.example.preetisharma.callforblessings.Adapter.InviteFriendAdapter;
import com.example.preetisharma.callforblessings.Adapter.MutualFriendsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.MutualFriendModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.pages.InviteFriendsActivity;
import com.example.preetisharma.callforblessings.pages.SinglePageActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 9/20/2017.
 */

public class SettingsBlock extends BaseActivity implements APIServerResponse {
    private List<MutualFriendModal.List> mList;
    BlockListUserAdapter _adapter;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.rv_block_users)
    RecyclerView rv_block_users;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_block);
        ButterKnife.bind(this);
        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("Block List");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getAllBlockUsers();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isConnectedToInternet()) {
                  getAllBlockUsers();
                } else {
                    hideLoading();
                    showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        });
    }

    public void getAllBlockUsers() {
        try {
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().getAllBlockUser(APIServerResponse.ALL_BLOCK_LIST, getUserSessionId(), getUserID(),this);

            } else {
                showSnack("Not connected to Internet ");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onSuccess(int tag, Response response) {

        mList = new ArrayList<MutualFriendModal.List>();
        hideLoading();
        MutualFriendModal userListingModal;
        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.ALL_BLOCK_LIST:
                        userListingModal = (MutualFriendModal) response.body();
                        if (userListingModal.getStatus().equals("1")) {
                            Log.e("List data", "List data" + userListingModal.getList().size());
                            mList = userListingModal.getList();
                        } else {
                        }
                        _adapter = new BlockListUserAdapter(SettingsBlock.this, mList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SettingsBlock.this);
                        rv_block_users.setLayoutManager(mLayoutManager);
                        rv_block_users.setAdapter(_adapter);
                        _adapter.notifyDataSetChanged();

                        if(swipeRefreshLayout.isRefreshing())
                        {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
