package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 11/2/2017.
 */

public class PlayListSongDescrption {
    private String album_id;

    private String song_id;

    private String is_downloaded;

    private String song_name;

    private String music_file;

    private String album_cover_image;

    private String album_name;

    private String payment_type;

    public String getAlbum_id ()
    {
        return album_id;
    }

    public void setAlbum_id (String album_id)
    {
        this.album_id = album_id;
    }

    public String getSong_id ()
    {
        return song_id;
    }

    public void setSong_id (String song_id)
    {
        this.song_id = song_id;
    }

    public String getIs_downloaded ()
    {
        return is_downloaded;
    }

    public void setIs_downloaded (String is_downloaded)
    {
        this.is_downloaded = is_downloaded;
    }

    public String getSong_name ()
    {
        return song_name;
    }

    public void setSong_name (String song_name)
    {
        this.song_name = song_name;
    }

    public String getMusic_file ()
    {
        return music_file;
    }

    public void setMusic_file (String music_file)
    {
        this.music_file = music_file;
    }

    public String getAlbum_cover_image ()
    {
        return album_cover_image;
    }

    public void setAlbum_cover_image (String album_cover_image)
    {
        this.album_cover_image = album_cover_image;
    }

    public String getAlbum_name ()
    {
        return album_name;
    }

    public void setAlbum_name (String album_name)
    {
        this.album_name = album_name;
    }

    public String getPayment_type ()
    {
        return payment_type;
    }

    public void setPayment_type (String payment_type)
    {
        this.payment_type = payment_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [album_id = "+album_id+", song_id = "+song_id+", is_downloaded = "+is_downloaded+", song_name = "+song_name+", music_file = "+music_file+", album_cover_image = "+album_cover_image+", album_name = "+album_name+", payment_type = "+payment_type+"]";
    }
}
