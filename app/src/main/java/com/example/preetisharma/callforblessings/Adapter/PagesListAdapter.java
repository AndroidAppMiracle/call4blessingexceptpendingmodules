package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.MyBooksModal;
import com.example.preetisharma.callforblessings.Server.Modal.PagesList;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.pages.SinglePageActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 10/12/2017.
 */

public class PagesListAdapter extends RecyclerView.Adapter<PagesListAdapter.DataViewHolder> {
    public List<PagesList> list;
    Activity mContext;
    private static int countNext = 0;
//   RadioButton rd_btn;


    public PagesListAdapter(Activity mContext, List<PagesList> list) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public PagesListAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custome_create_page_layout, parent, false);
        PagesListAdapter.DataViewHolder dataView = new PagesListAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(PagesListAdapter.DataViewHolder holder, final int position) {
        Glide.with(mContext).load(list.get(position).getProfile_img()).placeholder(R.drawable.placeholder_callforblessings).into(holder.pageImg);
        holder.pageName.setText(list.get(position).getName() + "");
        holder.pageDesc.setText(list.get(position).getDescription() + "");
        holder.created_by_me.setVisibility(View.VISIBLE);

        if(list.get(position).getCreated_by_me().equalsIgnoreCase("1")) {
            holder.created_by_me.setText("(Admin)");
        }else
        {
            holder.created_by_me.setText("(Member)");
        }
        holder.cv_createPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, SinglePageActivity.class);
                intent.putExtra("pageID",list.get(position).getId());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_vw_user_profile)
        AppCompatImageView pageImg;

        @BindView(R.id.txtvw_page_name)
        AppCompatTextView pageName;
        @BindView(R.id.txtvw_time_stamp)
        AppCompatTextView pageDesc;
        @BindView(R.id.created_by_me)
        AppCompatTextView created_by_me;
        //created_by_me
        @BindView(R.id.cv_createPage)
        CardView cv_createPage;

        //txtvw_time_stamp
        public DataViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
