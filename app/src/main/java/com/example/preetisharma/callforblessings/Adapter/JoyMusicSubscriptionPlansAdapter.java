package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.PaymentGatewayActivity;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSubscriptionPlansModal;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.BookDetailsActivity;
import com.example.preetisharma.callforblessings.demo.JoyMusicPaymentGatewayActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Kshitiz Bali on 5/11/2017.
 */

public class JoyMusicSubscriptionPlansAdapter extends RecyclerView.Adapter<JoyMusicSubscriptionPlansAdapter.DataViewHolder> {
    public List<JoyMusicSubscriptionPlansModal.ListBean> list;
    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;
    private Resources resources;

    public JoyMusicSubscriptionPlansAdapter(Activity mContext, List<JoyMusicSubscriptionPlansModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
        resources = this.mContext.getResources();
    }


    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<JoyMusicSubscriptionPlansModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public JoyMusicSubscriptionPlansAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_joy_music_subscription_item, parent, false);
        JoyMusicSubscriptionPlansAdapter.DataViewHolder dataView = new JoyMusicSubscriptionPlansAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final JoyMusicSubscriptionPlansAdapter.DataViewHolder holder, final int position) {

        String textStock = String.format(resources.getString(R.string.price_in_indian_rupees), list.get(position).getOffer_amount());

        if (list.get(position).getOffer_amount().equalsIgnoreCase("")) {
            holder.atv_amount.setVisibility(View.GONE);
            holder.atv_offer_amount.setText(String.format(resources.getString(R.string.price_in_indian_rupees), list.get(position).getAmount()));
            holder.atv_time_period.setText(list.get(position).getTime_period());
        } else {
            holder.atv_amount.setVisibility(View.VISIBLE);
            holder.atv_amount.setText(String.format(resources.getString(R.string.price_in_indian_rupees), list.get(position).getAmount()));
            holder.atv_offer_amount.setText(String.format(resources.getString(R.string.price_in_indian_rupees), list.get(position).getOffer_amount()));
            holder.atv_time_period.setText(list.get(position).getTime_period());
        }


    }

    public int getcount() {
        return list.size();

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.atv_amount)
        AppCompatTextView atv_amount;
        @BindView(R.id.atv_offer_amount)
        AppCompatTextView atv_offer_amount;
        @BindView(R.id.atv_time_period)
        AppCompatTextView atv_time_period;
        @BindView(R.id.ll_subscription_item)
        LinearLayout ll_subscription_item;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        @OnClick(R.id.ll_subscription_item)
        public void buySubscriptionPlan() {

            Intent purchasebookIntent = new Intent(mContext, JoyMusicPaymentGatewayActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.BUY_JOY_MUSIC_SUBSCRIPTION_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
            b.putString(Constants.BUY_JOY_MUSIC_SUBSCRIPTION_AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
            b.putString(Constants.BUY_JOY_MUSIC_SUBSCRIPTION_NAME, list.get(getAdapterPosition()).getTime_period());
            purchasebookIntent.putExtras(b);
            mContext.startActivity(purchasebookIntent);
            mContext.finish();

            /*if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }*/


        }

    }
}
