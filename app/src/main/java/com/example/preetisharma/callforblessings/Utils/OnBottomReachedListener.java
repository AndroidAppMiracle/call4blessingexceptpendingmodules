package com.example.preetisharma.callforblessings.Utils;

public interface OnBottomReachedListener {

    void onBottomReached(int position);

}
