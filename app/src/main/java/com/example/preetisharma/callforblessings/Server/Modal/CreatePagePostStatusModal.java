package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/13/2017.
 */

public class CreatePagePostStatusModal {
    private String message;

    private PagePostCreateModal detail;

    private String status;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public PagePostCreateModal getDetail ()
    {
        return detail;
    }

    public void setDetail (PagePostCreateModal detail)
    {
        this.detail = detail;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", detail = "+detail+", status = "+status+"]";
    }
}
