package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 5/22/2017.
 */

public class EventBusRemovedBookimagesModal {


    private final int imageCount;

    public EventBusRemovedBookimagesModal(int count) {
        this.imageCount = count;
    }

    public int getMessage() {
        return imageCount;
    }
}
