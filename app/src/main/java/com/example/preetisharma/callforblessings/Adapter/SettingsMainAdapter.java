package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsMainModal;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.SettingsDetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by satoti.garg on 6/27/2017.
 */

public class SettingsMainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public List<SettingsMainModal.ListBean> list;
    Activity mContext;
    private static final int ITEM = 0;
    private static final int ITEM_WITH_SPACE = 1;
    private int[] settingsImages = {R.mipmap.ic_settings_wall,
            R.mipmap.ic_setting_message,
            R.mipmap.ic_setting_about_info,
            R.mipmap.ic_settings_friend_req,
            R.mipmap.ic_settings_friend_req,
            R.mipmap.ic_settings_friend_req,
            R.mipmap.ic_settings_friend_req,
            R.mipmap.ic_settings_friend_req
    };

    public SettingsMainAdapter(Activity mContext, List<SettingsMainModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


 /*   public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }*/

  /*  public void addAllItems(List<MyBooksModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }*/


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;

        switch (viewType) {
            case ITEM:
                v = inflater.inflate(R.layout.adapter_settings_main_item, parent, false);
                viewHolder = new SettingsMainAdapter.DataViewHolder(v);
                Constants.setAppFont((ViewGroup) v, parent.getContext());
                break;
            case ITEM_WITH_SPACE:
                v = inflater.inflate(R.layout.adapter_settings_main_item_with_spaces, parent, false);
                viewHolder = new SettingsMainAdapter.DataViewHolderWithSpace(v);
                Constants.setAppFont((ViewGroup) v, parent.getContext());
                break;

        }
       /* View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_settings_main_item, parent, false);
        SettingsMainAdapter.DataViewHolder dataView = new SettingsMainAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());*/
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {


        switch (getItemViewType(position)) {
            case ITEM:

                final SettingsMainAdapter.DataViewHolder dataViewHolder = (SettingsMainAdapter.DataViewHolder) holder;

                dataViewHolder.atv_setting_name.setText(list.get(position).getName());

                dataViewHolder.aiv_setting_image.setImageResource(settingsImages[position]);
                //Glide.with(mContext).load(settingsImages[position]).placeholder(R.drawable.placeholder)/*.thumbnail(0.1f)*/.crossFade()/*.override(64, 64)*/.into(holder.aiv_setting_image);
                dataViewHolder.cv_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent i = new Intent(mContext, SettingsDetailActivity.class);
                        Bundle b = new Bundle();
                        b.putString(Constants.SETTING_ID, String.valueOf(list.get(holder.getAdapterPosition()).getId()));
                        b.putString(Constants.SETTING_VALUE, list.get(holder.getAdapterPosition()).getName());
                        i.putExtras(b);
                        mContext.startActivity(i);
                    }
                });

                break;

            case ITEM_WITH_SPACE:
                final SettingsMainAdapter.DataViewHolderWithSpace dataViewHolderWithSpace = (SettingsMainAdapter.DataViewHolderWithSpace) holder;
                dataViewHolderWithSpace.atv_setting_name.setText(list.get(position).getName());
                dataViewHolderWithSpace.aiv_setting_image.setImageResource(settingsImages[position]);
                //Glide.with(mContext).load(settingsImages[position]).placeholder(R.drawable.placeholder)/*.thumbnail(0.1f)*/.crossFade()/*.override(64, 64)*/.into(holder.aiv_setting_image);

                dataViewHolderWithSpace.cv_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent i = new Intent(mContext, SettingsDetailActivity.class);
                        Bundle b = new Bundle();
                        b.putString(Constants.SETTING_ID, String.valueOf(list.get(holder.getAdapterPosition()).getId()));
                        b.putString(Constants.SETTING_VALUE, list.get(holder.getAdapterPosition()).getName());
                        i.putExtras(b);
                        mContext.startActivity(i);
                    }
                });

                break;
        }


    }


    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 2 || position == 6) {

            return ITEM_WITH_SPACE;
        } else {
            return ITEM;
        }

        /*return super.getItemViewType(position);*/
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.atv_setting_name)
        AppCompatTextView atv_setting_name;

        @BindView(R.id.aiv_setting_image)
        AppCompatImageView aiv_setting_image;

        @BindView(R.id.cv_item)
        CardView cv_item;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }


    public class DataViewHolderWithSpace extends RecyclerView.ViewHolder {

        @BindView(R.id.atv_setting_name)
        AppCompatTextView atv_setting_name;

        @BindView(R.id.aiv_setting_image)
        AppCompatImageView aiv_setting_image;

        @BindView(R.id.cv_item)
        CardView cv_item;


        public DataViewHolderWithSpace(View v) {
            super(v);
            ButterKnife.bind(this, v);


        }

        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }
}
