package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSongsModal;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.JoyMusicViewAllAcitivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kshitiz Bali on 5/9/2017.
 */

public class JoyMusicAdapterSongs extends RecyclerView.Adapter<JoyMusicAdapterSongs.DataViewHolder> {
    private List<JoyMusicSongsModal.ListBean> mList;
    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;
    private SongsListAdapter songsListAdapter;

    public JoyMusicAdapterSongs(Activity mContext, List<JoyMusicSongsModal.ListBean> list) {
        this.mList = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        mList.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(ArrayList<JoyMusicSongsModal.ListBean> list) {
        this.mList.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public JoyMusicAdapterSongs.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.joy_music_item, parent, false);
        JoyMusicAdapterSongs.DataViewHolder dataView = new JoyMusicAdapterSongs.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    int i = 0;

    @Override
    public void onBindViewHolder(final JoyMusicAdapterSongs.DataViewHolder holder, final int position) {
        holder.album_title.setText(mList.get(holder.getAdapterPosition()).getLanguage());

        JoyMusicSongsModal.ListBean.SongsBean songsData = null;
        List<JoyMusicSongsModal.ListBean.SongsBean> songsList = new ArrayList<>();
        for (int i = 0; i < mList.get(holder.getAdapterPosition()).getSongs().size(); i++) {
            songsData = new JoyMusicSongsModal.ListBean.SongsBean();
            songsData.setSong_name(mList.get(holder.getAdapterPosition()).getSongs().get(i).getSong_name());
            //albumData.setId(mList.get(holder.getAdapterPosition()).getSongs().get(i).getgetId());
            songsData.setId(mList.get(holder.getAdapterPosition()).getSongs().get(i).getId());
            songsData.setMusic_file(mList.get(holder.getAdapterPosition()).getSongs().get(i).getMusic_file());
            songsData.setAlbum_name(mList.get(holder.getAdapterPosition()).getSongs().get(i).getAlbum_name());
            songsData.setAlbum_cover_image(mList.get(holder.getAdapterPosition()).getSongs().get(i).getAlbum_cover_image());
            songsData.setPayment_type(mList.get(holder.getAdapterPosition()).getSongs().get(i).getPayment_type());
            songsData.setIs_downloaded(mList.get(holder.getAdapterPosition()).getSongs().get(i).getIs_downloaded());
            songsList.add(songsData);
            //songsBeanArrayList.add(songsData);

        }

        /*mList.get(holder.getAdapterPosition()).getId()*/
        songsListAdapter = new SongsListAdapter(mContext, songsList, mList.get(holder.getAdapterPosition()).getId());
        holder.joy_music_album_recycler_view.setAdapter(songsListAdapter);
        holder.joy_music_album_recycler_view.scrollToPosition(0);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        holder.joy_music_album_recycler_view.setLayoutManager(mLayoutManager);


       /* holder.rl_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("CLICKLANG", "" + holder.getAdapterPosition());
            }
        });*/

    }

    public int getcount() {
        if (mList.size() != 0) {
            return mList.size();
        } else {
            return 0;
        }


    }

    @Override
    public int getItemCount() {
        if (mList.size() != 0) {
            return mList.size();
        } else {
            return 0;
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.album_title)
        AppCompatTextView album_title;
        @BindView(R.id.joy_music_album_recycler_view)
        RecyclerView joy_music_album_recycler_view;

        @BindView(R.id.atv_viewAll)
        AppCompatTextView atv_viewAll;

      /*  @BindView(R.id.ll_language_songs)
        RelativeLayout ll_language_songs;

        @BindView(R.id.rl_list)
        RelativeLayout rl_list;*/

       /* @BindView(R.id.horizontal_scroll)
        HorizontalScrollView horizontal_scroll;
        @BindView(R.id.linear_layout_scroll)
        LinearLayout linear_layout_scroll;
        @BindView(R.id.albums_preview)
        LinearLayout albums_preview;
*/

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            atv_viewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //ViewAll API

                    Intent listSongs = new Intent(mContext, JoyMusicViewAllAcitivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.LANGUAGE_ID, String.valueOf(mList.get(getAdapterPosition()).getId()));
                    bundle.putString(Constants.VIEW_ALL_FLAG, Constants.SONG);
                    bundle.putString(Constants.LANGUAGE_NAME, mList.get(getAdapterPosition()).getLanguage().trim());
                    //bundle.putString(Constants.ALBUM_NAME, mList.get(holder.getAdapterPosition()).getAlbum_name());
                    bundle.putInt(Constants.PLAYLIST_ID_VIEW_ALL, mList.get(getAdapterPosition()).getId());
                    listSongs.putExtras(bundle);
                    mContext.startActivity(listSongs);


                }
            });


        }
    }
}

