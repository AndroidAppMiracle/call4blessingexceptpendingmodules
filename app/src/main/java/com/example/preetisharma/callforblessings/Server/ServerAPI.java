package com.example.preetisharma.callforblessings.Server;


import android.content.SharedPreferences;

import com.example.preetisharma.callforblessings.OtpRequestActivity;
import com.example.preetisharma.callforblessings.ProfileActivity;
import com.example.preetisharma.callforblessings.QuestionsActivity;
import com.example.preetisharma.callforblessings.RequestActivity;
import com.example.preetisharma.callforblessings.Server.Modal.AddEventModal;
import com.example.preetisharma.callforblessings.Server.Modal.AlarmRequestAcceptModal;
import com.example.preetisharma.callforblessings.Server.Modal.AlarmRequestCountModal;
import com.example.preetisharma.callforblessings.Server.Modal.AlbumListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.AllAlbumImageResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AllAlbumList;
import com.example.preetisharma.callforblessings.Server.Modal.AllPagesModal;
import com.example.preetisharma.callforblessings.Server.Modal.AllPagesPost;
import com.example.preetisharma.callforblessings.Server.Modal.AllPlayList;
import com.example.preetisharma.callforblessings.Server.Modal.AllPlaylistSongObject;
import com.example.preetisharma.callforblessings.Server.Modal.AmenPrayerModal;
import com.example.preetisharma.callforblessings.Server.Modal.BlockResponse;
import com.example.preetisharma.callforblessings.Server.Modal.BookDetailsModal;
import com.example.preetisharma.callforblessings.Server.Modal.BookPaymentModal;
import com.example.preetisharma.callforblessings.Server.Modal.BooksBuyFreeBookModal;
import com.example.preetisharma.callforblessings.Server.Modal.BooksLanguagesModal;
import com.example.preetisharma.callforblessings.Server.Modal.BooksListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.BuyMusicDownloadedModal;
import com.example.preetisharma.callforblessings.Server.Modal.ChangePasswordModal;
import com.example.preetisharma.callforblessings.Server.Modal.ChangeProfileCoverPicModal;
import com.example.preetisharma.callforblessings.Server.Modal.CheckBlockFollow;
import com.example.preetisharma.callforblessings.Server.Modal.CommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.CommentsListModal;
import com.example.preetisharma.callforblessings.Server.Modal.CreateAlbumResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CreateGroupModal;
import com.example.preetisharma.callforblessings.Server.Modal.CreateGroupPostModal;
import com.example.preetisharma.callforblessings.Server.Modal.CreatePageModal;
import com.example.preetisharma.callforblessings.Server.Modal.DeleteEventModal;
import com.example.preetisharma.callforblessings.Server.Modal.DemoHomeModal;
import com.example.preetisharma.callforblessings.Server.Modal.DeviceListResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EditEventModal;
import com.example.preetisharma.callforblessings.Server.Modal.EditUserPostModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventDetailsModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventsInvitesModal;
import com.example.preetisharma.callforblessings.Server.Modal.FollowResponse;
import com.example.preetisharma.callforblessings.Server.Modal.ForgotPasswordModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupHomeWallModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupHomeWallPostLike;
import com.example.preetisharma.callforblessings.Server.Modal.GroupListingModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupPostDetailCommentsModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupPostViewModal;
import com.example.preetisharma.callforblessings.Server.Modal.HaleTestimonyModal;
import com.example.preetisharma.callforblessings.Server.Modal.HelpRequestListModal;
import com.example.preetisharma.callforblessings.Server.Modal.HelpRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicMyDownloadsModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSearchModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSongsModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSubscriptionCheckModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicSubscriptionPlansModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicViewAllAlbumModal;
import com.example.preetisharma.callforblessings.Server.Modal.JoyMusicViewAllSongsModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.MessagesModal;
import com.example.preetisharma.callforblessings.Server.Modal.MutualFriendModal;
import com.example.preetisharma.callforblessings.Server.Modal.MutualFriendResponce;
import com.example.preetisharma.callforblessings.Server.Modal.MyBooksModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyEventsCreatedModal;
import com.example.preetisharma.callforblessings.Server.Modal.MyInvitationRequestAcceptReject;
import com.example.preetisharma.callforblessings.Server.Modal.MyPrayerModal;
import com.example.preetisharma.callforblessings.Server.Modal.NotificationReposne;
import com.example.preetisharma.callforblessings.Server.Modal.NotificationsModal;
import com.example.preetisharma.callforblessings.Server.Modal.PageImagesArray;
import com.example.preetisharma.callforblessings.Server.Modal.PageStatusMessage;
import com.example.preetisharma.callforblessings.Server.Modal.PlaylistCreateResponse;
import com.example.preetisharma.callforblessings.Server.Modal.PlaylistSongsResponse;
import com.example.preetisharma.callforblessings.Server.Modal.PostDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.PrayerIndexModal;
import com.example.preetisharma.callforblessings.Server.Modal.PrayerRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.ProfileImageModal;
import com.example.preetisharma.callforblessings.Server.Modal.RemoveProfileModal;
import com.example.preetisharma.callforblessings.Server.Modal.SendEventInvitationModal;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsGetValueModal;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsMainModal;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsSetModal;
import com.example.preetisharma.callforblessings.Server.Modal.ShareModal;
import com.example.preetisharma.callforblessings.Server.Modal.SinglePostViewModal;
import com.example.preetisharma.callforblessings.Server.Modal.TestimonyModal;
import com.example.preetisharma.callforblessings.Server.Modal.TestimonyModalNew;
import com.example.preetisharma.callforblessings.Server.Modal.UPloadBookModal;
import com.example.preetisharma.callforblessings.Server.Modal.UnfriendAFriendModal;
import com.example.preetisharma.callforblessings.Server.Modal.UpdateGroupModal;
import com.example.preetisharma.callforblessings.Server.Modal.UplAlbumImageResponse;
import com.example.preetisharma.callforblessings.Server.Modal.VideoOnWallModal;
import com.example.preetisharma.callforblessings.SignInActivity;
import com.example.preetisharma.callforblessings.SignUpActivity;
import com.example.preetisharma.callforblessings.TestimonyRequestActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.pages.InviteFriendsActivity;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import okhttp3.Connection;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServerAPI {
    private static ServerAPI ourInstance = new ServerAPI();
    /*private static HttpLoggingInterceptor logging = new HttpLoggingInterceptor();*/
    private Retrofit mRetrofit;
    private APIReference apiReference;
    private SharedPreferences sharedPrefs;

    public ServerAPI() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        //  logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
       /* httpClient.networkInterceptors().add(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request request=chain.request();
                Request.Builder requestBuilder = request.newBuilder()
                        .addHeader("Content-Type", "application/x-www-form-urlencoded");
                request = requestBuilder.build();
                return chain.proceed(request);
            }

        });*/

// add your other interceptors …

// add logging as last interceptor
        httpClient.addNetworkInterceptor(logging);
        httpClient.addInterceptor(new AddHeaderInterceptor());
        // <-- this is the important line!
        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl("http://call4blessing.com/")//"http://www.call4blessing.com/"*/
               /* .baseUrl("http://dev.miracleglobal.com/cal-php/web/")*/
                    .addConverterFactory(GsonConverterFactory.create()).client(httpClient.build())
                    .build();
            apiReference = mRetrofit.create(APIReference.class);
        }

    }


    public class AddHeaderInterceptor implements Interceptor {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {

            Request.Builder builder = chain.request().newBuilder();
            builder.addHeader("Content-Type", "application/x-www-form-urlencoded");
            return chain.proceed(builder.build());
        }
    }


    public static ServerAPI getInstance() {
        return ourInstance;
    }

    public void register(final int tag, String email, String pass, String device_id, String device_type, String first_name, String last_name, String gender, String date_of_birth, String phone_number, String timezone, String actualDeviceId, final OtpRequestActivity apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("UserModel[email]", email);
        params.put("UserModel[password_hash]", pass);
        params.put("login-form[device_id]", device_id);
        params.put("login-form[type]", device_type);
        params.put("ProfileModel[firstname]", first_name);
        params.put("ProfileModel[lastname]", last_name);
        params.put("ProfileModel[gender]", gender);
        params.put("ProfileModel[DOB]", date_of_birth);
        params.put("ProfileModel[phone]", phone_number);
        params.put("login-form[timezone]", timezone);
        params.put("login-form[actual_device_id]", actualDeviceId);
        Call call = apiReference.registration(params);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void validateregisteration(final int tag, String email, String phone_number, final SignUpActivity apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("UserModel[email]", email);
        params.put("ProfileModel[phone]", phone_number);
        Call call = apiReference.validregistration(params);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void galleryListing(final int tag, String session_key, String type, final APIServerResponse apiServerResponse) {
        Call call = apiReference.galleryList(session_key, type);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void generateotp(final int tag, String device_id, String phone_number, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("phone", phone_number);
        params.put("device_id", device_id);
        Call call = apiReference.generateOTP(params);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void verifyOtp(final int tag, String code, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();

        params.put("code", code);
        Call call = apiReference.verifyOTP(params);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void login(final int tag, String email, String password, String device_type, String device_id, String timeZone, String actualDeviceId, String device_name, final SignInActivity apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("login-form[login]", email);
        params.put("login-form[password]", password);
        params.put("login-form[type]", device_type);
        params.put("login-form[device_id]", device_id);
        params.put("login-form[timezone]", timeZone);
        params.put("login-form[actual_device_id]", actualDeviceId);
        params.put("login-form[device_name]", device_name);
        //device_name

        Call call = apiReference.login(params);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void logOut(final int tag, String session_key,String userId, final APIServerResponse apiServerResponse) {
        Call call = apiReference.logout(session_key,userId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getJoyMusicList(final int tag, String session_key, final APIServerResponse apiServerResponse) {
        Call call = apiReference.getjoyMusicList(session_key);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void fetchPeofile(final int tag, String session_key,String userId, final APIServerResponse apiServerResponse) {
        Call call = apiReference.fetchUserProfile(session_key,userId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void saveProfile(final int tag, String session_key,String userId, String first_name, String last_name, String gender, String phone_number, String dob, String email, final ProfileActivity apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("UserModel[email]", email);
        params.put("ProfileModel[firstname]", first_name);
        params.put("ProfileModel[lastname]", last_name);
        params.put("ProfileModel[gender]", gender);
        params.put("ProfileModel[DOB]", dob);
        params.put("ProfileModel[phone]", phone_number);

        Call call = apiReference.saveProfile(session_key,userId, params);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void saveProfileWithProfile(final int tag, String session_key,String userId,  String first_name, String last_name, String gender, String phone_number, String dob, String email, String profile_pic, final ProfileActivity apiServerResponse) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(profile_pic));
        MultipartBody.Part body = MultipartBody.Part.createFormData("ProfileModel[profile_pic]", "user_profile.jpg", requestBody);
        RequestBody useremail = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody userfirstname = RequestBody.create(MediaType.parse("text/plain"), first_name);
        RequestBody userlastname = RequestBody.create(MediaType.parse("text/plain"), last_name);
        RequestBody usergender = RequestBody.create(MediaType.parse("text/plain"), gender);
        RequestBody userdob = RequestBody.create(MediaType.parse("text/plain"), dob);
        RequestBody userphone_number = RequestBody.create(MediaType.parse("text/plain"), phone_number);


        Call call = apiReference.saveProfileWithPic(session_key,userId, useremail, userfirstname, userlastname, usergender, userdob, userphone_number, body);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void questions(final int tag, final QuestionsActivity apiServerResponse) {
        Call call = apiReference.questions();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void Home(final int tag, String session_key, int pagenumber, final APIServerResponse apiServerResponse) {

        Call call = apiReference.user_home(session_key, String.valueOf(pagenumber));

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {

                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void requestTestimonyWithoutImage(final int tag, String userId, String name, String title, String description, String video_url, final TestimonyRequestActivity _prayerRequestResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("Testimony[name]", name);
        params.put("Testimony[title]", title);
        params.put("Testimony[desc]", description);
        params.put("Testimony[video_link]", video_url);
        params.put("Testimony[created_by]", video_url);

        Call call = apiReference.request_testimony(params,userId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                _prayerRequestResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                _prayerRequestResponse.onError(tag, t);
            }
        });
    }

    public void requestTestimonyWithImage(final int tag, String session_key, String userId, String name, String title, String description, String Video_url, String prayer_pic, final TestimonyRequestActivity _prayerRequestResponse) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(prayer_pic));
        MultipartBody.Part body = MultipartBody.Part.createFormData("Testimony[image]", "request_testimonial.jpg", requestBody);
        RequestBody username = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody prayertitle = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody prayerDescription = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody prayer_videoUrl = RequestBody.create(MediaType.parse("text/plain"), Video_url);
//userId
        RequestBody userIdReq = RequestBody.create(MediaType.parse("text/plain"), userId);
        Call<PrayerRequestModal> call = apiReference.request_testimony_with_image(session_key,userId, userIdReq, username, prayertitle, prayerDescription, prayer_videoUrl, body);
        call.enqueue(new Callback<PrayerRequestModal>() {
            @Override
            public void onResponse(Call<PrayerRequestModal> call, Response<PrayerRequestModal> response) {
                _prayerRequestResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<PrayerRequestModal> call, Throwable t) {
                _prayerRequestResponse.onError(tag, t);
            }
        });
    }

    public void requestPrayerWithoutImage(final int tag, String userId, String name, String title, String description, String video_url, final RequestActivity _prayerRequestResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("Prayer[name]", name);
        params.put("Prayer[title]", title);
        params.put("Prayer[desc]", description);
        params.put("Prayer[video_link]", video_url);
        params.put("Prayer[Prayer[created_by]]", userId);

        Call call = apiReference.request_prayer(params,userId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                _prayerRequestResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                _prayerRequestResponse.onError(tag, t);
            }
        });
    }

    public void requestPrayerWithImage(final int tag, String userId, String name, String title, String description, String Video_url, String prayer_pic, final RequestActivity _prayerRequestResponse)
    {
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(prayer_pic));
        MultipartBody.Part body = MultipartBody.Part.createFormData("Prayer[image]", "user_profile.jpg", requestBody);
        RequestBody username = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody prayertitle = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody prayerDescription = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody userIdReq = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody prayer_videoUrl = RequestBody.create(MediaType.parse("text/plain"), Video_url);


        Call call = apiReference.request_prayer_with_image(userId,userIdReq, username, prayertitle, prayerDescription, prayer_videoUrl, body);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                _prayerRequestResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                _prayerRequestResponse.onError(tag, t);
            }
        });
    }


    public void createPostWithoutImage(final int tag, String userId, String session_key, String postText, String user_type, String taggedUser, final APIServerResponse _createPostResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("PostModel[post_on]", userId);
        params.put("PostModel[post_type]", user_type);
        params.put("PostModel[post_content]", postText);
        params.put("tag_list", taggedUser);

        Call call = apiReference.create_post_without_image(session_key,userId, params);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                _createPostResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {

                _createPostResponse.onError(tag, t);
            }
        });
    }

    public void editUserPostWithoutImage(final int tag, String session_key,  String postID, String userId, String postText, String user_type, String taggedFriends, final APIServerResponse _createPostResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("PostModel[post_on]", userId);
        params.put("PostMedia[file]", "");
        params.put("PostMedia[media_type]", "");
        params.put("PostModel[post_type]", user_type);
        params.put("PostModel[post_content]", postText);
        params.put("tag_list", taggedFriends);

        Call<EditUserPostModal> call = apiReference.editUserPostWithoutImage(session_key,userId, postID, params);

        call.enqueue(new Callback<EditUserPostModal>() {
            @Override
            public void onResponse(Call<EditUserPostModal> call, Response<EditUserPostModal> response) {
                _createPostResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<EditUserPostModal> call, Throwable t) {
                _createPostResponse.onError(tag, t);
            }
        });
    }


    public void createPostWithImage(final int tag, String session_key, String loggedInUserId,String userId, String postText, String postImage, String mediaType, String user_type, String taggedString, final APIServerResponse _createPostResponse) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(postImage));
        MultipartBody.Part body = MultipartBody.Part.createFormData("PostMedia[file]", "user_profile.jpg", requestBody);
        RequestBody postTextdata = RequestBody.create(MediaType.parse("text/plain"), postText);
        RequestBody postUSerId = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody postType = RequestBody.create(MediaType.parse("text/plain"), user_type);
        RequestBody taggedList = RequestBody.create(MediaType.parse("text/plain"), taggedString);
        RequestBody postMediaType = RequestBody.create(MediaType.parse("text/plain"), mediaType);


        Call call = apiReference.create_post_with_image(session_key,loggedInUserId, postUSerId ,postTextdata, postMediaType, postType, taggedList, body);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                _createPostResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                _createPostResponse.onError(tag, t);
            }
        });
    }

    public void createPostWithVideo(final int tag, String session_key, String loggedInUserId, String userId, String postText, String postVideo, String postThumbnail, String mediaType, String user_type, String taggedString, final APIServerResponse _createPostResponse) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("video/*"), new File(postVideo));
        MultipartBody.Part body = MultipartBody.Part.createFormData("PostMedia[file]", "user_video.mp4", requestBody);
        RequestBody postTextdata = RequestBody.create(MediaType.parse("text/plain"), postText);
        RequestBody loggedInUserIdReq = RequestBody.create(MediaType.parse("text/plain"), loggedInUserId);
        RequestBody postUSerId = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody postType = RequestBody.create(MediaType.parse("text/plain"), user_type);
        RequestBody taggedList = RequestBody.create(MediaType.parse("text/plain"), taggedString);
        RequestBody postMediaType = RequestBody.create(MediaType.parse("text/plain"), mediaType);
        RequestBody postVideoThumbnail = RequestBody.create(MediaType.parse("image/*"), new File(postThumbnail));
        MultipartBody.Part bodyThnumbnail = MultipartBody.Part.createFormData("PostMedia[thumbnail_file]", "postthumbnail_picture.jpg", postVideoThumbnail);


        Call call = apiReference.create_post_with_video(session_key,loggedInUserId, postUSerId, postTextdata, postMediaType, postType, taggedList, bodyThnumbnail, body);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                _createPostResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {

                _createPostResponse.onError(tag, t);
            }
        });
    }

    public void editUserPost(final int tag, String session_key, String postID, String userId, String postText, String postImage, String mediaType, String user_type, String taggedString, final APIServerResponse _createPostResponse) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(postImage));
        MultipartBody.Part body = MultipartBody.Part.createFormData("PostMedia[file]", "post_picture.jpg", requestBody);
        RequestBody postTextdata = RequestBody.create(MediaType.parse("text/plain"), postText);
        RequestBody postUSerId = RequestBody.create(MediaType.parse("text/plain"), userId);
        RequestBody postType = RequestBody.create(MediaType.parse("text/plain"), user_type);
        RequestBody taggedList = RequestBody.create(MediaType.parse("text/plain"), taggedString);
        RequestBody postMediaType = RequestBody.create(MediaType.parse("text/plain"), mediaType);

        Call<EditUserPostModal> call = apiReference.editUserPost(session_key,userId, postID, postUSerId, postTextdata, postMediaType, postType, taggedList, body);
        call.enqueue(new Callback<EditUserPostModal>() {
            @Override
            public void onResponse(Call<EditUserPostModal> call, Response<EditUserPostModal> response) {
                _createPostResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<EditUserPostModal> call, Throwable t) {
                _createPostResponse.onError(tag, t);
            }
        });

    }


    public void createGroupPostWithImageOrVideo(final int tag, String sessionKey, String groupId, String postText, String multimediaFile, String postType, String videoThumbnail, final APIServerResponse apiServerResponse) {
        if (postType.equalsIgnoreCase(Constants.IMAGE)) {
            RequestBody postTextdata = RequestBody.create(MediaType.parse("text/plain"), postText);
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(multimediaFile));
            MultipartBody.Part imageBody = MultipartBody.Part.createFormData("GroupWallModel[wall_attachment_id]", "group_post_picture.jpg", requestBody);
            RequestBody postTypeData = RequestBody.create(MediaType.parse("text/plain"), postType);
            MultipartBody.Part postVideoThumb = null;
            Call<CreateGroupPostModal> call = apiReference.createGroupPostWithImageOrVideo(sessionKey, groupId, postTextdata, imageBody, postTypeData, postVideoThumb);

            call.enqueue(new Callback<CreateGroupPostModal>() {
                @Override
                public void onResponse(Call<CreateGroupPostModal> call, Response<CreateGroupPostModal> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call<CreateGroupPostModal> call, Throwable t) {
                    apiServerResponse.onError(tag, t);
                }
            });

        } else if (postType.equalsIgnoreCase(Constants.VIDEO)) {
            RequestBody postTextdata = RequestBody.create(MediaType.parse("text/plain"), postText);
            RequestBody requestBody = RequestBody.create(MediaType.parse("video/*"), new File(multimediaFile));
            MultipartBody.Part videoBody = MultipartBody.Part.createFormData("GroupWallModel[wall_attachment_id]", "group_post_video.mp4", requestBody);
            RequestBody postTypeData = RequestBody.create(MediaType.parse("text/plain"), postType);
            RequestBody requestBodyThumb = RequestBody.create(MediaType.parse("image/*"), new File(videoThumbnail));
            MultipartBody.Part videoThumb = MultipartBody.Part.createFormData("GroupWallModel[thumbnail_file]", "group_post_video_thumb.jpg", requestBodyThumb);

            Call<CreateGroupPostModal> call = apiReference.createGroupPostWithImageOrVideo(sessionKey, groupId, postTextdata, videoBody, postTypeData, videoThumb);
            call.enqueue(new Callback<CreateGroupPostModal>() {
                @Override
                public void onResponse(Call<CreateGroupPostModal> call, Response<CreateGroupPostModal> response) {
                    apiServerResponse.onSuccess(tag, response);
                }

                @Override
                public void onFailure(Call<CreateGroupPostModal> call, Throwable t) {
                    apiServerResponse.onError(tag, t);
                }
            });
        }
    }

    public void createGroupPostWithText(final int tag, String sessionKey,String loggedInUserId, String groupId, String postText, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("GroupWallModel[wall_description]", postText);
        params.put("GroupWallModel[wall_attachment_id]", "");
        params.put("GroupWallModel[attachment_type]", "");
        params.put("GroupWallModel[thumbnail_file]", "");

        Call<CreateGroupPostModal> call = apiReference.createGroupPostWithText(sessionKey,loggedInUserId, groupId, params);
        call.enqueue(new Callback<CreateGroupPostModal>() {
            @Override
            public void onResponse(Call<CreateGroupPostModal> call, Response<CreateGroupPostModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CreateGroupPostModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });


    }


    public void pagePostLike(final int tag, String session_key, String userId, String post_id, String postPrayerOrTestimony, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();

        params.put("post_id", post_id);
        params.put("post_type", postPrayerOrTestimony);

        Call<LikeModal> call = apiReference.like(session_key,userId, params);
        call.enqueue(new Callback<LikeModal>() {
            @Override
            public void onResponse(Call<LikeModal> call, Response<LikeModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<LikeModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }




    public void like(final int tag, String session_key, String userId, String post_id, String postPrayerOrTestimony, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();

        params.put("post_id", post_id);
        params.put("post_type", postPrayerOrTestimony);

        Call<LikeModal> call = apiReference.like(session_key,userId, params);
        call.enqueue(new Callback<LikeModal>() {
            @Override
            public void onResponse(Call<LikeModal> call, Response<LikeModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<LikeModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void share(final int tag, String session_key, String userId, String sharePostID, String sharePostType, String shareSharedText, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("PostShare[shared_post_id]", sharePostID);
        params.put("PostShare[shared_post_type]", sharePostType);
        params.put("PostShare[shared_text]", shareSharedText);

        Call<ShareModal> call = apiReference.share(session_key,userId, params);
        call.enqueue(new Callback<ShareModal>() {
            @Override
            public void onResponse(Call<ShareModal> call, Response<ShareModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ShareModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void sharePost(final int tag, String session_key,String userId, String sharePostID, String sharePostType, String shareSharedText, String taggedUser, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("PostShare[shared_post_id]", sharePostID);
        params.put("PostShare[shared_post_type]", sharePostType);
        params.put("PostShare[shared_text]", shareSharedText);
        params.put("tag_list", taggedUser);

        Call<ShareModal> call = apiReference.share(session_key,userId, params);
        call.enqueue(new Callback<ShareModal>() {
            @Override
            public void onResponse(Call<ShareModal> call, Response<ShareModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ShareModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void comment(final int tag, String session_key,String loggedInUserID, String sharePostID, String sharePostType, String comment, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("PostComment[post_id]", sharePostID);
        params.put("PostComment[post_type]", sharePostType);
        params.put("PostComment[comment]", comment);

        Call<CommentModal> call = apiReference.comment(session_key,loggedInUserID, params);
        call.enqueue(new Callback<CommentModal>() {
            @Override
            public void onResponse(Call<CommentModal> call, Response<CommentModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CommentModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }
    public void pagePostComment(final int tag, String session_key,String loggedInUser, String sharePostID, String comment, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("PagePostComment[content]", comment);
   /*     params.put("PostComment[post_type]", sharePostType);*/
        //params.put("PostComment[comment]", comment);

        Call<CommentModal> call = apiReference.pageComment(session_key,sharePostID,loggedInUser, params);
        call.enqueue(new Callback<CommentModal>() {
            @Override
            public void onResponse(Call<CommentModal> call, Response<CommentModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CommentModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void commentListing(final int tag, String session_key, String sharePostID, String post_type, final APIServerResponse apiServerResponse) {

        //Map<String, String> params = new HashMap<>();


        Call<CommentsListModal> call = apiReference.fetchComments(session_key, sharePostID, post_type);
        call.enqueue(new Callback<CommentsListModal>() {
            @Override
            public void onResponse(Call<CommentsListModal> call, Response<CommentsListModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CommentsListModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void pageCommentListing(final int tag, String session_key, String sharePostID,final APIServerResponse apiServerResponse) {

        Call<CommentsListModal> call = apiReference.pageComments(session_key, sharePostID);
        call.enqueue(new Callback<CommentsListModal>() {
            @Override
            public void onResponse(Call<CommentsListModal> call, Response<CommentsListModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CommentsListModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getPost(final int tag, String session_key, String sharePostID, String post_type, final APIServerResponse apiServerResponse) {

        Call<SinglePostViewModal> call = apiReference.getSinglePost(session_key, sharePostID, post_type);
        call.enqueue(new Callback<SinglePostViewModal>() {
            @Override
            public void onResponse(Call<SinglePostViewModal> call, Response<SinglePostViewModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SinglePostViewModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void changePassword(final int tag, String sessionKey,String userId, String currentPassword, String newPassword, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("UserModel[current_password]", currentPassword);
        params.put("UserModel[new_password]", newPassword);

        Call<ChangePasswordModal> call = apiReference.changePassword(sessionKey,userId, params);
        call.enqueue(new Callback<ChangePasswordModal>() {
            @Override
            public void onResponse(Call<ChangePasswordModal> call, Response<ChangePasswordModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ChangePasswordModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void forgotPassword(final int tag, String emailID, final APIServerResponse apiServerResponse) {
        Call<ForgotPasswordModal> call = apiReference.forgotPassword(emailID);
        call.enqueue(new Callback<ForgotPasswordModal>() {
            @Override
            public void onResponse(Call<ForgotPasswordModal> call, Response<ForgotPasswordModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ForgotPasswordModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getPrayers(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {

        Call<PrayerIndexModal> call = apiReference.getPrayers(sessionKey);
        call.enqueue(new Callback<PrayerIndexModal>() {
            @Override
            public void onResponse(Call<PrayerIndexModal> call, Response<PrayerIndexModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<PrayerIndexModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void getTestimony(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {
        Call<TestimonyModal> call = apiReference.getTestimony(sessionKey);
        call.enqueue(new Callback<TestimonyModal>() {
            @Override
            public void onResponse(Call<TestimonyModal> call, Response<TestimonyModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<TestimonyModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getTestimonyNew(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {
        Call<TestimonyModalNew> call = apiReference.getTestimonyNew(sessionKey);
        call.enqueue(new Callback<TestimonyModalNew>() {
            @Override
            public void onResponse(Call<TestimonyModalNew> call, Response<TestimonyModalNew> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<TestimonyModalNew> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void amenPrayerPost(final int tag, String sessionKey, String userId, String prayerPostID, final APIServerResponse apiServerResponse) {
        Call<AmenPrayerModal> call = apiReference.amenPrayerPost(sessionKey,userId, prayerPostID);
        call.enqueue(new Callback<AmenPrayerModal>() {
            @Override
            public void onResponse(Call<AmenPrayerModal> call, Response<AmenPrayerModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AmenPrayerModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void haleTestimonyPost(final int tag, String sessionKey, String user_id,String testimonyPostID, final APIServerResponse apiServerResponse) {
        Call<HaleTestimonyModal> call = apiReference.haleTestimonyPost(sessionKey,user_id, testimonyPostID);
        call.enqueue(new Callback<HaleTestimonyModal>() {
            @Override
            public void onResponse(Call<HaleTestimonyModal> call, Response<HaleTestimonyModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<HaleTestimonyModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void booksList(final int tag, String session_key, final APIServerResponse apiServerResponse) {

        Call<BooksListingModal> call = apiReference.booksListing(session_key);
        call.enqueue(new Callback<BooksListingModal>() {
            @Override
            public void onResponse(Call<BooksListingModal> call, Response<BooksListingModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<BooksListingModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

       /* Call call = apiReference.booksListing(session_key);

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });*/
    }

    public void userList(final int tag, String session_key,String userId, final APIServerResponse apiServerResponse) {
        Call call = apiReference.userListing(session_key,userId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void friendsRequestList(final int tag, String session_key,String userId, final APIServerResponse apiServerResponse) {
        Call call = apiReference.friendRequestListing(session_key,userId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void myfriendsList(final int tag, String session_key,String logginedId,int pageNumber ,final APIServerResponse apiServerResponse) {
        Call call = apiReference.friendsListing(session_key,logginedId,pageNumber);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void tagFriendList(final int tag, String session_key,String logginedId,int pageNumber ,final APIServerResponse apiServerResponse) {
        Call call = apiReference.tagFriendsListing(session_key,logginedId,pageNumber);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }




    public void mutualFriendList(final int tag, String session_key,String logginedId, String userId, final APIServerResponse apiServerResponse) {
        Call call = apiReference.mutualFriends(session_key, logginedId,userId);
        call.enqueue(new Callback<MutualFriendResponce>() {
            @Override
            public void onResponse(Call<MutualFriendResponce> call, Response<MutualFriendResponce> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MutualFriendResponce> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllInvitedUsers(final int tag, String session_key, String pageId, final APIServerResponse apiServerResponse) {
        Call call = apiReference.getAllInvitedUsers(session_key, pageId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllInvitationRequest(final int tag, String session_key, String pageId, final APIServerResponse apiServerResponse) {
        Call call = apiReference.getAllInviteRequestList(session_key, pageId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void request_accepted(final int tag, String session_key, String loggined_userid, String requested_user_id, String action, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("request_id", requested_user_id);
        params.put("action", action);
        Call call = apiReference.requestAccepted(session_key,loggined_userid, params);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void acceptRejectInvitation(final int tag, String session_key, String pageId, String userId, String statusId, final APIServerResponse apiServerResponse) {
        Call call = apiReference.acceptRejectInvitation(session_key, pageId, userId, statusId);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void sendRequestFriend(final int tag, String session_key,String logginedUserId, String userId, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("friend_user_id", userId);
        Call call = apiReference.sendRequest(session_key,logginedUserId, params);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void userTimeLine(final int tag, String session_key, String userId, int pageNumber, final APIServerResponse apiServerResponse) {
        Call call = apiReference.userTimeLine(session_key, userId, String.valueOf(pageNumber));
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void editEventWithoutPhoto(final int tag, String sessionKey, String eventID, String eventName, String eventDate, String eventLocation, String eventDescr, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("Event[event_name]", eventName);
        params.put("Event[event_date]", eventDate);
        params.put("Event[event_location]", eventLocation);
        params.put("Event[event_desc]", eventDescr);

        Call<EditEventModal> call = apiReference.editEventWithoutPic(sessionKey, eventID, params);
        call.enqueue(new Callback<EditEventModal>() {
            @Override
            public void onResponse(Call<EditEventModal> call, Response<EditEventModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<EditEventModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void sendEventInvite(final int tag, String sessionKey, String eventID, String eventInvitesFriendsID, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("EventInvites[event_id]", eventID);
        params.put("EventInvites[invite_users]", eventInvitesFriendsID);

        Call<SendEventInvitationModal> call = apiReference.sendEventInvite(sessionKey, params);
        call.enqueue(new Callback<SendEventInvitationModal>() {
            @Override
            public void onResponse(Call<SendEventInvitationModal> call, Response<SendEventInvitationModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SendEventInvitationModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void addBookwithcover(final int tag, String session_key, String book_title, String book_type, String author_name, String image, String description, String bookLanguage, String bookyear, String price, String any_other, String bookImage1, String bookImage2, String bookImage3, String bookImage4, String bookImage5, String bookImage6, final APIServerResponse apiServerResponse) {

        MultipartBody.Part book_image1 = null, book_image2 = null, book_image3 = null, book_image4 = null, book_image5 = null, book_image6 = null;
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(image));
        MultipartBody.Part body = MultipartBody.Part.createFormData("Book[book_cover]", "book_cover.jpg", requestBody);

        book_image1 = MultipartBody.Part.createFormData("AudioBooks[file_name][0]", "book_image1.jpg", RequestBody.create(MediaType.parse("image/*"), new File(bookImage1)));

        //switch ()
        if (!bookImage2.equalsIgnoreCase("")) {
            book_image2 = MultipartBody.Part.createFormData("AudioBooks[file_name][1]", "book_image2.jpg", RequestBody.create(MediaType.parse("image/*"), new File(bookImage2)));
        }

        if (!bookImage3.equalsIgnoreCase("")) {
            book_image3 = MultipartBody.Part.createFormData("AudioBooks[file_name][2]", "book_image3.jpg", RequestBody.create(MediaType.parse("image/*"), new File(bookImage3)));
        }
        if (!bookImage4.equalsIgnoreCase("")) {
            book_image4 = MultipartBody.Part.createFormData("AudioBooks[file_name][3]", "book_image4.jpg", RequestBody.create(MediaType.parse("image/*"), new File(bookImage4)));
        }
        if (!bookImage5.equalsIgnoreCase("")) {
            book_image5 = MultipartBody.Part.createFormData("AudioBooks[file_name][4]", "book_image5.jpg", RequestBody.create(MediaType.parse("image/*"), new File(bookImage5)));
        }
        if (!bookImage6.equalsIgnoreCase("")) {
            book_image6 = MultipartBody.Part.createFormData("AudioBooks[file_name][5]", "book_image6.jpg", RequestBody.create(MediaType.parse("image/*"), new File(bookImage6)));
        }


        RequestBody bookTitle = RequestBody.create(MediaType.parse("text/plain"), book_title);
        RequestBody bookDesc = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody bookAuthor = RequestBody.create(MediaType.parse("text/plain"), author_name);
        RequestBody otherBook = RequestBody.create(MediaType.parse("text/plain"), any_other);
        RequestBody bookType = RequestBody.create(MediaType.parse("text/plain"), book_type);
        //RequestBody bookPdfFile = RequestBody.create(MediaType.parse("text/plain"), pdfFile);
        RequestBody language = RequestBody.create(MediaType.parse("text/plain"), bookLanguage);
        RequestBody year = RequestBody.create(MediaType.parse("text/plain"), bookyear);
        RequestBody priceBook = RequestBody.create(MediaType.parse("text/plain"), price);
        Call<UPloadBookModal> call = apiReference.uploadBooksWithImage(session_key, bookTitle, bookType, bookDesc, bookAuthor, language, year, priceBook, otherBook/*, bookPdfFile*/, body, book_image1, book_image2, book_image3, book_image4, book_image5, book_image6);
        call.enqueue(new Callback<UPloadBookModal>() {
            @Override
            public void onResponse(Call<UPloadBookModal> call, Response<UPloadBookModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<UPloadBookModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void editEventWithPhoto(final int tag, String sessionKey, String eventID, String eventName, String eventDate, String eventLocation, String eventDescr, String eventPhoto, final APIServerResponse apiServerResponse) {

        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(eventPhoto));
        MultipartBody.Part body = MultipartBody.Part.createFormData("Event[event_photo]", "event_profile.jpg", requestBody);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), eventName);
        RequestBody date = RequestBody.create(MediaType.parse("text/plain"), eventDate);
        RequestBody location = RequestBody.create(MediaType.parse("text/plain"), eventLocation);
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), eventDescr);
        //RequestBody bookType = RequestBody.create(MediaType.parse("text/plain"), book_type);

        Call<EditEventModal> call = apiReference.editEventWithPic(sessionKey, eventID, name, date, location, description, body);
        call.enqueue(new Callback<EditEventModal>() {
            @Override
            public void onResponse(Call<EditEventModal> call, Response<EditEventModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<EditEventModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });


    }


    public void eventsList(final int tag, final APIServerResponse apiServerResponse) {
        Call call = apiReference.eventsListing();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void createEvent(final int tag, String sesionKey, String eventName, String eventDate, String eventLocation, String eventDescription, final APIServerResponse apiServerResponse) {
        HashMap<String, String> params = new HashMap<>();
        params.put("Event[event_name]", eventName);
        params.put("Event[event_date]", eventDate);
        params.put("Event[event_location]", eventLocation);
        params.put("Event[event_desc]", eventDescription);

        Call<AddEventModal> call = apiReference.createEvent(sesionKey, params);
        call.enqueue(new Callback<AddEventModal>() {
            @Override
            public void onResponse(Call<AddEventModal> call, Response<AddEventModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AddEventModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void deleteEvent(final int tag, String sesionKey, String eventId, final APIServerResponse apiServerResponse) {
        HashMap<String, String> params = new HashMap<>();
        params.put("event_id", eventId);

        Call<DeleteEventModal> call = apiReference.deleteEvent(sesionKey, params);
        call.enqueue(new Callback<DeleteEventModal>() {
            @Override
            public void onResponse(Call<DeleteEventModal> call, Response<DeleteEventModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<DeleteEventModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void createEventWithImage(final int tag, String sesionKey, String eventName, String eventDate, String eventLocation, String eventDescription, String eventImage, final APIServerResponse apiServerResponse) {

        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(eventImage));
        MultipartBody.Part body = MultipartBody.Part.createFormData("Event[event_photo]", "event_image.jpg", requestBody);
        RequestBody postEventName = RequestBody.create(MediaType.parse("text/plain"), eventName);
        RequestBody postEventDate = RequestBody.create(MediaType.parse("text/plain"), eventDate);
        RequestBody postEventLocation = RequestBody.create(MediaType.parse("text/plain"), eventLocation);
        RequestBody postEventDescription = RequestBody.create(MediaType.parse("text/plain"), eventDescription);

        Call<AddEventModal> call = apiReference.createEventWithImage(sesionKey, postEventName, postEventDate, postEventLocation, postEventDescription, body);
        call.enqueue(new Callback<AddEventModal>() {
            @Override
            public void onResponse(Call<AddEventModal> call, Response<AddEventModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AddEventModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void createHelpRequestWithoutImage(final int tag, String sessionKey, String userId, String name, String title, String description, String video_url, String emailId, String phoneNumber, String address, final APIServerResponse _prayerRequestResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("HelpRequest[name]", name);
        params.put("HelpRequest[title]", title);
        params.put("HelpRequest[description]", description);
        params.put("HelpRequest[video_link]", video_url);
        params.put("HelpRequest[email]", emailId);
        params.put("HelpRequest[address]", address);
        params.put("HelpRequest[phone_number]", phoneNumber);
        params.put("HelpRequest[created_by]", userId);
        Call<HelpRequestModal> call = apiReference.createHelpRequest(sessionKey,userId, params);

        call.enqueue(new Callback<HelpRequestModal>() {
            @Override
            public void onResponse(Call<HelpRequestModal> call, Response<HelpRequestModal> response) {
                _prayerRequestResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<HelpRequestModal> call, Throwable t) {
                _prayerRequestResponse.onError(tag, t);
            }
        });
    }


    public void createHelpRequestWithImage(final int tag, String sessionKey, String userId, String name, String title, String description, String Video_url, String prayer_pic, String emaild, String phoneNumbr, String address, final APIServerResponse _prayerRequestResponse) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(prayer_pic));
        MultipartBody.Part body = MultipartBody.Part.createFormData("HelpRequest[image]", "help_request.jpg", requestBody);
        RequestBody username = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody prayertitle = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody prayerDescription = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody prayer_videoUrl = RequestBody.create(MediaType.parse("text/plain"), Video_url);

        RequestBody prayer_emailId = RequestBody.create(MediaType.parse("text/plain"), emaild);
        RequestBody prayer_address = RequestBody.create(MediaType.parse("text/plain"), address);
        RequestBody prayer_phonenymber = RequestBody.create(MediaType.parse("text/plain"), phoneNumbr);
        RequestBody userIdReq = RequestBody.create(MediaType.parse("text/plain"), userId);

        Call<HelpRequestModal> call = apiReference.createHelpRequestWithImage(sessionKey,userId, userIdReq, username, prayertitle, prayerDescription, prayer_videoUrl, prayer_emailId, prayer_phonenymber, prayer_address, body);
        call.enqueue(new Callback<HelpRequestModal>() {
            @Override
            public void onResponse(Call<HelpRequestModal> call, Response<HelpRequestModal> response) {
                _prayerRequestResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<HelpRequestModal> call, Throwable t) {
                _prayerRequestResponse.onError(tag, t);
            }
        });

    }

    public void getHelpRequest(final int tag, String sessionKey,String userId, final APIServerResponse apiServerResponse) {

        Call<HelpRequestListModal> call = apiReference.getHelpRequest(sessionKey,userId);
        call.enqueue(new Callback<HelpRequestListModal>() {
            @Override
            public void onResponse(Call<HelpRequestListModal> call, Response<HelpRequestListModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<HelpRequestListModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getMyCreatedEvents(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {

        Call<MyEventsCreatedModal> call = apiReference.myCreatedEvents(sessionKey);
        call.enqueue(new Callback<MyEventsCreatedModal>() {
            @Override
            public void onResponse(Call<MyEventsCreatedModal> call, Response<MyEventsCreatedModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MyEventsCreatedModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getEventInvites(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {

        Call<EventsInvitesModal> call = apiReference.getEventsInvites(sessionKey);
        call.enqueue(new Callback<EventsInvitesModal>() {
            @Override
            public void onResponse(Call<EventsInvitesModal> call, Response<EventsInvitesModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<EventsInvitesModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getEventDetails(final int tag, String sessionKey, String postID, final APIServerResponse apiServerResponse) {
        Call<EventDetailsModal> call = apiReference.getEventDetails(sessionKey, postID);
        call.enqueue(new Callback<EventDetailsModal>() {
            @Override
            public void onResponse(Call<EventDetailsModal> call, Response<EventDetailsModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<EventDetailsModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void acceptOrRejectEventInvitation(final int tag, String sessionKey, String eventID, String action, final APIServerResponse apiServerResponse) {

        Call<MyInvitationRequestAcceptReject> call = apiReference.acceptOrRejectEventInvite(sessionKey, eventID, action);
        call.enqueue(new Callback<MyInvitationRequestAcceptReject>() {
            @Override
            public void onResponse(Call<MyInvitationRequestAcceptReject> call, Response<MyInvitationRequestAcceptReject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MyInvitationRequestAcceptReject> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void acceptAlarmRequest(final int tag, String session_key,String user_id ,final APIServerResponse apiServerResponse) {
        Call<AlarmRequestAcceptModal> call = apiReference.alarmAccept(session_key,user_id);
        call.enqueue(new Callback<AlarmRequestAcceptModal>() {
            @Override
            public void onResponse(Call<AlarmRequestAcceptModal> call, Response<AlarmRequestAcceptModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AlarmRequestAcceptModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void alarmRequestCount(final int tag, String sessionKey,String user_id , final APIServerResponse apiServerResponse) {
        Call<AlarmRequestCountModal> call = apiReference.alarmRequestCount(sessionKey,user_id);
        call.enqueue(new Callback<AlarmRequestCountModal>() {
            @Override
            public void onResponse(Call<AlarmRequestCountModal> call, Response<AlarmRequestCountModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AlarmRequestCountModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void changeProfileOrCoverPic(final int tag, String sessionKey,String userId, String profilePic, String coverPic, final APIServerResponse apiServerResponse) {

        MultipartBody.Part profileBody = null;
        MultipartBody.Part coverBody = null;
        if (!profilePic.isEmpty()) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(profilePic));
            profileBody = MultipartBody.Part.createFormData("ProfileModel[profile_pic]", "user_profile.jpg", requestBody);
        }

        if (!coverPic.isEmpty()) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(coverPic));
            coverBody = MultipartBody.Part.createFormData("ProfileModel[cover_photo]", "user_cover.jpg", requestBody);
        }

        Call<ChangeProfileCoverPicModal> call = apiReference.changeProfileOrCoverPic(sessionKey,userId, profileBody, coverBody);
        call.enqueue(new Callback<ChangeProfileCoverPicModal>() {
            @Override
            public void onResponse(Call<ChangeProfileCoverPicModal> call, Response<ChangeProfileCoverPicModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ChangeProfileCoverPicModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void createPage(final int tag, String sessionKey,String userId, String pageName, String pageDesc, String bannerImg, String dpImg, final APIServerResponse apiServerResponse) {

        MultipartBody.Part profileReq = null;
        RequestBody requestBody2 = RequestBody.create(MediaType.parse("image/*"), new File(dpImg));
        MultipartBody.Part bannerImgReq = null;
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(bannerImg));
        bannerImgReq = MultipartBody.Part.createFormData("PageModel[banner_img]", "bannerImage.jpg", requestBody);
        profileReq = MultipartBody.Part.createFormData("PageModel[profile_img]", "profile.jpg", requestBody2);
        RequestBody pageNamebody = RequestBody.create(MediaType.parse("text/plain"), pageName);
        RequestBody pageDescBody = RequestBody.create(MediaType.parse("text/plain"), pageDesc);
        Call<CreatePageModal> call = apiReference.createPage(sessionKey,userId, pageNamebody, pageDescBody, profileReq, bannerImgReq);
        call.enqueue(new Callback<CreatePageModal>() {
            @Override
            public void onResponse(Call<CreatePageModal> call, Response<CreatePageModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CreatePageModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void updatePage(final int tag, String sessionKey,String loggedInUserID, String pageId, String pageName, String pageDesc, String bannerImg, String dpImg, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("PageModel[name]", pageName);
        params.put("PageModel[description]", pageDesc);
        params.put("PageModel[banner_img]", bannerImg);
        params.put("PageModel[profile_img]", dpImg);

        Call<CreatePageModal> call = apiReference.updatePage(sessionKey,loggedInUserID, pageId, params);
        call.enqueue(new Callback<CreatePageModal>() {
            @Override
            public void onResponse(Call<CreatePageModal> call, Response<CreatePageModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CreatePageModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void delete(final int tag, String sessionKey,String userId, String pageId, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("PageModel[page_id]", pageId);

        Call<GroupDeleteModal> call = apiReference.deletePage(sessionKey,userId, params);
        call.enqueue(new Callback<GroupDeleteModal>() {
            @Override
            public void onResponse(Call<GroupDeleteModal> call, Response<GroupDeleteModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupDeleteModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void inviteUserForPage(final int tag, String sessionKey, String usersIDs, String pageId, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("InvitedUserModel[user_ids] ", usersIDs);

        Call<GroupDeleteModal> call = apiReference.inviteUserForPage(sessionKey, pageId, params);
        call.enqueue(new Callback<GroupDeleteModal>() {
            @Override
            public void onResponse(Call<GroupDeleteModal> call, Response<GroupDeleteModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupDeleteModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void updatePageDp(final int tag, String sessionKey,String loggedInUserId, String pageId, String image, final APIServerResponse apiServerResponse) {
        MultipartBody.Part imageBody = null;
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(image));
        imageBody = MultipartBody.Part.createFormData("PageModel[profile_img]", "pagedp.jpg", requestBody);
        Call<GroupDeleteModal> call = apiReference.changePageDp(sessionKey,loggedInUserId, pageId, imageBody);
        call.enqueue(new Callback<GroupDeleteModal>() {
            @Override
            public void onResponse(Call<GroupDeleteModal> call, Response<GroupDeleteModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupDeleteModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    //changeBannerImagePage
    public void updatePageBanner(final int tag, String sessionKey,String userId, String pageId, String image, final APIServerResponse apiServerResponse) {
        MultipartBody.Part imageBody = null;
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(image));
        imageBody = MultipartBody.Part.createFormData("PageModel[banner_img]", "bannerImage.jpg", requestBody);
        Call<GroupDeleteModal> call = apiReference.changeBannerImagePage(sessionKey,userId, pageId, imageBody);
        call.enqueue(new Callback<GroupDeleteModal>() {
            @Override
            public void onResponse(Call<GroupDeleteModal> call, Response<GroupDeleteModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupDeleteModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getPageDesc(final int tag, String sessionKey,String userId, String pageId, final APIServerResponse apiServerResponse) {
        Call<PageStatusMessage> call = apiReference.getPageDescription(sessionKey,userId, pageId);
        call.enqueue(new Callback<PageStatusMessage>() {
            @Override
            public void onResponse(Call<PageStatusMessage> call, Response<PageStatusMessage> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<PageStatusMessage> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllPages(final int tag, String sessionKey, String user_id, final APIServerResponse apiServerResponse) {
        Call<AllPagesModal> call = apiReference.getAllPages(sessionKey, user_id);
        call.enqueue(new Callback<AllPagesModal>() {
            @Override
            public void onResponse(Call<AllPagesModal> call, Response<AllPagesModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AllPagesModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllBlockUser(final int tag, String sessionKey, String userId,final APIServerResponse apiServerResponse) {
        Call<MutualFriendModal> call = apiReference.getAllBlockList(sessionKey,userId);
        call.enqueue(new Callback<MutualFriendModal>() {
            @Override
            public void onResponse(Call<MutualFriendModal> call, Response<MutualFriendModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MutualFriendModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllFollowers(final int tag, String sessionKey, String userId, final APIServerResponse apiServerResponse) {
        Call<MutualFriendModal> call = apiReference.getAllFollowers(sessionKey, userId);
        call.enqueue(new Callback<MutualFriendModal>() {
            @Override
            public void onResponse(Call<MutualFriendModal> call, Response<MutualFriendModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MutualFriendModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllDevices(final int tag, String sessionKey,String userID, final APIServerResponse apiServerResponse) {
        Call<DeviceListResponse> call = apiReference.getAllDevicesList(sessionKey,userID);
        call.enqueue(new Callback<DeviceListResponse>() {
            @Override
            public void onResponse(Call<DeviceListResponse> call, Response<DeviceListResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<DeviceListResponse> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    //getAllDevices
    public void accountDeactivate(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {
        Call<GroupDeleteModal> call = apiReference.deactivateAcount(sessionKey);
        call.enqueue(new Callback<GroupDeleteModal>() {
            @Override
            public void onResponse(Call<GroupDeleteModal> call, Response<GroupDeleteModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupDeleteModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    /* Call call = apiReference.registration(params);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });*/
    public void getAllAlbums(final int tag, String sessionKey,String userId, final APIServerResponse apiServerResponse) {
        Call<AllAlbumList> call = apiReference.getAllImagesAlbum(sessionKey,userId);
        call.enqueue(new Callback<AllAlbumList>() {
            @Override
            public void onResponse(Call<AllAlbumList> call, Response<AllAlbumList> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AllAlbumList> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void getAllImagesProfile(final int tag, String sessionKey, String userId, final APIServerResponse apiServerResponse) {
        Call<ProfileImageModal> call = apiReference.getAllProfileImages(sessionKey, userId);
        call.enqueue(new Callback<ProfileImageModal>() {
            @Override
            public void onResponse(Call<ProfileImageModal> call, Response<ProfileImageModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProfileImageModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllImagesCover(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {
        Call<ProfileImageModal> call = apiReference.getAllCoverImages(sessionKey);
        call.enqueue(new Callback<ProfileImageModal>() {
            @Override
            public void onResponse(Call<ProfileImageModal> call, Response<ProfileImageModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProfileImageModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllTimeLineImages(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {
        Call<ProfileImageModal> call = apiReference.getAllTimelineImages(sessionKey);
        call.enqueue(new Callback<ProfileImageModal>() {
            @Override
            public void onResponse(Call<ProfileImageModal> call, Response<ProfileImageModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<ProfileImageModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void createAlbum(final int tag, String sessionKey, String albumName, String albumDesc, int visibilitySt, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("Album[name]", albumName);
        params.put("Album[description]", albumDesc);
        params.put("Album[is_public]", visibilitySt + "");
        //Album[is_public]

        Call<CreateAlbumResponse> call = apiReference.createAlbum(sessionKey, params);
        call.enqueue(new Callback<CreateAlbumResponse>() {
            @Override
            public void onResponse(Call<CreateAlbumResponse> call, Response<CreateAlbumResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CreateAlbumResponse> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void createPlayList(final int tag, String sessionKey,String userId, String playlistName, String songId, final APIServerResponse apiServerResponse) {
        //RequestBody playlistNameRq = RequestBody.create(MediaType.parse("text/plain"), playlistName);
        Map<String, String> params = new HashMap<>();
        params.put("Playlist[name]", playlistName);
        Call<PlaylistCreateResponse> call = apiReference.createPlaylist(sessionKey,userId, songId, params);
        call.enqueue(new Callback<PlaylistCreateResponse>() {
            @Override
            public void onResponse(Call<PlaylistCreateResponse> call, Response<PlaylistCreateResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<PlaylistCreateResponse> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void uploadAlbumImage(final int tag, String sessionKey, String albumId, String albumImage, String albumDesc, final APIServerResponse apiServerResponse) {

        MultipartBody.Part profileBody = null;
        if (!albumImage.isEmpty()) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(albumImage));
            profileBody = MultipartBody.Part.createFormData("AlbumImage[image_file]", "album_img.jpg", requestBody);
        }
        RequestBody albmDesc = RequestBody.create(MediaType.parse("text/plain"), albumDesc);

        Call<UplAlbumImageResponse> call = apiReference.uploadAlbumImage(sessionKey, albumId, profileBody, albmDesc);
        call.enqueue(new Callback<UplAlbumImageResponse>() {
            @Override
            public void onResponse(Call<UplAlbumImageResponse> call, Response<UplAlbumImageResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<UplAlbumImageResponse> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void createPagePostWithText(final int tag, String session_key,String userId, String pageId, String postText,  final APIServerResponse _createPostResponse) {

        RequestBody postTextdata = RequestBody.create(MediaType.parse("text/plain"), postText);
        RequestBody fileType = RequestBody.create(MediaType.parse("text/plain"), "0");

        Call call = apiReference.createPagePostText(session_key, pageId, userId,postTextdata,fileType);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                _createPostResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                _createPostResponse.onError(tag, t);
            }
        });
    }

    public void createPostPageWithImage(final int tag, String session_key,String userId, String pageId, String postText, String postImage, final APIServerResponse _createPostResponse) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(postImage));
        MultipartBody.Part body = MultipartBody.Part.createFormData("PagePostModel[attached_file]", "user_profile.jpg", requestBody);
        RequestBody postTextdata = RequestBody.create(MediaType.parse("text/plain"), postText);
        RequestBody fileType = RequestBody.create(MediaType.parse("text/plain"), "1");

        Call call = apiReference.createPagePostImage(session_key, pageId,userId, postTextdata, fileType, body);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                _createPostResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                _createPostResponse.onError(tag, t);
            }
        });
    }

    public void createPostPageWithVideo(final int tag, String session_key,String userId, String pageId, String postText, String postVideo, String postThumbnail, final APIServerResponse _createPostResponse) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("video/*"), new File(postVideo));
        MultipartBody.Part body = MultipartBody.Part.createFormData("PagePostModel[attached_file]", "user_video.mp4", requestBody);
        RequestBody postTextdata = RequestBody.create(MediaType.parse("text/plain"), postText);
        RequestBody fileType = RequestBody.create(MediaType.parse("text/plain"), "2");
        RequestBody postVideoThumbnail = RequestBody.create(MediaType.parse("image/*"), new File(postThumbnail));
        MultipartBody.Part bodyThnumbnail = MultipartBody.Part.createFormData("PagePostModel[thumbnail_file]", "postthumbnail_picture.jpg", postVideoThumbnail);

        Call call = apiReference.createPagePostVedio(session_key, pageId,userId, postTextdata, fileType,bodyThnumbnail, body);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                _createPostResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {

                _createPostResponse.onError(tag, t);
            }
        });
    }

 /*   public void createPostWithoutImage(final int tag, String userId, String session_key, String postText, String user_type, String taggedUser, final APIServerResponse _createPostResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("PostModel[post_on]", userId);
        params.put("PostModel[post_type]", user_type);
        params.put("PostModel[post_content]", postText);
        params.put("tag_list", taggedUser);

        Call call = apiReference.create_post_without_image(session_key, params);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                _createPostResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {

                _createPostResponse.onError(tag, t);
            }
        });
    }
*/

    public void removeProfileOrCoverPic(final int tag, String sessionKey, String userId,String profilePic, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("type", profilePic);
        Call<RemoveProfileModal> call = apiReference.removeProfileOrCoverPic(sessionKey,userId, params);
        call.enqueue(new Callback<RemoveProfileModal>() {
            @Override
            public void onResponse(Call<RemoveProfileModal> call, Response<RemoveProfileModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<RemoveProfileModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void createGroup(final int tag, String sessionKey,String logginUserID, String groupName, String groupDescriprtion, String groupIsActive, String addedFriends, String groupPic, final APIServerResponse apiServerResponse) {

        MultipartBody.Part imageBody = null;
        if (!groupPic.equalsIgnoreCase("")) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(groupPic));
            imageBody = MultipartBody.Part.createFormData("Groups[group_icon]", "group_image.jpg", requestBody);
        }


        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), groupName);
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), groupDescriprtion);
        RequestBody isActive = RequestBody.create(MediaType.parse("text/plain"), groupIsActive);
        RequestBody friends = RequestBody.create(MediaType.parse("text/plain"), addedFriends);

        Call<CreateGroupModal> call = apiReference.createGroup(sessionKey,logginUserID, name, description, isActive, friends, imageBody);
        call.enqueue(new Callback<CreateGroupModal>() {
            @Override
            public void onResponse(Call<CreateGroupModal> call, Response<CreateGroupModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CreateGroupModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });


    }


    public void updateGroup(final int tag, String sessionKey, String groupId, String groupName, String groupDescriprtion, String groupIsActive, String addedFriends, String groupPic, final APIServerResponse apiServerResponse) {

        MultipartBody.Part imageBody = null;
        if (groupPic != null && !groupPic.equalsIgnoreCase("")) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(groupPic));
            imageBody = MultipartBody.Part.createFormData("Groups[group_icon]", "group_image.jpg", requestBody);
        }
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), groupName);
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), groupDescriprtion);
        RequestBody isActive = RequestBody.create(MediaType.parse("text/plain"), groupIsActive);
        RequestBody friends = RequestBody.create(MediaType.parse("text/plain"), addedFriends);

        Call<UpdateGroupModal> call = apiReference.updateGroup(sessionKey, groupId, name, description, isActive, friends, imageBody);
        call.enqueue(new Callback<UpdateGroupModal>() {
            @Override
            public void onResponse(Call<UpdateGroupModal> call, Response<UpdateGroupModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<UpdateGroupModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });


    }

    public void getGroupsList(final int tag, String sessionkey,String userId, final APIServerResponse apiServerResponse) {

      //  RequestBody userIdReq = RequestBody.create(MediaType.parse("text/plain"), userId);
        Call<GroupListingModal> call = apiReference.getGroupsList(sessionkey,userId);
        call.enqueue(new Callback<GroupListingModal>() {
            @Override
            public void onResponse(Call<GroupListingModal> call, Response<GroupListingModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupListingModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllAlbumImages(final int tag, String sessionKey, String albumId, final APIServerResponse apiServerResponse) {
        //AllAlbumImageResponse
        Call<AllAlbumImageResponse> call = apiReference.getAllAlbumImage(sessionKey, albumId);
        call.enqueue(new Callback<AllAlbumImageResponse>() {
            @Override
            public void onResponse(Call<AllAlbumImageResponse> call, Response<AllAlbumImageResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AllAlbumImageResponse> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void userFollowUnfollow(final int tag, String session_key, String id, String loginedUserdId, final APIServerResponse apiServerResponse) {
        Call<FollowResponse> call = apiReference.followUser(session_key, id,loginedUserdId);
        call.enqueue(new Callback<FollowResponse>() {
            @Override
            public void onResponse(Call<FollowResponse> call, Response<FollowResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<FollowResponse> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void userBlockUnBlock(final int tag, String session_key, String id,String userId, final APIServerResponse apiServerResponse) {
        Call<BlockResponse> call = apiReference.blockUnblockUser(session_key, id,userId);
        call.enqueue(new Callback<BlockResponse>() {
            @Override
            public void onResponse(Call<BlockResponse> call, Response<BlockResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<BlockResponse> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getMyNotificationSetting(final int tag, String session_key,String userId, final APIServerResponse apiServerResponse) {
        Call<NotificationReposne> call = apiReference.getMyNotificationSetting(session_key,userId);
        call.enqueue(new Callback<NotificationReposne>() {
            @Override
            public void onResponse(Call<NotificationReposne> call, Response<NotificationReposne> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<NotificationReposne> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getAllPlaylistSongs(final int tag, String session_key, String user_id,String  play_list_id, final APIServerResponse apiServerResponse) {
        Call<PlaylistSongsResponse> call = apiReference.getAllPlaylistSongs(session_key, play_list_id,user_id);
        call.enqueue(new Callback<PlaylistSongsResponse>() {
            @Override
            public void onResponse(Call<PlaylistSongsResponse> call, Response<PlaylistSongsResponse> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<PlaylistSongsResponse> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    /*String nameSt,String checkSt, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put(nameSt,checkSt);*/


    public void setNotification(final int tag, String session_key,String userId, String nameSt, String checkSt, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put(nameSt, checkSt);
        Call<NotificationReposne> call = apiReference.saveNotification(session_key,userId, params);
        call.enqueue(new Callback<NotificationReposne>() {
            @Override
            public void onResponse(Call<NotificationReposne> call, Response<NotificationReposne> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<NotificationReposne> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    //checkBlockFollow
    public void checkBlockFollow(final int tag, String session_key, String loginedUserId,String userId, final APIServerResponse apiServerResponse) {
        Call<CheckBlockFollow> call = apiReference.checkBlockFollow(session_key, loginedUserId,userId);
        call.enqueue(new Callback<CheckBlockFollow>() {
            @Override
            public void onResponse(Call<CheckBlockFollow> call, Response<CheckBlockFollow> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<CheckBlockFollow> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void addSongToPlaylist(final int tag, String sessionKey,String userId, String playlistId, String songId, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("id", playlistId);
        params.put("song_id", songId);
        params.put("user_id", userId);
        Call<GroupDeleteModal> call = apiReference.addSongToPlaylist(sessionKey, params);
        call.enqueue(new Callback<GroupDeleteModal>() {
            @Override
            public void onResponse(Call<GroupDeleteModal> call, Response<GroupDeleteModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupDeleteModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void deletePlaylist(final int tag, String sessionKey, String playlistId, final APIServerResponse apiServerResponse) {
        Call<GroupDeleteModal> call = apiReference.deletePlayList(sessionKey, playlistId);
        call.enqueue(new Callback<GroupDeleteModal>() {
            @Override
            public void onResponse(Call<GroupDeleteModal> call, Response<GroupDeleteModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupDeleteModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void deletSongFromPlaylist(final int tag, String sessionKey, String playlistId, String songId, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("id", playlistId);
        params.put("song_id", songId);
        Call<GroupDeleteModal> call = apiReference.removeSongPlaylist(sessionKey, params);
        call.enqueue(new Callback<GroupDeleteModal>() {
            @Override
            public void onResponse(Call<GroupDeleteModal> call, Response<GroupDeleteModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupDeleteModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    //api/playlist/my?id=215
    public void getAllPlayList(final int tag, String sessionKey, String userId, final APIServerResponse apiServerResponse) {
        //215
        Call<AllPlayList> call = apiReference.getAllPlaylist(sessionKey, userId);
        call.enqueue(new Callback<AllPlayList>() {
            @Override
            public void onResponse(Call<AllPlayList> call, Response<AllPlayList> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AllPlayList> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void deleteGroup(final int tag, String sessionKey, String groupId, final APIServerResponse apiServerResponse) {
        Call<GroupDeleteModal> call = apiReference.deleteGroup(sessionKey, groupId);
        call.enqueue(new Callback<GroupDeleteModal>() {
            @Override
            public void onResponse(Call<GroupDeleteModal> call, Response<GroupDeleteModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupDeleteModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void albumListing(final int tag, String sessionKey, String albumID, final APIServerResponse apiServerResponse) {
        Call<AlbumListingModal> call = apiReference.getAlbumListing(sessionKey, albumID);
        call.enqueue(new Callback<AlbumListingModal>() {
            @Override
            public void onResponse(Call<AlbumListingModal> call, Response<AlbumListingModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AlbumListingModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void getAllSettingsType(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {

        Call<SettingsMainModal> call = apiReference.getAllSettingsType(sessionKey);

        call.enqueue(new Callback<SettingsMainModal>() {
            @Override
            public void onResponse(Call<SettingsMainModal> call, Response<SettingsMainModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SettingsMainModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void setSettings(final int tag, String sessionKey,String userId, String settingTypeID, String settingTypeValue, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("SettingsModel[type]", settingTypeID);
        params.put("SettingsModel[type_value]", settingTypeValue);

        Call<SettingsSetModal> call = apiReference.setSettings(sessionKey,userId, params);
        call.enqueue(new Callback<SettingsSetModal>() {
            @Override
            public void onResponse(Call<SettingsSetModal> call, Response<SettingsSetModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SettingsSetModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getSettingValue(final int tag, String sessionKey,String userid, String settingTypeId, final APIServerResponse apiServerResponse) {


        Call<SettingsGetValueModal> call = apiReference.getSettingValue(sessionKey,userid, settingTypeId);

        call.enqueue(new Callback<SettingsGetValueModal>() {
            @Override
            public void onResponse(Call<SettingsGetValueModal> call, Response<SettingsGetValueModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<SettingsGetValueModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void getMyPrayers(final int tag, String sessionKey, String userId, final APIServerResponse apiServerResponse) {
        Call<MyPrayerModal> call = apiReference.getMyPrayers(sessionKey, userId);
        call.enqueue(new Callback<MyPrayerModal>() {
            @Override
            public void onResponse(Call<MyPrayerModal> call, Response<MyPrayerModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MyPrayerModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getMyTestimony(final int tag, String sessionKey, String userId, final APIServerResponse apiServerResponse) {
        Call<TestimonyModalNew> call = apiReference.getMyTestimony(sessionKey, userId);
        call.enqueue(new Callback<TestimonyModalNew>() {
            @Override
            public void onResponse(Call<TestimonyModalNew> call, Response<TestimonyModalNew> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<TestimonyModalNew> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void getMyHelpRequest(final int tag, String sessionKey, String userId, final APIServerResponse apiServerResponse) {
        Call<HelpRequestListModal> call = apiReference.getMyHelpRequest(sessionKey, userId);
        call.enqueue(new Callback<HelpRequestListModal>() {
            @Override
            public void onResponse(Call<HelpRequestListModal> call, Response<HelpRequestListModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<HelpRequestListModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }
   /* //////////////////

    public void albumListingExoDemo(final int tag, String sessionKey, String albumID, final APIServerResponse apiServerResponse) {
        Call<AlbumListingModalExoMediaDemo> call = apiReference.getAlbumListingExoDemo(sessionKey, albumID);
        call.enqueue(new Callback<AlbumListingModalExoMediaDemo>() {
            @Override
            public void onResponse(Call<AlbumListingModalExoMediaDemo> call, Response<AlbumListingModalExoMediaDemo> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<AlbumListingModalExoMediaDemo> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    ///////////////////////////*/

    public void getBookDetails(final int tag, String sessionkey, String bookID, final APIServerResponse apiServerResponse) {

        Call<BookDetailsModal> call = apiReference.getBookDetails(sessionkey, bookID);
        call.enqueue(new Callback<BookDetailsModal>() {
            @Override
            public void onResponse(Call<BookDetailsModal> call, Response<BookDetailsModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<BookDetailsModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void deletePost(final int tag, String sessionKey, String postID, final APIServerResponse apiServerResponse) {

        Call<PostDeleteModal> call = apiReference.deletePost(sessionKey, postID);
        call.enqueue(new Callback<PostDeleteModal>() {
            @Override
            public void onResponse(Call<PostDeleteModal> call, Response<PostDeleteModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<PostDeleteModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void deletPagePost(final int tag, String sessionKey, String postID, final APIServerResponse apiServerResponse) {

        Call<PostDeleteModal> call = apiReference.deletePagePost(sessionKey, postID);
        call.enqueue(new Callback<PostDeleteModal>() {
            @Override
            public void onResponse(Call<PostDeleteModal> call, Response<PostDeleteModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<PostDeleteModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }



    public void unfriend(final int tag, String sessionKey, String userId,String friendID, final APIServerResponse apiServerResponse) {
        Call<UnfriendAFriendModal> call = apiReference.unfriend(sessionKey,userId, friendID);
        call.enqueue(new Callback<UnfriendAFriendModal>() {
            @Override
            public void onResponse(Call<UnfriendAFriendModal> call, Response<UnfriendAFriendModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<UnfriendAFriendModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getBooksLanguage(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {
        Call<BooksLanguagesModal> call = apiReference.getBooksLanguages(sessionKey);
        call.enqueue(new Callback<BooksLanguagesModal>() {
            @Override
            public void onResponse(Call<BooksLanguagesModal> call, Response<BooksLanguagesModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<BooksLanguagesModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void sendPaymentDeatilstoServer(final int tag, String sessionKey, String bookId, String type, String firstname, String lastname, String email, String city, String zipcode, String mobilenumber, String streetaddress, String userCredentials, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("BookOrder[firstname]", firstname);
        params.put("BookOrder[lastname]", lastname);
        params.put("BookOrder[email]", email);
        params.put("BookOrder[city]", city);
        params.put("BookOrder[zipcode]", zipcode);
        params.put("BookOrder[mobile_number]", mobilenumber);
        params.put("BookOrder[street_add]", streetaddress);
        params.put("user_credentials", userCredentials);
        Call<BookPaymentModal> call = apiReference.getBookpaymentHash(sessionKey, bookId, type, params);
        call.enqueue(new Callback<BookPaymentModal>() {
            @Override
            public void onResponse(Call<BookPaymentModal> call, Response<BookPaymentModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<BookPaymentModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void sendDonationPaymentDetailsToServer(final int tag, String sessionKey, String firstName, String lastName, String emailID, String city, String zipcode, String mobileNo, String address, String amount, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("DonationDetails[first_name]", firstName);
        params.put("DonationDetails[last_name]", lastName);
        params.put("DonationDetails[email]", emailID);
        params.put("DonationDetails[city]", city);
        params.put("DonationDetails[zipcode]", zipcode);
        params.put("DonationDetails[mobile_number]", mobileNo);
        params.put("DonationDetails[street_add]", address);
        params.put("DonationDetails[amount]", amount);

        Call<BookPaymentModal> call = apiReference.getDonationPaymentHash(sessionKey, params);
        call.enqueue(new Callback<BookPaymentModal>() {
            @Override
            public void onResponse(Call<BookPaymentModal> call, Response<BookPaymentModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<BookPaymentModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getMyBooks(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {
        Call<MyBooksModal> call = apiReference.getMyBooks(sessionKey);
        call.enqueue(new Callback<MyBooksModal>() {
            @Override
            public void onResponse(Call<MyBooksModal> call, Response<MyBooksModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MyBooksModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getMyMessages(final int tag, String sessionkey, String userId, final APIServerResponse apiServerResponse) {

        Call<MessagesModal> call = apiReference.getMessages(sessionkey, userId);
        call.enqueue(new Callback<MessagesModal>() {
            @Override
            public void onResponse(Call<MessagesModal> call, Response<MessagesModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<MessagesModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getMyNotifications(final int tag, String sessionKey, String userId, final APIServerResponse apiServerResponse) {
        Call<NotificationsModal> call = apiReference.getNotifications(sessionKey,userId);
        call.enqueue(new Callback<NotificationsModal>() {
            @Override
            public void onResponse(Call<NotificationsModal> call, Response<NotificationsModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<NotificationsModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void searchMusic(final int tag, String sessionKey, String query, final APIServerResponse apiServerResponse) {
        Call<JoyMusicSearchModal> call = apiReference.searchMusic(sessionKey, query);
        call.enqueue(new Callback<JoyMusicSearchModal>() {
            @Override
            public void onResponse(Call<JoyMusicSearchModal> call, Response<JoyMusicSearchModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JoyMusicSearchModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getJoyMusicSongs(final int tag, String sessionkey, final APIServerResponse apiServerResponse) {

        Call<JoyMusicSongsModal> call = apiReference.getJoyMusicSongs(sessionkey);
        call.enqueue(new Callback<JoyMusicSongsModal>() {
            @Override
            public void onResponse(Call<JoyMusicSongsModal> call, Response<JoyMusicSongsModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JoyMusicSongsModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void getJoyMusicSongsViewAll(final int tag, String sessionkey, String languageID, String pageNumber, final APIServerResponse apiServerResponse) {

        Map<String, String> params = new HashMap<>();
        params.put("id", languageID);
        params.put("page", pageNumber);

        Call<JoyMusicViewAllSongsModal> call = apiReference.getAllSongsList(sessionkey, params);
        call.enqueue(new Callback<JoyMusicViewAllSongsModal>() {
            @Override
            public void onResponse(Call<JoyMusicViewAllSongsModal> call, Response<JoyMusicViewAllSongsModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JoyMusicViewAllSongsModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void buyDownloadMusic(final int tag, String sessionKey, String id, String type, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("id", id);
        params.put("type", type);

        Call<BuyMusicDownloadedModal> call = apiReference.buyDownloadMusic(sessionKey, params);
        call.enqueue(new Callback<BuyMusicDownloadedModal>() {
            @Override
            public void onResponse(Call<BuyMusicDownloadedModal> call, Response<BuyMusicDownloadedModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<BuyMusicDownloadedModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void getJoyMusicAlbumsViewAll(final int tag, String sessionkey, String languageID, String pageNumber, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("id", languageID);
        params.put("page", pageNumber);

        Call<JoyMusicViewAllAlbumModal> call = apiReference.getAllAlbumsList(sessionkey, params);
        call.enqueue(new Callback<JoyMusicViewAllAlbumModal>() {
            @Override
            public void onResponse(Call<JoyMusicViewAllAlbumModal> call, Response<JoyMusicViewAllAlbumModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JoyMusicViewAllAlbumModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });


    }


    public void getJoyMusicSubscriptionPlans(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {


        Call<JoyMusicSubscriptionPlansModal> call = apiReference.getJoyMusicSubscriptionPlans(sessionKey);

        call.enqueue(new Callback<JoyMusicSubscriptionPlansModal>() {
            @Override
            public void onResponse(Call<JoyMusicSubscriptionPlansModal> call, Response<JoyMusicSubscriptionPlansModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JoyMusicSubscriptionPlansModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void checkjoyMusicSubscription(final int tag, String sessionKey,String userId, final APIServerResponse apiServerResponse) {
        Call<JoyMusicSubscriptionCheckModal> call = apiReference.checkJoyMusicSubscription(sessionKey,userId);
        call.enqueue(new Callback<JoyMusicSubscriptionCheckModal>() {
            @Override
            public void onResponse(Call<JoyMusicSubscriptionCheckModal> call, Response<JoyMusicSubscriptionCheckModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JoyMusicSubscriptionCheckModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void myDownloads(final int tag, String sessionKey,String loggedInUserId, final APIServerResponse apiServerResponse) {
        Call<JoyMusicMyDownloadsModal> call = apiReference.myDownloads(sessionKey,loggedInUserId);
        call.enqueue(new Callback<JoyMusicMyDownloadsModal>() {
            @Override
            public void onResponse(Call<JoyMusicMyDownloadsModal> call, Response<JoyMusicMyDownloadsModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JoyMusicMyDownloadsModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void  booksBuyFreeBook(final int tag, String sessionKey, String bookId, String firstname, String lastname, String email, String city, String zipcode, String mobilenumber, String streetaddress, final APIServerResponse apiServerResponse) {
        Map<String, String> params = new HashMap<>();
        params.put("BookOrder[firstname]", firstname);
        params.put("BookOrder[lastname]", lastname);
        params.put("BookOrder[email]", email);
        params.put("BookOrder[city]", city);
        params.put("BookOrder[zipcode]", zipcode);
        params.put("BookOrder[mobile_number]", mobilenumber);
        params.put("BookOrder[street_add]", streetaddress);
        Call<BooksBuyFreeBookModal> call = apiReference.buyFreeBook(sessionKey, bookId, params);
        call.enqueue(new Callback<BooksBuyFreeBookModal>() {
            @Override
            public void onResponse(Call<BooksBuyFreeBookModal> call, Response<BooksBuyFreeBookModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<BooksBuyFreeBookModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void getHomePostsDemo(final int tag, String sessionKey, String userId,String currentPage, final APIServerResponse apiServerResponse) {

        Call<DemoHomeModal> call = apiReference.getHomePosts(sessionKey,userId, currentPage);
        call.enqueue(new Callback<DemoHomeModal>() {
            @Override
            public void onResponse(Call<DemoHomeModal> call, Response<DemoHomeModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<DemoHomeModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void getAllPagesPosts(final int tag, String sessionKey, String pageId,String userId, String currentPage, final APIServerResponse apiServerResponse) {
        Call<DemoHomeModal> call = apiReference.getAllPagePosts(sessionKey, pageId,userId);
        call.enqueue(new Callback<DemoHomeModal>() {
            @Override
            public void onResponse(Call<DemoHomeModal> call, Response<DemoHomeModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<DemoHomeModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getGroupHomeWallPosts(final int tag, String sessionKey, String groupId, String pageNumber, final APIServerResponse apiServerResponse) {

        Call<GroupHomeWallModal> call = apiReference.getGroupHomeWallPosts(sessionKey, groupId, pageNumber);
        call.enqueue(new Callback<GroupHomeWallModal>() {
            @Override
            public void onResponse(Call<GroupHomeWallModal> call, Response<GroupHomeWallModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupHomeWallModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void likeGroupPost(final int tag, String sessionKey,String userId, String groupPostId, final APIServerResponse apiServerResponse) {

        Call<GroupHomeWallPostLike> call = apiReference.likeGroupPost(sessionKey, groupPostId,userId);
        call.enqueue(new Callback<GroupHomeWallPostLike>() {
            @Override
            public void onResponse(Call<GroupHomeWallPostLike> call, Response<GroupHomeWallPostLike> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupHomeWallPostLike> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void getGroupPostDetailsAndComments(final int tag, String sessionKey, String groupPostId, final APIServerResponse apiServerResponse) {


        Call<GroupPostDetailCommentsModal> call = apiReference.getGroupPostDetailAndComments(sessionKey, groupPostId);
        call.enqueue(new Callback<GroupPostDetailCommentsModal>() {
            @Override
            public void onResponse(Call<GroupPostDetailCommentsModal> call, Response<GroupPostDetailCommentsModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupPostDetailCommentsModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void commentOnGroupPost(final int tag, String sessionKey,String userId, String groupPostId, String comment, final APIServerResponse apiServerResponse) {

        Call<GroupCommentModal> call = apiReference.commentOnGroupPost(sessionKey,userId, groupPostId, comment);
        call.enqueue(new Callback<GroupCommentModal>() {
            @Override
            public void onResponse(Call<GroupCommentModal> call, Response<GroupCommentModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupCommentModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }

    public void getVideoOnWall(final int tag, String sessionKey, final APIServerResponse apiServerResponse) {

        Call<VideoOnWallModal> call = apiReference.getVideoOnWall(sessionKey);
        call.enqueue(new Callback<VideoOnWallModal>() {
            @Override
            public void onResponse(Call<VideoOnWallModal> call, Response<VideoOnWallModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<VideoOnWallModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }


    public void groupPostView(final int tag, String sessionKey, String postId, final APIServerResponse apiServerResponse) {

        Call<GroupPostViewModal> call = apiReference.viewGroupPost(sessionKey, postId);
        call.enqueue(new Callback<GroupPostViewModal>() {
            @Override
            public void onResponse(Call<GroupPostViewModal> call, Response<GroupPostViewModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupPostViewModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }


    public void getAllPageImages(final int tag, String sessionKey,String userId, String pageId, final APIServerResponse apiServerResponse) {
        Call<PageImagesArray> call = apiReference.getAllPageImages(sessionKey,userId, pageId);
        call.enqueue(new Callback<PageImagesArray>() {
            @Override
            public void onResponse(Call<PageImagesArray> call, Response<PageImagesArray> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<PageImagesArray> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void setStatus(final int tag, String sessionKey, String userId, String pageId, String image, final APIServerResponse apiServerResponse) {
        MultipartBody.Part imageBody = null;
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), new File(image));
        imageBody = MultipartBody.Part.createFormData("PageModel[banner_img]", "bannerImage.jpg", requestBody);
        Call<GroupDeleteModal> call = apiReference.changeBannerImagePage(sessionKey,userId, pageId, imageBody);
        call.enqueue(new Callback<GroupDeleteModal>() {
            @Override
            public void onResponse(Call<GroupDeleteModal> call, Response<GroupDeleteModal> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<GroupDeleteModal> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }



    public void getAllLikeUserList(final int tag, String session_key, String userId,  String postID,String type,final APIServerResponse apiServerResponse) {
        Call call = apiReference.getHomeLikeUser(session_key, userId,postID,type);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

    public void getLikeUserListOfPage(final int tag, String session_key, String userId,  String postID,String likeType,final APIServerResponse apiServerResponse) {
        Call call = apiReference.getLikedUserListOfPage(session_key, userId,postID);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }

//getProfileById

    public void getAboutInfo(final int tag, String session_key, String userId,String friendid,final APIServerResponse apiServerResponse) {
        Call call = apiReference.getProfileById(session_key, userId,friendid);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                apiServerResponse.onSuccess(tag, response);
                /*{
    "status": "1",
    "detail": {
        "id": 215,
        "username": "babitanayal1226",
        "email": "babitanayal1226@gmail.com",
        "updated": 1,
        "profile_details": {
            "firstname": "Pushpeen",
            "lastname": "Nayal",
            "d_o_b": "1992-7-26",
            "phone": "8800845355",
            "gender": "FEMALE",
            "location": "",
            "country": "",
            "state": "Andaman and Nicobar Islands",
            "cover_pic": "http://call4blessing.com/uploads/coverphoto/1517482048user_cover.jpg",
            "profile_pic": "http://call4blessing.com/uploads/profilepic/1516894598user_profile.jpg"
        },
        "is_friend": 2,
        "request_by_me": false,
        "request_respond": "NO"
    }
}*/

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });
    }



    public void likePagePost(final int tag, String session_key, String userId, String post_id, String postPrayerOrTestimony, final APIServerResponse apiServerResponse) {
        Call<JsonObject> call = apiReference.likePagePost(session_key,post_id,userId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                apiServerResponse.onSuccess(tag, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                apiServerResponse.onError(tag, t);
            }
        });

    }



}
