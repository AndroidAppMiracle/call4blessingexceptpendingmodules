package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by satoti.garg on 10/31/2017.
 */

public class InivedUsers {
    private String status;

    private List<InvitedUsersId> list;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<InvitedUsersId> getList ()
    {
        return list;
    }

    public void setList (List<InvitedUsersId> list)
    {
        this.list = list;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", list = "+list+"]";
    }
}
