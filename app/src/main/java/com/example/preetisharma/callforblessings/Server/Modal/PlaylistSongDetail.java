package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/28/2017.
 */

public class PlaylistSongDetail {
    private String album_id;

    private String id;

    private String is_downloaded;

    private String song_name;

    private String music_file;

    private String payment_type;

    public String getAlbum_id ()
    {
        return album_id;
    }

    public void setAlbum_id (String album_id)
    {
        this.album_id = album_id;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getIs_downloaded ()
    {
        return is_downloaded;
    }

    public void setIs_downloaded (String is_downloaded)
    {
        this.is_downloaded = is_downloaded;
    }

    public String getSong_name ()
    {
        return song_name;
    }

    public void setSong_name (String song_name)
    {
        this.song_name = song_name;
    }

    public String getMusic_file ()
    {
        return music_file;
    }

    public void setMusic_file (String music_file)
    {
        this.music_file = music_file;
    }

    public String getPayment_type ()
    {
        return payment_type;
    }

    public void setPayment_type (String payment_type)
    {
        this.payment_type = payment_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [album_id = "+album_id+", id = "+id+", is_downloaded = "+is_downloaded+", song_name = "+song_name+", music_file = "+music_file+", payment_type = "+payment_type+"]";
    }
}
