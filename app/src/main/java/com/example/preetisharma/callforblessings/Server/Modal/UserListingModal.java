package com.example.preetisharma.callforblessings.Server.Modal;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by preeti.sharma on 1/24/2017.
 */

public class UserListingModal {


    /**
     * status : 1
     * list : [{"id":243,"username":"bagade.aakash","email":"bagade.aakash@gmail.com","updated":1,"profile_details":{"firstname":"Aakash","lastname":"Bagade","d_o_b":"1994-10-02","phone":"7039964483","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/1498162079user_cover.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1498161754user_profile.jpg"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"},{"id":242,"username":"vhodkmm","email":"vhodkmm@eurolegis.eu","updated":0,"profile_details":{"firstname":"Sancekix","lastname":"Sancekix","d_o_b":"1994-5-13","phone":"8586176289","gender":"","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"},{"id":241,"username":"suswbqr","email":"suswbqr@eurolegis.eu","updated":0,"profile_details":{"firstname":"Sancekix","lastname":"Sancekix","d_o_b":"2007-9-21","phone":"8637889662","gender":"","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"},{"id":239,"username":"frozenmoments42graphy","email":"frozenmoments42graphy@gmail.com","updated":0,"profile_details":{"firstname":"Anu","lastname":"Gaikwad","d_o_b":"1986-02-28","phone":"9594757502","gender":"FEMALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://call4blessing.com/theme/images/female-avatar.png"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"},{"id":238,"username":"abc23","email":"abc23@gmail.com","updated":0,"profile_details":{"firstname":"Abc","lastname":"Abc","d_o_b":"2017-05-23","phone":"9466997178","gender":"FEMALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://call4blessing.com/theme/images/female-avatar.png"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"},{"id":237,"username":"miracle","email":"miracle@test.com","updated":0,"profile_details":{"firstname":"Miracle","lastname":"Miracle","d_o_b":"1977-05-22","phone":"9988690694","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://call4blessing.com/theme/images/male-avatar.png"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":1,"request_respond":"NO"},{"id":236,"username":"geet1","email":"g1234@gmail.com","updated":0,"profile_details":{"firstname":"Geet","lastname":"","d_o_b":"","phone":"","gender":"","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"},{"id":232,"username":"ivan","email":"ivanwalter493@gmail.com","updated":0,"profile_details":{"firstname":"Ivan","lastname":"Walter","d_o_b":"","phone":"","gender":"","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"},{"id":231,"username":"arun","email":"arunverma3352@gmail.com","updated":0,"profile_details":{"firstname":"Arun","lastname":"Verma","d_o_b":"","phone":"","gender":"","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":1,"request_respond":"NO"},{"id":229,"username":"sanjaynewborn1995","email":"sanjaynewborn1995@gmail.com","updated":0,"profile_details":{"firstname":"sanjay","lastname":"deka","d_o_b":"1983-2-2","phone":"9641390444","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/14931417401447632673033.jpg","profile_pic":"http://call4blessing.com/theme/images/male-avatar.png"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"},{"id":227,"username":"g.ajaygupta12387","email":"g.ajaygupta12387@gmail.com","updated":0,"profile_details":{"firstname":"Ajay","lastname":"Gupta","d_o_b":"2017-04-20","phone":"9654231400","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://call4blessing.com/theme/images/male-avatar.png"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"},{"id":226,"username":"roomana4jesus","email":"roomana4jesus@gamil.com","updated":1,"profile_details":{"firstname":"Roomana","lastname":"Khan","d_o_b":"1991-08-20","phone":"9635853030","gender":"FEMALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1492607641user_profile.jpg"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"},{"id":224,"username":"pushpeen","email":"babitanayal@7colorsindia.com","updated":0,"profile_details":{"firstname":"","lastname":"","d_o_b":"","phone":"","gender":"","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"},{"id":223,"username":"Mukesh.singh","email":"Mukesh.singh@emptask.com","updated":1,"profile_details":{"firstname":"Mukesh","lastname":"Kumar","d_o_b":"1991/09/08","phone":"9988549399","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492602899image.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1495439742user_profile.jpg"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":1,"request_respond":"NO"},{"id":219,"username":"ashwanim002","email":"ashwanim002@gmail.com","updated":1,"profile_details":{"firstname":"Ashwani","lastname":"Kumar","d_o_b":"1987-06-30","phone":"9897333222","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/1492375718user_cover.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1497013149user_profile.jpg"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"},{"id":216,"username":"rajneeshjesus21","email":"rajneeshjesus21@gmail.com","updated":1,"profile_details":{"firstname":"Rajneesh ","lastname":"Kumar","d_o_b":"1988-08-21","phone":"9045171793","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/theme/images/cover-picture.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1496250098user_profile.jpg"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"PUBLIC","MESSAGE":"PUBLIC","is_friend":1,"request_respond":"NO"},{"id":215,"username":"babitanayal1226","email":"babitanayal1226@gmail.com","updated":1,"profile_details":{"firstname":"Pushpeen","lastname":"Nayal","d_o_b":"1992-7-26","phone":"8800845355","gender":"FEMALE","location":"","country":"","state":"Andaman and Nicobar Islands","cover_pic":"http://call4blessing.com/uploads/coverphoto/1495437303WhatsApp Image 2017-05-22 at 07.57.36.jpeg","profile_pic":"http://call4blessing.com/uploads/profilepic/1492345123user_profile.jpg"},"WALL":"PUBLIC","ABOUT INFO":"PUBLIC","FRIEND REQUEST":"FRIEND","MESSAGE":"PUBLIC","is_friend":0,"request_respond":"NO"}]
     */

    private String status;
    private List<ListBean> list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 243
         * username : bagade.aakash
         * email : bagade.aakash@gmail.com
         * updated : 1
         * profile_details : {"firstname":"Aakash","lastname":"Bagade","d_o_b":"1994-10-02","phone":"7039964483","gender":"MALE","location":"","country":"","state":"","cover_pic":"http://call4blessing.com/uploads/coverphoto/1498162079user_cover.jpg","profile_pic":"http://call4blessing.com/uploads/profilepic/1498161754user_profile.jpg"}
         * WALL : PUBLIC
         * ABOUT INFO : PUBLIC
         * FRIEND REQUEST : PUBLIC
         * MESSAGE : PUBLIC
         * is_friend : 0
         * request_respond : NO
         */

        private int id;
        private String username;
        private String email;
        private int updated;
        private ProfileDetailsBean profile_details;
        private String WALL;
        @SerializedName("ABOUT INFO")
        private String about_info;
        @SerializedName("FRIEND REQUEST")
        private String friend_request;
        private String MESSAGE;
        private int is_friend;
        private String request_respond;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getUpdated() {
            return updated;
        }

        public void setUpdated(int updated) {
            this.updated = updated;
        }

        public ProfileDetailsBean getProfile_details() {
            return profile_details;
        }

        public void setProfile_details(ProfileDetailsBean profile_details) {
            this.profile_details = profile_details;
        }

        public String getWALL() {
            return WALL;
        }

        public void setWALL(String WALL) {
            this.WALL = WALL;
        }

        public String getabout_info() {
            return about_info;
        }

        public void setabout_info(String about_info) {
            this.about_info = about_info;
        }

        public String getfriend_request() {
            return friend_request;
        }

        public void setfriend_request(String friend_request) {
            this.friend_request = friend_request;
        }

        public String getMESSAGE() {
            return MESSAGE;
        }

        public void setMESSAGE(String MESSAGE) {
            this.MESSAGE = MESSAGE;
        }

        public int getIs_friend() {
            return is_friend;
        }

        public void setIs_friend(int is_friend) {
            this.is_friend = is_friend;
        }

        public String getRequest_respond() {
            return request_respond;
        }

        public void setRequest_respond(String request_respond) {
            this.request_respond = request_respond;
        }

        public static class ProfileDetailsBean {
            /**
             * firstname : Aakash
             * lastname : Bagade
             * d_o_b : 1994-10-02
             * phone : 7039964483
             * gender : MALE
             * location :
             * country :
             * state :
             * cover_pic : http://call4blessing.com/uploads/coverphoto/1498162079user_cover.jpg
             * profile_pic : http://call4blessing.com/uploads/profilepic/1498161754user_profile.jpg
             */

            private String firstname;
            private String lastname;
            private String d_o_b;
            private String phone;
            private String gender;
            private String location;
            private String country;
            private String state;
            private String cover_pic;
            private String profile_pic;

            public String getFirstname() {
                return firstname;
            }

            public void setFirstname(String firstname) {
                this.firstname = firstname;
            }

            public String getLastname() {
                return lastname;
            }

            public void setLastname(String lastname) {
                this.lastname = lastname;
            }

            public String getD_o_b() {
                return d_o_b;
            }

            public void setD_o_b(String d_o_b) {
                this.d_o_b = d_o_b;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCover_pic() {
                return cover_pic;
            }

            public void setCover_pic(String cover_pic) {
                this.cover_pic = cover_pic;
            }

            public String getProfile_pic() {
                return profile_pic;
            }

            public void setProfile_pic(String profile_pic) {
                this.profile_pic = profile_pic;
            }
        }
    }
}
