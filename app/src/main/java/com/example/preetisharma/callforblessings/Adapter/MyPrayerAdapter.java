package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AmenPrayerModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.PrayerListModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CommentsListingActivity;
import com.example.preetisharma.callforblessings.demo.HomeLikeUserListActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 10/16/2017.
 */

public class MyPrayerAdapter extends RecyclerView.Adapter<MyPrayerAdapter.DataViewHolder> {

    public List<PrayerListModal> mList = new ArrayList<>();
    Activity mContext;
    private boolean expand = true;
    private static String likescount, commentsCount, amen = "Not amen";
    private static String liked = "not liked";

    public MyPrayerAdapter(Activity mContext, List<PrayerListModal> list) {
        mList = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        mList.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<PrayerListModal> items) {
        mList.addAll(items);
        notifyDataSetChanged();
    }


    @Override
    public MyPrayerAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.prayer_item, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final MyPrayerAdapter.DataViewHolder holder, int position) {
        holder.txtvw_prayer_title.setText(mList.get(position).getTitle());
        holder.txtvw_prayer_description.setText(mList.get(position).getDesc());
        //holder.txtvw_hallelujah_count.setText("0 Amen");
        holder.name_user_txt.setText( "("+mList.get(position).getName()+")");
        if (Integer.parseInt(mList.get(position).getLike_count()) == 0 || Integer.parseInt(mList.get(position).getLike_count()) == 1) {
            holder.txtvw_likes_count.setText(mList.get(position).getLike_count() + " "+"Like");
        } else {
            holder.txtvw_likes_count.setText(mList.get(position).getLike_count() + " "+"Likes");
        }

        if (Integer.parseInt(mList.get(position).getComment_count()) == 0 || Integer.parseInt(mList.get(position).getComment_count()) == 1) {
            holder.txtvw_comments_count.setText(mList.get(position).getComment_count() + " ");
        } else {
            holder.txtvw_comments_count.setText(mList.get(position).getComment_count() + " ");
        }
        if (Integer.parseInt(mList.get(position).getAmen_count()) == 0 || Integer.parseInt(mList.get(position).getAmen_count()) == 1) {
            holder.txtvw_hallelujah_count.setText(mList.get(position).getAmen_count() + " ");
        } else {
            holder.txtvw_hallelujah_count.setText(mList.get(position).getAmen_count() + " ");
        }
        if (mList.get(holder.getAdapterPosition()).getAmen_flag().equalsIgnoreCase("Not Amen")) {
            int imgResource = R.drawable.prayer_amen;
            holder.txtvw_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
        } else {
            int imgResource = R.drawable.ic_selected_amen;
            holder.txtvw_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
        }
        if (mList.get(holder.getAdapterPosition()).getLike_flag().equalsIgnoreCase("Not liked")) {
            int imgResource = R.drawable.prayer_like_icon;
            holder.txtvw_like_prayer.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
        } else {
            int imgResource = R.drawable.ic_liked;
            holder.txtvw_like_prayer.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
        }

        holder.child_layout.setVisibility(View.VISIBLE);

        holder.txtvw_likes_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        if (mList.get(position).getImage() != null) {
            if (mList.get(position).getImage().equalsIgnoreCase("")) {
                holder.post_img.setVisibility(View.VISIBLE);
                Glide.with(mContext).load(mList.get(position).getImage()).placeholder(R.drawable.placeholder_callforblessings).thumbnail(0.1f).into(holder.post_img);
            } else {
                holder.post_img.setVisibility(View.GONE);
            }
        }
        holder.txtvw_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(mContext, CommentsListingActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.COMMENTS, String.valueOf(mList.get(holder.getAdapterPosition()).getId()));
                b.putString(Constants.POSTTYPE, Constants.PRAYER);
                i.putExtras(b);
                mContext.startActivity(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {


        //Prayer List
        @BindView(R.id.txtvw_prayer_title)
        AppCompatTextView txtvw_prayer_title;
        @BindView(R.id.name_user_txt)
        AppCompatTextView name_user_txt;
        @BindView(R.id.child_layout)
        LinearLayout child_layout;
        @BindView(R.id.txtvw_prayer_description)
        AppCompatTextView txtvw_prayer_description;
        @BindView(R.id.txtvw_like_prayer)
        AppCompatTextView txtvw_like_prayer;
        @BindView(R.id.txtvw_comment)
        AppCompatTextView txtvw_comment;
        @BindView(R.id.txtvw_amen)
        AppCompatTextView txtvw_amen;
        @BindView(R.id.txtvw_likes_count)
        AppCompatTextView txtvw_likes_count;
        @BindView(R.id.txtvw_comments_count)
        AppCompatTextView txtvw_comments_count;
        @BindView(R.id.txtvw_hallelujah_count)
        AppCompatTextView txtvw_hallelujah_count;
        @BindView(R.id.card_video)
        CardView card_video;
        @BindView(R.id.post_img)
        AppCompatImageView post_img;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }


        @OnClick(R.id.txtvw_amen)
        public void amenPrayerPost() {
            //((BaseActivity) mContext).showToast("adapter pos " + getAdapterPosition(), Toast.LENGTH_SHORT);
            //((BaseActivity) mContext).showToast("adapter flag " + mList.get(getAdapterPosition()).getAmen_flag(), Toast.LENGTH_SHORT);

            if (((BaseActivity) mContext).isConnectedToInternet()) {
                if (txtvw_amen.isEnabled()) {
                    txtvw_amen.setEnabled(false);
                }
                ServerAPI.getInstance().amenPrayerPost(APIServerResponse.AMEN_PRAYER, ((BaseActivity) mContext).getUserSessionId(), ((BaseActivity) mContext).getUserID(), String.valueOf(mList.get(getAdapterPosition()).getId()), this);
            } else {
                ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }

            //amen = mList.get(getAdapterPosition()).getAmen_flag();
            //amencount = mList.get(getAdapterPosition()).getAmen_count();
            /*if (amen.equalsIgnoreCase("Not amen")) {
                int imgResource = R.drawable.ic_selected_amen;
                txtvw_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                if (((BaseActivity) mContext).isConnectedToInternet()) {
                    ((BaseActivity) mContext).showLoading();
                    ServerAPI.getInstance().amenPrayerPost(APIServerResponse.AMEN_PRAYER, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(mList.get(getAdapterPosition()).getId()), this);
                } else {
                    ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }

            } else {
                int imgResource = R.drawable.prayer_amen;
                txtvw_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                if (((BaseActivity) mContext).isConnectedToInternet()) {
                    ((BaseActivity) mContext).showLoading();
                    ServerAPI.getInstance().amenPrayerPost(APIServerResponse.UNAMEN, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(mList.get(getAdapterPosition()).getId()), this);
                } else {
                    ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }*/

        }


        @OnClick(R.id.txtvw_like_prayer)
        public void likePost() {

            if (txtvw_like_prayer.isEnabled()) {
                txtvw_like_prayer.setEnabled(false);
            }
            liked = mList.get(getAdapterPosition()).getLike_flag();
            likescount = mList.get(getAdapterPosition()).getLike_count();

            if (liked.equalsIgnoreCase("Not Liked")) {
                int imgResource = R.drawable.ic_liked;
                txtvw_like_prayer.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                if (((BaseActivity) mContext).isConnectedToInternet()) {

                    ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) mContext).getUserSessionId(), ((BaseActivity) mContext).getUserID(), String.valueOf(mList.get(getAdapterPosition()).getId()), "PRAYER", this);
                } else {
                    ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }

            } else {
                int imgResource = R.drawable.prayer_like_icon;
                txtvw_like_prayer.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                if (((BaseActivity) mContext).isConnectedToInternet()) {

                    ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) mContext).getUserSessionId(), ((BaseActivity) mContext).getUserID(), String.valueOf(mList.get(getAdapterPosition()).getId()), "PRAYER", this);
                } else {
                    ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                }
            }
        }

        @OnClick(R.id.txtvw_no_of_likes)
        public void userLikedPost() {
            if (Integer.parseInt(mList.get(getAdapterPosition()).getLike_count()) >= 1) {
                Intent i = new Intent(mContext, HomeLikeUserListActivity.class);
                Bundle b = new Bundle();
                b.putString("postId", String.valueOf(mList.get(getAdapterPosition()).getId()));
                i.putExtras(b);
                mContext.startActivity(i);
            }
        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {
                    LikeModal likeModal;
                    AmenPrayerModal amenPrayerModal;
                    switch (tag) {
                        case APIServerResponse.LIKE: {
                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                if (!txtvw_like_prayer.isEnabled()) {
                                    txtvw_like_prayer.setEnabled(true);
                                }
                                ((BaseActivity) mContext).showToast("Prayer Liked", Toast.LENGTH_SHORT);
                                int imgResource = R.drawable.ic_liked;
                                txtvw_like_prayer.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(likescount);
                                likescountValue += 1;
                                liked = "liked";
                                mList.get(getAdapterPosition()).setLike_flag("liked");
                                mList.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));

                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_likes_count.setText(String.valueOf(likescountValue) + " ");
                                } else {
                                    txtvw_likes_count.setText(String.valueOf(likescountValue) + " ");
                                }
                            }
                        }
                        break;

                        case APIServerResponse.UNLIKE:
                            likeModal = (LikeModal) response.body();
                            if (likeModal.getStatus().equals("1")) {
                                if (!txtvw_like_prayer.isEnabled()) {
                                    txtvw_like_prayer.setEnabled(true);
                                }
                                int imgResource = R.drawable.prayer_like;
                                txtvw_like_prayer.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                int likescountValue = Integer.parseInt(likescount);
                                likescountValue--;
                                liked = "not liked";
                                mList.get(getAdapterPosition()).setLike_flag("not liked");
                                mList.get(getAdapterPosition()).setLike_count(String.valueOf(likescountValue));

                                if (likescountValue == 0 || likescountValue == 1) {
                                    txtvw_likes_count.setText(String.valueOf(likescountValue) + " ");
                                } else {
                                    txtvw_likes_count.setText(String.valueOf(likescountValue) + " ");
                                }
                            }
                            break;

                        case APIServerResponse.AMEN_PRAYER:
                            amenPrayerModal = (AmenPrayerModal) response.body();
                            if (amenPrayerModal.getStatus().equals("1")) {
                                if (!txtvw_amen.isEnabled()) {
                                    txtvw_amen.setEnabled(true);
                                }
                                if (amenPrayerModal.getAmen().equalsIgnoreCase("YES")) {
                                    int imgResourceAmen = R.drawable.ic_selected_amen;
                                    txtvw_amen.setCompoundDrawablesWithIntrinsicBounds(imgResourceAmen, 0, 0, 0);

                                    int hallecount = Integer.parseInt(mList.get(getAdapterPosition()).getAmen_count());
                                    //mList.get(getAdapterPosition()).setAmen_flag("Amen");
                                    hallecount++;

                                    mList.get(getAdapterPosition()).setAmen_count(String.valueOf(hallecount));
                                    if (hallecount == 0 || hallecount == 1) {
                                        txtvw_hallelujah_count.setText(String.valueOf(hallecount) + " ");
                                    } else {
                                        txtvw_hallelujah_count.setText(String.valueOf(hallecount) + " ");
                                    }
                                } else {
                                    int imgResourceNotAmen = R.drawable.prayer_amen;
                                    txtvw_amen.setCompoundDrawablesWithIntrinsicBounds(imgResourceNotAmen, 0, 0, 0);
                                    //mList.get(getAdapterPosition()).setAmen_flag("Not amen");
                                    int hallecountValue = Integer.parseInt(mList.get(getAdapterPosition()).getAmen_count());
                                    hallecountValue--;
                                    // liked = "not amen";
                                    //mList.get(getAdapterPosition()).setAmen_flag("Not amen");
                                    mList.get(getAdapterPosition()).setAmen_count(String.valueOf(hallecountValue));

                                    if (hallecountValue == 0 || hallecountValue == 1) {
                                        txtvw_hallelujah_count.setText(String.valueOf(hallecountValue) + " ");
                                    } else {
                                        txtvw_hallelujah_count.setText(String.valueOf(hallecountValue) + " ");
                                    }
                                }
                            }
                            break;
                       /* case APIServerResponse.UNAMEN: {
                            amenPrayerModal = (AmenPrayerModal) response.body();
                            if (amenPrayerModal.getStatus().equals("1")) {
                            }
                            int imgResource2 = R.drawable.prayer_amen;
                            txtvw_hallelujah_count.setCompoundDrawablesWithIntrinsicBounds(imgResource2, 0, 0, 0);
                            int hallecountValue = Integer.parseInt(amencount);
                            hallecountValue--;
                            liked = "not amen";
                            mList.get(getAdapterPosition()).setAmen_flag("not amen");
                            mList.get(getAdapterPosition()).setAmen_count(String.valueOf(hallecountValue));

                            if (hallecountValue == 0 || hallecountValue == 1) {
                                txtvw_hallelujah_count.setText(String.valueOf(hallecountValue) + "Amen");
                            } else {
                                txtvw_hallelujah_count.setText(String.valueOf(hallecountValue) + "Amens");
                            }
                        }
                        break;*/
                        default:
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onError(int tag, Throwable throwable) {
            try {
                ((BaseActivity) mContext).hideLoading();
                throwable.printStackTrace();
                switch (tag) {
                    case APIServerResponse.LIKE:
                        System.out.println("Error");
                        break;

                    case APIServerResponse.AMEN_PRAYER:
                        System.out.println("Error");
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }




}
