package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 6/20/2017.
 */

public class EventBusSingPostCommentModal {


    private final int comment_count;

    public EventBusSingPostCommentModal(int count) {
        this.comment_count = count;
    }

    public int getMessage() {
        return comment_count;
    }

}
