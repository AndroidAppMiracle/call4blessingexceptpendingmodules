package com.example.preetisharma.callforblessings.demo;

import android.Manifest;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.GridAdapterUpload;
import com.example.preetisharma.callforblessings.Utils.AutofitRecyclerView;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.FileUtils;

import java.util.ArrayList;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Kshitiz Bali on 5/2/2017.
 */

public class DemoAddbook extends BaseActivity {

    AppCompatImageView imgvw_upload_book_cover;
    RecyclerView recyclerView_myc;
    static int PICK_IMAGE_MULTIPLE = 1;
    static final int REQUEST_IMAGE_CAPTURE = 2;
    static final int RC_CAMERA_PERM_AND_STORAGE = 3;
    static final int RC_GALLERY_PERM_AND_STORAGE = 4;
    String[] perms = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    String[] galleryPerms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    ArrayList<String> filePaths = new ArrayList<>();
    GridAdapterUpload mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demodemo_add_book);

        imgvw_upload_book_cover = (AppCompatImageView) findViewById(R.id.imgvw_upload_book_cover);
        recyclerView_myc = (RecyclerView) findViewById(R.id.recyclerView_myc);
        /*recyclerView_myc.setLayoutManager(new GridLayoutManager(DemoAddbook.this, 3));*/
        recyclerView_myc.setLayoutManager(new LinearLayoutManager(DemoAddbook.this, LinearLayoutManager.HORIZONTAL, false));

        imgvw_upload_book_cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!filePaths.isEmpty() && filePaths.size() < 7) {
                    showAlertDialogSelectBookImages();
                } else {
                    showToast(getString(R.string.cant_share_more_than_6_media_items), Toast.LENGTH_SHORT);
                }

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_MULTIPLE) {
            if (data != null) {

                // If data.getData() == null means multiple images selected, else single image selected.
                if (data.getData() == null) {
                    ClipData clipData = data.getClipData();
                    if (clipData != null) {
                        for (int i = 0; i < clipData.getItemCount(); i++) {
                            ClipData.Item item = clipData.getItemAt(i);
                            Uri uri = item.getUri();
                            filePaths.add(FileUtils.getPath(DemoAddbook.this, uri));
                        }
                    }
                } else {
                    Log.i("HelloURIGAll", "" + data.getData());
                    filePaths.add(FileUtils.getPath(DemoAddbook.this, data.getData()));
                }
                //sendToServer();
                //previewMediaInGrid(filePaths);
            }
        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            //Log.i("HelloURIGAll", "" + data.getExtras());
            Log.i("HelloURIGAll", "" + data.getData());
            filePaths.add(FileUtils.getPath(DemoAddbook.this, data.getData()));
            //Bitmap imageBitmap = (Bitmap) extras.get("data");
            //mImageView.setImageBitmap(imageBitmap);
        }
        previewMediaInGrid(filePaths);
    }

    @AfterPermissionGranted(RC_CAMERA_PERM_AND_STORAGE)
    private void dispatchTakePictureIntent() {
        if (EasyPermissions.hasPermissions(DemoAddbook.this, perms)) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.access_camera_storage),
                    RC_CAMERA_PERM_AND_STORAGE, perms);
        }

    }

    @AfterPermissionGranted(RC_GALLERY_PERM_AND_STORAGE)
    private void dispatchSelectGalleryPicIntent() {

        if (EasyPermissions.hasPermissions(DemoAddbook.this, galleryPerms)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Pictures"), PICK_IMAGE_MULTIPLE);
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.access_camera_storage),
                    RC_CAMERA_PERM_AND_STORAGE, perms);
        }

    }

    private void previewMediaInGrid(ArrayList<String> mArrayUri) {
        //recyclerView_myc = (AutofitRecyclerView) findViewById(R.id.recyclerView_myc); //recyclerView_myc will be autofitrecyelrview
        recyclerView_myc.setVisibility(View.VISIBLE);
        //vidPreview.setVisibility(View.GONE);
        //imgPreview.setVisibility(View.GONE);
        mAdapter = new GridAdapterUpload(mArrayUri, getApplicationContext());
        recyclerView_myc.setAdapter(mAdapter);

    }


    private void showAlertDialogSelectBookImages() {

        try {
            //final CharSequence[] quantity = quantityList.toArray(new String[quantityList.size()]);

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle("Select image");
            /*dialogBuilder.setMessage("Select ");*/
            dialogBuilder.setCancelable(true);
            dialogBuilder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    dispatchTakePictureIntent();


                }
            });
            dialogBuilder.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    dispatchSelectGalleryPicIntent();


                }
            });

           /* dialogBuilder.setItems(quantity, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    String selectedText = quantity[item].toString();  //Selected item in listview
                    //ToastMsg.showShortToast(ProductDetailNew.this, selectedText);
                    qty_text.setText(selectedText);
                }
            });*/
            //Create alert dialog object via builder
            AlertDialog alertDialogObject = dialogBuilder.create();
            //Show the dialog
            alertDialogObject.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

}
