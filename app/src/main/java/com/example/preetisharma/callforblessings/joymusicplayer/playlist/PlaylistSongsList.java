package com.example.preetisharma.callforblessings.joymusicplayer.playlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.DemoDataPlayListSongsAdaper;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AllPlaylistSongObject;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.PlayListSongDescrption;
import com.example.preetisharma.callforblessings.Server.Modal.PlayListSongsListObject;
import com.example.preetisharma.callforblessings.Server.Modal.PlaylistSongDetail;
import com.example.preetisharma.callforblessings.Server.Modal.PlaylistSongsResponse;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.ItemClickSupport;
import com.example.preetisharma.callforblessings.demo.JoyMusicActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.AudioPlayerActivity;
import com.example.preetisharma.callforblessings.joymusicplayer.PlayMusicModal;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 7/26/2017.
 */

public class PlaylistSongsList extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, APIServerResponse {


    @BindView(R.id.main_collapsing_bar_tab_layout)
    CollapsingToolbarLayout main_collapsing_bar_tab_layout;
    @BindView(R.id.iv_playlist_list_banner)
    ImageView iv_playlist_list_banner;
    @BindView(R.id.atv_playlist_name)
    AppCompatTextView atv_playlist_name;
    @BindView(R.id.button_PlayAll)
    Button button_PlayAll;
    @BindView(R.id.main_toolbar)
    Toolbar main_toolbar;
    @BindView(R.id.nestedscroll)
    NestedScrollView nestedscroll;
    @BindView(R.id.rv_playlist)
    RecyclerView rv_playlist;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.fab_book_options)
    FloatingActionButton fab_book_options;

    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode;

    List<DemoDataModal> list = new ArrayList<>();
    List<PlaylistSongDetail> listSongsDetail = new ArrayList<>();

    ArrayList<PlayMusicModal> arrayListMusic = new ArrayList<>();
    DemoDataPlayListSongsAdaper adapter;
    List<PlayListSongDescrption> playListSongsListObject;
    ImageView delete_img, add_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist_songs_list);
        ButterKnife.bind(this);

        setSupportActionBar(main_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        add_img = (ImageView) findViewById(R.id.add_img);

        main_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaylistSongsList.this.finish();
            }
        });


        swipeRefreshLayout.setOnRefreshListener(this);

        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            if (extras.getString(Constants.PLAYLIST_ID, "1") != null && !extras.getString(Constants.PLAYLIST_ID, "1").equalsIgnoreCase("")) {
                main_collapsing_bar_tab_layout.setTitle("My Playlists " + extras.getString(Constants.PLAYLIST_ID, "1"));

                if (getIntent().hasExtra(Constants.PLAYLIST_NAME)) {
                    main_collapsing_bar_tab_layout.setTitle(getIntent().getStringExtra(Constants.PLAYLIST_NAME));
                }
                getAllPlaylistSong();
            }


        }
        add_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PlaylistSongsList.this, JoyMusicActivity.class));
            }
        });


        rv_playlist.setLayoutManager(new LinearLayoutManager(this));
        rv_playlist.setItemAnimator(new DefaultItemAnimator());
        rv_playlist.addItemDecoration(new DividerItemDecoration(this, GridLayoutManager.VERTICAL));
        rv_playlist.setNestedScrollingEnabled(false);

        actionModeCallback = new ActionModeCallback();

        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        getAllPlaylistSong();
                    }
                }
        );

        ItemClickSupport.addTo(rv_playlist).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                if (adapter.getSelectedItemCount() > 0) {
                    enableActionMode(position);
                } else {
                    playAudio(position);
                }

            }


        }).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                enableActionMode(position);
                return true;
            }
        });


        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                ///Toast.makeText(MainActivity.this, "getAdapterPosition" + viewHolder.getAdapterPosition(), Toast.LENGTH_SHORT).show();
                adapter.removeData(viewHolder.getAdapterPosition());
                adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                Snackbar.make(rv_playlist, "Song removed from playlist.", Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                if (actionMode != null & adapter.getSelectedItemCount() > 0) return 0;
                return super.getSwipeDirs(recyclerView, viewHolder);
            }
        };


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rv_playlist);
    }


    @OnClick(R.id.delete_img)
    public void deletePlaylist() {
        try {
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().deletePlaylist(APIServerResponse.DELETE_PLAYLIST, getUserSessionId(), getIntent().getStringExtra(Constants.PLAYLIST_ID), this);

            } else {
                showSnack("Not connected to Internet ");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void getAllPlaylistSong() {

        try {
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().getAllPlaylistSongs(APIServerResponse.GET_ALL_PLAYLIST_SONGS, getUserSessionId(),getUserID(), getIntent().getStringExtra(Constants.PLAYLIST_ID), this);
            } else {
                showSnack("Not connected to Internet ");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @OnClick(R.id.fab_book_options)
    public void playAllSongs() {
        if (playListSongsListObject.size()!=0) {
            playAudio(0);
        }else
        {
            Toast.makeText(PlaylistSongsList.this, "No Songs avaialable in the playlist", Toast.LENGTH_SHORT).show();
        }
    }

    public void addItemsToList() {
        if (!list.isEmpty()) {
            list.clear();
        }
        for (int i = 0; i < 10; i++) {
            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setName("Song " + (i + 1));
            demoDataModal.setId(i);
            demoDataModal.setSongUrl("https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3");
            demoDataModal.setAlbumArt("http://www.flat-e.com/flate5/wp-content/uploads/cover-960x857.jpg");
            demoDataModal.setImage("http://www.happybirthdayvinyl.co.uk/images/album-artwork/big/grace-jones-nightclubbing-deluxe-edition-disc1.jpg");
            demoDataModal.setPaymentType("FREE");
            demoDataModal.setIsDownloaded(false);
            list.add(demoDataModal);
        }
        swipeRefreshLayout.setRefreshing(false);

        //  adapter = new DemoDataPlayListSongsAdaper(PlaylistSongsList.this, listSongsDetail);
        //   rv_playlist.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        getAllPlaylistSong();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        try {
            if (response.isSuccessful()) {
                hideLoading();
                try {
                    switch (tag) {
                        case APIServerResponse.GET_ALL_PLAYLIST_SONGS:
                            PlaylistSongsResponse allPlaylistSongObject = (PlaylistSongsResponse) response.body();
                            playListSongsListObject = allPlaylistSongObject.getList();
                            if (playListSongsListObject.size() != 0) {
                                adapter = new DemoDataPlayListSongsAdaper(PlaylistSongsList.this, playListSongsListObject, getIntent().getStringExtra(Constants.PLAYLIST_ID));
                                rv_playlist.setAdapter(adapter);
                            } else {
                                Toast.makeText(PlaylistSongsList.this, "Please add songs into playlist", Toast.LENGTH_SHORT).show();
                                /*PlaylistSongsList.this.finish();*/
                            }
                            break;
                        case APIServerResponse.DELETE_PLAYLIST:
                            GroupDeleteModal groupDeleteModal = (GroupDeleteModal) response.body();
                            if (groupDeleteModal.getStatus().equalsIgnoreCase("1")) {
                                showToast(groupDeleteModal.getMessage(), Toast.LENGTH_SHORT);
                                PlaylistSongsList.this.finish();
                            }

                            break;

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_delete, menu);

            // disable swipe refresh if action mode is enabled
            swipeRefreshLayout.setEnabled(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_delete_item:
                    // delete all the selected messages
                    deleteMessages();
                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            adapter.clearSelections();
            swipeRefreshLayout.setEnabled(true);
            actionMode = null;
            /*recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    mAdapter.resetAnimationIndex();
                    // mAdapter.notifyDataSetChanged();
                }
            });*/
        }
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        adapter.toggleSelection(position);
        int count = adapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    private void deleteMessages() {
        List<Integer> selectedItemPositions =
                adapter.getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            adapter.removeData(selectedItemPositions.get(i));
        }
        adapter.notifyDataSetChanged();
    }

    private void playAudio(int position) {

        if (!arrayListMusic.isEmpty()) {
            arrayListMusic.clear();
        }
        for (int i = 0; i < playListSongsListObject.size(); i++) {
            PlayMusicModal playMusicModal = new PlayMusicModal();
            playMusicModal.setMediaUrl(playListSongsListObject.get(i).getMusic_file());
            playMusicModal.setThumbnailUrl(playListSongsListObject.get(i).getMusic_file());
            playMusicModal.setArtworkUrl(playListSongsListObject.get(i).getAlbum_cover_image());
            playMusicModal.setTitle(playListSongsListObject.get(i).getSong_name());
            playMusicModal.setAlbum(playListSongsListObject.get(i).getAlbum_name());
            playMusicModal.setArtist(playListSongsListObject.get(i).getAlbum_name());
            playMusicModal.setPaymentType(playListSongsListObject.get(i).getPayment_type());
            playMusicModal.setDownloaded(String.valueOf(playListSongsListObject.get(i).getIs_downloaded()));
            arrayListMusic.add(playMusicModal);
        }

        //mPlayListList = mList;

        Intent intent = new Intent(PlaylistSongsList.this, AudioPlayerActivity.class);
        intent.putExtra(AudioPlayerActivity.EXTRA_INDEX, position);
        intent.putExtra(AudioPlayerActivity.PLAYLIST_ID_TAG, playListSongsListObject.get(position).getSong_id());
        intent.putExtra(Constants.SONG_NAME, playListSongsListObject.get(position).getSong_name());
        intent.putExtra(Constants.ALBUM_COVER, playListSongsListObject.get(position).getAlbum_name());
        intent.putExtra(Constants.MUSIC, Constants.SONG);

        intent.putExtra(Constants.ALBUM_NAME, playListSongsListObject.get(position).getAlbum_name());
        intent.putExtra(Constants.TYPE, Constants.SONG_CAPS);
        intent.putExtra(Constants.SONG_ID, playListSongsListObject.get(position).getSong_id());
        intent.putExtra(Constants.SONG_DOWNLOAD_URL, playListSongsListObject.get(position).getMusic_file());
        intent.putExtra(Constants.POSITION, position);
        intent.putExtra(Constants.IS_DOWNLOADED, playListSongsListObject.get(position).getIs_downloaded());

        Bundle b = new Bundle();
        b.putParcelableArrayList(Constants.SONGS_LIST, arrayListMusic);
        intent.putExtra("songs_bundle", b);
        //intent.putParcelableArrayListExtra(Constants.SONGS_LIST, songsBeanArrayList);
        startActivity(intent);


    }
}
