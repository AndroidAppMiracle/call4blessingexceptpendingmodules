package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 12/7/2017.
 */

public class Profile_details {
    private String phone;

    private String d_o_b;

    private String cover_pic;

    private String location;

    private String state;

    private String gender;

    private String lastname;

    private String profile_pic;

    private String firstname;

    private String country;

    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getD_o_b ()
    {
        return d_o_b;
    }

    public void setD_o_b (String d_o_b)
    {
        this.d_o_b = d_o_b;
    }

    public String getCover_pic ()
    {
        return cover_pic;
    }

    public void setCover_pic (String cover_pic)
    {
        this.cover_pic = cover_pic;
    }

    public String getLocation ()
    {
        return location;
    }

    public void setLocation (String location)
    {
        this.location = location;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getLastname ()
    {
        return lastname;
    }

    public void setLastname (String lastname)
    {
        this.lastname = lastname;
    }

    public String getProfile_pic ()
    {
        return profile_pic;
    }

    public void setProfile_pic (String profile_pic)
    {
        this.profile_pic = profile_pic;
    }

    public String getFirstname ()
    {
        return firstname;
    }

    public void setFirstname (String firstname)
    {
        this.firstname = firstname;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [phone = "+phone+", d_o_b = "+d_o_b+", cover_pic = "+cover_pic+", location = "+location+", state = "+state+", gender = "+gender+", lastname = "+lastname+", profile_pic = "+profile_pic+", firstname = "+firstname+", country = "+country+"]";
    }
}
