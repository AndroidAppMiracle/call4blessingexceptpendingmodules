package com.example.preetisharma.callforblessings.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.SettingsMainAdapter;
import com.example.preetisharma.callforblessings.pages.CreatePageFirstActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.SettingsMainModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 6/27/2017.
 */

public class SettingsMainActivity extends BaseActivity implements APIServerResponse {


    @BindView(R.id.rv_settings)
    RecyclerView rv_settings;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_main);
        ButterKnife.bind(this);
       /* if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
*/

        txtvw_header_title.setText("Privacy Settings");
        img_view_change_password.setVisibility(View.GONE);

        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsMainActivity.this.finish();
            }
        });

        if (isConnectedToInternet()) {

            showLoading();
            ServerAPI.getInstance().getAllSettingsType(APIServerResponse.SETTINGS_MAIN, getUserSessionId(), this);

        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }


    /*@Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();


    }*/




    @Override
    public void onSuccess(int tag, Response response) {

        try {

            if (response.isSuccessful()) {
                SettingsMainModal settingsMainModal;

                switch (tag) {

                    case APIServerResponse.SETTINGS_MAIN:
                        settingsMainModal = (SettingsMainModal) response.body();
                        if (settingsMainModal.getStatus().equalsIgnoreCase("1")) {
                            List<SettingsMainModal.ListBean> settingsList = settingsMainModal.getList();

                            SettingsMainAdapter adapter = new SettingsMainAdapter(SettingsMainActivity.this, settingsList);
                            rv_settings.setAdapter(adapter);
                            rv_settings.setItemAnimator(new DefaultItemAnimator());
                            rv_settings.setLayoutManager(new LinearLayoutManager(SettingsMainActivity.this));
                        }
                        hideLoading();
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();
        throwable.printStackTrace();
    }


    /*@Subscribe
    public void isGroupCreatedPost(EventBusSettingsChanged eventBusSettingsChanged) {

        if (isConnectedToInternet()) {
            try {
                if (eventBusSettingsChanged.getIsSettingChanged()) {
                    showToast("Changd", Toast.LENGTH_SHORT);
                    ServerAPI.getInstance().getAllSettingsType(APIServerResponse.SETTINGS_MAIN, getUserSessionId(), this);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            hideLoading();
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }*/
}
