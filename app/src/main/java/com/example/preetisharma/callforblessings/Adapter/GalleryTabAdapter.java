package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.preetisharma.callforblessings.Fragment.GalleryImagesFragment;
import com.example.preetisharma.callforblessings.Fragment.GalleryVideosFragment;
import com.example.preetisharma.callforblessings.photos.AlbumsFragment;
import com.example.preetisharma.callforblessings.photos.AlbumsVedioFragment;
import com.example.preetisharma.callforblessings.photos.UploadedByYouFragment;

/**
 * Created by preeti.sharma on 2/9/2017.
 */

public class GalleryTabAdapter extends FragmentPagerAdapter {
    //integer to count number of tabs
    int tabCount;
    Context mContext;
    String userId = "";

    //Constructor to the class
    public GalleryTabAdapter(FragmentManager fm, int tabCount, Context mContext, String userId) {
        super(fm);
        //Initializing tab count
        this.tabCount = tabCount;
        this.mContext = mContext;
        this.userId=userId;
    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        Bundle bundle;
        switch (position) {
            case 0:
                bundle = new Bundle();
                bundle.putString("userId", userId);
                // fragment = new GalleryImagesFragment();
                fragment = new AlbumsFragment();
                fragment.setArguments(bundle);
                //((BaseActivity) mContext).replaceFragment(fragment, false);
                return fragment;
            case 1:
                bundle = new Bundle();
                bundle.putString("userId", userId);
                fragment = new GalleryVideosFragment();
                fragment.setArguments(bundle);
                /*fragment=new AlbumsFragment();*/
                return fragment;

            default:
                bundle = new Bundle();
                bundle.putString("userId", userId);
                /*fragment = new GalleryImagesFragment();*/
                fragment = new UploadedByYouFragment();
                fragment.setArguments(bundle);
                //((BaseActivity) mContext).replaceFragment(fragment, false);
                notifyDataSetChanged();
                return fragment;
        }

    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "My Photos";
            case 1:
                return "Videos";

            default:
                return "My Photos";
        }
        //return null;
    }
}
