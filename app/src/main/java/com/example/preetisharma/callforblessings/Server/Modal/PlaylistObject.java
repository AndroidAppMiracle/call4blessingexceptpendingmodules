package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/25/2017.
 */

public class PlaylistObject {
    private String created_by;

    private String id;

    private String name;

    private String created_at;

    public String getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (String created_by)
    {
        this.created_by = created_by;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [created_by = "+created_by+", id = "+id+", name = "+name+", created_at = "+created_at+"]";
    }
}
