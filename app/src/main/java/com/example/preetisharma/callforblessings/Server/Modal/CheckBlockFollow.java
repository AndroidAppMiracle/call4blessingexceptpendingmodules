package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/25/2017.
 */

public class CheckBlockFollow {
    private String is_blocked;

    private String status;

    private String is_followed;

    public String getIs_blocked ()
    {
        return is_blocked;
    }

    public void setIs_blocked (String is_blocked)
    {
        this.is_blocked = is_blocked;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getIs_followed ()
    {
        return is_followed;
    }

    public void setIs_followed (String is_followed)
    {
        this.is_followed = is_followed;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [is_blocked = "+is_blocked+", status = "+status+", is_followed = "+is_followed+"]";
    }
}
