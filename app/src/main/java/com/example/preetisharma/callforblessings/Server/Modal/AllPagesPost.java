package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by satoti.garg on 10/13/2017.
 */

public class AllPagesPost {
    private String status;

    private List<AllPostPageModal> posts;

    private String user_id;

    private String currentPage;

    private String totalPages;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<AllPostPageModal> getPagePostsModal ()
    {
        return posts;
    }

    public void setPagePostsModal (List<AllPostPageModal> posts)
    {
        this.posts = posts;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getCurrentPage ()
    {
        return currentPage;
    }

    public void setCurrentPage (String currentPage)
    {
        this.currentPage = currentPage;
    }

    public String getTotalPages ()
    {
        return totalPages;
    }

    public void setTotalPages (String totalPages)
    {
        this.totalPages = totalPages;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", posts = "+posts+", user_id = "+user_id+", currentPage = "+currentPage+", totalPages = "+totalPages+"]";
    }
}
