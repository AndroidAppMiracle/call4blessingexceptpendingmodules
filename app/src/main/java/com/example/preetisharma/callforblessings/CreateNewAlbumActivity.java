package com.example.preetisharma.callforblessings;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.CreateAlbumResponse;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

public class CreateNewAlbumActivity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.edit_album_name)
    AppCompatEditText editAlbumName;
    @BindView(R.id.edit_album_desc)
    AppCompatEditText editAlbumDesc;
    @BindView(R.id.switch1)
    Switch switch1;

    @BindView(R.id.txtvw_create_album)
    AppCompatTextView txtCreateAlbum;
    private int RC_INTERNET = 101;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_album);
        ButterKnife.bind(this);
        txtvw_header_title.setText("Create new album");
        img_view_change_password.setVisibility(View.GONE);
    }


    @OnClick(R.id.img_view_back)
    public void backButtonPress()
    {
        CreateNewAlbumActivity.this.finish();
    }
    @Override
    public void onSuccess(int tag, Response response) {


        hideLoading();
        if (response.isSuccessful()) {

            try {
                switch (tag) {
                    case APIServerResponse.CREATE_ALBUM:
                        final CreateAlbumResponse createAlbumResponse = (CreateAlbumResponse) response.body();
                        if (createAlbumResponse.getStatus().equalsIgnoreCase("1")) {
                            Toast.makeText(getApplicationContext(), "" + createAlbumResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    hideLoading();
                                    Intent intent=new Intent(getApplicationContext(),UploadImageToAlbum.class);
                                    intent.putExtra("albumId",createAlbumResponse.getAlbumDetail().getId());
                                    startActivity(intent);
                                }
                            }, 1000);
                        }
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }


    @OnClick(R.id.txtvw_create_album)
    public void createAlbum() {
        int isPublic = 0;
        try {
            if (Validation(editAlbumName.getText().toString(), editAlbumDesc.getText().toString())) {

                if (EasyPermissions.hasPermissions(this, android.Manifest.permission.INTERNET)) {
                    if (isConnectedToInternet()) {
                        showLoading();
                        if (switch1.isChecked()) {
                            isPublic = 1;
                        } else {
                            isPublic = 0;
                        }
                        ServerAPI.getInstance().createAlbum(APIServerResponse.CREATE_ALBUM, getUserSessionId(), editAlbumName.getText().toString(), editAlbumDesc.getText().toString(), isPublic, this);
                        finish();
                    } else {
                        showSnack("Not connected to Internet ");
                    }
                } else {
                    EasyPermissions.requestPermissions(this, getString(R.string.internet_access),
                            RC_INTERNET, android.Manifest.permission.INTERNET);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean Validation(String albumName, String albumDesc) {
        if (albumName.isEmpty()) {
            showToast(Constants.ALBUM_NAME_NOT_BLENK, Toast.LENGTH_SHORT);
            return false;
        } else if (albumName.length() < 4) {
            showToast(Constants.ALBUM_NAME_NOT_VALID, Toast.LENGTH_SHORT);
            return false;
        } else if (albumDesc.isEmpty()) {
            showToast(Constants.ALBUM_DESC_NOT_BLANK, Toast.LENGTH_SHORT);
            return false;
        } else if (albumDesc.length() < 4) {
            showToast(Constants.ALBUM_DESC_NOT_VALID, Toast.LENGTH_SHORT);
            return false;
        } else if (!albumDesc.matches(albumDesc)) {
            return false;
        }
        return true;


    }
}
