package com.example.preetisharma.callforblessings.demo;

import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by preeti.sharma on 4/10/2017.
 */

public class OrderConfirmationActivity extends BaseActivity {

    @BindView(R.id.tv_orderID)
    TextView tv_orderID;
    @BindView(R.id.iv_order_confirmation)
    ImageView iv_order_confirmation;
    @BindView(R.id.tv_orderPaymentStatus)
    TextView tv_orderPaymentStatus;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.appCompatTextView_htp)
    AppCompatTextView appCompatTextView_htp;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.tv_product_deliver)
    TextView tv_product_deliver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirmation);
        ButterKnife.bind(this);
        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("Order Confirmation");
        appCompatTextView_htp.setVisibility(View.GONE);

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString(Constants.TRANSACTIONSTATUS).equalsIgnoreCase("success")) {
                iv_order_confirmation.setImageResource(R.drawable.ic_success_transaction);
                tv_orderID.setText("TransId" + getIntent().getExtras().getString(Constants.TRANSACTIONID));
                tv_product_deliver.setText(Constants.BOOK_DELIVER);
            } else {
                iv_order_confirmation.setImageResource(R.drawable.ic_cancel_transaction);
                tv_orderID.setText("TransId" + getIntent().getExtras().getString(Constants.TRANSACTIONID));
                tv_orderPaymentStatus.setText("Transaction Failed");
                tv_product_deliver.setVisibility(View.GONE);
            }
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
