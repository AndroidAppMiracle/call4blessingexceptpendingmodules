package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by Kshitiz Bali on 2/9/2017.
 */

public class EventInviteSelectedFriendsModal {

    private int id;
    private String friend_name;
    private String profile_pic;

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getFriend_name() {
        return friend_name;
    }

    public void setFriend_name(String friend_name) {
        this.friend_name = friend_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
