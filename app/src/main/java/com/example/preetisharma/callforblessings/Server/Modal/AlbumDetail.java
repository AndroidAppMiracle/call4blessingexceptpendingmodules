package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/24/2017.
 */

public class AlbumDetail {
    private String id;

    private String is_public;

    private String description;

    private String name;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getIs_public ()
    {
        return is_public;
    }

    public void setIs_public (String is_public)
    {
        this.is_public = is_public;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", is_public = "+is_public+", description = "+description+", name = "+name+"]";
    }
}
