package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.NotificationObjectResponse;
import com.example.preetisharma.callforblessings.Server.Modal.NotificationReposne;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 9/20/2017.
 */

public class SettingNotificationAdapter extends RecyclerView.Adapter<SettingNotificationAdapter.DataViewHolder> implements APIServerResponse {

    public List<NotificationObjectResponse> list;
    Activity mContext;
    List<String> settingsList = new ArrayList<>();

    String notificationSt = "";
    int i = 0;
    String userId;
    List<NotificationObjectResponse> notificationObjectResponse;

    public SettingNotificationAdapter(Activity mContext,String userId, List<NotificationObjectResponse> list) {
        this.list = list;
        this.userId=userId;
        this.mContext = mContext;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_settings_notifications, parent, false);
        DataViewHolder dataView = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
       /* if (position < 2) {
            holder.cv_setting_item.setVisibility(View.GONE);
        } else {*/
        holder.cv_setting_item.setVisibility(View.VISIBLE);
        holder.atv_general_setting_name.setText(list.get(position).getSetting() + "");
        if (list.get(position).getValue().equalsIgnoreCase("0")) {
            holder.switchOnOff.setChecked(false);
        } else {
            holder.switchOnOff.setChecked(true);
        }
        /*}*/

        holder.switchOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                String notificationName="";
                if (buttonView.isPressed()) {

                    if(list.get(position).getSetting().equalsIgnoreCase("Post Like"))
                    {
                        notificationName="post_like";
                    }else if(list.get(position).getSetting().equalsIgnoreCase("Post Comment"))
                    {
                        notificationName="post_comment";
                    }else if(list.get(position).getSetting().equalsIgnoreCase("Post Share"))
                    {
                        notificationName="post_share";
                    }else if(list.get(position).getSetting().equalsIgnoreCase("Wall Post"))
                    {
                        notificationName="wall_post";
                    }else if(list.get(position).getSetting().equalsIgnoreCase("Group Post"))
                    {
                        notificationName="group_post";
                    }else if(list.get(position).getSetting().equalsIgnoreCase("Group Like"))
                    {
                        notificationName="group_like";
                    }else if(list.get(position).getSetting().equalsIgnoreCase("Group Comment"))
                    {
                        notificationName="group_comment";
                    }else if(list.get(position).getSetting().equalsIgnoreCase("Group Share"))
                    {
                        notificationName="group_share";
                    }else if(list.get(position).getSetting().equalsIgnoreCase("Friend Request"))
                    {
                        notificationName="friend_request";
                    }
                    else if(list.get(position).getSetting().equalsIgnoreCase("Tagged Post"))
                    {
                        notificationName="tagged_post";
                    }





                    notificationSt = "NotifySetting[" +notificationName + "]";
                    if (!isChecked) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                    try {
                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().setNotification(APIServerResponse.SAVE_NOTIFICATION, ((BaseActivity) mContext).getUserSessionId(),userId, notificationSt, String.valueOf(i), SettingNotificationAdapter.this);
                        } else {
                            ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }


                }
            }
        });



/*        holder.switchOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (position == 1) {
                    if (isChecked) {
                        notificationSt = "NotifySetting[post_like]";
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        notificationSt = "NotifySetting[post_like]";
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }


                }else if (position == 2) {
                    notificationSt = "NotifySetting[post_share]";
                    if (isChecked) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }

                    *//*try {
                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().setNotification(APIServerResponse.SAVE_NOTIFICATION, ((BaseActivity) mContext).getUserSessionId(), notificationSt, String.valueOf(i), SettingNotificationAdapter.this);
                        } else {
                            ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }*//*
                }else if (position == 3) {
                    notificationSt = "NotifySetting[wall_post]";
                    if (isChecked) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }

                   *//* try {
                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().setNotification(APIServerResponse.SAVE_NOTIFICATION, ((BaseActivity) mContext).getUserSessionId(), notificationSt, String.valueOf(i), SettingNotificationAdapter.this);
                        } else {
                            ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }*//*

                } else if (position == 4) {
                    notificationSt = "NotifySetting[group_post]";
                    if (isChecked) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }

                 *//*   try {
                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().setNotification(APIServerResponse.SAVE_NOTIFICATION, ((BaseActivity) mContext).getUserSessionId(), notificationSt, String.valueOf(i), SettingNotificationAdapter.this);
                        } else {
                            ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }*//*

                } else if (position == 5) {
                    notificationSt = "NotifySetting[group_like]";
                    if (isChecked) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }

                 *//*   try {
                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().setNotification(APIServerResponse.SAVE_NOTIFICATION, ((BaseActivity) mContext).getUserSessionId(), notificationSt, String.valueOf(i), SettingNotificationAdapter.this);
                        } else {
                            ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
*//*
                } else if (position == 6) {
                    notificationSt = "NotifySetting[group_comment]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
*//*

                    try {
                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().setNotification(APIServerResponse.SAVE_NOTIFICATION, ((BaseActivity) mContext).getUserSessionId(), notificationSt, String.valueOf(i), SettingNotificationAdapter.this);
                        } else {
                            ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
*//*

                } else if (position == 7) {
                    notificationSt = "NotifySetting[group_share]";
                    if (isChecked) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                  *//*  try {
                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().setNotification(APIServerResponse.SAVE_NOTIFICATION, ((BaseActivity) mContext).getUserSessionId(), notificationSt, String.valueOf(i), SettingNotificationAdapter.this);
                        } else {
                            ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }*//*
                } else if (position == 8) {
                    notificationSt = "NotifySetting[friend_request]";
                    if (isChecked) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }

                   *//* try {
                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().setNotification(APIServerResponse.SAVE_NOTIFICATION, ((BaseActivity) mContext).getUserSessionId(), notificationSt, String.valueOf(i), SettingNotificationAdapter.this);
                        } else {
                            ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }*//*
                } else if (position == 9) {
                    notificationSt = "NotifySetting[tagged_post]";
                    if (isChecked) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }

                  *//*  try {
                        if (((BaseActivity) mContext).isConnectedToInternet()) {
                            ((BaseActivity) mContext).showLoading();
                            ServerAPI.getInstance().setNotification(APIServerResponse.SAVE_NOTIFICATION, ((BaseActivity) mContext).getUserSessionId(), notificationSt, String.valueOf(i), SettingNotificationAdapter.this);
                        } else {
                            ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }*//*
                }


                try {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {

                        ServerAPI.getInstance().setNotification(APIServerResponse.SAVE_NOTIFICATION, ((BaseActivity) mContext).getUserSessionId(), notificationSt, String.valueOf(i), SettingNotificationAdapter.this);
                    } else {
                        ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                *//*if (position == 1) {
                    notificationSt = "NotifySetting[post_like]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 2) {
                    notificationSt = "NotifySetting[post_share]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 3) {
                    notificationSt = "NotifySetting[wall_post]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 4) {
                    notificationSt = "NotifySetting[group_post]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 5) {
                    notificationSt = "NotifySetting[group_like]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 6) {
                    notificationSt = "NotifySetting[group_comment]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 7) {
                    notificationSt = "NotifySetting[group_share]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 8) {
                    notificationSt = "NotifySetting[friend_request]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 9) {
                    notificationSt = "NotifySetting[tagged_post]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                }

                try {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ((BaseActivity) mContext).showLoading();
                        ServerAPI.getInstance().setNotification(APIServerResponse.SAVE_NOTIFICATION, ((BaseActivity) mContext).getUserSessionId(), notificationSt, String.valueOf(i), SettingNotificationAdapter.this);
                    } else {
                        ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }*//*


            }
        });


       *//* holder.switchOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 1) {
                    notificationSt = "NotifySetting[post_like]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 2) {
                    notificationSt = "NotifySetting[post_share]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 3) {
                    notificationSt = "NotifySetting[wall_post]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 4) {
                    notificationSt = "NotifySetting[group_post]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 5) {
                    notificationSt = "NotifySetting[group_like]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 6) {
                    notificationSt = "NotifySetting[group_comment]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 7) {
                    notificationSt = "NotifySetting[group_share]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 8) {
                    notificationSt = "NotifySetting[friend_request]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                } else if (position == 9) {
                    notificationSt = "NotifySetting[tagged_post]";
                    if (holder.switchOnOff.isChecked()) {
                        i = 0;
                        holder.switchOnOff.setChecked(false);
                    } else {
                        i = 1;
                        holder.switchOnOff.setChecked(true);
                    }
                }

                try {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ((BaseActivity) mContext).showLoading();
                        ServerAPI.getInstance().setNotification(APIServerResponse.SAVE_NOTIFICATION, ((BaseActivity) mContext).getUserSessionId(), notificationSt, String.valueOf(i), SettingNotificationAdapter.this);
                    } else {
                        ((BaseActivity) mContext).showSnack("Not connected to Internet ");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });*/
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onSuccess(int tag, Response response) {
        ((BaseActivity) mContext).hideLoading();
        try {
            switch (tag) {
                case APIServerResponse.SAVE_NOTIFICATION:
                    NotificationReposne notificationsModal = (NotificationReposne) response.body();
                    if (notificationsModal.getStatus().equalsIgnoreCase("1")) {
                        list = notificationsModal.getDetail();

                        /*adapter = new SettingNotificationAdapter(SettingsNotifications.this, notificationObjectResponse);
                        rv_security_settings.setAdapter(adapter);
                        adapter.notifyDataSetChanged();*/

                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(mContext, "No Notification found", Toast.LENGTH_SHORT).show();
                    }
                    ((BaseActivity) mContext).hideLoading();
                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            ((BaseActivity) mContext).hideLoading();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        throwable.printStackTrace();
    }

    public void refresh(int position) {
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, list.size());
        notifyDataSetChanged();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.atv_general_setting_name)
        AppCompatTextView atv_general_setting_name;

        @BindView(R.id.cv_setting_item)
        CardView cv_setting_item;

        @BindView(R.id.switch1)
        Switch switchOnOff;


        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

         /*   cv_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        if (!selectedSettingName.equalsIgnoreCase("")) {
                            ServerAPI.getInstance().setSettings(APIServerResponse.SETTINGS_SET, ((BaseActivity) mContext).getUserSessionId(), settingId, selectedSettingName, DataViewHolder.this);
                        } else {

                            ((BaseActivity) mContext).showToast("Please select an option", Toast.LENGTH_SHORT);
                        }
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }


                }
            });*/

        }



        /*@OnClick(R.id.txtvw_download_book)
        public void downloadBook() {

            if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("FREE")) {
                new AlertDialog.Builder(mContext)
                        .setTitle(list.get(getAdapterPosition()).getName())
                        .setMessage("We have received your interest. We will connect with you shortly.")

                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })

                        .setIcon(R.mipmap.ic_app_icon)
                        .show();
            } else if (list.get(getAdapterPosition()).getPayment_type().equalsIgnoreCase("PAID")) {
                Intent purchasebookIntent = new Intent(mContext, PaymentGatewayActivity.class);
                Bundle b = new Bundle();
                b.putString(Constants.BOOK_ID, String.valueOf(list.get(getAdapterPosition()).getId()));
                b.putString(Constants.AMOUNT, String.valueOf(list.get(getAdapterPosition()).getAmount()));
                b.putString(Constants.BOOK_DESC, list.get(getAdapterPosition()).getDescription());
                purchasebookIntent.putExtras(b);
                mContext.startActivity(purchasebookIntent);

            }


        }*/

    }



}
