package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by satoti.garg on 12/8/2017.
 */

public class PageImagesArray {
    private String status;

    private List<String> list;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<String> getList ()
    {
        return list;
    }

    public void setList (List<String> list)
    {
        this.list = list;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", list = "+list+"]";
    }
}
