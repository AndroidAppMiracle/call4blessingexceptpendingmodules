package com.example.preetisharma.callforblessings.demo;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Fragment.GroupAdminFragment;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.GroupListingModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class GroupVeiwPagerActivity extends BaseActivity implements TabLayout.OnTabSelectedListener, APIServerResponse {

    @BindView(R.id.viewPager_friendsTab)
    ViewPager viewPager;

    @BindView(R.id.tabs_friends)
    TabLayout tabLayout;

    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;

    List<GroupListingModal.ListBean> groupList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_veiw_pager);

        ButterKnife.bind(this);
        getAllGroups();
        tabLayout.addTab(tabLayout.newTab().setText("My Created"));
        tabLayout.addTab(tabLayout.newTab().setText("Other Created"));
    }


    public void getAllGroups() {
        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getGroupsList(APIServerResponse.GROUP_LISTING, getUserSessionId(),getUserID(), this);
        } else {
            showToast("Please check your internet connection.", Toast.LENGTH_LONG);
        }
    }

    @OnClick(R.id.img_view_back)
    public void backPressed() {
        GroupVeiwPagerActivity.this.finish();
    }

    @Override
    public void onBackPressed() {
        GroupVeiwPagerActivity.this.finish();
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onSuccess(int tag, Response response) {

        if (response.isSuccessful()) {
            try {
                switch (tag) {
                    case APIServerResponse.GROUP_LISTING:
                        GroupListingModal groupListingModal = (GroupListingModal) response.body();
                        if (groupListingModal.getStatus().equals("1")) {
                            groupList = groupListingModal.getList();
                        }
                        viewPager.setAdapter(new EventsAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), groupList));
                        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                        viewPager.setOffscreenPageLimit(0);
                        tabLayout.setOnTabSelectedListener(GroupVeiwPagerActivity.this);


                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        /**/
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }

    public class EventsAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        List<GroupListingModal.ListBean> groupList = new ArrayList<>();

        public EventsAdapter(FragmentManager fm, int NumOfTabs, List<GroupListingModal.ListBean> groupList) {
            super(fm);
            this.groupList = groupList;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {

                case 0:
                    fragment = new GroupAdminFragment();
                    Bundle arg = new Bundle();
                  //  arg.putParcelableArray("groupList", groupList);
                    fragment.setArguments(arg);
                    return fragment;
                case 1:
                    fragment = new GroupAdminFragment();
                    Bundle arg1 = new Bundle();
                    //arg1.putParcelableArrayList("vip", vipLists);
                    fragment.setArguments(arg1);
                    return fragment;
                default:
                    fragment = new GroupAdminFragment();
                    Bundle arg2 = new Bundle();
                    // arg2.putParcelableArrayList("vip", vipLists);
                    fragment.setArguments(arg2);
                    return fragment;

            }
        }

        @Override
        public int getCount() {
            return 2;
        }

    }


}
