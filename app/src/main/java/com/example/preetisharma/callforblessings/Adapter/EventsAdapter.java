package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DeleteEventModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventsListingModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by preeti.sharma on 1/21/2017.
 */

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.DataViewHolder> {
    public List<EventsListingModal.ListBean> list;
    Activity mContext;
    private static int countNext = 0;
    RadioButton rd_btn;

    public EventsAdapter(Activity mContext, List<EventsListingModal.ListBean> list) {
        this.list = list;
        this.mContext = mContext;
    }


    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    public void addAllItems(List<EventsListingModal.ListBean> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public EventsAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.events_item_card, parent, false);
        EventsAdapter.DataViewHolder dataView = new EventsAdapter.DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataView;
    }

    @Override
    public void onBindViewHolder(final EventsAdapter.DataViewHolder holder, final int position) {
        holder.txtvw_event_name.setText(list.get(position).getEvent_name());
        holder.txtvw_event_date.setText(list.get(position).getEvent_date());
        holder.txtvw_event_time.setText(list.get(position).getEvent_date());
        holder.txtvw_event_location.setText(list.get(position).getEvent_location());

    }

    public int getcount() {
        return list.size();

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements APIServerResponse {

        @BindView(R.id.txtvw_event_name)
        AppCompatTextView txtvw_event_name;
        @BindView(R.id.txtvw_remove_event)
        AppCompatTextView txtvw_remove_event;
        @BindView(R.id.txtvw_event_date)
        AppCompatTextView txtvw_event_date;
        @BindView(R.id.txtvw_event_time)
        AppCompatTextView txtvw_event_time;
        @BindView(R.id.txtvw_event_location)
        AppCompatTextView txtvw_event_location;

        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            txtvw_remove_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((BaseActivity) mContext).isConnectedToInternet()) {
                        ServerAPI.getInstance().deleteEvent(APIServerResponse.DELETEEVENT, ((BaseActivity) mContext).getUserSessionId(), String.valueOf(list.get(getAdapterPosition()).getId()), EventsAdapter.DataViewHolder.this);
                    } else {
                        ((BaseActivity) mContext).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                    }
                }
            });
        }

        @Override
        public void onSuccess(int tag, Response response) {

            try {
                ((BaseActivity) mContext).hideLoading();
                if (response.isSuccessful()) {
                    DeleteEventModal deleteEvent;

                    switch (tag) {
                        case APIServerResponse.DELETEEVENT:
                            deleteEvent = (DeleteEventModal) response.body();
                            if (deleteEvent.getStatus().equals("1")) {
                                ((BaseActivity) mContext).showToast("Event Deleted", Toast.LENGTH_SHORT);
                                refresh(getAdapterPosition());
                            }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(int tag, Throwable throwable) {
            switch (tag) {
                case APIServerResponse.DELETEEVENT:
                    ((BaseActivity) mContext).showToast(throwable.getMessage().toString(), Toast.LENGTH_SHORT);

            }
        }
    }

    public void refresh(int position) {
        Log.e("Refreshed Position", "Pos" + position);
        list.remove(position);
        notifyItemRemoved(position);
    }
}
