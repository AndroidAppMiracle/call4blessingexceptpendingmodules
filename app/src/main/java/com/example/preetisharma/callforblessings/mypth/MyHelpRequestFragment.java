package com.example.preetisharma.callforblessings.mypth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.HelpRequestAdapter;
import com.example.preetisharma.callforblessings.HelpRequest;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.HelpRequestListModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by satoti.garg on 8/1/2017.
 */

public class MyHelpRequestFragment extends Fragment implements APIServerResponse {

    /*View rootView = inflater.inflate(R.layout.fragment_my_help_request, container, false);*/
    @BindView(R.id.help_list)
    RecyclerView help_list;
    HelpRequestAdapter _adapter;

    List<HelpRequestListModal.ListBean> listAllPrayers;
    HelpRequestListModal helpRequestListModal;

    SwipeRefreshLayout swipe_refresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.activity_help_request_list, container, false);

        ButterKnife.bind(this, rootView);
        swipe_refresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh);
        help_list = (RecyclerView) rootView.findViewById(R.id.help_list);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllHelpRequest();
            }
        });
        getAllHelpRequest();

        return rootView;
    }


    public void getAllHelpRequest() {
        try {
            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().getMyHelpRequest(APIServerResponse.MYAMENPRAYER, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), this);
            } else {
                ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onSuccess(int tag, Response response) {
        try {

            listAllPrayers = new ArrayList<>();

            ((BaseActivity) getActivity()).hideLoading();
            if (response.isSuccessful()) {
                  switch (tag) {
                    case APIServerResponse.MYAMENPRAYER:
                        helpRequestListModal = (HelpRequestListModal) response.body();
                        if (helpRequestListModal.getStatus().equals("1")) {
                            listAllPrayers = helpRequestListModal.getList();
                            _adapter = new HelpRequestAdapter(getActivity(), listAllPrayers);
                            help_list.setAdapter(_adapter);
                            help_list.setItemAnimator(new DefaultItemAnimator());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            help_list.setLayoutManager(mLayoutManager);
                            if (swipe_refresh.isRefreshing()) {
                                swipe_refresh.setRefreshing(false);
                            }
                        } else {
                            Toast.makeText(getContext(), "No Help Request found", Toast.LENGTH_SHORT).show();
                            if (swipe_refresh.isRefreshing()) {
                                swipe_refresh.setRefreshing(false);
                            }
                        }
                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Subscribe
    public void onEvent(DemoCommentModal event) {
        try {
            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().getHelpRequest(APIServerResponse.MYAMENPRAYER, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(),this);
            } else {
                ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {
            ((BaseActivity) getActivity()).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.MYAMENPRAYER:
                    System.out.println("Error");
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        help_list.smoothScrollToPosition(0);
       /* try {
            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().getHelpRequest(APIServerResponse.GET_HELP_REQUEST, ((BaseActivity) getActivity()).getUserSessionId(), this);
            } else {
                ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
       getAllHelpRequest();
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_add, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_add_new_item) {
            Intent signUpIntent = new Intent(getActivity(), HelpRequest.class);
            startActivity(signUpIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            //Load Data
            try {

                ((BaseActivity) getActivity()).showLoading();
                if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                    ServerAPI.getInstance().getHelpRequest(APIServerResponse.GET_HELP_REQUEST, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(),this);
                } else {
                    ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //No Loading of Data
        }
    }
}
