package com.example.preetisharma.callforblessings.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by satoti.garg on 10/13/2017.
 */

public class PagePostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int LOADING = 0;
    public final int IMAGE = 2, VIDEO = 3, TEXT = 4;
    public final int SHARE = 1;
    public final int SHARE_IMAGE = 5;
    public final int SHARE_VIDEO = 6;
    public final int SHARE_TEXT = 7;
    public final int SHARE_REQUEST = 8;
    public final int PROFILE_PIC = 9;
    public final int COVER_PIC = 10;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
