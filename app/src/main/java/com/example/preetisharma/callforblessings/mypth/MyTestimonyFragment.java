package com.example.preetisharma.callforblessings.mypth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.TestimonyAdapter;
import com.example.preetisharma.callforblessings.Adapter.TestimonyNewAdapter;
import com.example.preetisharma.callforblessings.Fragment.TestimonyFragment;
import com.example.preetisharma.callforblessings.LandingScreenActivity;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoCommentModal;
import com.example.preetisharma.callforblessings.Server.Modal.HaleTestimonyModal;
import com.example.preetisharma.callforblessings.Server.Modal.LikeModal;
import com.example.preetisharma.callforblessings.Server.Modal.TestimonyModal;
import com.example.preetisharma.callforblessings.Server.Modal.TestimonyModalNew;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.TestimonyRequestActivity;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.demo.CommentsListingActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 8/1/2017.
 */

public class MyTestimonyFragment extends Fragment implements APIServerResponse {


    @BindView(R.id.testimony_list)
    RecyclerView testimony_list;
    @BindView(R.id.ll_testimonial_of_day)
    LinearLayout ll_testimonial_of_day;
    List<TestimonyModal.ListBean> lisAllTestimonial;
    @BindView(R.id.txtvw_testimony_of_the_title)
    AppCompatTextView txtvw_testimony_of_the_title;
    @BindView(R.id.txtvw_testimony_of_the_day_description)
    AppCompatTextView txtvw_testimony_of_the_day_description;
    @BindView(R.id.txtvw_testimony_of_the_day_likes_count)
    AppCompatTextView txtvw_testimony_of_the_day_likes_count;
    @BindView(R.id.txtvw_testimony_of_the_day_comments_count)
    AppCompatTextView txtvw_testimony_of_the_day_comments_count;
    //@BindView(R.id.txtvw_testimony_of_the_day_like)
    //AppCompatTextView txtvw_testimony_of_the_day_like;
    @BindView(R.id.txtvw_testimony_of_the_day_comment)
    AppCompatTextView txtvw_testimony_of_the_day_comment;
    //@BindView(R.id.txtvw_testimony_of_the_day_amen)
    //AppCompatTextView txtvw_testimony_of_the_day_amen;
    @BindView(R.id.txtvw_testimony_of_the_day_halle_count)
    AppCompatTextView txtvw_testimony_of_the_day_halle_count;
    @BindView(R.id.card_view_title)
    CardView card_view_title;
    TestimonyAdapter adapter;
    private String prayerId;
    TestimonyModal testimonyModal;
    TestimonyModalNew testimonyModalNew;
    HaleTestimonyModal haleTestimonyModal;
    int halleCount = 0;

    AppCompatTextView txtvw_testimony_of_the_day_like;
    AppCompatTextView txtvw_testimony_of_the_day_amen;

    String hallecount;
    private static String likescount, commentsCount, amencount;
    private String liked = "not liked";
    private String hallelujah = "Not Hallelujah";

    private TestimonyNewAdapter adapterNew;
    private List<TestimonyModalNew.ListBean> listNew = new ArrayList<>();
    boolean flag, flagLike;
    SwipeRefreshLayout swipe_refresh;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        onCreate(savedInstanceState);
        setRetainInstance(true);
        View rootView = inflater.inflate(R.layout.testimony_tab, container, false);
        ButterKnife.bind(this, rootView);
        card_view_title.setVisibility(View.GONE);
        swipe_refresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh);
        txtvw_testimony_of_the_day_like = (AppCompatTextView) rootView.findViewById(R.id.txtvw_testimony_of_the_day_like);
        txtvw_testimony_of_the_day_amen = (AppCompatTextView) rootView.findViewById(R.id.txtvw_testimony_of_the_day_amen);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        getAllTestimonial();
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllTestimonial();
            }
        });
        txtvw_testimony_of_the_day_amen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (testimonyModalNew != null) {
                    try {

                        if (txtvw_testimony_of_the_day_amen.isEnabled()) {
                            txtvw_testimony_of_the_day_amen.setEnabled(false);
                        }

                        hallelujah = testimonyModalNew.getList().get(0).getHallelujah_flag();
                        hallecount = testimonyModalNew.getList().get(0).getHallelujah_count();

                        if (((BaseActivity) getActivity()).isConnectedToInternet()) {

                            ServerAPI.getInstance().haleTestimonyPost(APIServerResponse.HALE_TESTIMONY, ((BaseActivity) getActivity()).getUserSessionId(),((BaseActivity) getActivity()).getUserID(),  String.valueOf(testimonyModalNew.getList().get(0).getId()), MyTestimonyFragment.this);
                        } else {
                            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
                   /* if (flag) {
                        int imgResource = R.drawable.ic_selected_amen;
                        txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        // amencount = String.valueOf(Integer.parseInt(txtvw_testimony_of_the_day_halle_count.getText().toString()) + 1);
                        txtvw_testimony_of_the_day_halle_count.setText(amencount + 1);
                        flag = false;
                    } else {
                        int imgResource = R.drawable.prayer_amen;
                        txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        //amencount = String.valueOf(Integer.parseInt(txtvw_testimony_of_the_day_halle_count.getText().toString()) - 1);
                        txtvw_testimony_of_the_day_halle_count.setText(String.valueOf(Integer.parseInt(amencount) - 1));
                        flag = true;
                    }*/


            }
            //Log.i("Hall Flag ", testimonyModalNew.getList().getHallelujah_flag());
            //Log.i("Hall amen ", testimonyModalNew.getList().getHallelujah_count());

            // ((BaseActivity) getActivity()).showToast("testofdat " + testimonyModalNew.getList().getHallelujah_flag(), Toast.LENGTH_LONG);

/*        if (hallelujah.equalsIgnoreCase("Not Hallelujah")) {
            int imgResource = R.drawable.ic_selected_amen;
            txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ((BaseActivity) getActivity()).showLoading();
                ServerAPI.getInstance().haleTestimonyPost(APIServerResponse.HALE_TESTIMONY, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(testimonyModal.getList().getId()), this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }

        } else {
            int imgResource = R.drawable.prayer_amen;
            txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ((BaseActivity) getActivity()).showLoading();
                ServerAPI.getInstance().haleTestimonyPost(APIServerResponse.UNHALE, ((BaseActivity) getActivity()).getUserSessionId(), String.valueOf(testimonyModal.getList().getId()), this);
            } else {
                ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            }
        }*/

        });
        return rootView;
    }


    public void getAllTestimonial() {
        try {

            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                //ServerAPI.getInstance().getTestimony(APIServerResponse.TESTIMONY_INDEX, ((BaseActivity) getActivity()).getUserSessionId(), this);
                ServerAPI.getInstance().getMyTestimony(APIServerResponse.MYTESTIMONY, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), this);

            } else {
                ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.txtvw_testimony_of_the_day_comment)
    public void comment_prayer_of_day() {
        if (testimonyModalNew != null) {
            Intent i = new Intent(getActivity(), CommentsListingActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.COMMENTS, String.valueOf(testimonyModalNew.getList().get(0).getId()));
            b.putString(Constants.POSTTYPE, Constants.TESTIMONY);
            i.putExtras(b);
            startActivity(i);
        }
    }

    public void RefreshScroll() {
        testimony_list.scrollToPosition(0);
        testimony_list.smoothScrollToPosition(0);
    }

    @Override
    public void onSuccess(int tag, Response response) {

        try {
            lisAllTestimonial = new ArrayList<>();

            /*AmenPrayerModal amenPrayerModal;*/
            LikeModal likeModal;
            ((BaseActivity) getActivity()).hideLoading();

            if (response.isSuccessful()) {
                switch (tag) {
                    case APIServerResponse.LIKE: {
                        likeModal = (LikeModal) response.body();
                        if (likeModal.getStatus().equals("1")) {
                            if (!txtvw_testimony_of_the_day_like.isEnabled()) {
                                txtvw_testimony_of_the_day_like.setEnabled(true);
                            }
                            int imgResource = R.drawable.ic_liked;
                            txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                            int likescountValue = Integer.parseInt(likescount);
                            likescountValue++;
                            if (likescountValue == 0 || likescountValue == 1) {
                                txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + " " + "Like");
                            } else {
                                txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + " " + "Likes");
                            }
                        }
                        if (swipe_refresh.isRefreshing()) {
                            swipe_refresh.setRefreshing(false);
                        }

                    }
                    break;
                    case APIServerResponse.UNLIKE: {
                        likeModal = (LikeModal) response.body();
                        if (likeModal.getStatus().equals("1")) {

                            if (!txtvw_testimony_of_the_day_like.isEnabled()) {
                                txtvw_testimony_of_the_day_like.setEnabled(true);
                            }
                            int imgResource = R.drawable.prayer_like;
                            txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                            int likescountValue = Integer.parseInt(likescount);
                            likescountValue--;
                            liked = "not liked";
                            testimonyModalNew.getList().get(0).setLike_flag("not liked");
                            testimonyModalNew.getList().get(0).setLike_count(String.valueOf(likescountValue));

                            if (likescountValue == 0 || likescountValue == 1) {
                                txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + " " + "Like");
                            } else {
                                txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + " " + "Likes");
                            }
                        }
                        if (swipe_refresh.isRefreshing()) {
                            swipe_refresh.setRefreshing(false);
                        }

                    }
                    break;
                    case APIServerResponse.HALE_TESTIMONY: {
                        haleTestimonyModal = (HaleTestimonyModal) response.body();
                        if (haleTestimonyModal.getStatus().equals("1")) {
                            if (!txtvw_testimony_of_the_day_amen.isEnabled()) {
                                txtvw_testimony_of_the_day_amen.setEnabled(true);
                            }

                            if (haleTestimonyModal.getHallelujau().equalsIgnoreCase("YES")) {
                                int imgResource = R.drawable.ic_selected_amen;
                                txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                halleCount = Integer.parseInt(testimonyModalNew.getList().get(0).getHallelujah_count());
                                halleCount++;
                                hallelujah = "Hallelujah";
                                testimonyModalNew.getList().get(0).setHallelujah_flag("Hallelujah");
                                testimonyModalNew.getList().get(0).setHallelujah_count(String.valueOf(halleCount));
                                if (halleCount == 0 || halleCount == 1) {
                                    txtvw_testimony_of_the_day_halle_count.setText(String.valueOf(halleCount) + " " + "Hallelujah");
                                } else {
                                    txtvw_testimony_of_the_day_halle_count.setText(String.valueOf(halleCount) + " " + "Hallelujah");
                                }
                            } else {
                                int imgResource = R.drawable.prayer_amen;
                                txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                halleCount = Integer.parseInt(testimonyModalNew.getList().get(0).getHallelujah_count());
                                halleCount--;
                                /*testimonyModal.getList().setHallelujah_count(halleCount);*/
                                hallelujah = "Not Hallelujah";
                                testimonyModalNew.getList().get(0).setHallelujah_flag("Not Hallelujah");
                                testimonyModalNew.getList().get(0).setHallelujah_count(String.valueOf(halleCount));

                                if (halleCount == 0 || halleCount == 1) {
                                    txtvw_testimony_of_the_day_halle_count.setText(String.valueOf(halleCount) + " " + "Hallelujah");
                                } else {
                                    txtvw_testimony_of_the_day_halle_count.setText(String.valueOf(halleCount) + " " + "Hallelujah");
                                }
                            }
                            if (swipe_refresh.isRefreshing()) {
                                swipe_refresh.setRefreshing(false);
                            }

                        } else {
                            Toast.makeText(getContext(), "No Help Request found", Toast.LENGTH_SHORT).show();
                            if (swipe_refresh.isRefreshing()) {
                                swipe_refresh.setRefreshing(false);
                            }
                        }


                    }
                    break;
/*                    case APIServerResponse.UNHALE: {
                        haleTestimonyModal = (HaleTestimonyModal) response.body();
                        if (haleTestimonyModal.getStatus().equals("1")) {
                        }
                        int imgResource = R.drawable.prayer_amen;
                        txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        int likescountValue = Integer.parseInt(likescount);
                        likescountValue--;
                        hallelujah = "Not Hallelujah ";
                        testimonyModal.getList().setHallelujah_flag("Not Hallelujah");
                        testimonyModal.getList().setLike_count(String.valueOf(likescountValue));

                        if (likescountValue == 0 || likescountValue == 1) {
                            txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + "Like");
                        } else {
                            txtvw_testimony_of_the_day_likes_count.setText(String.valueOf(likescountValue) + "Likes");
                        }
                    }
                    break;*/

/*                    case APIServerResponse.TESTIMONY_INDEX:
                        try {
                            testimonyModal = (TestimonyModal) response.body();
                            if (testimonyModal.getStatus().equals("1")) {

                                ll_testimonial_of_day.setVisibility(View.VISIBLE);
                                if (testimonyModal.getList().getLike_flag().equalsIgnoreCase("Not liked")) {
                                    int imgResource = R.drawable.prayer_like_icon;
                                    txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                } else {
                                    int imgResource = R.drawable.ic_liked;
                                    txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                }
                                txtvw_testimony_of_the_title.setText(testimonyModal.getList().getTitle());
                                txtvw_testimony_of_the_day_description.setText(testimonyModal.getList().getDesc());
                                prayerId = String.valueOf(testimonyModal.getList().getId());
                                lisAllTestimonial = testimonyModal.getList();

                                if (Integer.parseInt(testimonyModal.getList().getLike_count()) == 0 || Integer.parseInt(testimonyModal.getList().getLike_count()) == 1) {
                                    txtvw_testimony_of_the_day_likes_count.setText(testimonyModal.getList().getLike_count() + "Like");
                                } else {
                                    txtvw_testimony_of_the_day_likes_count.setText(testimonyModal.getList().getLike_count() + "Likes");
                                }
                                if (Integer.parseInt(testimonyModal.getList().getComment_count()) == 0 || Integer.parseInt(testimonyModal.getList().getComment_count()) == 1) {
                                    txtvw_testimony_of_the_day_comments_count.setText(testimonyModal.getList().getComment_count() + "Comment");
                                } else {
                                    txtvw_testimony_of_the_day_comments_count.setText(testimonyModal.getList().getComment_count() + "Comments");
                                }
                                if (Integer.parseInt(testimonyModal.getList().getHallelujah_count()) == 0 || Integer.parseInt(testimonyModal.getList().getHallelujah_count()) == 1) {
                                    txtvw_testimony_of_the_day_halle_count.setText(testimonyModal.getList().getHallelujah_count() + "Hallelujah");
                                } else {
                                    txtvw_testimony_of_the_day_halle_count.setText(testimonyModal.getList().getHallelujah_count() + "Hallelujahs");
                                }

                                likescount = testimonyModal.getList().getLike_count();
                            } else {
                                ll_testimonial_of_day.setVisibility(View.GONE);
                                ((BaseActivity) getActivity()).showToast("No testimony found", Toast.LENGTH_LONG);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        adapter = new TestimonyAdapter(getActivity(), lisAllTestimonial);
                        testimony_list.setAdapter(adapter);

                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        testimony_list.setLayoutManager(mLayoutManager);
                        testimony_list.setItemAnimator(new DefaultItemAnimator());
                        break;*/
                    case APIServerResponse.MYTESTIMONY:
                        testimonyModalNew = (TestimonyModalNew) response.body();
                        if (testimonyModalNew.getStatus().equalsIgnoreCase("1")) {

                            if (testimonyModalNew.getList().size() > 0) {
                                ll_testimonial_of_day.setVisibility(View.VISIBLE);
                                if (testimonyModalNew.getList().get(0).getLike_flag().equalsIgnoreCase("Not liked")) {
                                    int imgResource = R.drawable.prayer_like_icon;
                                    txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                } else {
                                    int imgResource = R.drawable.ic_liked;
                                    txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                }
                                if (testimonyModalNew.getList().get(0).getHallelujah_flag().equalsIgnoreCase("Not hallelujah")) {
                                    int imgResource = R.drawable.prayer_amen;
                                    txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                } else {
                                    int imgResource = R.drawable.ic_selected_amen;
                                    txtvw_testimony_of_the_day_amen.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                                }
                                txtvw_testimony_of_the_title.setText(testimonyModalNew.getList().get(0).getTitle());
                                txtvw_testimony_of_the_day_description.setText(testimonyModalNew.getList().get(0).getDesc());
                                prayerId = String.valueOf(testimonyModalNew.getList().get(0).getId());
                                if (Integer.parseInt(testimonyModalNew.getList().get(0).getLike_count()) == 0 || Integer.parseInt(testimonyModalNew.getList().get(0).getLike_count()) == 1) {
                                    txtvw_testimony_of_the_day_likes_count.setText(testimonyModalNew.getList().get(0).getLike_count() + " " + "Like");
                                } else {
                                    txtvw_testimony_of_the_day_likes_count.setText(testimonyModalNew.getList().get(0).getLike_count() + " " + "Likes");
                                }
                                if (Integer.parseInt(testimonyModalNew.getList().get(0).getComment_count()) == 0 || Integer.parseInt(testimonyModalNew.getList().get(0).getComment_count()) == 1) {
                                    txtvw_testimony_of_the_day_comments_count.setText(testimonyModalNew.getList().get(0).getComment_count() + " " + "Comment");
                                } else {
                                    txtvw_testimony_of_the_day_comments_count.setText(testimonyModalNew.getList().get(0).getComment_count() + " " + "Comments");
                                }
                                if (Integer.parseInt(testimonyModalNew.getList().get(0).getHallelujah_count()) == 0 || Integer.parseInt(testimonyModalNew.getList().get(0).getHallelujah_count()) == 1) {
                                    txtvw_testimony_of_the_day_halle_count.setText(testimonyModalNew.getList().get(0).getHallelujah_count() + " " + "Hallelujah");
                                } else {
                                    txtvw_testimony_of_the_day_halle_count.setText(testimonyModalNew.getList().get(0).getHallelujah_count() + " " + "Hallelujah");
                                }

                                likescount = testimonyModalNew.getList().get(0).getLike_count();
                            }
                            listNew = testimonyModalNew.getList();
                        } else {
                            ((BaseActivity) getActivity()).showToast("No testimonial found", Toast.LENGTH_SHORT);
                        }

                        adapterNew = new TestimonyNewAdapter(getActivity(), listNew);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        testimony_list.setLayoutManager(mLayoutManager);
                        testimony_list.setAdapter(adapterNew);
                        testimony_list.scrollToPosition(0);
                        testimony_list.setItemAnimator(new DefaultItemAnimator());
                        if (swipe_refresh.isRefreshing()) {
                            swipe_refresh.setRefreshing(false);
                        }
                        break;

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtvw_testimony_of_the_day_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (txtvw_testimony_of_the_day_like.isEnabled()) {
                        txtvw_testimony_of_the_day_like.setEnabled(false);
                    }
                    liked = testimonyModalNew.getList().get(0).getLike_flag();
                    likescount = testimonyModalNew.getList().get(0).getLike_count();
                    if (liked.equalsIgnoreCase("Not liked")) {
                        int imgResource = R.drawable.ic_liked;
                        txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                            ServerAPI.getInstance().like(APIServerResponse.LIKE, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), String.valueOf(testimonyModalNew.getList().get(0).getId()), "TESTIMONY", MyTestimonyFragment.this);
                        } else {
                            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }

                    } else {
                        int imgResource = R.drawable.prayer_like_icon;
                        txtvw_testimony_of_the_day_like.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                            ServerAPI.getInstance().like(APIServerResponse.UNLIKE, ((BaseActivity) getActivity()).getUserSessionId(),  ((BaseActivity) getActivity()).getUserID(),String.valueOf(testimonyModalNew.getList().get(0).getId()), "TESTIMONY", MyTestimonyFragment.this);
                        } else {
                            ((BaseActivity) getActivity()).showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        });

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        try {

            ((BaseActivity) getActivity()).hideLoading();
            throwable.printStackTrace();
            switch (tag) {
                case APIServerResponse.MYTESTIMONY:
                    System.out.println("Error");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*  @OnClick(R.id.txtvw_testimony_of_the_day_like)
      public void likeTestimonyOfTheDay() {


      }
  */


    @Override
    public void onResume() {
        super.onResume();
        card_view_title.requestFocus();
        card_view_title.setFocusable(true);
        testimony_list.smoothScrollToPosition(0);
    /*    try {


            ((BaseActivity) getActivity()).showLoading();
            if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                ServerAPI.getInstance().getTestimonyNew(APIServerResponse.TESTIMONY_INDEX, ((BaseActivity) getActivity()).getUserSessionId(), this);
            } else {
                ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
            }
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        getAllTestimonial();
    }

    public void refreshList() {
        ((BaseActivity) getActivity()).showLoading();
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().getMyTestimony(APIServerResponse.MYTESTIMONY, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), this);
        } else {
            ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
        }
    }

    @Subscribe
    public void onEvent(DemoCommentModal event) {
        // your implementation
        ((BaseActivity) getActivity()).showLoading();
        if (((BaseActivity) getActivity()).isConnectedToInternet()) {
            ServerAPI.getInstance().getMyTestimony(APIServerResponse.MYTESTIMONY, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), this);
        } else {
            ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
        }

    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_add, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_add_new_item) {
            Intent signUpIntent = new Intent(getActivity(), TestimonyRequestActivity.class);
            startActivity(signUpIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            try {
                ((BaseActivity) getActivity()).showLoading();
                if (((BaseActivity) getActivity()).isConnectedToInternet()) {
                    //ServerAPI.getInstance().getTestimony(APIServerResponse.TESTIMONY_INDEX, ((BaseActivity) getActivity()).getUserSessionId(), this);
                    ServerAPI.getInstance().getMyTestimony(APIServerResponse.MYTESTIMONY, ((BaseActivity) getActivity()).getUserSessionId(), ((BaseActivity) getActivity()).getUserID(), this);

                } else {
                    ((BaseActivity) getActivity()).showToast("Check Your Internet Connection", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //Load Data
        } else {
            //No Loading of Data
        }
    }
}
