package com.example.preetisharma.callforblessings.Adapter;

import android.content.Context;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.TouchImageSlidingView;

import java.util.ArrayList;


public class SlidingImage extends PagerAdapter {
    private ArrayList<String> IMAGES;
    private LayoutInflater inflater;
    private Context context;
    // private AQuery aQuery;


    public SlidingImage(Context context, ArrayList<String> IMAGES) {
        this.context = context;
        //aQuery = new AQuery(context);
        this.IMAGES = IMAGES;
        Log.e("Images ", "Images" + IMAGES.get(0));
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages, view, false);

        assert imageLayout != null;
        final TouchImageSlidingView imageView = (TouchImageSlidingView) imageLayout
                .findViewById(R.id.image);
        // aQuery.id(imageView).image(IMAGES.get(position), true, true, 0, 0, null, AQuery.FADE_IN);
        //Picasso.with(context).load(Uri.parse(IMAGES.get(position))).resize(100, 100).into(imageView);

        Glide.with(context)
                .load(Uri.parse(IMAGES.get(position)))
                .thumbnail(0.1f).centerCrop()
                .into(imageView);

        //imageView.setMaxZoom(4f);
        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}
