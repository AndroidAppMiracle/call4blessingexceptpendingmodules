package com.example.preetisharma.callforblessings.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.BlockListUserAdapter;
import com.example.preetisharma.callforblessings.Adapter.SettingSecurityAdapter;
import com.example.preetisharma.callforblessings.ChangePassword;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DeviceListResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DeviceObject;
import com.example.preetisharma.callforblessings.Server.Modal.GroupDeleteModal;
import com.example.preetisharma.callforblessings.Server.Modal.LogoutModal;
import com.example.preetisharma.callforblessings.Server.Modal.MutualFriendModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.SignInActivity;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by satoti.garg on 7/20/2017.
 */

public class SettingsSecurity extends BaseActivity implements APIServerResponse {

    @BindView(R.id.rv_security_settings)
    RecyclerView rv_security_settings;

    @BindView(R.id.atv_change_password)
    AppCompatTextView atv_change_password;
    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    private List<DeviceObject> mList;
    SettingSecurityAdapter _adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_security);
        ButterKnife.bind(this);
        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText("Security Settings");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getAllDevice();

    }

    @OnClick(R.id.atv_change_password)
    public void changePassword() {
        Intent intent = new Intent(this, ChangePassword.class);
        startActivity(intent);
    }


    public void getAllDevice() {
        try {
            if (isConnectedToInternet()) {
                showLoading();
                ServerAPI.getInstance().getAllDevices(APIServerResponse.DEVICE_REPONSE, getUserSessionId(),getUserID(), this);
            } else {
                showSnack("Not connected to Internet ");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        mList = new ArrayList<DeviceObject>();
        hideLoading();
        try {
            if (response.isSuccessful()) {
                hideLoading();
                switch (tag) {
                    case APIServerResponse.DEVICE_REPONSE:
                        DeviceListResponse addEventModal = (DeviceListResponse) response.body();
                        if (addEventModal.getStatus().equals("1")) {
                            mList = addEventModal.getList();
                            //   showToast(addEventModal.getMessage(), Toast.LENGTH_LONG);
                        } else {
                            // showToast(addEventModal.getMessage(), Toast.LENGTH_LONG);
                        }

                        if (mList.size() != 0) {
                            _adapter = new SettingSecurityAdapter(SettingsSecurity.this, mList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SettingsSecurity.this);
                            rv_security_settings.setLayoutManager(mLayoutManager);
                            rv_security_settings.setAdapter(_adapter);
                            _adapter.notifyDataSetChanged();
                        } else {
                            showToast("No logined on any device yet", Toast.LENGTH_SHORT);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
