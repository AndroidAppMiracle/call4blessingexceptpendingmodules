package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/13/2017.
 */

public class PageStatusMessage {
    private String status;

    private PageDescription list;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public PageDescription getList ()
    {
        return list;
    }

    public void setList (PageDescription list)
    {
        this.list = list;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", list = "+list+"]";
    }
}
