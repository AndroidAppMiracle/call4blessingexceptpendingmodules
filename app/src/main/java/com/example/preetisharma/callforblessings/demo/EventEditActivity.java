package com.example.preetisharma.callforblessings.demo;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.EditEventModal;
import com.example.preetisharma.callforblessings.Server.Modal.EventDetailsModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by Kshitiz Bali on 2/10/2017.
 */


public class EventEditActivity extends BaseActivity implements APIServerResponse, EasyPermissions.PermissionCallbacks, ImagePickerCallback {

    AppCompatTextView txtvw_header_title;
    // @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    //@BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;


    @BindView(R.id.img_view_event_pic)
    AppCompatImageView img_view_event_pic;

    @BindView(R.id.edt_txt_event_name)
    AppCompatEditText edt_txt_event_name;

    @BindView(R.id.edt_txt_event_date_time)
    AppCompatTextView edt_txt_event_date_time;

    @BindView(R.id.edt_txt_event_description)
    AppCompatEditText edt_txt_event_description;

    @BindView(R.id.edt_txt_event_location)
    AppCompatEditText edt_txt_event_location;

    @BindView(R.id.txtvw_update)
    AppCompatTextView txtvw_update;

    private static final int PLACE_PICKER_REQUEST = 1;

    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));

    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;
    private static final int RC_CAMERA_PERM = 342;
    private static final int RC_GALLERY_PERM = 545;
    private String pickerPath = "", eventID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_edit_layout);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {
            eventID = getIntent().getExtras().getString(Constants.EVENT_ID);
        }

        txtvw_header_title = (AppCompatTextView) findViewById(R.id.txtvw_header_title);
        img_view_back = (AppCompatImageView) findViewById(R.id.img_view_back);
        img_view_change_password = (AppCompatImageView) findViewById(R.id.img_view_change_password);

        txtvw_header_title.setText("Edit Event");
        img_view_change_password.setVisibility(View.GONE);


        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventEditActivity.this.finish();
            }
        });

        if (isConnectedToInternet()) {
            showLoading();
            ServerAPI.getInstance().getEventDetails(APIServerResponse.EVENT_DETAILS, getUserSessionId(), eventID, this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }


    @OnClick(R.id.img_view_event_pic)
    public void takeImage() {
        new AlertDialog.Builder(EventEditActivity.this)

                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }

    @OnClick(R.id.edt_txt_event_date_time)
    void date_picker_dialog() {
        hideKeyboard();
        openDateDia();
    }


    Calendar calendar = Calendar.getInstance();

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void openDateDia() {
        final Calendar cal = Calendar.getInstance();

        DatePickerDialog datePicker = new DatePickerDialog(this, R.style.DialogTheme,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

//                        calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.MONTH, (monthOfYear));
                        Log.e("Selected date", "Date" + dayOfMonth + "Month" + monthOfYear + "Year" + year);
                        String format = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                        Log.e("Date is", "Date is" + format);
                        try {
                            if (isValidDate(format)) {
                                edt_txt_event_date_time.setText(format);
                            } else {
                                showToast("Select Valid Date of Event", Toast.LENGTH_SHORT);
                                edt_txt_event_date_time.requestFocus();
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


//                        }else{
//                            Utils.showToast(BookAppointmentActivity.this, "Please select future date");
//                        }

//                        dateTime = year + "-" + (int)(monthOfYear+1) + "-" + dayOfMonth;

                    }
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));

//        datePicker.setsetFirstDayOfWeek(Calendar.MONDAY);

        datePicker.setCancelable(true);

        datePicker.getDatePicker().setFirstDayOfWeek(Calendar.SUNDAY);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(datePicker.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        datePicker.show();
    }


    /*Check valid date of birth is valid*/
    public static boolean isValidDate(String pDateString) throws ParseException {
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(pDateString);
        return new Date().before(date);
    }


    @Override
    public void onSuccess(int tag, Response response) {
        try {
            hideLoading();
            EventDetailsModal eventDetailsModal;
            EditEventModal editEventModal;
            if (response.isSuccessful()) {
                switch (tag) {
                    case APIServerResponse.EVENT_DETAILS:
                        eventDetailsModal = (EventDetailsModal) response.body();

                        if (eventDetailsModal.getStatus().equals("1")) {
                            Log.i("response ", eventDetailsModal.getDetail().getEvent_name());
                            edt_txt_event_name.setText(eventDetailsModal.getDetail().getEvent_name());
                            edt_txt_event_date_time.setText(eventDetailsModal.getDetail().getEvent_date());
                            edt_txt_event_description.setText(eventDetailsModal.getDetail().getEvent_desc());
                            edt_txt_event_location.setText(eventDetailsModal.getDetail().getEvent_location());
                            //img_view_event_pic
                            Glide.with(EventEditActivity.this).load(eventDetailsModal.getDetail().getEvent_photo()).placeholder(R.drawable.placeholder_callforblessings).into(img_view_event_pic);
                        } else {
                            showToast("Unable to get details.", Toast.LENGTH_LONG);
                        }
                        break;
                    case APIServerResponse.EDIT_EVENT:

                        editEventModal = (EditEventModal) response.body();
                        if (editEventModal.getStatus().equals("1")) {

                            showToast(editEventModal.getMessage(), Toast.LENGTH_LONG);
                            EventEditActivity.this.finish();
                        }

                        break;


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {
        hideLoading();

    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        ChosenImage image = list.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else
                pickerPath = image.getOriginalPath();
            img_view_event_pic.setImageURI(Uri.fromFile(new File(pickerPath)));
        } else
            showSnack("Invalid Image");
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
            Intent intent = intentBuilder.build(EventEditActivity.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
           /* if (requestCode == PLACE_PICKER_REQUEST) {

                final Place place = PlacePicker.getPlace(this, data);
                final CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                String attributions = (String) place.getAttributions();


                edt_txt_location.setText(name);
                txtvw_state.setText(place.getLocale().getCountry());
                txtvw_country.setText(place.getAddress());


            }*/
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.reinitialize(pickerPath);
                }
                cameraPicker.submit(data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @OnClick(R.id.txtvw_update)
    public void submitEditEvent() {

        try {

            if (Validation(edt_txt_event_name.getText().toString(), edt_txt_event_date_time.getText().toString(), edt_txt_event_location.getText().toString(), edt_txt_event_description.getText().toString())) {

                if (pickerPath.equals("")) {
                    showLoading();
                    hideKeyboard();
                    ServerAPI.getInstance().editEventWithoutPhoto(APIServerResponse.EDIT_EVENT, getUserSessionId(), eventID, edt_txt_event_name.getText().toString(), edt_txt_event_date_time.getText().toString(), edt_txt_event_location.getText().toString(), edt_txt_event_description.getText().toString(), this);
                } else {
                    showLoading();
                    hideKeyboard();
                    Glide.with(EventEditActivity.this).load(pickerPath).into(img_view_event_pic);
                    ServerAPI.getInstance().editEventWithPhoto(APIServerResponse.EDIT_EVENT, getUserSessionId(), eventID, edt_txt_event_name.getText().toString(), edt_txt_event_date_time.getText().toString(), edt_txt_event_location.getText().toString(), edt_txt_event_description.getText().toString(), pickerPath, this);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        if (EasyPermissions.hasPermissions(EventEditActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            cameraPicker = new CameraImagePicker(this);
            cameraPicker.shouldGenerateMetadata(true);
            cameraPicker.shouldGenerateThumbnails(true);
            cameraPicker.setImagePickerCallback(this);
            pickerPath = cameraPicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_CAMERA_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(EventEditActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }


    public boolean Validation(String eventName, String eventDateTime, String eventLocation, String eventDescription) {
        if (eventName.isEmpty()) {
            showToast("Enter event name.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventName.equals("")) {
            showToast("Enter event name.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventDateTime.isEmpty()) {
            showToast("Enter event date & time.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventDateTime.equals("")) {
            showToast("Enter event date & time.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventLocation.isEmpty()) {
            showToast("Enter event location.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventLocation.equals("")) {
            showToast("Enter event location.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventDescription.isEmpty()) {
            showToast("Enter event description.", Toast.LENGTH_SHORT);
            return false;
        } else if (eventDescription.equals("")) {
            showToast("Enter event description.", Toast.LENGTH_SHORT);
            return false;
        }
        return true;


    }
}
