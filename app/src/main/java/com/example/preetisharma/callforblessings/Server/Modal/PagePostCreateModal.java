package com.example.preetisharma.callforblessings.Server.Modal;

/**
 * Created by satoti.garg on 10/13/2017.
 */

public class PagePostCreateModal {
    private String comment_count;

    private String id;

    private String like_count;

    private String created_at;

    private String attached_file;

    private String[] comments;

    private String post_content;

    public String getComment_count ()
    {
        return comment_count;
    }

    public void setComment_count (String comment_count)
    {
        this.comment_count = comment_count;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getLike_count ()
    {
        return like_count;
    }

    public void setLike_count (String like_count)
    {
        this.like_count = like_count;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getAttached_file ()
    {
        return attached_file;
    }

    public void setAttached_file (String attached_file)
    {
        this.attached_file = attached_file;
    }

    public String[] getComments ()
    {
        return comments;
    }

    public void setComments (String[] comments)
    {
        this.comments = comments;
    }

    public String getPost_content ()
    {
        return post_content;
    }

    public void setPost_content (String post_content)
    {
        this.post_content = post_content;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [comment_count = "+comment_count+", id = "+id+", like_count = "+like_count+", created_at = "+created_at+", attached_file = "+attached_file+", comments = "+comments+", post_content = "+post_content+"]";
    }
}
