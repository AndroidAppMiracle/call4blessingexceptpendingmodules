package com.example.preetisharma.callforblessings.joymusicplayer;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kshitiz Bali on 5/18/2017.
 */

public class PlayMusicModal implements Parcelable {

    private long id;
    private long playlistId;
    private boolean isAudio;
    private int mediaType;
    private String mediaUrl;
    private String downloadedMediaUri;
    private String thumbnailUrl;
    private String artworkUrl;
    private String title;
    private String album;
    private String artist;
    private String paymentType;
    private String isDownloaded;


    public PlayMusicModal(Parcel in) {
        id = in.readLong();
        playlistId = in.readLong();
        isAudio = in.readByte() != 0;
        mediaType = in.readInt();
        mediaUrl = in.readString();
        downloadedMediaUri = in.readString();
        thumbnailUrl = in.readString();
        artworkUrl = in.readString();
        title = in.readString();
        album = in.readString();
        artist = in.readString();
        paymentType = in.readString();
        isDownloaded = in.readString();
    }

    public static final Creator<PlayMusicModal> CREATOR = new Creator<PlayMusicModal>() {
        @Override
        public PlayMusicModal createFromParcel(Parcel in) {
            return new PlayMusicModal(in);
        }

        @Override
        public PlayMusicModal[] newArray(int size) {
            return new PlayMusicModal[size];
        }
    };

    public PlayMusicModal() {
        id = 0;
        playlistId = 0;
        isAudio = true;
        mediaType = 0;
        mediaUrl = "";
        downloadedMediaUri = "";
        thumbnailUrl = "";
        artworkUrl = "";
        title = "";
        album = "";
        artist = "";
        paymentType = "";
        isDownloaded = "";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(long playlistId) {
        this.playlistId = playlistId;
    }

    public boolean isAudio() {
        return isAudio;
    }

    public void setAudio(boolean audio) {
        isAudio = audio;
    }

    public int getMediaType() {
        return mediaType;
    }

    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public String getDownloadedMediaUri() {
        return downloadedMediaUri;
    }

    public void setDownloadedMediaUri(String downloadedMediaUri) {
        this.downloadedMediaUri = downloadedMediaUri;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getArtworkUrl() {
        return artworkUrl;
    }

    public void setArtworkUrl(String artworkUrl) {
        this.artworkUrl = artworkUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String isDownloaded() {
        return isDownloaded;
    }

    public void setDownloaded(String downloaded) {
        isDownloaded = downloaded;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(playlistId);
        dest.writeByte((byte) (isAudio ? 1 : 0));
        dest.writeInt(mediaType);
        dest.writeString(mediaUrl);
        dest.writeString(downloadedMediaUri);
        dest.writeString(thumbnailUrl);
        dest.writeString(artworkUrl);
        dest.writeString(title);
        dest.writeString(album);
        dest.writeString(artist);
        dest.writeString(paymentType);
        dest.writeString(isDownloaded);
    }
}
