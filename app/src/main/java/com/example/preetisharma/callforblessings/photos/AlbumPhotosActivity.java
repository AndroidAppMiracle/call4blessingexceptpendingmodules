package com.example.preetisharma.callforblessings.photos;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.AlbumPhotoAdapter;
import com.example.preetisharma.callforblessings.Adapter.DemoDataMyPhotosAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.AlbumImageListObject;
import com.example.preetisharma.callforblessings.Server.Modal.AllAlbumImageResponse;
import com.example.preetisharma.callforblessings.Server.Modal.DemoDataModal;
import com.example.preetisharma.callforblessings.Server.Modal.DemoHomeModal;
import com.example.preetisharma.callforblessings.Server.Modal.ProfileImageModal;
import com.example.preetisharma.callforblessings.Server.Modal.ProfileObjectListModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.UploadImageToAlbum;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.example.preetisharma.callforblessings.Utils.ItemClickSupport;
import com.example.preetisharma.callforblessings.demo.ChangeCoverProfilePicActivity;
import com.example.preetisharma.callforblessings.demo.ChangeProfilePicActivity;
import com.example.preetisharma.callforblessings.demo.CreatePostActivity;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Response;

/**
 * Created by satoti.garg on 8/3/2017.
 */

public class AlbumPhotosActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, ImagePickerCallback, EasyPermissions.PermissionCallbacks, APIServerResponse {

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;
    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    ArrayList<DemoDataModal> list = new ArrayList<>();
    String[] images = {"https://i.pinimg.com/originals/6e/ab/f6/6eabf6c1fda5194f076c402192b5858b.jpg",
            "http://t.wallpaperweb.org/wallpaper/3d_landscape/1024x768/fantasy_art_scenery_wallpaper_sarel_theron_03_1024x768.jpg",
            "http://2.bp.blogspot.com/-bQp0j6m1O7M/UEyE743yh6I/AAAAAAAAAdI/4zPjg4UNpCQ/s1600/beautiful-autumn-scenery-723-2.jpg",
            "https://s-media-cache-ak0.pinimg.com/originals/03/2c/2b/032c2b83402721bf65db22c597594fdd.jpg",
            "http://cdn.pcwallart.com/images/arizona-scenery-sunset-wallpaper-4.jpg",
            "https://lh6.ggpht.com/YKKQHs9gzT9OWOrr6dwXakjB30Rb33IgXolZpNY_pwXxZMngNPRT1PpV1kS6bo_s7w=h900",
            "http://www.wallmaya.com/wp-content/uploads/2016/03/scenery-photography-wallpaper-latest-hd-collections.jpg",
            "https://southeastgreenway.org/wp-content/uploads/2016/08/Scenery-33.jpg",
            "http://wallpapercave.com/wp/cJ5iQB1.jpg",
            "http://www.desktop-background.com/download/1024x768/2015/10/29/1033760_wallpapers-onepiece-nature-beautiful-natural-scenery-1366x768_1366x768_h.jpg"};


    List<ProfileObjectListModal> listProfileModal = new ArrayList<>();
    List<AlbumImageListObject> imageObject = new ArrayList<>();

    @BindView(R.id.rv_photos)
    RecyclerView rv_photos;

    DemoDataMyPhotosAdapter adapter;
    AlbumPhotoAdapter albumPhotoAdapter;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;
    private String pickerPath = "";
    private static final int PLACE_PICKER_REQUEST = 1;
    String[] perms = {android.Manifest.permission.CAMERA, Manifest.permission.INTERNET};
    private int RC_LOCATION_INTERNET = 102;
    private static final int RC_GALLERY_PERM = 545;
    private static final String TAG = "CallForBlessings";
    private static final int RC_CAMERA_PERM = 342;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_photos);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        img_view_change_password.setVisibility(View.VISIBLE);
        getAllImages();

        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            if (extras.getString(Constants.ALBUM_NAME, "1") != null && !extras.getString(Constants.ALBUM_NAME, "1").equalsIgnoreCase("")) {
                txtvw_header_title.setText(extras.getString(Constants.ALBUM_NAME, "Album"));
            }
        }


        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlbumPhotosActivity.this.finish();
            }
        });

        swipe_refresh_layout.setOnRefreshListener(AlbumPhotosActivity.this);
        rv_photos.setLayoutManager(new GridLayoutManager(AlbumPhotosActivity.this, 2));
        rv_photos.setItemAnimator(new DefaultItemAnimator());

        swipe_refresh_layout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        /*addDummyImages();*/
                        getAllImages();
                    }
                }
        );

        ItemClickSupport.addTo(rv_photos).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                Intent intentViewImages = new Intent(AlbumPhotosActivity.this, PhotosSlideView.class);
                intentViewImages.putExtra(Constants.CURRENT_POSITION, position);
                Bundle b = new Bundle();
                b.putParcelableArrayList(Constants.IMAGE_SLIDESHOW, (ArrayList<? extends Parcelable>) listProfileModal);
                intentViewImages.putExtra(Constants.IMAGE_BUNDLE, b);
                startActivity(intentViewImages);

            }
        });

        if (getIntent().hasExtra("position")) {

            img_view_change_password.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getIntent().hasExtra("position")) {
                        if (getIntent().getStringExtra("position").equals("1")) {
                            Intent changePic = new Intent(getApplicationContext(), ChangeProfilePicActivity.class);
                            Bundle b = new Bundle();
                            int positionNew = Integer.parseInt(getIntent().getStringExtra("position"));
                            b.putString(Constants.PROFILE_PREVIEW, getUserImage());
                            b.putBoolean(Constants.TIMELINE_ENABLED, true);
                            changePic.putExtras(b);
                            startActivity(changePic);
                        } else if (getIntent().getStringExtra("position").equals("2")) {
                            Intent changePic = new Intent(getApplicationContext(), ChangeCoverProfilePicActivity.class);
                            Bundle b = new Bundle();
                            int positionNew = Integer.parseInt(getIntent().getStringExtra("position"));
                            b.putString(Constants.PROFILE_PREVIEW, getUserCoverImage());
                            b.putBoolean(Constants.TIMELINE_ENABLED, true);
                            changePic.putExtras(b);
                            startActivity(changePic);
                        } else {
                           /* imagePickerDialog();*/
                         /*   Intent createPostIntent = new Intent(getApplicationContext(), CreatePostActivity.class);
                            Bundle b = new Bundle();
                            b.putString(Constants.POSTTYPE, Constants.WALL);
                            createPostIntent.putExtras(b);
                            startActivity(createPostIntent);*/
                            if (getIntent().hasExtra("albumId")) {
                                Intent intent = new Intent(getApplicationContext(), UploadImageToAlbum.class);
                                intent.putExtra("albumId",getIntent().getStringExtra("albumId"));
                                startActivity(intent);
                            }

                        }
                    }
                }
            });

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllImages();
    }

    public void getAllImages() {
        try {
            showLoading();
            if (getIntent().getStringExtra("position").equals("1")) {
                ServerAPI.getInstance().getAllImagesProfile(APIServerResponse.ALL_IMAGES_PROFILE, getUserSessionId(),getUserID(), this);
            } else if (getIntent().getStringExtra("position").equals("2")) {
                ServerAPI.getInstance().getAllImagesCover(APIServerResponse.ALL_IMAGES_COVER, getUserSessionId(), this);
            } else if (getIntent().getStringExtra("position").equals("3")) {
                ServerAPI.getInstance().getAllTimeLineImages(APIServerResponse.ALL_IMAGES_TIMELINE, getUserSessionId(), this);
            } else {
                if (getIntent().hasExtra("albumId")) {
                    ServerAPI.getInstance().getAllAlbumImages(APIServerResponse.ALL_ALBUM_IMAGES, getUserSessionId(),getIntent().getStringExtra("albumId"), this);
                } else {
                    Toast.makeText(getApplicationContext(), "no album found ", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void imagePickerDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Select Image")
                .setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickImageSingle();
                    }
                })
                .setIcon(R.mipmap.ic_app_icon)
                .show();
    }


    @AfterPermissionGranted(RC_CAMERA_PERM)
    public void takePicture() {
        try {
            if (EasyPermissions.hasPermissions(this, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)) {
                cameraPicker = new CameraImagePicker(this);
                cameraPicker.shouldGenerateMetadata(true);
                cameraPicker.shouldGenerateThumbnails(true);
                cameraPicker.setImagePickerCallback(this);
                pickerPath = cameraPicker.pickImage();

            } else {
                EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                        RC_CAMERA_PERM, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterPermissionGranted(RC_GALLERY_PERM)
    public void pickImageSingle() {
        if (EasyPermissions.hasPermissions(this, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)) {
            imagePicker = new ImagePicker(this);
            imagePicker.shouldGenerateMetadata(true);
            imagePicker.shouldGenerateThumbnails(true);
            imagePicker.setImagePickerCallback(this);
            imagePicker.pickImage();
        } else {
            EasyPermissions.requestPermissions(this, "Need Permission to access your Gallery and Camera",
                    RC_GALLERY_PERM, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
        }
    }



    @Override
    public void onRefresh() {
        //     addDummyImages();
        getAllImages();
    }

    public void addDummyImages() {

        if (!list.isEmpty()) {
            list.clear();
        }

        for (int i = 0; i < 10; i++) {

            DemoDataModal demoDataModal = new DemoDataModal();
            demoDataModal.setId(i);
            demoDataModal.setImage(images[i]);
            demoDataModal.setName("Image " + (i + 1));

            list.add(demoDataModal);
        }

        if (swipe_refresh_layout != null) {
            swipe_refresh_layout.setRefreshing(false);
        }


    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        ChosenImage image = images.get(0);
        if (image != null) {
            if (image.getThumbnailPath() != null && image.getThumbnailPath().length() != 0)
                pickerPath = image.getThumbnailPath();
            else
                pickerPath = image.getOriginalPath();
            //imgvw_upload_image.setImageURI(Uri.fromFile(new File(pickerPath)));
        } else
            showSnack("Invalid Image");
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // Handle negative button on click listener
        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Let's show a toast
                //Toast.makeText(getContext(), R.string.settings_dialog_canceled, Toast.LENGTH_SHORT).show();
            }
        };

        // (Optional) Check whether the user denied permissions and checked NEVER ASK AGAIN.
        // This will display a dialog directing them to enable the permission in app settings.
        EasyPermissions.checkDeniedPermissionsNeverAskAgain(this,
                "Need Permission to access your Gallery and Camera",
                R.string.setting, R.string.cancel, onClickListener, perms);
    }

    @Override
    public void onSuccess(int tag, Response response) {

        hideLoading();
        if (response.isSuccessful()) {
            hideLoading();
            try {
                switch (tag) {
                    case APIServerResponse.ALL_IMAGES_PROFILE:
                        ProfileImageModal profileImageModal = (ProfileImageModal) response.body();
                        if (profileImageModal.getStatus().equalsIgnoreCase("1")) {
                            listProfileModal = profileImageModal.getList();
                            adapter = new DemoDataMyPhotosAdapter(AlbumPhotosActivity.this, listProfileModal);
                            rv_photos.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            if(swipe_refresh_layout.isRefreshing())
                            {
                                swipe_refresh_layout.setRefreshing(false);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "No Profile images found", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case APIServerResponse.ALL_IMAGES_COVER:
                        ProfileImageModal profileImageModal1 = (ProfileImageModal) response.body();
                        if (profileImageModal1.getStatus().equalsIgnoreCase("1")) {
                            listProfileModal = profileImageModal1.getList();
                            adapter = new DemoDataMyPhotosAdapter(AlbumPhotosActivity.this, listProfileModal);
                            rv_photos.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            if(swipe_refresh_layout.isRefreshing())
                            {
                                swipe_refresh_layout.setRefreshing(false);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "No Cover images found", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case APIServerResponse.ALL_IMAGES_TIMELINE:
                        ProfileImageModal allImages = (ProfileImageModal) response.body();
                        if (allImages.getStatus().equalsIgnoreCase("1")) {
                            listProfileModal = allImages.getList();
                            adapter = new DemoDataMyPhotosAdapter(AlbumPhotosActivity.this, listProfileModal);
                            rv_photos.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            if(swipe_refresh_layout.isRefreshing())
                            {
                                swipe_refresh_layout.setRefreshing(false);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "No Images are posted yet", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    //imageObject
                    case APIServerResponse.ALL_ALBUM_IMAGES:
                        AllAlbumImageResponse allAlbumImageResponse = (AllAlbumImageResponse) response.body();
                        if (allAlbumImageResponse.getStatus().equalsIgnoreCase("1")) {
                            imageObject = allAlbumImageResponse.getList();
                            albumPhotoAdapter = new AlbumPhotoAdapter(AlbumPhotosActivity.this, imageObject);
                            rv_photos.setAdapter(albumPhotoAdapter);
                            albumPhotoAdapter.notifyDataSetChanged();
                            if(swipe_refresh_layout.isRefreshing())
                            {
                                swipe_refresh_layout.setRefreshing(false);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "No Images are posted yet", Toast.LENGTH_SHORT).show();
                        }
                        break;

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
