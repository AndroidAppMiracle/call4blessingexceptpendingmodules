package com.example.preetisharma.callforblessings.Server.Modal;

import java.util.List;

/**
 * Created by satoti.garg on 1/18/2017.
 */

public class PrayerIndexModal {


    /**
     * status : 1
     * prayer_of_day : []
     * list : [{"id":2,"name":"check 1","desc":"hi........\r\nevery one....","title":"check 1","video_link":"","image":"http://www.call4blessing.com/uploads/prayer/1492496593agree.jpg","comments":[],"amen_count":"0","amen_flag":"Not amen","like_flag":"Not Liked","comment_count":"0","like_count":"0"},{"id":1,"name":"Pushpeen","desc":"I m not well","title":"Pray for my health","video_link":"","image":"http://www.call4blessing.com/uploads/prayer/1492317841user_profile.jpg","comments":[],"amen_count":"0","amen_flag":"Not amen","like_flag":"Not Liked","comment_count":"0","like_count":"0"}]
     */

    private String status;
    private List<ListBean> prayer_of_day;
    private List<ListBean> list;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ListBean> getPrayer_of_day() {
        return prayer_of_day;
    }

    public void setPrayer_of_day(List<ListBean> prayer_of_day) {
        this.prayer_of_day = prayer_of_day;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 2
         * name : check 1
         * desc : hi........
         every one....
         * title : check 1
         * video_link :
         * image : http://www.call4blessing.com/uploads/prayer/1492496593agree.jpg
         * comments : []
         * amen_count : 0
         * amen_flag : Not amen
         * like_flag : Not Liked
         * comment_count : 0
         * like_count : 0
         */

        private int id;
        private String name;
        private String desc;
        private String title;
        private String video_link;
        private String image;
        private String amen_count;
        private String amen_flag;
        private String like_flag;
        private String comment_count;
        private String like_count;
        private List<?> comments;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getVideo_link() {
            return video_link;
        }

        public void setVideo_link(String video_link) {
            this.video_link = video_link;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getAmen_count() {
            return amen_count;
        }

        public void setAmen_count(String amen_count) {
            this.amen_count = amen_count;
        }

        public String getAmen_flag() {
            return amen_flag;
        }

        public void setAmen_flag(String amen_flag) {
            this.amen_flag = amen_flag;
        }

        public String getLike_flag() {
            return like_flag;
        }

        public void setLike_flag(String like_flag) {
            this.like_flag = like_flag;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getLike_count() {
            return like_count;
        }

        public void setLike_count(String like_count) {
            this.like_count = like_count;
        }

        public List<?> getComments() {
            return comments;
        }

        public void setComments(List<?> comments) {
            this.comments = comments;
        }
    }
}
