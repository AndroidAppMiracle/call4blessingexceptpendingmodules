package com.example.preetisharma.callforblessings.Adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.google.gson.JsonArray;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LikeUserAdapter extends RecyclerView.Adapter<LikeUserAdapter.DataViewHolder> {

    Activity mContext;
    JsonArray list;

    public LikeUserAdapter(Activity mContext, JsonArray list) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.like_friend_item, parent, false);
        DataViewHolder dataViewHolder = new DataViewHolder(v);
        Constants.setAppFont((ViewGroup) v, parent.getContext());
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, int position) {
        holder.text_vw_person_name.setText(list.get(position).getAsJsonObject().get("username").getAsString());
        Glide.with(mContext).load(list.get(position).getAsJsonObject().get("profile_details").getAsJsonObject().get("profile_pic").getAsString()).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                holder.mProgressbar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                // image ready, hide progress now
                holder.mProgressbar.setVisibility(View.GONE);
                return false;   // return false if you want Glide to handle everything else.
            }
        }).placeholder(R.drawable.placeholder_callforblessings).centerCrop().into(holder.iv_person_request_image);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_person_request_image)
        AppCompatImageView iv_person_request_image;
        @BindView(R.id.text_vw_person_name)
        AppCompatTextView text_vw_person_name;

        @BindView(R.id.movie_progress)
        ProgressBar mProgressbar;
        public DataViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
