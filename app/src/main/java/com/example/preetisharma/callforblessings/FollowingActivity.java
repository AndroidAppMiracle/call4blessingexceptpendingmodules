package com.example.preetisharma.callforblessings;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.callforblessings.R;
import com.example.preetisharma.callforblessings.Adapter.FollowingAdapter;
import com.example.preetisharma.callforblessings.Adapter.FriendsAdapter;
import com.example.preetisharma.callforblessings.Server.APIServerResponse;
import com.example.preetisharma.callforblessings.Server.Modal.FriendRequestModal;
import com.example.preetisharma.callforblessings.Server.Modal.MutualFriendModal;
import com.example.preetisharma.callforblessings.Server.ServerAPI;
import com.example.preetisharma.callforblessings.Utils.BaseActivity;
import com.example.preetisharma.callforblessings.Utils.Constants;
import com.google.android.gms.internal.hi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class FollowingActivity extends BaseActivity implements APIServerResponse {


    @BindView(R.id.rv_following_friends)
    RecyclerView rv_following_friends;
    private List<MutualFriendModal.List> mList;
    FollowingAdapter followingAdapter;
    private LinearLayoutManager mLayoutManager;

    @BindView(R.id.txtvw_header_title)
    AppCompatTextView txtvw_header_title;
    @BindView(R.id.img_view_back)
    AppCompatImageView img_view_back;

    @BindView(R.id.img_view_change_password)
    AppCompatImageView img_view_change_password;
    String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);
        ButterKnife.bind(this);

        img_view_change_password.setVisibility(View.GONE);
        txtvw_header_title.setText(
                "Followers ");
        img_view_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getIntent().hasExtra("userId")) {
            userId = getIntent().getStringExtra("userId");
        }


        getAllFollowersList();
    }


    public void getAllFollowersList() {
        showLoading();
        if (isConnectedToInternet()) {
            ServerAPI.getInstance().getAllFollowers(APIServerResponse.ALL_FOLLOWERS, getUserSessionId(),userId, this);
        } else {
            showToast(Constants.INTERNET_CONNECTION, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onSuccess(int tag, Response response) {
        mList = new ArrayList<MutualFriendModal.List>();
        hideLoading();
        MutualFriendModal friendRequestModal;

        if (response.isSuccessful()) {
            try {

                switch (tag) {
                    case APIServerResponse.ALL_FOLLOWERS: {
                        friendRequestModal = (MutualFriendModal) response.body();
                        if (friendRequestModal.getStatus().equals("1")) {
                          //  Log.e("List data", "List data" + friendRequestModal.getList().size());
                            mList = friendRequestModal.getList();
                            followingAdapter = new FollowingAdapter(this, mList);
                            mLayoutManager = new LinearLayoutManager(this) {
                                @Override
                                public boolean canScrollVertically() {
                                    return false;
                                }
                            };

                            rv_following_friends.setLayoutManager(mLayoutManager);
                            rv_following_friends.setNestedScrollingEnabled(false);
                            rv_following_friends.setItemAnimator(new DefaultItemAnimator());
                            rv_following_friends.setAdapter(followingAdapter);
                        } else {
                            Toast.makeText(getApplicationContext(), "No followers found yet", Toast.LENGTH_SHORT).show();
                        }
                    }


                    hideLoading();
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onError(int tag, Throwable throwable) {

    }
}
